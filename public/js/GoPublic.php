<?php
/**
 * Project: jltdining3
 * Author: Muhammad Asad
 * Email: asad.developers(at)gmail.com
 * Date: 10/23/2016
 *
 */
namespace App;

class GoPublic extends \Illuminate\Foundation\Application
{
    /**
     * Get the path to the public / web directory.
     *
     * @return string
     */
    public function publicPath()
    {
        return $this->basePath.DIRECTORY_SEPARATOR.'../../public_html/jltdining';
    }
}