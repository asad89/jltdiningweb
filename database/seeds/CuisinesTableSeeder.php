<?php

use Illuminate\Database\Seeder;
use App\Cuisines;

class CuisinesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Cuisines::create(array(
            'name'  => 'Pakistani Cuisine',
            'slug'  => str_slug('Pakistani Cuisine'),
            'status'    => 1,
        ));

        Cuisines::create(array(
            'name'  => 'Indian Cuisine',
            'slug'  => str_slug('Indian Cuisine'),
            'status'    => 1,
        ));

        Cuisines::create(array(
            'name'  => 'American Cuisine',
            'slug'  => str_slug('American Cuisine'),
            'status'    => 1,
        ));

        Cuisines::create(array(
            'name'  => 'French Cuisine',
            'slug'  => str_slug('French Cuisine'),
            'status'    => 1,
        ));

        Cuisines::create(array(
            'name'  => 'Chinese Cuisine',
            'slug'  => str_slug('Chinese Cuisine'),
            'status'    => 1,
        ));

        Cuisines::create(array(
            'name'  => 'Italian Cuisine',
            'slug'  => str_slug('Italian Cuisine'),
            'status'    => 1,
        ));

        Cuisines::create(array(
            'name'  => 'Spanish Cuisine',
            'slug'  => str_slug('Spanish Cuisine'),
            'status'    => 1,
        ));
    }
}
