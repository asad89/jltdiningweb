<?php

use Illuminate\Database\Seeder;
use App\Tags;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Tags::create(array(
            'name' => 'News',
            'slug' => str_slug('News'),
            'status'  => 1));

        Tags::create(array(
            'name' => 'Food',
            'slug' => str_slug('Food'),
            'status'  => 1));

        Tags::create(array(
            'name' => 'Healthy Food',
            'slug' => str_slug('Healthy Food'),
            'status'  => 1));

        Tags::create(array(
            'name' => 'Chicken',
            'slug' => str_slug('Chicken'),
            'status'  => 1));

        Tags::create(array(
            'name' => 'Meat',
            'slug' => str_slug('Meat'),
            'status'  => 1));
        }
}
