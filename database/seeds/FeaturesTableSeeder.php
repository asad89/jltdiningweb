<?php

use Illuminate\Database\Seeder;
use App\Features;

class FeaturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Features::create(array(
            'name' => 'Free Wifi',
            'slug' => str_slug('Free Wifi'),
            'status'  => 1));

        Features::create(array(
            'name' => 'Free Parking',
            'slug' => str_slug('Free Parking'),
            'status'  => 1));

        Features::create(array(
            'name' => '24 hours service',
            'slug' => str_slug('24 hours service'),
            'status'  => 1));
    }
}
