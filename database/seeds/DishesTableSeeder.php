<?php

use Illuminate\Database\Seeder;
use App\Dishes;
class DishesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Dishes::create(array(
            'name' => 'Chicken',
            'slug' => str_slug('Chicken'),
            'status'  => 1));
        Dishes::create(array(
            'name' => 'Chicken BBQ',
            'slug' => str_slug('Chicken BBQ'),
            'status'  => 1));
        Dishes::create(array(
            'name' => 'Mutton',
            'slug' => str_slug('Mutton'),
            'status'  => 1));
        Dishes::create(array(
            'name' => 'Mutton BBQ',
            'slug' => str_slug('Mutton BBQ'),
            'status'  => 1));

        Dishes::create(array(
            'name' => 'Chicken Biryani',
            'slug' => str_slug('Chicken Biryani'),
            'status'  => 1));
    }
}
