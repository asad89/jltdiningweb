<?php
use Illuminate\Database\Seeder;
use App\Settings;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Settings::create(array(
                'website' => 'JLT Dining',
                'info_email' => 'info@jltdining.com',
                'admin_email' => 'admin@jltdining.com',
                'address' => 'P.O. Box 75879, Dubai, United Arab Emirates',
                'copyrights' => 'JLT Dining',
                'designed' => 'sps:digital – Strategic Partnership Solutions',
                'facebook' => 'https://www.facebook.com/JLTdining',
                'twitter' => 'https://twitter.com/JLTdining',
                'instagram' => 'https://www.instagram.com/dining_in_jlt/',
            )
        );
    }
}