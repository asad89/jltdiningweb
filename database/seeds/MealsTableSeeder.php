<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Meals;

class MealsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Meals::create(array(
            'name' => 'Pocket-friendly-delivery',
            'slug' => str_slug('Pocket-friendly-delivery'),
            'status'  => 1));
        Meals::create(array(
            'name' => 'Breakfast',
            'slug' => str_slug('Breakfast'),
            'status'  => 1));

        Meals::create(array(
            'name' => 'Lunch',
            'slug' => str_slug('Lunch'),
            'status'  => 1));
        Meals::create(array(
            'name' => 'Dinner',
            'slug' => str_slug('Dinner'),
            'status'  => 1));
        Meals::create(array(
            'name' => 'Cafes',
            'slug' => str_slug('Cafes'),
            'status'  => 1));
        Meals::create(array(
            'name' => 'Drinks & Nightlife',
            'slug' => str_slug('Drinks & Nightlife'),
            'status'  => 1));

        Meals::create(array(
            'name' => 'Indian Restaurants',
            'slug' => str_slug('Indian Restaurants'),
            'status'  => 1));
        Meals::create(array(
            'name' => 'Luxury Dining',
            'slug' => str_slug('Luxury Dining'),
            'status'  => 1));
        Meals::create(array(
            'name' => 'Desserts & Bakes',
            'slug' => str_slug('Desserts & Bakes'),
            'status'  => 1));
    }
}
