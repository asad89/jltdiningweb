<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->call(UserTableSeeder::class);
        $this->call(MealsTableSeeder::class);
        $this->call(CuisinesTableSeeder::class);
        $this->call(TagsTableSeeder::class);
        $this->call(FeaturesTableSeeder::class);
        $this->call(DishesTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
       /* $this->call(SettingsTableSeeder::class);*/
    }
}
