<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::create([
            'name' => 'Muhammad Asad',
            'email' => 'asad.developers@gmail.com',
            'phone' => '0525214640',
            'password' => bcrypt('123456789'),
            'role'  => 1,
        ]);


        User::create([
            'name' => 'Martin',
            'email' => 'martin@spsaffinity.com',
            'phone' => '0525214640',
            'password' => bcrypt('123456789'),
            'role'  => 1,
        ]);

        User::create([
            'name' => 'Aneek',
            'email' => 'aneek@spsaffinity.com',
            'phone' => '0525214640',
            'password' => bcrypt('admin123'),
            'role'  => 1,
        ]);





    }
}
