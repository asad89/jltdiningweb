<?php

use Illuminate\Database\Seeder;
use App\Categories;
class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Categories::create(array(
            'name'  => 'Healthy Food',
            'slug'  => str_slug('Healthy Food'),
            'status'    => 1,
        ));

        Categories::create(array(
            'name'  => 'Fresh Food',
            'slug'  => str_slug('Fresh Food'),
            'status'    => 1,
        ));

        Categories::create(array(
            'name'  => 'Food in Dubai',
            'slug'  => str_slug('Food in Dubai'),
            'status'    => 1,
        ));


    }
}
