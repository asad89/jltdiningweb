<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('website');
            $table->string('logo');
            $table->string('info_email');
            $table->string('admin_email');
            $table->string('contact');
            $table->text('address');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('copyrights');
            $table->string('designed');
            $table->text('google_analytics');
            $table->text('facebook_code');
            $table->string('facebook');
            $table->string('twitter');
            $table->string('instagram');
            $table->string('google');
            $table->string('linkedin');
            $table->string('meta_title');
            $table->string('meta_keywords');
            $table->text('meta_description');
            $table->boolean('robots')->default(1)->comment('0=nofollow,noindex 1= follow,index');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('settings');
    }
}
