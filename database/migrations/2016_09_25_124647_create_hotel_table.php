<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->string('logo');
            $table->string('image');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->boolean('ad_type')->default(0)->comment('0=free, 1= premium');
            $table->string('phone');
            $table->string('email');
            $table->boolean('owner')->comment('0=Not owner, 1= Owner');
            $table->string('website');
            $table->text('address')->comment('Hotel Address');
            $table->string('pincode');
            $table->text('landmark')->comment('Near By');
            $table->longText('details');
            $table->string('menu');
            $table->string('working_days');
            $table->time('opening_time');
            $table->time('closing_time');
            $table->string('video')->comment('link from youtube video');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('facebook');
            $table->string('twitter');
            $table->string('linkedin');
            $table->string('instagram');
            $table->string('google');
            $table->boolean('status')->default(0)->comment('0=approved,1=pending');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hotels');
    }
}
