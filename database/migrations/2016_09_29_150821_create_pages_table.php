<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('title');
            $table->string('slug');
            $table->longText('post_content');
            $table->boolean('post_type')->default(0)->comment('0 = pages, 1 = post');
            $table->boolean('status')->default(1)->comment('1 = Published,0= draft');
            $table->integer('user_id');
            $table->string('meta_title');
            $table->text('meta_keywords');
            $table->text('meta_description');
            $table->boolean('meta_robots')->default(0);
            $table->string('feature_image');
            $table->softDeletes();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pages');
    }
}
