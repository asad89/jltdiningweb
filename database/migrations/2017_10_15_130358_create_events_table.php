<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('title');
            $table->string('slug');
            $table->text('description');
            $table->string('feature_image');
            $table->date('ending_date');
            $table->string('status')->default('active')->comment('active,pending');
            $table->string('meta_title');
            $table->text('meta_keywords');
            $table->text('meta_description');
            $table->boolean('meta_robots')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('events');
    }
}
