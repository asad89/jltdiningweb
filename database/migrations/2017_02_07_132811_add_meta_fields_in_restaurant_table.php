<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMetaFieldsInRestaurantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('hotels', function($table) {
            $table->string('meta_title');
            $table->text('meta_keywords');
            $table->text('meta_description');
            $table->boolean('meta_robots')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('hotels', function($table) {
            $table->dropColumn('meta_title','meta_keywords','meta_description','meta_robots');
        });
    }
}
