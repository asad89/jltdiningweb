<?php

namespace App;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Hotel;

class Meals extends Model
{
    //
    /*  use SoftDeletes;
      protected $dates = ['deleted_at'];*/
    use HasSlug;
    protected $fillable = ['name', 'status', 'slug'];


    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

function get_meal_hotel()
{
    return $this->belongsToMany('App\Hotel', 'hotel_meals', 'meal_id', 'hotel_id');
}


}
