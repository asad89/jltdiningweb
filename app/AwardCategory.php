<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AwardCategory extends Model
{
    //
    protected $table = 'awardcategory';
    protected $fillable = ['name','slug','status'];

    public function getCategoryVotes() {
        return $this->hasMany('App\Votecast','category_id');
    }

}
