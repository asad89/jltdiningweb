<?php

namespace App\Jobs;

use App\Contact;
use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Mail\Mailer;

class SendContactEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $contact;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($contact)
    {
        //
        $this->contact = $contact;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        //

        $mailer->send('emails.welcome.index', ['contact' => $this->contact], function ($m) {
            //
            $m->from('info@jltdining.com', 'JLT Dining');
            $m->to('asad@digitalsps.com', 'Asad')->subject('Contact Request!');
        });


    }
}
