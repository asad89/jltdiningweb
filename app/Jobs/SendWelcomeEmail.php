<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\User;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendWelcomeEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        //
        $user = User::all();
        foreach($user as $user) {
            $mailer->send('emails.welcome.index', ['user' => $user], function ($m) {
                //
                $m->from('info@jltdining.com', 'JLT Dining');
                $m->to('asad@digitalsps.com', 'Asad')->subject('Contact Request!');
            });
        }


    }
}
