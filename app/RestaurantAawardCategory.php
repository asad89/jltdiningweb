<?php

namespace App;

use App\Votecast;
use Illuminate\Database\Eloquent\Model;
class RestaurantAawardCategory extends Model
{
    //
    protected $table = 'restaurant_awardcategory';
    protected $fillable = ['restaurant_id','category_id'];


    public function getCategory(){
        return $this->belongsTo('App\AwardCategory','category_id');
    }

    /*Get Restaurant name*/

    public function getRestaurant() {
        return $this->belongsTo('App\AwardRestaurant','restaurant_id');
    }
}

