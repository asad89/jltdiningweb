<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;
use App\PageTags;
use App\Categories;
use App\PageCategory;

class Pages extends Model
{
    //

    use HasSlug;
    protected $fillable = ['name', 'title', 'slug', 'post_content',
        'status', 'meta_title', 'meta_keywords',
        'meta_description', 'meta_robots', 'post_type', 'user_id', 'feature_image','offer'];


    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');


    }

    /* Get the tags associate with Page*/


    public function page_tags()
    {

        return $this->belongsToMany('App\Tags', 'page_tags', 'page_id', 'tag_id');

    }

    public function page_categories()
    {
        return $this->belongsToMany('App\Categories', 'page_category', 'page_id', 'category_id');
    }
}
