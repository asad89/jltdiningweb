<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Features;

class HotelFeatures extends Model
{
    //
    protected $table = 'hotel_features';

    function get_feature()
    {
        return $this->belongsTo('App\Features');
    }

    public function getRouteKeyName()
    {
        return 'slug';


    }


}
