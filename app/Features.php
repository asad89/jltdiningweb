<?php

namespace App;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;

class Features extends Model
{
    //
    use HasSlug;
    protected $fillable = ['name', 'status', 'slug'];

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

public
function get_feature_hotel()
{
    return $this->belongsToMany('App\Hotel', 'hotel_features', 'feature_id', 'hotel_id');
}

}
