<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Votecast;
class AwardRestaurant extends Model
{
    //
    protected $table = 'awardrestaurant';
    protected $fillable = array('name', 'slug', 'website', 'facebook', 'twitter', 'instagram','first_name','last_name','email','phone','created_at');



}
