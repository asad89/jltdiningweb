<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelMeals extends Model
{
    //
    protected $table = 'hotel_meals';
}
