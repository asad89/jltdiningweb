<?php

namespace App\Http\Controllers;

//use Illuminate\Support\Facades\Mail;

use App\Contact;
use Illuminate\Http\Request;
/*use App\Jobs\SendContactEmail;*/
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Mail;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contact.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'first_name' => 'required|min:3|max:30',
            'last_name' => 'required|min:3|max:30',
            'email' => 'required|email',
            'message' => 'required|min:15|max:300',
            'captcha' => 'required|captcha',
        ]);

        $contact = Contact::create($request->all());

        // Send Email to Admin
        /* Mail port is not configured properly on AWS i
        /*
         * I am using PHP native email function for the time being
         *
        */

        // To send HTML mail, the Content-type header must be set
        $headers[] = 'MIME-Version: 1.0';
        $headers[] = 'Content-type: text/html; charset=iso-8859-1';

        // Additional headers
        $headers[] = 'To: Administrator <munch@jltdining.com>';
        $headers[] = 'From: JLT Dining <info@jltdining.com>';
        $headers[] = 'Cc: hello@spsaffinity.com';
        $headers[] = 'Bcc: asad@spsaffinity.com';

        // Mail it

        $message = "Dear Admin, We have received a request on contact us form. With following details:<br>Person: ".$contact->first_name. "<br>Email: ".$contact->email."<br>Phone: ".$contact->phone."<br>Message: ".$contact->message;

       // mail('munch@jltdining.com', 'Contact Request Received', $message, implode("\r\n", $headers));


        Mail::send('emails.contact.admin', ['contact' => $contact], function ($m) use ($contact)  {
            $m->from('info@jltdining.com', 'JLT Dining');
            $m->to('munch@jltdining.com', 'Admin')->subject('Contact request was received ');
            $m->bcc('asad.developers@gmail.com','Developer');
            $m->bcc('hello@spsaffinity.com','Team');
        });



      /* Mail::send('emails.contact.admin', ['contact' => $contact], function ($m) use ($contact)  {
            $m->from('info@jltdining.com', 'JLT Dining');
            $m->to('asad@spsaffinity.com', 'Admin')->subject('Contact request was received ');
        });*/

        // Now send email to user

       /* Mail::send('emails.contact.index', ['contact' => $contact], function ($m) use ($contact)  {
            $m->from('info@jltdining.com', 'JLT Dining');
            $m->to($contact->email, $contact->name)->subject('Request received');
        });*/



        //$this->dispatch(new SendContactEmail());
        /* Send Email to user*/



        flash('Thank you we have received your request!', 'success');
        return redirect()->to('contact');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
