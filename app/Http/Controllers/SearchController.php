<?php

namespace App\Http\Controllers;

use App\Hotel;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Response;
use Illuminate\Support\Facades\Input;
use App\Meals;
use App\Cuisines;

class SearchController extends Controller
{
    //
    public function autocomplete()
    {

        //$term = Input::get('term');
        $term = '%' . \Input::get('term') . '%';
        $results = array();
        $hotels = DB::table('hotels')->where('status',1)
            ->where('name', 'LIKE', $term)->get();
        $meal = DB::table('meals')->where('name', 'LIKE', $term)->get();
        $features = DB::table('features')->where('name', 'LIKE', $term)->get();
        $cuisines = DB::table('cuisines')->where('name', 'LIKE', $term)->get();
        // dd($results);

//        $queries = DB::table('hotels')
//            ->where('name', 'LIKE', '%' . $term . '%')
//            /*  ->orWhere('name', 'LIKE', '%'.$term.'%')*/
//            ->take(5)->get();

        $output1 = [];
        $output2 = [];
        $output3 = [];
        $output4 = [];
        foreach ($hotels as $hotel) {
            $output1[] = ['id' => $hotel->id, 'value' => $hotel->name, 'desc' => '(Place)'];
        }
        foreach ($meal as $m) {
            $output2[] = ['id' => $m->id, 'value' => $m->name, 'desc' => '(Food)'];
        }
        foreach ($features as $feature) {
            $output3[] = ['id' => $feature->id, 'value' => $feature->name, 'desc' => '(Feature)'];
        }
        foreach ($cuisines as $cuisine) {
            $output4[] = ['id' => $cuisine->id, 'value' => $cuisine->name, 'desc' => '(Cuisine)'];
        }


        $results = array_merge($output1, $output2, $output3, $output4);
        return Response::json($results);


        /*$hardware_items = \DB::table('hardware_items')
            ->select(\DB::raw("inventory_number, id, serial_number, name, 'hardware' AS 'type'"))
            ->where('inventory_number', 'LIKE', $search)
            ->orWhere('name', 'LIKE', $search)
            ->orWhere('serial_number', 'LIKE', $search);

        $software_items  = \DB::table('software_items')
            ->select(\DB::raw("inventory_number, id, serial_number, name, 'software' AS 'type'"))
            ->where('inventory_number', 'LIKE', $search)
            ->orWhere('name', 'LIKE', $search)
            ->orWhere('serial_number', 'LIKE', $search);*/


    }

    /*Search Results Show*/
    public function search(Request $request)
    {
        $params = $request->all();


        $query = array_get($params, 'q', '');
        $meal = array_get($params, 'meal', []);
        $cuisines = array_get($params, 'cuisines', []);
        $feature = array_get($params, 'features', []);
        $cluster = array_get($params, 'cluster', []);


        /*
                echo "<pre>";
                print_r($feature);
                dd();*/


        $hotel = Hotel::where('name', 'LIKE', '%' . $query . '%');
        if (!empty($meal)) {
            $hotel->whereHas('get_hotel_meals', function ($q) use ($meal) {
                //$meal = explode(',', $meal);
                $q->whereIn('name', $meal);
            });
        }
        if (!empty($feature)) {
            $hotel->whereHas('get_hotel_features', function ($q) use ($feature) {
                //$feature = explode(',', $feature);
                $q->whereIn('slug', $feature);
            });
        }

        if (!empty($cuisines)) {
            $hotel->whereHas('get_hotel_cuisines', function ($q) use ($cuisines) {
                // $cuisines = explode(',', $cuisines);
                $q->whereIn('slug', $cuisines);
            });
        }

        if (!empty($cluster)) {
            //$hotel = Hotel::where('cluster', 'LIKE', '%' . $cluster . '%');
            $hotel = Hotel::where('cluster', 'LIKE',  $cluster);
        }

        $result = $hotel->where('status',1)->paginate(12);
        /*echo "<pre>";
        print_r($result);
        dd();*/


        return view('search.list', compact('result'));
    }

    /*Random search for Feeling Lucky*/
    public function rendom_search()
    {
        $result = Hotel::all()->random(12);
        return view('search.lucky', compact('result'));

    }


}
