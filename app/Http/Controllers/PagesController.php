<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Pages;
use App\PageTags;

class PagesController extends Controller
{
    //
    public function index($slug = null)
    {
//        $page = Pages::where('slug', $slug)->take(1)->get();

        $page = Pages::where('slug', $slug)->where('status',1)->first();
        $rest_pages = array(58,59,56,106,89,100,109);
        if(isset($page->id) && in_array($page->id,$rest_pages) && !(auth()->user())) {
           flash('The Page you are trying to view is restricted!','info');
            return redirect()->to('/');
            //echo "Page is resticted!";
        }


        /*Its a temporary patch
         As this was not initial requirement, later on i will fix
        */

        /*if ($page->id==59 || $page->id==58)
            return redirect()->home();*/
        if (isset($page->post_type) && $page->post_type == 1)
            // Return post view
            return view('blog.show', compact('page'));
        else if (isset($page->post_type) && $page->post_type == 0)
            // Return page view
            return view('page.show', compact('page'));
        else
            // Page not found view
           // return view('page.404');
            abort(404);
    }

   /*
    * Festive Event
    * */

    public function festiveJlt() {

        return view('page.festive');
    }


}
