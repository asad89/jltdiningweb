<?php

namespace App\Http\Controllers;

use App\Pages;
use Illuminate\Http\Request;

use App\Http\Requests;

class RestaurateursLoginController extends Controller
{


    public function __construct()
    {

        $this->middleware('auth', ['except' => ['index']]);
    }

    public function index() {
        return view('restaurateur.login');
    }

    public function restaurateurs_area() {
       $posts = Pages::where('post_type',3)->where('status',1)->paginate(10);
        return view('restaurateur.dashboard',compact('posts'));
    }
}