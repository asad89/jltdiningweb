<?php

namespace App\Http\Controllers;

use App\Award;
use App\AwardCategory;
use App\HiddenGem;
use App\RestaurantAawardCategory;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Mail;
class AwardController extends Controller
{
    //
    public function __construct() {
        $this->middleware('auth')->only('index');
    }
    /*
     * */
    public function index() {
        $categories = AwardCategory::where('status','active')->get();
        return view('award.index',compact('categories'));
    }

    /*
     *
     * */
    public function store(Request $request) {

        $this->validate($request, [
            'name' => 'required|min:3|max:30',
            'category' => 'required|not_in:0',
            'facebook'  => 'url',
            'first_name' => 'required|min:3',
            'last_name' => 'required|min:3',
            'email' => 'required|email',
            'phone' => 'required|min:9|max:10',
            //'captcha' => 'required|captcha',
        ]);

        $category = implode(',', $request->category);

        $contact = Award::create($request->all());

        if(request()->has('category')) {
            $category = $request->category;
            foreach ($category as $key => $value ) {
                RestaurantAawardCategory::create([
                   'category_id' => $value,
                   'restaurant_id' => $contact->id,
                ]);
            }
        }

        //Send email to notify admin
        Mail::send('emails.awards.admin-nomination', ['contact' => $contact], function ($m) use ($contact)  {
            $m->from('munch@jltdining.com', 'JLT Dining');
            $m->to('munch@jltdining.com', 'Administrator')->subject('JLT Restaurant Awards 2019!');
            $m->bcc('asad@spsaffinity.com');
            $m->cc('hello@spsaffinity.com');
        });
        // Redirect after submission
        flash('Thank you! We have received your nomination(s).','success');
        return redirect()->to('thank-you');
    }

    /*
     *Restaurnt hidden Gems
     * */
      public function hiddenGem() {
        //
       return redirect()->to('restaurant-awards-2019-voting');
        // we will uncomment below next year InshaAllah
       // return view('award.hidden-gem');

    }

    public function jlt_hidden_get_nominate(Request $request){
        $this->validate($request, [
            'first_name' => 'required|min:3|max:30',
            'last_name' => 'required|min:3|max:30',
            'email' => 'required|email',
            //'phone'  => 'required|min:9|max:10',
            'restaurant' => 'required|min:3',
        ]);
    $nomination = HiddenGem::create($request->all());
        //Send email to notify admin
        // To send HTML mail, the Content-type header must be set
        $headers[] = 'MIME-Version: 1.0';
        $headers[] = 'Content-type: text/html; charset=iso-8859-1';
        // Additional headers
        $headers[] = 'To: Administrator <munch@jltdining.com>';
       // $headers[] = 'To: Administrator <asad.developers@gmail.com>';
        $headers[] = 'From: JLT Dining <info@jltdining.com>';
       // $headers[] = 'Cc: hello@spsaffinity.com';
        $headers[] = 'Bcc: asad@spsaffinity.com';
        $message = "Dear Admin, We have received a new nomination for JLT Hidden GEM 2018. With following details:<br>Person Name: ".$nomination->name. "<br>Person Email: ".$nomination->name."<br>Phone: ".$nomination->phone."<br>Restaurant: ".$nomination->restaurant."<br>";
       // mail('munch@jltdining.com', 'JLT Restaurant Awards 2017!', $message, implode("\r\n", $headers));
        mail('munch@jltdining.com', 'JLT Hidden Gem 2018!', $message, implode("\r\n", $headers));
        flash('Thank you! We have received your nomination.','success');
        return redirect()->back();
    }
}