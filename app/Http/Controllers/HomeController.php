<?php

namespace App\Http\Controllers;
use App\Award;
use App\AwardRestaurant;
use App\Cuisines;
use App\Features;
use App\HotelImages;
use App\Http\Requests;
use App\Jobs\SendContactEmail;
use App\Meals;
use App\RestaurantAawardCategory;
use App\SyncDB;
use App\User;
use Illuminate\Http\Request;
use App\Hotel;
use App\Pages;
use App\Jobs\SendReminderEmail;
use App\Jobs\SendWelcomeEmail;
use App\Helpers\Helper;
use Intervention\Image\ImageManagerStatic as Image;
use Maatwebsite\Excel\Facades\Excel;
use Mail;


class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $premium = Hotel::where('ad_type',1)->where('status',1)->whereHas('get_hotel_images', function ($q) {
        })
            ->with(['get_hotel_images' => function ($img) {
                $img->inRandomOrder();
                $img->take(10);
            }])->orderBy('id', 'desc')

            ->get();
        /*Get Latest Listings*/
        $meals = Meals::all();
        $cuisines = Cuisines::all();
        $features = Features::all();
        $hotels = Hotel::where('status',1)->orderBy('id', 'desc')->limit(4)->get();
        //$premium = HotelImages::inRandomOrder()->limit(15)->get();
        /*Get Latest Posts for Home page*/
        $latest_posts = Pages::orderBy('id', 'desc')->where('post_type', 1)->limit(3)->get();
        return view('home.index', compact('hotels', 'latest_posts', 'meals', 'premium', 'cuisines', 'features'));
    }

    public function partners() {
        return view('partner.index');
    }


}