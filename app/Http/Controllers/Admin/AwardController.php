<?php

namespace App\Http\Controllers\Admin;

use App\Award;
use App\HiddenGem;
use App\RestaurantAawardCategory;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
class AwardController extends Controller
{
    //
    /*
     * @param registerd restaurant
     * @return list of registered restaurant
     * */
    public function index() {
        $register_users = Award::orderBy('id', 'asc')->paginate(10);
        return view('admin.award.register_restaurant',compact('register_users'));
    }

    /*
     * @param generate report
     * return excel report
     * */
    public function getReport() {
        $register_users = Award::all();
        Excel::create('Registered Restaurants', function($excel) use ($register_users) {
            $excel->sheet('Excel sheet', function($sheet) use ($register_users) {
                $sheet->loadView('admin.reports.register-restaurant',compact('register_users'));
                $sheet->setOrientation('landscape');
            });
        })->export('xls');
    }


    public function destroy($id)
    {
        //
        // First Delete categories

        $restaurant = Award::find($id);

        $rest_award_cat = RestaurantAawardCategory::where('restaurant_id',$restaurant->id);
        $rest_award_cat->delete();


        $restaurant->delete();
        flash('Registered Restaurant has been deleted!', 'success');

        //return redirect()->route('tasks.index');
        return redirect()->back();

    }

    public function pub_nomination_category() {
        $nomination = HiddenGem::paginate(15);
        return view('admin.hidden_gem.index',compact('nomination'));
    }

    public function hidden_gem_report() {

        $nomination = HiddenGem::all();
        //return view('admin.reports.hidden-gem-report',compact('nomination'));
         Excel::create('Hidden Gem', function($excel) use ($nomination) {
            $excel->sheet('Excel sheet', function($sheet) use ($nomination) {
                $sheet->loadView('admin.reports.hidden-gem-report',compact('nomination'));
                $sheet->setOrientation('landscape');
            });
        })->export('xls');
    }

}
