<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Cuisines;
use App\Dishes;
use App\CuisineDishes;
use Illuminate\Support\Facades\Input;

class CuisineDishesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.cuisine-dishes.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $cuisines = Cuisines::all();
        $dishes = Dishes::all();
        return view('admin.cuisine-dishes.create', compact('cuisines', 'dishes'));

    }


    /*
     * Load Dishes in a Cuisine Using Ajax
     * */

    public function get_cuisine_dishes(Request $request)
    {
        $id = $request->cuisine;
        $cuisine_dishes = Cuisines::find($id);
        return response()->json($cuisine_dishes->dishes);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'cuisine_id' => 'required|unique_with:cuisines_dishes,cuisine_id,dish_id',
        ]);

        CuisineDishes::create(Input::all());
        flash('Dish has been Added to Cuisine!', 'success');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view('admin.cuisine-dishes.show');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return view('admin.cuisine-dishes.edit');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        echo $id;
        $cuisinedish = CuisineDishes::find($id);
        dd();
        $cuisinedish->delete();
        flash('Cuisine dish has been deleted!', 'success');
        return redirect()->back();
    }
}
