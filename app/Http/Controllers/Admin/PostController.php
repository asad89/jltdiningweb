<?php

namespace App\Http\Controllers\Admin;

use App\Categories;
use App\PageCategory;
use App\PageTags;
use Illuminate\Http\Request;
use App\Helpers\Helper;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Pages;
use App\Tags;
use Intervention\Image\ImageManagerStatic as Image;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $posts = Pages::where('post_type', 1)->orderBy('created_at', 'desc')->get();
        return view('admin.posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $tags = Tags::all();
        $categories = Categories::all();
        return view('admin.posts.create', compact('tags', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        /*Validate page
         * */
        $this->validate($request, [
            'name' => 'required',
            'post_content' => 'required|min:10',
        ]);

        $post = Pages::create(Input::all());

        if ($request->has('tags')) {
            foreach ($request->tags as $key => $value) {
                $post_tags = new PageTags();
                $post_tags->page_id = $post->id;
                $post_tags->tag_id = $value;
                $post_tags->save();
            }
        }

        if ($request->has('category')) {
            foreach ($request->category as $key => $value) {
                $post_category = new PageCategory();
                $post_category->page_id = $post->id;
                $post_category->category_id = $value;
                $post_category->save();
            }
        }

        $image = "";
        if (Input::file('feature_image')) {
            $img_settings = array('large' => '430x328');
            $image = Helper::upload_image(Input::file('feature_image'), 'uploads/posts', 1, $img_settings);
        }
        $post_update = Pages::findorFail($post->id);
        $data = array(
            'user_id' => Auth::user()->id,
            'feature_image' => $image,
        );
        $post_update->fill($data)->save();
        flash('Post has been Added!', 'success');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view('admin.posts.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $categories = Categories::pluck('name', 'id');
        $tags = Tags::pluck('name', 'id');
        $post = Pages::findorFail($id);
        $selected_tags = $post->page_tags()->pluck('tag_id')->toArray();
        $selected_categories = $post->page_categories()->pluck('category_id')->toArray();
        return view('admin.posts.edit', compact('post', 'categories', 'tags', 'selected_tags', 'selected_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $input = $request->all();
        $post = Pages::findorFail($id);
        $feature_image = $post->feature_image;


        $this->validate($request, [
            'name' => 'required|min:2',
        ]);


        if (Input::hasFile('feature_image')) {
            // First Remove old Logo
            $img_settings = array('large' => '430x328');
            if (!empty($post->feature_image) && file_exists('uploads/posts/large' . $post->feature_image)) {
                File::delete('uploads/posts/large' . $post->feature_image);
            }
            if (!empty($post->feature_image) && file_exists('uploads/posts/thumb' . $post->feature_image)) {
                File::delete('uploads/posts/thumb' . $post->feature_image);
            }
            $feature_image = Helper::upload_image(Input::file('feature_image'), 'uploads/posts', 1, $img_settings);
        }

        if ($request->has('category')) {
            // Lets update Features Table
            $post->page_categories()->sync($request->category);
        } else
            $post->page_categories()->sync([]);


        if ($request->has('tags')) {
            // Lets update Features Table
            $post->page_tags()->sync($request->tags);
        } else
            $post->page_tags()->sync([]);


        $img_array = array(
            'feature_image' => $feature_image,
        );
        $input = array_merge($input, $img_array);
        $post->fill($input)->save();


        flash('Post has been updated!', 'success');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $post = Pages::findOrFail($id);
        $post->delete();
        flash('Post has been deleted!', 'success');
        return redirect()->back();
    }
}
