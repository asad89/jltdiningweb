<?php

namespace App\Http\Controllers\Admin;

use App\Pages;
use Illuminate\Http\Request;
use App\Helpers\Helper;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $posts = Pages::where('post_type',3)->get();
        return view('admin.offers.index', compact('posts'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.offers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

       $menu = "";
        $this->validate($request, [
            'name' => 'required|unique:pages',
            'post_content' => 'required',
        ]);


        if ($request->hasFile('offer') && $request->file('offer')->isValid()) {
            // File was successfully uploaded and is now in temp storage.
            // Move it somewhere more permanent to access it later.
            $file = Input::file('offer');
            $filename = strtolower(str_replace(" ", "-", time() . '-' . $file->getClientOriginalName()));
            if (Input::file('offer')->move('uploads/offers/', $filename)) {
                $menu = $filename;
                // File successfully saved to permanent storage
            }
        }

        $data = array(
            'post_type' => 3,
            'user_id' => auth()->user()->id,
            'offer' => $menu,
        );

        $input = Input::all();
        $input = array_merge($input, $data);
        $page = Pages::create($input);

        if (Input::file('feature_image')) {
            $img_settings = array('large' => '760x230');
            $image = Helper::upload_image(Input::file('feature_image'), 'uploads/offers',1, $img_settings,1);
            $data = array(
                'feature_image' => $image,
            );
            $page->fill($data)->save();
        }
        flash('you have createed a new offer successfully','success');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $post = Pages::find($id);
        return view('admin.offers.edit',compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $input = $request->all();
        $post = Pages::findorFail($id);
        $image = $post->feature_image;
        $offer = $post->offer;

        $this->validate($request, [
            'name' => 'required|min:2',
        ]);


        if ($request->hasFile('offer') && $request->file('offer')->isValid()) {
            // File was successfully uploaded and is now in temp storage.
            // Move it somewhere more permanent to access it later.
            if (!empty($post->offer) && file_exists('uploads/offers' . $post->offer)) {
                File::delete('uploads/offers' . $post->offer);
            }
            $file = Input::file('offer');
            $filename = strtolower(str_replace(" ", "-", time() . '-' . $file->getClientOriginalName()));
            if (Input::file('offer')->move('uploads/offers/', $filename)) {
                $offer = $filename;
                // File successfully saved to permanent storage
            }
        }



        if (Input::hasFile('feature_image')) {
            // First Remove old Logo
            $img_settings = array('large' => '760x230');
            if (!empty($post->feature_image) && file_exists('uploads/offers/large' . $post->feature_image)) {
                File::delete('uploads/offers/large' . $post->feature_image);
            }
            if (!empty($post->feature_image) && file_exists('uploads/offers' . $post->feature_image)) {
                File::delete('uploads/offers' . $post->feature_image);
            }
            $image = Helper::upload_image(Input::file('feature_image'), 'uploads/offers',1, $img_settings,1);
        }

        $img_array = array(
            'feature_image' => $image,
            'user_id' => auth()->user()->id,
            'offer' => $offer,
        );
        $input = array_merge($input, $img_array);
        $post->fill($input)->save();


        flash('Post has been updated!', 'success');
        return redirect()->back();


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
