<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Cuisines;
use Illuminate\Support\Facades\Input;

class CuisinesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $cuisines = Cuisines::all();
        return view('admin.cuisines.index', compact('cuisines'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.cuisines.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required|max:50|min:2|unique:cuisines',
        ]);

        Cuisines::create(Input::all());
        //Cuisines::save();

        flash('Cuisine has been Added!', 'success');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $cuisine = Cuisines::find($id);
        return view('admin.cuisines.show', compact('cuisine'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $cuisine = Cuisines::find($id);
        return view('admin.cuisines.edit', compact('cuisine'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $cuisine = Cuisines::findorFail($id);
        $this->validate($request, [
            'name' => 'required|max:50|min:2|unique:meals',
        ]);
        $cuisine->fill($request->all())->save();
        flash('Cuisine has been updated!', 'success');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $cuisine = Cuisines::findOrFail($id);
        $cuisine->delete();
        flash('Cuisine has been deleted!', 'success');
        return redirect()->back();
    }
}
