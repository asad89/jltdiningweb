<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use App\Events;
use File;
use App\Contact;
use Illuminate\Support\Facades\Input;
use Intervention\Image\ImageManagerStatic as Image;
class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $events = Events::orderBy('created_at', 'desc')->get();
        return view('admin.events.index',compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.events.create');
    }


    public function pastEvent() {
        $events = Events::where('status',2)->orderBy('created_at', 'desc')->get();
        return view('admin.events.past',compact('events'));
    }


    /*
     * Event Registeration listing
     * */
    public function getEventUsers() {
        $users = Contact::where('type',1)->orderBy('id','desc')->get();
        return view('admin.events.registered-users',compact('users'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'name' => 'required|unique:events',
            'title' => 'required',
            'description' => 'required',
            'ending_date' => 'required',
        ]);

        // Validation passed now save event
        $event = Events::create(Input::all());
        $event->slug = str_slug($request->name);
        $event->save();
        if (Input::file('feature_image')) {
            $img_settings = array('large' => '555x277');
            $image = Helper::upload_image(Input::file('feature_image'), 'uploads/events', 1, $img_settings, 1);
            $data = array(
                'feature_image' => $image,
            );
            $event->fill($data)->save();
        }
        // Add Event Keywords
        flash('Event has been Added/Updated!', 'success');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
       $event = Events::findorfail($id);
       return view('admin.events.edit',compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $event = Events::findorfail($id);
        $event->name = $request->name;
        $event->slug = str_slug($request->name);
        $event->title = $request->title;
        $event->ending_date = $request->ending_date;
        $event->description = $request->description;
        $event->meta_title = $request->meta_title;
        $event->meta_keywords = $request->meta_keywords;
        $event->meta_description = $request->meta_description;
        $event->meta_robots = 1;
        $event->save();
        // Check if banner need to be replaced
        if (Input::file('feature_image')) {
            // First delete existing banners
            if (file_exists('uploads/events/large/' . $event->feature_image)) {
                    File::delete('uploads/events/large/' . $event->feature_image);
            }

            $img_settings = array('large' => '555x277');
            $image = Helper::upload_image(Input::file('feature_image'), 'uploads/events', 1, $img_settings, 1);
            $data = array(
                'feature_image' => $image,
            );
            $event->fill($data)->save();
        }
        // Add Event Keywords
        flash('Event has been Updated!', 'success');
        return redirect()->to('admin/events');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $event = Events::findorFail($id);
        /*If hotel is premium it may have images*/

        if (!empty($event->feature_image)) {
            if (file_exists('uploads/events/large/' . $event->feature_image)) {
                File::delete('uploads/events/large/' . $event->feature_image);
            }
        }
        $event->delete();
        flash('Event has been deleted!', 'success');
        return redirect()->back();
    }
}
