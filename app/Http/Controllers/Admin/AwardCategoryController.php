<?php

namespace App\Http\Controllers\Admin;

use App\AwardCategory;
use App\RestaurantAawardCategory;
use App\Votecast;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class AwardCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $categories = AwardCategory::paginate(10);;
        return view('admin.award_category.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.award_category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
           'name'  => 'required|unique:awardcategory|min:2|max:260',
        ]);
        AwardCategory::create([
            'name'   => request()->name,
            'slug' => str_slug(request()->name),
        ]);

        flash('Category was created successfully');
        return redirect()->back();


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(AwardCategory $award_category)
    {
        //
        return view('admin.award_category.edit',compact('award_category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[
            'name'  => 'required|min:2|max:260',
        ]);

        $award = AwardCategory::find($id);

        $award->name = request()->name;
        $award->slug = str_slug(request()->name);

        $award->save();
        flash('Award Category name has been updated');
        return redirect()->back();
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $awardCat = AwardCategory::find($id);
        $awardCat->delete();
        flash('Award Category has been deleted');
        return redirect()->back();

        //
    }

    /*
     * @param Category id
     * @retuen list of restaurant in category
     * */
    public function categoryRestaurant($category_id) {

        // Get Category Name
        $category = AwardCategory::find($category_id);
        $restaurants = RestaurantAawardCategory::select('restaurant_id')->where('category_id',$category_id)->get();
        // dd();
        return view('admin.award_category.category_restaurants',compact('restaurants','category'));
    }

    /*
     * @param request to export excel file
     * @return excel file with list of category restaurants
     * */

    public function getReport($category_id) {
        $category = AwardCategory::find($category_id);

        $restaurants = RestaurantAawardCategory::select('restaurant_id','category_id')->where('category_id',$category_id)->get();
        Excel::create(ucfirst($category->name).' Restaurants', function($excel) use ($restaurants) {
            $excel->sheet('Excel sheet', function($sheet) use ($restaurants) {
                $sheet->loadView('admin.reports.register-category-restaurant',compact('restaurants'));
                $sheet->setOrientation('landscape');
            });
        })->export('xls');
    }



}
