<?php

namespace App\Http\Controllers\admin;

use App\Hotel;
use App\HotelImages;
use Illuminate\Http\Request;
use File;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use AnkitPokhrel\LaravelImage\ImageUploadService;
use Illuminate\Support\Facades\Input;


class HotelImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $image;

    /**
     * @param ImageUploadService $image
     */
    public function __construct(ImageUploadService $image)
    {


        //set properties for file upload
        $this->image = $image;
        $this->image->setUploadField('image'); //default is image
        $this->image->setUploadFolder('hotels/premium'); //default is public/uploads/contents
    }

    public function index()
    {
        //
        $hotel_images = HotelImages::all();
        return view('admin.hotel_images.index', compact('hotel_images'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Upload Image
        $input = $request->all();

        if (Input::hasFile('image') && $this->image->upload()) {
            //image is uploaded, get uploaded image info
            $uploadedFileInfo = $this->image->getUploadedFileInfo();

            print_r($uploadedFileInfo);
            dd();


            //do whatever you like with the information
            $input = array_merge($input, $uploadedFileInfo);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $hotel = Hotel::find($id);
        $hotel_images = HotelImages::where('hotel_id', $id)->get();
        return view('admin.hotel_images.show', compact('hotel_images', 'hotel'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function delete(Request $request)
    {

        if ($request->has('file')) {
            $id = $request->input('file');
            $hotel = HotelImages::findorfail($id);
            if (file_exists('uploads/hotels/premium/large/' . $hotel->image)) {
                File::delete('uploads/hotels/premium/large/' . $hotel->image);
            }
            if (file_exists('uploads/hotels/premium/' . $hotel->image)) {
                File::delete('uploads/hotels/premium/' . $hotel->image);
            }
            $hotel->delete();
            echo "Image has been deleted, No need to click save please!";
            //return '';
        }
    }


    public function destroy($id)
    {
        //
        echo "test";

    }
}
