<?php

namespace App\Http\Controllers\Admin;

use App\Award;
use App\AwardCategory;
use App\Events;
use App\Events\Event;
use App\Hotel;
use App\RestaurantVote;
use App\User;
use Dompdf\FrameDecorator\Page;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Pages;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $posts = Pages::where('post_type', 1)->count();
        $pages = Pages::where('post_type', 0)->count();
        $users = User::get()->count();
        $restaurants = Hotel::get()->count();
        $premium = Hotel::where('ad_type', 1)->count();
        $awardCategories = AwardCategory::get()->count();
        $awardNominations = Award::get()->count();
        $votes_received = RestaurantVote::get()->count();
        $events = Events::get()->count();

        return view('admin.dashboard.index', compact('posts', 'events','pages', 'users', 'restaurants', 'premium','awardCategories','awardNominations','votes_received'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.user.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
