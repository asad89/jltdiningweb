<?php

namespace App\Http\Controllers\Admin;

use App\AwardCategory;
use App\AwardRestaurant;
use App\RestaurantVote;
use App\Votecast;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
class VotingController extends Controller
{
    //
    public function index() {
        $votes =  RestaurantVote::latest()->get();
        return view('admin.voting.index',compact('votes'));
    }

    /*
     * @pararm user id who did vote
     * return details of vote casted
     * */
    public function show(Request $request , $id) {
       $vote = RestaurantVote::find($id);
       return view('admin.voting.user_vote_casted',compact('vote'));
    }

    /*
   * @param get vote by category
   * @return votes casted by use
   *
   * */

    public function voteByCategory() {
        // First Get All Categories
        $category = AwardCategory::all();
        return view('admin.voting.votes_by_category',compact('category'));
    }

    /*
     *@param category_id
     * return list of 10 restaurant with highest votes
     * */

    public function restaurantTopVotes($category_id) {
        //$top = Votecast::where('category_id',$category_id)->count();
        $category = AwardCategory::find($category_id);


        $votes = DB::select(
            "SELECT category_id, restaurant_id,
            (SELECT name from awardrestaurant where id = restaurant_id) as restaurant_name ,
             COUNT(*) as votes
            FROM vote_casted where category_id=".$category_id."
            GROUP BY restaurant_id HAVING COUNT(*) > 1");
        return view('admin.voting.top_voting_restaurant',compact('votes','category'));
    }

    public function topRestaurantReport($category_id) {
        $category = AwardCategory::find($category_id);

        $votes = DB::select(
            "SELECT category_id, restaurant_id,
            (SELECT name from awardrestaurant where id = restaurant_id) as restaurant_name ,
             COUNT(*) as votes
            FROM vote_casted where category_id=".$category_id."
            GROUP BY restaurant_id HAVING COUNT(*) > 1");

        Excel::create(' Restaurants', function($excel) use ($votes) {
            $excel->sheet('Excel sheet', function($sheet) use ($votes) {
                $sheet->loadView('admin.reports.top-category-restaurant',compact('votes'));
                $sheet->setOrientation('landscape');
            });
        })->export('xls');
    }

    public function subscriberReport() {

        $newsletter = RestaurantVote::where('newsletter',1)->get();
        Excel::create('Subscribers', function($excel) use ($newsletter) {
            $excel->sheet('Excel sheet', function($sheet) use ($newsletter) {
                $sheet->loadView('admin.reports.newsletter',compact('newsletter'));
                $sheet->setOrientation('landscape');
            });
        })->export('xls');
    }











}
