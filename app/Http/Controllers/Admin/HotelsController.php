<?php
namespace App\Http\Controllers\admin;

use App\Cuisines;
use App\Dishes;
use App\Features;
use App\HotelCuisines;
use App\HotelFeatures;
use App\HotelImages;
use App\HotelTags;
use App\Tags;
use File;
use PDF;
use Illuminate\Http\Request;
use App\Helpers\Helper;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Hotel;
use App\Meals;
use App\HotelMeals;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Intervention\Image\ImageManagerStatic as Image;

class HotelsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $hotels = Hotel::all();
        return view('admin.hotels.index', compact('hotels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $meals = Meals::all();
        $features = Features::all();
        $cuisines = Cuisines::all();
        $dishes = Dishes::all();
        $tags = Tags::all();
        return view('admin.hotels.create', compact('tags', 'meals', 'tags', 'features', 'cuisines', 'dishes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         /*Empty veriables to initiate*/
        $logo = "";
        $menu = "";
        $image = "";
        /*$this->validate($request, [
            'name' => 'bail|required|max:50|min:4',
            'phone' => 'required|max:20|min:8',
            'email' => 'required|max:40',
            'details' => 'required|min:10',
            'address' => 'required|min:5|max:60',
            //'file' => 'required|image|max:3000',

        ]);
*/
        $this->validate($request, [
            'name' => 'required|max:50|min:4',
            'address' => 'required|max:200|min:4',
            //'email' => 'required|max:40',
            //'details' => 'required|min:10',
            //'address' => 'required|min:5|max:60',
            //'file' => 'required|image|max:3000',

        ]);

        if ($request->hasFile('logo')) {
            $img_settings = array('thumb' => '150x150');
            $logo = Helper::upload_image(Input::file('logo'), 'uploads/hotels/logo', 1, $img_settings, 1);
        }

        if ($request->hasFile('image')) {
            $img_settings = array('large' => '580x408');
            $image = Helper::upload_image(Input::file('image'), 'uploads/hotels', 1, $img_settings);
        }

        if ($request->hasFile('menu') && $request->file('menu')->isValid()) {
            // File was successfully uploaded and is now in temp storage.
            // Move it somewhere more permanent to access it later.
            $file = Input::file('menu');
            $filename = strtolower(str_replace(" ", "-", time() . '-' . $file->getClientOriginalName()));
            if (Input::file('menu')->move('uploads/hotels/menu/', $filename)) {
                $menu = $filename;
                // File successfully saved to permanent storage
            }
        }

        $data = array(
            'logo' => $logo,
            'image' => $image,
            'menu' => $menu,
        );

        $input = Input::all();
        $input = array_merge($input, $data);
        $hotel = Hotel::create($input);


        if ($request->hasFile('files') && !empty($request->file('files'))) {
            $images = Input::file('files');
            foreach ($images as $img) {
                $img_settings = array('large' => '800 x 484');
                $image = Helper::upload_image($img, 'uploads/hotels/premium', 1, $img_settings);
                $hotel_image = new HotelImages();
                $hotel_image->hotel_id = $hotel->id;
                $hotel_image->image = $image;
                $hotel_image->save();
            }
        }

        if ($request->hasFile('file')) {
            $files = $request->file('file');
            foreach ($files as $file) {
                $img_settings = array('large' => '800 x 356');
                $image = Helper::upload_image($file, 'uploads/hotels/premium', 1, $img_settings);
                $hotel_image = new HotelImages();
                $hotel_image->hotel_id = $hotel->id;
                $hotel_image->image = $image;
                $hotel_image->save();
            }
        }


        if ($request->has('meals')) {
            foreach ($request->meals as $key => $value) {
                $hotel_meal = new HotelMeals();
                $hotel_meal->hotel_id = $hotel->id;
                $hotel_meal->meal_id = $value;
                $hotel_meal->save();
            }
        }

        if ($request->has('cuisines')) {
            foreach ($request->cuisines as $key => $value) {
                $hotel_cuisine = new HotelCuisines();
                $hotel_cuisine->hotel_id = $hotel->id;
                $hotel_cuisine->cuisine_id = $value;
                $hotel_cuisine->save();
            }
        }

        if ($request->has('features')) {
            foreach ($request->features as $key => $value) {
                $hotel_features = new HotelFeatures();
                $hotel_features->hotel_id = $hotel->id;
                $hotel_features->feature_id = $value;
                $hotel_features->save();
            }
        }
        if ($request->has('tags')) {
            foreach ($request->tags as $key => $value) {
                $hotel_tags = new HotelTags();
                $hotel_tags->hotel_id = $hotel->id;
                $hotel_tags->tag_id = $value;
                $hotel_tags->save();
            }
        }
        flash('Restaurant has been Added!', 'success');
        //return redirect()->to('admin/hotels');
        return redirect()->back();
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view('admin.hotels.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $meals = Meals::pluck('name', 'id');
        $features = Features::pluck('name', 'id');
        $cuisines = Cuisines::pluck('name', 'id');
        $tags = Tags::pluck('name', 'id');
        $hotel = Hotel::findorFail($id);
        $selected_tags = $hotel->get_hotel_tags()->pluck('tag_id')->toArray();
        $selected_meals = $hotel->get_hotel_meals()->pluck('meal_id')->toArray();
        $selected_cuisines = $hotel->get_hotel_cuisines()->pluck('cuisine_id')->toArray();
        $selected_features = $hotel->get_hotel_features()->pluck('feature_id')->toArray();
        return view('admin.hotels.edit', compact('tags', 'meals', 'tags', 'features', 'cuisines', 'hotel', 'selected_tags', 'selected_meals', 'selected_cuisines', 'selected_features'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //


        $input = $request->all();
        $hotel = Hotel::findorFail($id);
        $logo = $hotel->logo;
        $image = $hotel->image;
        $menu = $hotel->menu;

        /*   $this->validate($request, [
               'name' => 'required|max:50|min:2',
           ]);*/

        if (Input::hasFile('logo') && $request->file('logo')->isValid()) {
            // First Remove old Logo
            $img_settings = array('thumb' => '150x150');

            if (file_exists('uploads/hotels/logo/thumb/' . $hotel->logo)) {
                File::delete('uploads/hotels/logo/thumb/' . $hotel->logo);
            }
            $logo = Helper::upload_image(Input::file('logo'), 'uploads/hotels/logo', 1, $img_settings, 1);
        }

        if (Input::hasFile('image')) {
            if (file_exists('uploads/hotels/large/' . $hotel->image)) {
                File::delete('uploads/hotels/large/' . $hotel->image);
            }
            $img_settings = array('large' => '580x408');
            $image = Helper::upload_image(Input::file('image'), 'uploads/hotels', 1, $img_settings);
        }

        if (Input::hasFile('menu') && $request->file('menu')->isValid()) {
           // \Log::info('I came inside menu');


            // File was successfully uploaded and is now in temp storage.

            if (file_exists('uploads/hotels/menu/' . $hotel->menu)) {
                File::delete('uploads/hotels/menu/' . $hotel->menu);
            }
            // Move it somewhere more permanent to access it later.
            $file = Input::file('menu');
            $filename = strtolower(str_replace(" ", "-", time() . '-' . $file->getClientOriginalName()));
            if (Input::file('menu')->move('uploads/hotels/menu/', $filename)) {
                $menu = $filename;
             //   \Log::info('I came inside menu'.$menu);
                // File successfully saved to permanent storage
            }
        }
        $img_array = array(
            'logo' => $logo,
            'image' => $image,
            'menu' => $menu,
        );


        if ($request->has('features')) {
            // Lets update Features Table
            $hotel->get_hotel_features()->sync($request->features);
        } else
            $hotel->get_hotel_features()->sync([]);

        if ($request->has('tags')) {
            // Lets update tags
            $hotel->get_hotel_tags()->sync($request->tags);
        } else
            $hotel->get_hotel_tags()->sync([]);
        if ($request->has('meals')) {
            $hotel->get_hotel_meals()->sync($request->meals);
        } else
            $hotel->get_hotel_meals()->sync([]);

        if ($request->has('cuisines')) {
            $hotel->get_hotel_cuisines()->sync($request->cuisines);
        } else
            $hotel->get_hotel_cuisines()->sync([]);
        $input = array_merge($input, $img_array);
        $hotel->fill($input)->save();


        /*   /*Chaypi for the time being*/


        if (isset($_FILES['files']) && !$_FILES['files']['name'][0] == null) {

            $images = Input::file('files');
            foreach ($images as $img) {
                $img_settings = array('large' => '600 x 600');
                $image = Helper::upload_image($img, 'uploads/hotels/premium', 1, $img_settings);
                $hotel_image = new HotelImages();
                $hotel_image->hotel_id = $hotel->id;
                $hotel_image->image = $image;
                $hotel_image->save();
            }
        }
        flash('Restaurant Information has been updated!', 'success');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hotel = Hotel::findorFail($id);
        /*If hotel is premium it may have images*/
        if (!empty($hotel->logo)) {
            if (file_exists('uploads/hotels/logo/' . $hotel->logo)) {
                File::delete('uploads/hotels/logo/' . $hotel->logo);
            }
            if (file_exists('uploads/hotels/logo/thumb/' . $hotel->logo)) {
                File::delete('uploads/hotels/logo/thumb/' . $hotel->logo);
            }
        }

        if (!empty($hotel->image)) {
            if (file_exists('uploads/hotels/' . $hotel->image)) {
                File::delete('uploads/hotels/' . $hotel->image);
            }
            if (file_exists('uploads/hotels/large/' . $hotel->image)) {
                File::delete('uploads/hotels/large/' . $hotel->image);
            }
        }

        if ($hotel->ad_type == 1) {
            /*Get hotel Images*/
            $hotel_images = $hotel->get_hotel_images;
            if (count($hotel_images)) {
                foreach ($hotel_images as $hotel_img) {
                    if (file_exists('uploads/hotels/premium/' . $hotel_img->image)) {
                        File::delete('uploads/hotels/premium/' . $hotel_img->image);
                    }
                    if (file_exists('uploads/hotels/premium/large/' . $hotel_img->image)) {
                        File::delete('uploads/hotels/premium/large/' . $hotel_img->image);
                    }
                }
            }
        }
        $hotel->delete();
        flash('Restaurant has been deleted!', 'success');
        return redirect()->back();
    }
}