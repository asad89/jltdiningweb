<?php

namespace App\Http\Controllers\Admin;

use App\Pages;
use App\Tags;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $pages = Pages::where('post_type', 0)->orderBy('created_at', 'desc')->get();
        return view('admin.pages.index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $tags = Tags::all();
        return view('admin.pages.create', compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate Page
        $this->validate($request, [
            'name' => 'required|unique:pages',
            'post_content' => 'required',
        ]);
        $page = Pages::create(Input::all());
        if ($request->has('tags')) {
            foreach ($request->tags as $key => $value) {
                $post_tags = new PageTags();
                $post_tags->page_id = $page->id;
                $post_tags->tag_id = $value;
                $post_tags->save();
            }
        }

        if (Input::file('feature_image')) {
            $img_settings = array('large' => '760x230');
            $image = Helper::upload_image(Input::file('feature_image'), 'uploads/pages', 1, $img_settings);
            $data = array(
                'feature_image' => $image,
            );
            $page->fill($data)->save();
        }
        flash('Page has been Added!', 'success');
        return redirect()->to('admin/pages');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view('admin.pages.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $page = Pages::find($id);
        return view('admin.pages.edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $page = Pages::findorFail($id);
        $this->validate($request, [
            'name' => 'required|min:2',
        ]);
        $page->fill($request->all())->save();
        flash('Page has been updated!', 'success');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        /*
         * Delete a selected page
         * */
        $page = Pages::findOrFail($id);
        $page->delete();
        flash('Page has been deleted!', 'success');
        return redirect()->back();
    }
}
