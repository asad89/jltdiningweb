<?php

namespace App\Http\Controllers;

use App\Cuisines;
use App\Features;
use App\Meals;
use Illuminate\Http\Request;
use App\Hotel;
use Mapper;
use App\Helpers\Helper;
use App\Http\Requests;


class HotelController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        //
        $total_hotel = Hotel::all()->count();
        $hotels = Hotel::where('status',1)->orderBy('id', 'desc')->paginate(12);
        $meals = Meals::all();
        $features = Features::all();
        $cuisines = Cuisines::all();
        return view('hotels.index', compact('hotels', 'meals', 'features', 'cuisines', 'total_hotel'));
    }



    public function featurehotels($feature) {
        echo $feature;
        die();
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //


        return view('hotels.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $hotel = Hotel::where('slug', $id)->get()->first();
        $features = Hotel::find($hotel->id);
        if ($hotel->ad_type == 1) {
            $hotel_video = Helper::youtube($hotel->video);
            return view('hotels.premium', compact('hotel', 'features', 'hotel_video'));
        } else
            return view('hotels.show', compact('hotel', 'features'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /*
     * @param link of pdf
     * read pdf menu
     * */
    public function readMenu($menu) {
        return view('hotels.read_menu',compact('menu'));
    }


}
