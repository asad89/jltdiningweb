<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

class RestaurateursRegisterController extends Controller
{
    //
   /*
    * @param Restaurateurs Register
    * @return Registeration Signup form
    *
    * */
    public function index() {
        return view('restaurateur.register');
    }

    /*
     * @param Post_Store
     * @return Store form  values
     * */

    public function store(Request $request) {

        $this->validate($request,[
           'first_name' => 'required|max:255',
           'last_name' => 'required',
           'outlet' => 'required',
           'position' => 'required',
           'phone' => 'required|max:30|min:6',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',

        ]);
        $user = User::create([
            'name' =>  $request->first_name,
            'last_name' => $request->last_name,
            'outlet' => $request->outlet,
            'position' => $request->position,
            'email' => $request->email,
            'phone' => $request->phone,
            // Role 3 stands for Restaurant
            'role' => 3,
            'confirmed' => 0,
            'password' => bcrypt($request->password),

        ]);

//        Mail::send('emails.restaurateurs.user-account-signup', ['user' => $user], function ($m) use ($user)  {
//            $m->from('info@jltdining.com', 'JLT Dining');
//            $m->to('munch@jltdining.com', 'Admin')->subject('New sign up request received!');
//            $m->bcc('asad.developers@gmail.com','Developer');
//            $m->bcc('hello@spsaffinity.com','Team');
//        });



        //$data = array('user' => $request->first_name.' '. $request->last_name, 'email' => $request->email);

        // Now Send email
        /*Mail::send('emails.welcome.index', $data, function ($message) use ($data) {
            $message->from('info@jltdining.com', 'JLT Dining');
            $message->subject('Thanks for signup');
            $message->to($data['email'], $name = null);
        });*/
        flash('Your account signup has been completed, please check your email','success');

        $headers[] = 'MIME-Version: 1.0';
        $headers[] = 'Content-type: text/html; charset=iso-8859-1';

        // Additional headers
        $headers[] = 'To: Administrator <munch@jltdining.com>';
        $headers[] = 'From: JLT Dining <info@jltdining.com>';
        $headers[] = 'Cc: hello@spsaffinity.com';
        $headers[] = 'Bcc: asad@spsaffinity.com';

        // Mail it

        $message = "Dear Admin, We have received a new registeration request:<br>Person: ".$request->first_name. "<br>Email: ".$request->email."<br>Phone: ".$request->phone. "Please approve the request as soon as possible!";
        //mail('munch@jltdining.com', 'Contact Request Received', $message, implode("\r\n", $headers));
        return redirect()->to('/');
        //  return $user;
    }
}
