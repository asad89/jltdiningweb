<?php

namespace App\Http\Controllers;

use App\AwardCategory;
use App\RestaurantAawardCategory;
use App\RestaurantVote;
use App\Votecast;
use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;


class VotingController extends Controller
{
    //
    public function index() {
        $categories = AwardCategory::select('id','name')->where('status','active')->get();
        return view('voting.index',compact('categories'));
    }

    /*
     * @param category id
     * @return list of restaurants nominated in category
     * */

    public function getCategoryRestaurant() {
      if(request()->has('category_id') && !empty(request()->category_id)) {
          // Now get the hotels of this category
          $restaurants = RestaurantAawardCategory::select('restaurant_id','category_id')->where('category_id',request()->category_id)->get();
            // Get Category Name
          $category_name  = AwardCategory::find(request()->category_id);
          ?>
          <label style="margin-bottom:10px;margin-top:10px;">Choose a restaurant to vote in category <?php echo $category_name->name; ?></label>
          <select name="vote[<?php echo $category_name->id?>]" class="form-control"><?php
          ?><option value="0"> Select your favourite! </option><?php
          foreach ($restaurants as $restaurant) {
              ?>
                <option value="<?php echo $restaurant->restaurant_id?>"><?php echo $restaurant->getRestaurant->name; ?></option>
              <?php
         }
         ?>
          </select>
         <?php
      }
    }
    /*
     * @param voting form
     * return vote has been casted
     * */

    public function store(Request $request) {

        $newsletter = 0;
        // validate basic info
        $this->validate($request,[
            'first_name' => 'required',
            'email' => 'required|email|unique:restaurant_votes',
            'vote' => 'required',
        ],
            $messages = [
                'vote.required' => 'Please select a category to vote',

                'email.email' => 'Please use a valid email address',
                //'email.unique' => 'You have already casted your vote',

            ]
        );


        // store vote information
        if(request()->has('newsletter'))
            $newsletter = 1;

        $voter_id = RestaurantVote::create([
               'name' => request()->first_name.'-'.request()->last_name,
               'email' => request()->email,
               'verify_code'    => rand('1234567890','0987654321'),
                'newsletter'    => $newsletter,
               'ip_address' => request()->ip(),
                'device'    => $request->header('User-Agent'),
           ]);

        // Now save vote details
        // array vote has key as categord_id and value as restaurant

       if($request->has('vote') && is_array(request()->vote)){
           foreach (request()->vote as $cat_id => $restaurant_id) {
               Votecast::create([
                   'user_id' => $voter_id->id,
                   'restaurant_id' => $restaurant_id,
                   'category_id' => $cat_id,
               ]);
           }
        }
        // Send email to voter
        Mail::send('emails.vote.vote_cast_notice', ['user' => $voter_id], function ($m) use ($voter_id)  {
            $m->from('munch@jltdining.com', 'JLT Dining');
            $m->to($voter_id->email, $voter_id->name)->subject('Thanks for voting in the 2019 JLT Dining Awards!');
           // $m->bcc('asad.developers@gmail.com','Developer');
        });
        flash('Thank you we have received your vote');
        return redirect()->back();
    }

}