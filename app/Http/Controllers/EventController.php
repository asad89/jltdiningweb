<?php

namespace App\Http\Controllers;

use App\Events;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Contact;
//use Carbon\Carbon;

class EventController extends Controller
{
    //

    /*
     * @paraem Events
     * @return current active events upcoming
     * */

    public function index() {

        $events = Events::where('status',1)->orderBy('id','desc')->get();
        $event_title = "Upcoming Events";
        return view('events.index',compact('events','event_title'));
    }

    /*
     * @param event_id
     * return even details
     * */


    public function pastEvent() {
        //shortcut wil fix it later
        $event_title = "Past Events";
        $events = Events::where('status',2)->orderBy('id','desc')->get();
        return view('events.index',compact('events','event_title'));
    }



    public function show() {

    }

    /*
     * @param past event
     * @return list of past events
     *
     * */

    public function eventRegister(Request $request) {

       /*validate information*/
        $this->validate($request, [
            'first_name' => 'required|min:3|max:30',
            'last_name' => 'required|min:3|max:30',
            'email' => 'required|email',
            'captcha' => 'required|captcha',
        ]);

        $contact = Contact::create($request->all());
        $contact->type = 1;
        $contact->save();

        // Send Email to Admin
        /* Mail port is not configured properly on AWS i
        /*
         * I am using PHP native email function for the time being
         *
        */

        // To send HTML mail, the Content-type header must be set
        $headers[] = 'MIME-Version: 1.0';
        $headers[] = 'Content-type: text/html; charset=iso-8859-1';

        // Additional headers
        $headers[] = 'To: Administrator <munch@jltdining.com>';
        $headers[] = 'From: JLT Dining <info@jltdining.com>';
        $headers[] = 'Cc: hello@spsaffinity.com';
        $headers[] = 'Bcc: asad@spsaffinity.com';

        // Mail it





        $message = "Dear Admin, We have received a request of event registeration. With following details:<br>Person: ".$contact->first_name. "<br>Email: ".$contact->email."<br>Phone: ".$contact->phone."<br>Event: The 2019 JLT Beer Festival";

        mail('munch@jltdining.com', 'Registration Request Received', $message, implode("\r\n", $headers));



















        $message = "Hello from JLT Dining!<br><br>Thanks for registering your interest in the 2019 JLT Beer Festival!<br>We're currently putting the final touches on the event and will soon release full details.<br>As an \"early bird\" you'll receive a special rate for the festival ticket and we'll email you as soon as they are on sale.<br>In the meantime, check out our other upcoming events at https://www.jltdining.com/events.<br>Foodielicious greetings,<br>The JLT Dining Team";

        mail($contact->email, 'Registration Request Received', $message, implode("\r\n", $headers));




     /*   Mail::send('emails.contact.admin', ['contact' => $contact], function ($m) use ($contact)  {
            $m->from('info@jltdining.com', 'JLT Dining');
            $m->to('munch@jltdining.com', 'Admin')->subject('New registration was received ');
            $m->bcc('hello@spsaffinity.com','Team');
        });*/
        flash('Thank you we have received your request!', 'success');
        return redirect()->to('the-jlt-beer-festival-2019');

    }




}
