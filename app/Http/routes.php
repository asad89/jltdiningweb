<?php
//URL::forceSchema('https');
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
Route::get('/jlt-dining-current-partners','HomeController@partners');
Route::get('/send-email','EmailController@index');
Route::get('/blog', 'BlogController@index');
Route::get('/events', 'EventController@index');
Route::get('/past-events', 'EventController@pastEvent');
Route::get('/events/{event}', 'EventController@show');
Route::get('/blog/{slug}', 'PagesController@index');
Route::resource('restaurants', 'HotelController');
Route::get('/newsletter', 'NewsletterController@index');
Route::resource('contact', 'ContactController');
Route::post('event-register','EventController@eventRegister');
//Route for special page
Route::get('festive-jlt-2019','PagesController@festiveJlt');
Route::get('restaurant/menu/{menu}','HotelController@readMenu');
Route::auth();
//Route::post('search/', 'SearchController@search');
Route::any('search/', 'SearchController@search');
Route::get('search/autocomplete', 'SearchController@autocomplete');
Route::get('feeling-hungry-and-lucky', 'SearchController@rendom_search');
Route::get('restaurants/feature/{feature}','HotelController@featurehotels');
//Route::get('/restaurant-awards-2017-nominations','AwardController@index');
//Route::get('/restaurant-awards-2018-nominations','AwardController@index');
Route::get('/restaurant-awards-2019-nominations','AwardController@index');
Route::get('/restaurant-awards-2019-voting','VotingController@index');
Route::get('/jlt-hidden-gem-2019','AwardController@hiddenGem');




Route::post('/vote/restaurant/category','VotingController@getCategoryRestaurant');
Route::post('/cast-your-vote','VotingController@store');
Route::get('/restaurateurs-login','RestaurateursLoginController@index');
Route::get('/restaurateurs-area','RestaurateursLoginController@restaurateurs_area');
Route::get('/restaurateurs-register','RestaurateursRegisterController@index');
Route::post('/restaurateurs-register','RestaurateursRegisterController@store');
/*Nominate restaurant for JLT Hidden GEM*/
Route::post('/nominate-your-restaurant','AwardController@jlt_hidden_get_nominate');
Route::post('/register-for-award','AwardController@store');
Route::get('/view_menu/{menu}','HotelController@view_menu');
/*Route::get('/jlt-hidden-gem-2018', function () {
    return redirect()->to('/restaurant-awards-2018');
});*/


Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => ['admin']], function () {
    Route::resource('/', 'AdminController');
    Route::post('/hotels/premium-images', 'HotelsController@premiumImages');
    // Route::get('/hotels/delete-image/{id}','HotelImagesController@destroy_image');
    Route::resource('/posts', 'PostController');
    Route::resource('/categories', 'CategoriesController');
    Route::resource('/media', 'MediaController');
    Route::resource('/users', 'UserController');
    Route::resource('/contact', 'ContactController');
    Route::resource('/settings', 'SettingsController');
    Route::resource('/cuisines', 'CuisinesController');
    Route::resource('/dishes', 'DishesController');
    Route::resource('/features', 'FeaturesController');
    Route::resource('/meals', 'MealsController');
    Route::resource('/hotels', 'HotelsController');
    Route::post('/delete-hotel-image/', 'HotelImagesController@delete');
    Route::resource('/hotel-images', 'HotelImagesController');
    Route::resource('/tags', 'TagsController');
    Route::resource('cuisine-dishes', 'CuisineDishesController');
    Route::resource('award-category', 'AwardCategoryController');
    Route::get('award-category-restaurant/{category_id}', 'AwardCategoryController@categoryRestaurant');
    Route::post('/show-cuisine-dishes', 'CuisineDishesController@get_cuisine_dishes');
    Route::get('/delete-cuisine-dish/{id}', 'CuisineDishesController@destroy');
    Route::resource('/pages', 'PagesController');

    Route::get('event/past','EventsController@pastEvent');
    Route::resource('/events', 'EventsController');
    /*To load a cousine dishes with ajax*/
    Route::get('/show-cuisine-dishes', 'CuisineDishesController@get_cuisine_dishes');
    Route::get('/register-restaurant','AwardController@index');
    Route::get('/export-register-restaurant','AwardController@getReport');
    Route::get('/export-category-restaurant/{category_id}','AwardCategoryController@getReport');
    Route::DELETE('/register-restaurant/{id}','AwardController@destroy')->name('admin.award-user.destroy');
    Route::get('/voting-pool','VotingController@index');
    Route::get('/voting-pool/{id}','VotingController@show');
    Route::get('/votes-by-category', 'VotingController@voteByCategory');
    Route::get('/votes-by-category/top-restaurants/{category_id}', 'VotingController@restaurantTopVotes');
    Route::get('/top-restaurants-report/{category_id}', 'VotingController@topRestaurantReport');
    Route::get('/subscriber-newsletter-report', 'VotingController@subscriberReport');
    Route::resource('/offers', 'OfferController');
    Route::resource('/restaurateurs', 'RestaurateursController');
    Route::get('/jlt-hidden-gem-nominations','AwardController@pub_nomination_category');
    Route::get('/get-hidden-gem-report','AwardController@hidden_gem_report');
    Route::get('/event-participants','EventsController@getEventUsers');
});



Route::get('/{slug}', 'PagesController@index');
/*
*/
//Route::resource('blog','BlogController');