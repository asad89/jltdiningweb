<?php

namespace App;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use App\HotelFeatures;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Features;
use App\Tags;
use App\Meals;
use App\Cuisines;
use App\HotelImages;

class Hotel extends Model
{
    //
    use HasSlug;
    /*  use SoftDeletes;
      protected $dates = ['deleted_at'];*/
    protected $fillable = [
        'name', 'slug', 'logo', 'image', 'user_id', 'ad_type', 'phone', 'email','cluster',
        'owner', 'website', 'address', 'pincode', 'landmark', 'details', 'menu','booking_url',
        'working_days', 'opening_time', 'closing_time', 'video', 'latitude', 'longitude',
        'facebook', 'twitter', 'linkedin', 'instagram', 'google', 'status','meta_title','meta_keywords','meta_description','meta_robots'
    ];

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }


function get_hotel_images()
{
    return $this->hasMany('App\HotelImages', 'hotel_id');

}

function get_hotel_banner_images()
{
    return $this->hasMany('App\HotelImages', 'hotel_id')->take(3);

}

function get_hotel_features()
{
    return $this->belongsToMany('App\Features', 'hotel_features', 'hotel_id', 'feature_id');
}

function get_hotel_tags()
{
    return $this->belongsToMany('App\Tags', 'hotel_tags', 'hotel_id', 'tag_id');
}

function get_hotel_meals()
{
    return $this->belongsToMany('App\Meals', 'hotel_meals', 'hotel_id', 'meal_id');
}

function get_hotel_cuisines()
{
    return $this->belongsToMany('App\Cuisines', 'hotel_cuisines', 'hotel_id', 'cuisine_id');
}


}
