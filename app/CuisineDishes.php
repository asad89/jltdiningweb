<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CuisineDishes extends Model
{
    //
    protected $table = 'cuisines_dishes';
    protected $fillable = ['cuisine_id', 'dish_id'];
}
