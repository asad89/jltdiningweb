<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    //
    protected $table = 'events';
    protected $fillable = ['name','title','slug','description',
            'feature_image','ending_date','status',
            'meta_title','meta_keywords','meta_description','meta_robots'
    ];
}
