<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelImages extends Model
{
    //
    protected $table = 'hotel_images';
}
