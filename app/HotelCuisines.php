<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelCuisines extends Model
{
    //
    protected $table = 'hotel_cuisines';
}
