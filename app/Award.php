<?php

namespace App;
use App\RestaurantAawardCategory;
use Illuminate\Database\Eloquent\Model;

class Award extends Model
{
    //
    protected $table = 'awardrestaurant';
    protected $fillable = array('name', 'website', 'facebook', 'twitter', 'instagram','first_name','last_name','email','phone');

    public function getRestaurantCategroy() {
        return $this->hasMany('App\RestaurantAawardCategory', 'restaurant_id');
    }

    /*
     * @pararm restaurant id
     * retuen list if vote casted
     * */
    public function getRestaurantVotes() {
        return $this->hasMany('App\Votecast','restaurant_id');
    }



}
