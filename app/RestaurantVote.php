<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class RestaurantVote extends Model
{
    //
    protected $table = 'restaurant_votes';
    protected $fillable = ['name','email','phone','ip_address','verify_code','newsletter','device'];

    public function getRestaurant() {
       return $this->belongsTo('App\AwardRestaurant','hotel_id');
    }

    public function getCategory() {
        return $this->belongsTo('App\AwardCategory','category_id');
    }

    public function voteInfo() {
        return $this->hasMany('App\Votecast','user_id');
    }


}
