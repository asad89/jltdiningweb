<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Votecast extends Model
{
    //
    protected $table = 'vote_casted';
    protected $fillable = ['category_id','restaurant_id','user_id'];

    public function getCategory() {
        return $this->belongsTo('App\AwardCategory','category_id');
    }

    public function getRestaurant() {
        return $this->belongsTo('App\AwardRestaurant','restaurant_id');
    }

}
