<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HiddenGem extends Model
{
    //
    protected $table = 'pub_nomination_category';
    protected $fillable = ['first_name','last_name','email','phone','restaurant'];
}
