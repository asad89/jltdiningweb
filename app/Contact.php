<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    //
    protected $table = 'contact';
    protected $fillable = array('first_name', 'last_name', 'phone', 'email', 'help', 'message');
}
