<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    //
    protected $table = 'settings';
    protected $fillable = ['website', 'logo', 'info_email', 'admin_email', 'contact',
        'address', 'latitude', 'longitude', 'copyrights', 'designed',
        'google_analytics', 'facebook_code', 'facebook', '	twitter', 'instagram',
        'google', 'linkedin', 'meta_title', 'meta_keywords', 'meta_description',
        'robots'
    ];
}
