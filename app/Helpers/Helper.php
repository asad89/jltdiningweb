<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;
use Illuminate\Support\Facades\Input;
use Intervention\Image\ImageManagerStatic as Image;

class Helper
{
    public static function upload_image($file = '', $upload_path = 'uploads', $resize = null, $img_size = null, $logo = null)
    {


        $watermark = Image::make(public_path() . '/images/logo.png')->opacity(30);
        $filename = strtolower(str_replace(" ", "-", time() . '-' . $file->getClientOriginalName()));
        $path_image = public_path($upload_path . '/' . $filename);
        if (!$logo == 1) {
            Image::make($file->getRealPath())->insert($watermark, 'bottom-right', 10, 10)
                ->save($path_image);
        } else {
            Image::make($file->getRealPath())->save($path_image);

        }

        /*Image::make($file->getRealPath())->insert($watermark, 'bottom-right', 10, 10)
            ->save($path_image);*/
        if (!$resize == null) {
            if (count($img_size) && is_array($img_size))
                foreach ($img_size as $key => $value) {
                    $new_size = explode("x", $value);
                    $path = public_path($upload_path . '/' . $key);
                    if (!file_exists($path))
                        mkdir($path, $mode = 0777, true);
                    $path_image = $upload_path . '/' . $key . "/" . $filename;

                    if ($logo == null) {

                        Image::make($file->getRealPath())->resize($new_size[0], $new_size[1], function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        })->insert($watermark, 'bottom-right', 20, 60)->save($path_image);

                    } else {
                        Image::make($file->getRealPath())->resize($new_size[0], $new_size[1], function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        })->save($path_image);
                    }

                }
        }
        return $filename;
    }

    public static function addhttp($url) {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
            $url = "http://" . $url;
        }

        return $url;
    }



    public static function youtube($url)
    {
        if (strlen($url) > 11) {
            if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match)) {
                return $match[1];
            } else
                return false;
        }

        return $url;
    }
}