<?php

namespace App;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;
use App\Cuisines;

class Dishes extends Model
{
    //
    use HasSlug;
    protected $fillable = ['name', 'status', 'slug'];

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

public
function cuisines()
{
    return $this->$this->belongsToMany('App\Cuisines', 'cuisines_dishes', 'dish_id', 'cuisine_id');


}


}
