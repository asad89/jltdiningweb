<?php

namespace App;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;
use App\Dishes;
use App\Hotel;
use App\HotelCuisines;

class Cuisines extends Model
{
    //
    use HasSlug;
    protected $fillable = ['name', 'status', 'slug'];

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

public
function dishes()
{
    return $this->belongsToMany('App\Dishes', 'cuisines_dishes', 'cuisine_id', 'dish_id');
}

function get_cuisine_hotel()
{
    return $this->belongsToMany('App\Hotel', 'hotel_cuisine', 'cuisine_id', 'hotel_id');
}


}
