@extends('master')
@section('meta_tags')
    <title>@if(isset($page->meta_title) && !empty($page->meta_title)){{$page->meta_title}} @else{{config('app.meta_title')}}@endif</title>
    <meta name="keywords"
          content="@if(isset($page->meta_keywords) && !empty($page->meta_keywords)){{$page->meta_keywords}} @else{{config('app.meta_keywords')}}@endif">
    <meta name="description"
          content="@if(isset($page->meta_description) && !empty($page->meta_description)){{$page->meta_description}} @else{{config('app.meta_description')}}@endif">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css">

    {{--<link rel="stylesheet" href="{{asset('/css/countdown.css')}}">
    <script type="text/javascript" src="{{asset('/js/countdown.js')}}"></script>--}}

    @endsection
    @section('content')
            <!--==================================Section Open=================================-->
    <section class="aliceblue brown-bg">
        <div class="container breadcrumb-wrap">
            {{--    <ol class="breadcrumb">
                    <li><a href="{{url('/')}}">Home</a></li>
                    <li><a href="{{url($page->slug)}}">{{$page->name}}</a></li>
                </ol>--}}
        </div>

        <div class="container page-container-second">
            <div class="row">
                <div class="col-md-9 padding-40 blog-single-inner-container lp-border lp-border-radius-8">
                    <div class="popup-gallery">
                        <h1 class="page-heading-custom"
                            style="text-align: center;margin-bottom:20px;font-size:24px;font-family: Oxygen, sans-serif!important;">{{"Vote for your favourite JLT restaurant, bar, or café in the 2019 JLT Dining Restaurant Awards! "}}</h1>
                            <br>
                            {{--<div id="counter" style="text-align: center;margin-bottom:20px;font-size:24px;font-family: Oxygen, sans-serif!important;"></div>
--}}
                {{--        <h1 class="page-heading-custom"
                            style="text-align: center;margin-bottom:20px;font-size:24px;font-family: Oxygen, sans-serif!important;">{{"Vote for your favourite JLT restaurant, bar, or cafe in the 2018 JLT Dining Restaurant Awards!"}}</h1>--}}

                        <p style="text-align: center;">Voting for the 2019 JLT Dining Awards is now closed! We'll reveal the shortlisted restaurants shortly. Be the first to find out, subscribe to our <a href="https://jltdining.com/newsletter" target="_blank">newsletter!</a></p>


                       {{-- <p style="text-align:center;">We'll announce the shortlisted restaurants across our social media platforms and on our blog from Sunday, 5th November onward.
                            The awards ceremony will take place on Monday, 27th November 2017, and the winners and runner ups will be revealed on the night.
                            Interested in attending or sponsoring the ceremony? <a href="mailto:munch@jltdining.com?subject=JLT Restaurant Awards - Awards Ceremony Inquiry">Drop us a line</a> or keep an eye on our Facebook page!</p>--}}
                      {{--  {!! Form::open(array('url' => '/cast-your-vote','class'=>'form-horizontal','method' => 'POST')) !!}--}}

                   {{--     <div class="row">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if (session()->has('flash_notification.message'))
                                <div class="alert alert-{{ session('flash_notification.level') }}">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-hidden="true">&times;</button>
                                    {!! session('flash_notification.message') !!}
                                </div>
                            @endif
                        </div>--}}

                     {{--   <div class="form-body" style="font-family: Oxygen, sans-serif;font-size:14px;line-height:21px;color:#333333;">
                          --}}{{-- @include('partials.voting-form')--}}{{--
                                @include('partials.voting-form')
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        @include('partials.sponsers')
                                    </div>
                                </div>
                            </div>
                        </div>--}}
                        {{--{!! Form::close() !!}--}}
                    </div>
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12 col-lg-3">
                    <div class="sidebar-post">
                        <div class="widget-box  widget-bg-color post-author-box lp-border-radius-5">
                            <div class="widget-content">
                                <a href="https://business.facebook.com/events/1733357806972872" target="_blank">
                                    <img src="{{asset('images/voting-banner.png')}}" alt="Award "{{date('Y')}}>
                                </a>
                            </div>
                        </div>
                        <div class="widget-box  widget-bg-color post-author-box lp-border-radius-5">
                            <div class="widget-content text-center">
                                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                <!-- Voting Page Advertisement -->
                                <ins class="adsbygoogle"
                                     style="display:block"
                                     data-ad-client="ca-pub-8842445166702832"
                                     data-ad-slot="8792317640"
                                     data-ad-format="auto"
                                     data-full-width-responsive="true"></ins>
                                <script>
                                    (adsbygoogle = window.adsbygoogle || []).push({});
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--==================================Section Close=================================-->
    <style>
        .modal-backdrop.in {
            opacity: 0;
        }
    </style>
    <div class="modal" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">JLT Dining Restaurant Awards Terms & Conditions</h4>
                </div>

                <div class="modal-body">
                    <ol>
                        <li>By making a nomination to the 2017 JLT Dining Restaurant Awards (“the awards”), entrants
                            hereby agree to abide by the following terms and conditions.
                        </li>
                        <li>Entry requirements
                            <ul>
                                <li>Only outlets located in Jumeirah Lakes Towers are eligible to enter the awards</li>
                                <li>You can enter an outlet into multiple categories. The organisers will make the final
                                    decision which categories an outlet is included in.
                                </li>
                                <li>Nominations must be made using the online nomination form; other formats will not be
                                    considered.
                                </li>
                            </ul>
                        </li>
                        <li>We will accept no responsibility for submissions that are submitted incorrectly, lost,
                            mislaid or delayed, regardless of cause. Any submissions made after the submission deadline
                            will not be accepted.
                        </li>
                        <li>By nominating your outlet for the awards, you agree to be added to the JLT Dining mailing
                            list to receive occasional updates.
                        </li>
                        <li>The organisers may, at any time and without notice, amend or alter categories and/or
                            deadlines, or cancel the awards.
                        </li>
                        <li>The organisers reserve the right, at their sole discretion, to disqualify any entrant who
                            breaches any of these terms and conditions.
                        </li>
                        <li>In order to feature on the online voting form, you need to submit the nomination form before
                            30th September 2017.
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('site_footer')


<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
{{--<script src="/dist/scripts.min.js"></script>--}}
@endpush