@extends('master')
@section('meta_tags')
<title>Feeling Hungry and Lucky - Random Restaurant in JLT | JLT Dining</title>
<meta name="keywords" content="random restaurants dubai, feeling hungry dubai, jlt restaurants, feeling hungry and lucky jlt, restaurants in jlt">
<meta name="description" content="Feeling Hungry and Lucky? Pick out a random Restaurant in Jumeirah Lakes Towers, Dubai listed by JLT Dining here.">
@endsection
@section('content')
<!--==================================Section Open=================================-->
<section>
    <div class="container page-container">
        <div class="row">
            <div class="col-md-12">
                <div class="LPtagsContainer "></div>
            </div>
        </div>
        <div class="row listing-page-result-row margin-bottom-25">
            <div class="col-md-4 col-sm-4 text-left">
                <p>Viewing <?php echo count($result) ?> Restaurants!</p>
            </div>
        </div>
        <div class="row lp-list-page-grid">
             @foreach($result as $hotel)
                <div class="col-md-3 col-sm-6 lp-grid-box-contianer lp-grid-box-contianer1">
                    <div class="lp-grid-box lp-border lp-border-radius-8">
                        <div class="lp-grid-box-thumb-container">
                            <div class="lp-grid-box-thumb">
                                @if(!empty($hotel->image))
                                    @laravelImage('uploads/hotels/large/', $hotel->image, 272, 231, [
                                            'fit' => 'crop-top-center'
                                             ], [
                                             'alt' => $hotel->name,
                                              'class' => 'custom-class'
                                            ])
                                @else
                                    @laravelImage('uploads/restaurant/', 'coming-soon.png', 272, 231, [
                                           'fit' => 'crop-top-center'
                                            ], [
                                            'alt' => $hotel->name,
                                             'class' => 'custom-class'
                                           ])
                                @endif
                            </div><!-- ../grid-box-thumb -->
                            <div class="lp-grid-box-quick">
                                <ul class="lp-post-quick-links">
                                    <li><a class="icon-quick-eye md-trigger" data-modal="modal-2"><i class="fa fa-eye"></i></a></li>
                                </ul>
                            </div><!-- ../grid-box-quick-->
                        </div>
                        <div class="lp-grid-box-description ">
                            <h4 class="lp-h4"><a href="{{url('/restaurants/'.$hotel->slug)}}">{{$hotel->name}}</a></h4>
                            <p><i class="fa fa-map-marker"></i><span>{{$hotel->address}}</span></p>
                        </div><!-- ../grid-box-description-->
                    </div><!-- ../grid-box -->
                </div>
            @endforeach
        </div>
    </div>
</section>
<!--==================================Section Close=================================-->
@endsection