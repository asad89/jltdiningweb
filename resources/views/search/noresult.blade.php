@extends('master')
@section('meta_tags')
    <title>{{config('app.meta_title')}}</title>
    <meta name="keywords" content="{{config('app.meta_keywords')}}">
    <meta name="description" content="{{config('app.meta_description')}}">
    @endsection
@section('content')
        <!--==================================Section Open=================================-->
<section class="aliceblue">
    <div class="container page-container-second">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 padding-40 blog-single-inner-container lp-border lp-border-radius-8">

                <div class="blog-content popup-gallery">
                    <h1>We are sorry no results found!</h1>
                </div>

            </div>
        </div>
    </div>
</section>
<!--==================================Section Close=================================-->
@endsection