@extends('master')
@section('meta_tags')
    <title>{{config('app.meta_title')}}</title>
    <meta name="keywords" content="{{config('app.meta_keywords')}}">
    <meta name="description" content="{{config('app.meta_description')}}">
    @endsection
@section('content')
        <!--==================================Section Open=================================-->
<section>
    <div class="container page-container">

        <div class="row">
            <div class="col-md-12">
                <div class="LPtagsContainer "></div>
            </div>
        </div>
        <div class="row listing-page-result-row margin-bottom-25">
            <div class="col-md-4 col-sm-4 text-left">
                <p>Viewing <?php echo count($result) ?> Restaurants!</p>
            </div>
        </div>

        <div class="row lp-list-page-grid">
            <?php
            //Columns must be a factor of 12 (1,2,3,4,6,12)
            $numOfCols = 4;
            $rowCount = 0;
            $bootstrapColWidth = 12 / $numOfCols;
            ?>
            <div class="row">
            @foreach($result as $hotel)
               <div class="col-md-3 col-sm-6 lp-grid-box-contianer lp-grid-box-contianer1"
                     data-title="{{$hotel->name}}" data-reviews="4" data-number="{{$hotel->phone}}"
                     data-email="{{$hotel->email}}" data-website="{{$hotel->website}}"
                     data-description="{{$hotel->details}}" data-userimage="images/user-thumb-94x94.png"
                     data-username="" data-userlisting="14" data-fb="{{$hotel->facebook}}"
                     data-gplus="www.plus.google.com" data-linkedin="www.linkedin.com"
                     data-instagram="www.instagram.com" data-twitter="www.twitter.com" data-lattitue="40.6700"
                     data-longitute="-73.9400" data-id="6" data-posturl="{{url('hotels/'.$hotel->slug)}}"
                     data-authorurl="author.html">
                    <div class="lp-grid-box lp-border lp-border-radius-8">
                        <div class="lp-grid-box-thumb-container">
                            <div class="lp-grid-box-thumb">
                                @if(!empty($hotel->image))
                                    @laravelImage('uploads/hotels/large/', $hotel->image, 272, 231, [
                                            'fit' => 'crop-top-center'
                                             ], [
                                             'alt' => $hotel->name,
                                              'class' => 'custom-class'
                                            ])
                                @else
                                    @laravelImage('restaurant/', 'coming-soon.png', 272, 231, [
                                           'fit' => 'crop-top-center'
                                            ], [
                                            'alt' => $hotel->name,
                                             'class' => 'custom-class'
                                           ])
                                @endif
                            </div><!-- ../grid-box-thumb -->
                            <div class="lp-grid-box-quick">
                                <ul class="lp-post-quick-links">
                                    <li>
                                        <a class="icon-quick-eye md-trigger" data-modal="modal-2"><i
                                                    class="fa fa-eye"></i></a>
                                    </li>
                                </ul>
                            </div><!-- ../grid-box-quick-->
                        </div>
                        <div class="lp-grid-box-description ">
                            <h4 class="lp-h4">
                                <a href="{{url('/restaurants/'.$hotel->slug)}}">
                                    {{$hotel->name}}
                                </a>
                            </h4>

                            <p>
                                <i class="fa fa-map-marker"></i>
                                <span>{{$hotel->address}}</span>
                            </p>

                        </div><!-- ../grid-box-description-->

                    </div><!-- ../grid-box -->


                </div>

                    <?php
                    $rowCount++;
                    if($rowCount % $numOfCols == 0) echo '</div><div class="row">';
                    ?>
                @endforeach
            </div>
        </div>

        <nav aria-label="Page navigation">
            {{ $result->links() }}
        </nav>
    </div>
</section>
<!--==================================Section Close=================================-->
@endsection