
            <!--==================================Section Open=================================-->
    <section class="clearfix">
        <div class="col-md-12">
            <h3 class="margin-top-10 margin-bottom-10 lp-border-bottom padding-bottom-20">Register Today!</h3>
            {!! Form::open(array('url' => '/event-register','class'=>'form-horizontal')) !!}

            <div class="row">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (session()->has('flash_notification.message'))
                    <div class="alert alert-{{ session('flash_notification.level') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {!! session('flash_notification.message') !!}
                    </div>
                @endif
            </div>

            <div class="form-body">
                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-6">
                            {!! Form::text('first_name',null,array('class'=>'form-control nameform','placeholder'=>'First Name:','required' => '')) !!}
                        </div>
                        <div class="col-sm-6">
                            {!! Form::text('last_name',null,array('class'=>'form-control nameform','placeholder'=>'Last Name:','required' => '')) !!}
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-6">
                            {!! Form::text('email',null,array('class'=>'form-control nameform','type'=> 'email','placeholder'=>'Email:','required' => '')) !!}
                        </div>
                         <div class="col-sm-6 col-md-6">
                             {!! Form::text('phone',null,array('class'=>'form-control nameform','placeholder'=>'Phone:','required' => '')) !!}
                         </div>
                    </div>


                </div>






                <div class="row">
                    <div class="form-group">
                        <div class="col-md-6">
                            <label class="text-center"> <?php echo captcha_img();?></label>
                            {!! Form::text('captcha',null,array('class' => 'form-control')) !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="submit" id="submit" name="submit" value="Submit"
                                   class="lp-review-btn btn-second-hover">
                        </div>
                    </div>
                </div>


            </div>
            <div id="success">
            <span class="green textcenter"><p>Your message was sent successfully! I will be in touch as soon as I
                    can.</p></span>
            </div>
            <div id="error">
                <span><p>Something went wrong, try refreshing and submitting the form again.</p></span>
            </div>
            {!! Form::close() !!}

        </div>
    </section>
    <!--==================================Section Close=================================-->