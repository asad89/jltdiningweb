@extends('master')
@section('content')
        <!--==================================Section Open=================================-->
<section class="aliceblue brown-bg">
    <div class="container breadcrumb-wrap">
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}">{{"Home"}}</a></li>
            <li><a href="{{url('/newsletter')}}">{{"Newsletter"}}</a></li>
        </ol>
    </div>

    <div class="container page-container-second">
        <div class="row">
            <div class="col-md-9 padding-40 blog-single-inner-container lp-border lp-border-radius-8">
                <div class="blog-content popup-gallery">
                    <!-- Begin MailChimp Signup Form -->
                    <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
                    <style type="text/css">
                        #mc_embed_signup {
                            background: #fff;
                            clear: left;
                            font: 14px Helvetica, Arial, sans-serif;
                        }

                        /* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
                           We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
                    </style>
                    <div id="mc_embed_signup">
                        <form action="//jltdining.us13.list-manage.com/subscribe/post?u=42d057905e11e68c47265978e&amp;id=f6ef5d0d49"
                              method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form"
                              class="validate" target="_blank" novalidate>
                            <div id="mc_embed_signup_scroll">
                                <h2>Subscribe to our mailing list</h2>

                                <div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
                                <div class="form-group">
                                    <label for="mce-EMAIL">Email Address <span class="asterisk">*</span>
                                    </label>
                                    <input type="email" value="" name="EMAIL" class="required email form-control"
                                           id="mce-EMAIL">
                                </div>
                                <div class="form-group">
                                    <label for="mce-FNAME">First Name </label>
                                    <input type="text" value="" name="FNAME" class="form-control" id="mce-FNAME">
                                </div>
                                <div class="form-group">
                                    <label for="mce-LNAME">Last Name </label>
                                    <input type="text" value="" name="LNAME" class="form-control" id="mce-LNAME">
                                </div>
                                <div class="form-group">
                                    <strong>What's your favourite meal of the day? </strong>

                                    <div class="checkbox">
                                        <input type="checkbox" value="1" name="group[10961][1]"
                                               id="mce-group[10961]-10961-0">
                                        <label for="mce-group[10961]-10961-0">Breakfast</label>
                                    </div>
                                    <div class="checkbox">
                                        <input type="checkbox" value="2" name="group[10961][2]"
                                               id="mce-group[10961]-10961-1">
                                        <label for="mce-group[10961]-10961-1">Lunch</label>
                                    </div>

                                    <div class="checkbox">
                                        <input type="checkbox" value="4" name="group[10961][4]"
                                               id="mce-group[10961]-10961-2">
                                        <label for="mce-group[10961]-10961-2">Dinner</label></div>
                                    <div class="checkbox">
                                        <input type="checkbox" value="8" name="group[10961][8]"
                                               id="mce-group[10961]-10961-3">
                                        <label for="mce-group[10961]-10961-3">Anything, so long as there's great
                                            food!</label>
                                    </div>

                                </div>
                                <div class="mc-field-group input-group form-control">
                                    <strong>Email Format </strong>

                                    <div class="radio">

                                        <label for="mce-EMAILTYPE-0">
                                            <input type="radio" value="html" name="EMAILTYPE" id="mce-EMAILTYPE-0">
                                            html</label>
                                    </div>
                                    <div class="radio">

                                        <label for="mce-EMAILTYPE-1">
                                            <input type="radio" value="text" name="EMAILTYPE" id="mce-EMAILTYPE-1">
                                            text</label>
                                    </div>
                                </div>
                               <div class="clearfix"></div>
                                <div class="row" style="margin-top: 25px;">
                                   <div class="col-md-12">
                                    <div id="mce-responses" class="clear">
                                        <div class="response" id="mce-error-response" style="display:none"></div>
                                        <div class="response" id="mce-success-response" style="display:none"></div>
                                    </div>
                                    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text"
                                                                                                              name="b_42d057905e11e68c47265978e_f6ef5d0d49"
                                                                                                              tabindex="-1"
                                                                                                              value="">
                                    </div>
                                    <div class="clear"><input type="submit" value="Subscribe" name="subscribe"
                                                              id="mc-embedded-subscribe" class="button btn btn-primary"></div>
                                   </div>

                                </div>

                            </div>
                        </form>
                    </div>

                    <!--End mc_embed_signup-->

                </div>

            </div>
            <div class="col-md-3">
                <div class="sidebar-post">
                    <div class="widget-box  widget-bg-color post-author-box lp-border-radius-5">
                        <div class="widget-content">
                            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                            <!-- Small text banner -->
                            <ins class="adsbygoogle"
                                 style="display:block"
                                 data-ad-client="ca-pub-8842445166702832"
                                 data-ad-slot="7219573107"
                                 data-ad-format="auto"
                                 data-full-width-responsive="true"></ins>
                            <script>
                                (adsbygoogle = window.adsbygoogle || []).push({});
                            </script>
                        </div>
                    </div>
                    <div class="widget-box  widget-bg-color post-author-box lp-border-radius-5">
                        <div class="widget-content text-center">
                            <a href="https://www.jltdining.com/restaurant-awards-2017" target="_blank">
                                <img src="{{asset('images/voting-banner.png')}}" alt="Award 2019" class="img-responsive text-center">
                            </a>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</section>
<!--==================================Section Close=================================-->
@endsection