@extends('admin.master')
@push('site_header')
<link href="{{asset('private/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}"
      rel="stylesheet" type="text/css"/>
<link href="{{asset('private/assets/global/plugins/morris/morris.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('private/assets/global/plugins/fullcalendar/fullcalendar.min.css')}}" rel="stylesheet"
      type="text/css"/>
<link href="{{asset('private/assets/global/plugins/jqvmap/jqvmap/jqvmap.css')}}" rel="stylesheet" type="text/css"/>
@endpush
@section('content')
<!-- BEGIN PAGE BAR -->
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{url('/admin')}}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Dashboard</span>
        </li>
    </ul>

</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"> Admin Dashboard
    <small>statistics, charts, recent events and reports</small>
</h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<!-- BEGIN DASHBOARD STATS 1-->
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
            <div class="visual">
                <i class="fa fa-cutlery"></i>
            </div>
            <div class="details">
                <div class="number">
                    @if(isset($restaurants))
                        <span data-counter="counterup" data-value="{{ $restaurants }}">0</span>
                    @endif
                </div>
                <div class="desc">  {{'Restaurants in total'}}</div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
            <div class="visual">
                <i class="fa fa-cutlery"></i>
            </div>
            <div class="details">
                <div class="number">
                    @if(isset($premium))
                        <span data-counter="counterup" data-value="{{ $premium }}">0</span>
                    @endif
                </div>
                <div class="desc">  {{'Premium Listing Restaurants'}}</div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
            <div class="visual">
                <i class="fa fa-cutlery"></i>
            </div>
            <div class="details">
                <div class="number">
                    @if(isset($premium))
                        <span data-counter="counterup" data-value="{{ $restaurants - $premium }}">0</span>
                    @endif
                </div>
                <div class="desc">  {{'Basic Listing Restaurants'}}</div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
            <div class="visual">
                <i class="fa fa-page"></i>
            </div>
            <div class="details">
                <div class="number">
                    @if(isset($pages))
                        <span data-counter="counterup" data-value="{{ $pages }}">0</span>
                    @endif
                </div>
                <div class="desc">  {{'Pages'}}</div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
            <div class="visual">
                <i class="fa fa-page"></i>
            </div>
            <div class="details">
                <div class="number">
                    @if(isset($posts))
                        <span data-counter="counterup" data-value="{{ $posts }}">0</span>
                    @endif
                </div>
                <div class="desc">  {{'Posts'}}</div>
            </div>
        </a>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
            <div class="visual">
                <i class="fa fa-page"></i>
            </div>
            <div class="details">
                <div class="number">
                    @if(isset($awardCategories))
                        <span data-counter="counterup" data-value="{{ $awardCategories }}">0</span>
                    @endif
                </div>
                <div class="desc">  {{'Award 2020 Categories'}}</div>
            </div>
        </a>
    </div>



    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
            <div class="visual">
                <i class="fa fa-page"></i>
            </div>
            <div class="details">
                <div class="number">
                    @if(isset($awardNominations))
                        <span data-counter="counterup" data-value="{{ $awardNominations }}">0</span>
                    @endif
                </div>
                <div class="desc">  {{'Award 2020 Nominations'}}</div>
            </div>
        </a>
    </div>



    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
            <div class="visual">
                <i class="fa fa-page"></i>
            </div>
            <div class="details">
                <div class="number">
                    @if(isset($votes_received))
                        <span data-counter="counterup" data-value="{{ $votes_received }}">0</span>
                    @endif
                </div>
                <div class="desc">  {{'Votes Received So Far!'}}</div>
            </div>
        </a>
    </div>





    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
            <div class="visual">
                <i class="fa fa-page"></i>
            </div>
            <div class="details">
                <div class="number">
                    @if(isset($events))
                        <span data-counter="counterup" data-value="{{ $events }}">0</span>
                    @endif
                </div>
                <div class="desc">  {{'Current Events!'}}</div>
            </div>
        </a>
    </div>



</div>
<div class="clearfix"></div>
<!-- END DASHBOARD STATS 1-->
{{--<div class="row">
    <div class="col-lg-6 col-xs-12 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bar-chart font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase">Site Visits</span>
                    <span class="caption-helper">weekly stats...</span>
                </div>
                <div class="actions">
                    <div class="btn-group btn-group-devided" data-toggle="buttons">
                        <label class="btn red btn-outline btn-circle btn-sm active">
                            <input type="radio" name="options" class="toggle" id="option1">New</label>
                        <label class="btn red btn-outline btn-circle btn-sm">
                            <input type="radio" name="options" class="toggle" id="option2">Returning</label>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div id="site_statistics_loading">
                    <img src="../assets/global/img/loading.gif" alt="loading" /> </div>
                <div id="site_statistics_content" class="display-none">
                    <div id="site_statistics" class="chart"> </div>
                </div>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
    <div class="col-lg-6 col-xs-12 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-share font-red-sunglo hide"></i>
                    <span class="caption-subject font-dark bold uppercase">Revenue</span>
                    <span class="caption-helper">monthly stats...</span>
                </div>
                <div class="actions">
                    <div class="btn-group">
                        <a href="" class="btn dark btn-outline btn-circle btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Filter Range
                            <span class="fa fa-angle-down"> </span>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="javascript:;"> Q1 2014
                                    <span class="label label-sm label-default"> past </span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;"> Q2 2014
                                    <span class="label label-sm label-default"> past </span>
                                </a>
                            </li>
                            <li class="active">
                                <a href="javascript:;"> Q3 2014
                                    <span class="label label-sm label-success"> current </span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;"> Q4 2014
                                    <span class="label label-sm label-warning"> upcoming </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div id="site_activities_loading">
                    <img src="../assets/global/img/loading.gif" alt="loading" /> </div>
                <div id="site_activities_content" class="display-none">
                    <div id="site_activities" style="height: 228px;"> </div>
                </div>
                <div style="margin: 20px 0 10px 30px">
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                            <span class="label label-sm label-success"> Revenue: </span>
                            <h3>$13,234</h3>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                            <span class="label label-sm label-info"> Tax: </span>
                            <h3>$134,900</h3>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                            <span class="label label-sm label-danger"> Shipment: </span>
                            <h3>$1,134</h3>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                            <span class="label label-sm label-warning"> Orders: </span>
                            <h3>235090</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>--}}











<!-- END QUICK SIDEBAR -->
   {{-- <div class="row">
        <div class="col-md-12">


            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box blue-madison">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-globe"></i>{{'Dashboard'}}
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="reload">
                        </a>
                        <a href="javascript:;" class="remove">
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    @if (session()->has('flash_notification.message'))
                        <div class="alert alert-{{ session('flash_notification.level') }}">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {!! session('flash_notification.message') !!}
                        </div>
                    @endif
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="dashboard-stat blue-madison">
                                    <div class="visual">
                                        <i class="fa fa-comments"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            @if(isset($posts))
                                                {{ $posts }}
                                            @endif
                                        </div>
                                        <div class="desc">
                                            {{'Posts'}}
                                        </div>
                                    </div>
                                    <a class="more" href="{{url('admin/posts')}}">
                                        {{'View Details'}} <i class="m-icon-swapright m-icon-white"></i>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="dashboard-stat blue-madison">
                                    <div class="visual">
                                        <i class="fa fa-comments"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            @if(isset($pages))
                                                {{ $pages }}
                                            @endif
                                        </div>
                                        <div class="desc">
                                            {{'Pages'}}
                                        </div>
                                    </div>
                                    <a class="more" href="{{url('admin/pages')}}">
                                        {{'View Details'}} <i class="m-icon-swapright m-icon-white"></i>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="dashboard-stat blue-madison">
                                    <div class="visual">
                                        <i class="fa fa-comments"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            @if(isset($users))
                                                {{ $users }}
                                            @endif
                                        </div>
                                        <div class="desc">
                                            {{'Users'}}
                                        </div>
                                    </div>
                                    <a class="more" href="{{url('admin/users')}}">
                                        {{'View Details'}} <i class="m-icon-swapright m-icon-white"></i>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="dashboard-stat blue-madison">
                                    <div class="visual">
                                        <i class="fa fa-comments"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            @if(isset($restaurants))
                                                {{ $restaurants }}
                                            @endif
                                        </div>
                                        <div class="desc">
                                            {{'Restaurants'}}
                                        </div>
                                    </div>
                                    <a class="more" href="{{url('admin/hotels')}}">
                                        {{'View Details'}} <i class="m-icon-swapright m-icon-white"></i>
                                    </a>
                                </div>
                            </div>


                        </div>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>--}}
@endsection





@push('site_footer')
<script src="{{asset('private/assets/global/plugins/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}"
        type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/morris/morris.min.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/morris/raphael-min.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/counterup/jquery.waypoints.min.js')}}"
        type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/counterup/jquery.counterup.min.js')}}"
        type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/amcharts/amcharts/amcharts.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/amcharts/amcharts/serial.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/amcharts/amcharts/pie.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/amcharts/amcharts/radar.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/amcharts/amcharts/themes/light.js')}}"
        type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/amcharts/amcharts/themes/patterns.js')}}"
        type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/amcharts/amcharts/themes/chalk.js')}}"
        type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/amcharts/ammap/ammap.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/amcharts/ammap/maps/js/worldLow.js')}}"
        type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/amcharts/amstockcharts/amstock.js')}}"
        type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/fullcalendar/fullcalendar.min.js')}}"
        type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/horizontal-timeline/horizontal-timeline.js')}}"
        type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/flot/jquery.flot.min.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/flot/jquery.flot.resize.min.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/flot/jquery.flot.categories.min.js')}}"
        type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js')}}"
        type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/jquery.sparkline.min.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js')}}"
        type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js')}}"
        type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js')}}"
        type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js')}}"
        type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js')}}"
        type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js')}}"
        type="text/javascript"></script>


<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{asset('private/assets/pages/scripts/dashboard.min.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
@endpush