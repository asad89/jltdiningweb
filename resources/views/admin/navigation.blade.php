<div class="page-sidebar navbar-collapse collapse">
    <!-- BEGIN SIDEBAR MENU -->
    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true"
        data-slide-speed="200" style="padding-top: 20px">
        <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        <li class="sidebar-toggler-wrapper hide">
            <div class="sidebar-toggler">
                <span></span>
            </div>
        </li>
        <!-- END SIDEBAR TOGGLER BUTTON -->
        <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
        <li class="sidebar-search-wrapper">
            <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
            <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
            <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
            <form class="sidebar-search  " action="#" method="POST">
                <a href="javascript:;" class="remove">
                    <i class="icon-close"></i>
                </a>

                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search...">
                                        <span class="input-group-btn">
                                            <a href="javascript:;" class="btn submit">
                                                <i class="icon-magnifier"></i>
                                            </a>
                                        </span>
                </div>
            </form>
            <!-- END RESPONSIVE QUICK SEARCH FORM -->
        </li>
        <li class="nav-item start active open">
            <a href="{{url('/admin')}}" class="nav-link nav-toggle">
                <i class="icon-home"></i>
                <span class="title">Dashboard</span>
                <span class="selected"></span>
                <span class="arrow open"></span>
            </a>
        </li>
        <li class="nav-item">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="icon-docs"></i>
                <span class="title">{{'Posts'}}</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  ">
                    <a href="{{url('/admin/posts')}}" class="nav-link">
                        {{'All Posts'}}</a>
                </li>
                <li class="nav-item">
                    <a href="{{url('/admin/posts/create')}}" class="nav-link">
                        <i class="fa fa-plus"></i>
                        {{'Add New'}}</a>
                </li>

                <li class="nav-item">
                    <a href="{{url('/admin/categories')}}">
                        <i class="fa fa-filter" aria-hidden="true"></i>
                        {{'Categories'}}</a>
                </li>
                <li class="nav-item">
                    <a href="{{url('/admin/categories/create')}}">
                        <i class="fa fa-plus"></i>
                        {{'Add New Category'}}</a>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-file" aria-hidden="true"></i>
                <span class="title">{{'Pages'}}</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  ">
                    <a href="{{url('/admin/pages')}}" class="nav-link">
                        <i class="fa fa-file" aria-hidden="true"></i>
                        {{'All Pages'}}</a>
                </li>
                <li class="nav-item">
                    <a href="{{url('/admin/pages/create')}}" class="nav-link">
                        <i class="fa fa-plus"></i>
                        {{'Add New'}}</a>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="icon-users"></i>
                <span class="title">{{'Users'}}</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  ">
                    <a href="{{url('/admin/users')}}" class="nav-link">
                        <i class="icon-user"></i>
                        {{'User Listing'}}</a>


                </li>
                <li class="nav-item">
                    <a href="{{url('/admin/users/create')}}" class="nav-link">
                        <i class="fa fa-user-plus" aria-hidden="true"></i>
                        {{'Add new User'}}</a>


                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-tags" aria-hidden="true"></i>
                <span class="title">Offers</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item">
                    <a href="{{route('admin.offers.index')}}" class="nav-link">
                        <i class="fa fa-tags" aria-hidden="true"></i>
                        {{'Offers Listing'}}</a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.offers.create')}}" class="nav-link">
                        <i class="fa fa-tag" aria-hidden="true"></i>
                        {{'Create new offer'}}</a>
                </li>
            </ul>
        </li>


        <li class="nav-item">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-users" aria-hidden="true"></i>
                <span class="title">Restaurateurs</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item">
                    <a href="{{route('admin.restaurateurs.index')}}" class="nav-link">
                        <i class="fa fa-tags" aria-hidden="true"></i>
                        {{'Restaurateurs Listing'}}</a>
                </li>
            </ul>
        </li>







        <li class="nav-item">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-tags" aria-hidden="true"></i>
                <span class="title">Tags</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item">
                    <a href="{{url('/admin/tags')}}" class="nav-link">
                        <i class="fa fa-tags" aria-hidden="true"></i>
                        {{'Tags Listing'}}</a>
                </li>
                <li class="nav-item">
                    <a href="{{url('admin/tags/create')}}" class="nav-link">
                        <i class="fa fa-tag" aria-hidden="true"></i>
                        {{'Add New Tags'}}</a>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                <span class="title">{{'Contact'}}</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item">
                    <a href="{{url('/admin/contact')}}" class="nav-link">
                        <i class="fa fa-envelope-o"></i>
                        {{'Queries'}}</a>
                </li>
            </ul>
        </li>


        <li class="nav-item">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-users" aria-hidden="true"></i>
                <span class="title">{{'Event Participants'}}</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item">
                    <a href="{{url('/admin/event-participants')}}" class="nav-link">
                        <i class="fa fa-users"></i>
                        {{'Participants'}}</a>
                </li>
            </ul>
        </li>


        <li class="nav-item">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-spoon"></i>
                <span class="title">{{'Meal Category Listing'}}</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item"><a href="{{url('admin/meals')}}" class="nav-link"><i
                                class="fa fa-spoon"></i>{{'Meal Category Listing'}}</a></li>
                <li class="nav-item"><a href="{{url('/admin/meals/create')}}" class="nav-link"><i
                                class="fa fa-plus"></i>{{'Add Food Meal Category'}}</a></li>
            </ul>
        </li>
        <li class="nav-item">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-cutlery"></i>
                <span class="title">{{'Food Cuisines'}}</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item"><a href="{{url('admin/cuisines')}}" class="nav-link"><i
                                class="fa fa-spoon"></i>{{'Food Cuisines Listing'}}</a></li>
                <li class="nav-item"><a href="{{url('admin/cuisines/create')}}" class="nav-link"><i
                                class="fa fa-plus"></i>{{'Add Food Cuisines'}}</a></li>
                <li class="nav-item"><a href="{{url('admin/cuisine-dishes')}}" class="nav-link"><i
                                class="fa fa-cutlery"></i>{{'Cuisine Dishes'}}</a></li>
                <li class="nav-item"><a href="{{url('admin/cuisine-dishes/create')}}" class="nav-link">
                        <i class="fa fa-plus"></i>{{'Add Dishes to Cuisines'}}</a>
                </li>

            </ul>
        </li>
        <li class="nav-item">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-cutlery"></i>
                <span class="title">{{'Food Dishes'}}</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item"><a href="{{url('admin/dishes')}}" class="nav-link"><i
                                class="fa fa-cutlery"></i>{{'Food Dishes Listing'}}</a></li>
                <li class="nav-item"><a href="{{url('admin/dishes/create')}}" class="nav-link"><i
                                class="fa fa-plus"></i>{{'Add Food Dishes'}}</a></li>
            </ul>
        </li>

        <li class="nav-item">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-cutlery"></i>
                <span class="title">{{'Features'}}</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item"><a href="{{url('admin/features')}}" class="nav-link"><i
                                class="fa fa-cutlery"></i>{{'Features Listing'}}</a></li>
                <li class="nav-item"><a href="{{url('admin/features/create')}}" class="nav-link"><i
                                class="fa fa-plus"></i>{{'Add Feature'}}</a></li>
            </ul>
        </li>

        <li class="nav-item">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-bed"></i>
                <span class="title">{{'Restaurants'}}</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item"><a href="{{url('/admin/hotels')}}" class="nav-link"><i class="fa fa-bed"
                                                                                            aria-hidden="true"></i>{{'Restaurants Listing'}}
                    </a>
                </li>
                <li class="nav-item"><a href="{{url('/admin/hotels/create')}}" class="nav-link"><i
                                class="fa fa-plus"></i>{{'Add New Restaurant'}}</a></li>


            </ul>
        </li>


        <li class="nav-item">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-smile-o"></i><span class="title">{{'Manage Events'}}</span><span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item"><a href="{{route('admin.events.index')}}" class="nav-link">
                        <i class="fa fa-bed" aria-hidden="true"></i>{{'Upcoming Events'}}
                    </a>
                </li>
                <li class="nav-item"><a href="{{url('/admin/event/past')}}" class="nav-link">
                        <i class="fa fa-arrow" aria-hidden="true"></i>{{'Past Events'}}
                    </a>
                </li>
            </ul>
        </li>


        <li class="nav-item">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-smile-o"></i><span class="title">{{'Restaurant Awards '. date('Y')}}</span><span class="arrow"></span>
            </a>
            <ul class="sub-menu">

                <li class="nav-item"><a href="{{url('/admin/jlt-hidden-gem-nominations')}}" class="nav-link">
                        <i class="fa fa-circle" aria-hidden="true"></i>{{'JLT Hidden Gem '. date('Y')}}
                    </a>
                </li>
                <li class="nav-item"><a href="{{route('admin.award-category.index')}}" class="nav-link">
                        <i class="fa fa-circle" aria-hidden="true"></i>{{'Restaurant Award Categories'}}
                    </a>
                </li>
                <li class="nav-item"><a href="{{url('/admin/register-restaurant')}}" class="nav-link">
                        <i class="fa fa-bed" aria-hidden="true"></i>{{'Registered Restaurants'}}
                    </a>
                </li>

                <li class="nav-item"><a href="{{url('/admin/votes-by-category')}}" class="nav-link">
                        <i class="fa fa-arrow" aria-hidden="true"></i>{{'Votes by Category'}}
                    </a>
                </li>
                <li class="nav-item"><a href="{{url('/admin/voting-pool')}}" class="nav-link">
                        <i class="fa fa-hand-peace-o" aria-hidden="true"></i>{{'Voting Pool'}}
                    </a>
                </li>




            </ul>
        </li>





        <li class="nav-item">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="icon-settings"></i>
                <span class="title">{{'Settings'}}</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item">
                    <a href="{{url('/admin/settings')}}" class="nav-link">
                        <i class="fa fa-wrench"></i>
                        {{'Website Settings'}}</a>
                </li>



            </ul>
        </li>


    </ul>
    <!-- END SIDEBAR MENU -->
    <!-- END SIDEBAR MENU -->
</div>


