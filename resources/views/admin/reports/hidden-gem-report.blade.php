<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
    <thead>

    <tr>
        <th>SR#</th>
        <th>
            {{'First Name'}}
        </th>

        <th>
            {{'Last Name'}}
        </th>

        <th>
            {{'Email Address'}}
        </th>
        <th>Restaurant</th>

        {{--  <th>
              {{'Actions'}}
          </th>--}}
    </tr>
    </thead>
    <tbody>

    @if(isset($nomination) && !empty($nomination))
        <?php $i = 1;?>
        @foreach($nomination as $nominate)
            <tr class="odd gradeX">
                <td>{{$i++}}</td>
                <td>
                    {{$nominate->first_name}}
                </td>
                <td>
                    {{$nominate->last_name}}
                </td>
                <td>{{$nominate->email}}</td>

                <td>{{$nominate->restaurant}}</td>

                {{-- <td>
                     <div class="btn-group">
                         <button class="btn btn-xs green dropdown-toggle" type="button"
                                 data-toggle="dropdown" aria-expanded="false"> Actions
                             <i class="fa fa-angle-down"></i>
                         </button>
                         <ul class="dropdown-menu pull-left" role="menu">
                             <li>
                                 <a href="{{route('admin.award-category.edit',$category->id)}}" class="btn btn-sm">Edit</a>
                                 {!! Form::open([
                            'method' => 'DELETE',
                            'route' => ['admin.award-category.destroy', $category->id]
                    ]) !!}
                                 {!! Form::submit('Delete?', ['class' => 'btn btn-sm red']) !!}
                                 {!! Form::close() !!}


                             </li>


                         </ul>
                     </div>


                 </td>
--}}

            </tr>
        @endforeach
    @endif
   {{-- <tr>
        <td colspan="5">{{ $nomination->links() }}</td>
    </tr>
--}}
    </tbody>

</table>