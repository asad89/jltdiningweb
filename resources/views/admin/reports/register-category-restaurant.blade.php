<table class="table table-striped table-bordered table-hover table-checkable order-column"
       id="sample_1">
    <thead>
    <tr>
        <th>SR#</th>
        <th>Outlet Name</th>
        <th>Category</th>
           <th>Website</th>
           <th>Facebook</th>
           <th>Twitter</th>
           <th>Instagram</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Email</th>
        <th>Phone</th>
    </tr>
    </thead>
    <tbody>
    @if(isset($restaurants) && !empty($restaurants))
        <?php $i=1; ?>
        @foreach($restaurants as $item)
            <tr>
                <td>{{$i++}}</td>
                <td>{{$item->getRestaurant->name}}</td>
                <td>
                    {{$item->getCategory->name}}
                </td>
                 <td>{{$item->getRestaurant->website}}</td>
                 <td>{{$item->getRestaurant->facebook}}</td>
                 <td>{{$item->getRestaurant->twitter}}</td>
                 <td>{{$item->getRestaurant->instagram}}</td>
                <td>{{$item->getRestaurant->first_name}}</td>
                <td>{{$item->getRestaurant->last_name}}</td>
                <td>{{$item->getRestaurant->email}}</td>
                <td>{{$item->getRestaurant->phone}}</td>
            </tr>
        @endforeach
    @endif


    </tbody>
</table>