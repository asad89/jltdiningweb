<table class="table table-striped table-bordered table-hover table-checkable order-column"
       id="sample_1">
    <thead>
    <tr>
        <th>SR#</th>
        <th>Outlet Name</th>
        <th>Category</th>
           <th>Website</th>
           <th>Facebook</th>
           <th>Twitter</th>
           <th>Instagram</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Email</th>
        <th>Phone</th>
    </tr>
    </thead>
    <tbody>
    @if(isset($register_users) && !empty($register_users))
        <?php $i=1; ?>
        @foreach($register_users as $item)
            <tr>
                <td>{{$i++}}</td>
                <td>{{$item->name}}</td>
                <td>
                    @foreach ($item->getRestaurantCategroy as $item2)
                        {{$item2->getCategory->name . " ,"}}

                    @endforeach
                </td>
                 <td>{{$item->website}}</td>
                 <td>{{$item->facebook}}</td>
                 <td>{{$item->twitter}}</td>
                 <td>{{$item->instagram}}</td>
                <td>{{$item->first_name}}</td>
                <td>{{$item->last_name}}</td>
                <td>{{$item->email}}</td>
                <td>{{$item->phone}}</td>
            </tr>
        @endforeach
    @endif


    </tbody>
</table>