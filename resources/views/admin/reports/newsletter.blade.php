<table class="table table-striped table-bordered table-hover table-checkable order-column"
       id="sample_1">
    <thead>
    <tr>
        <th>SR#</th>
        <th>First Name</th>
        <th>Sure Name</th>
        <th>Email</th>
        <th>Date of Vote</th>
    </tr>
    </thead>
    <tbody>
    @if(isset($newsletter) && !empty($newsletter))
        <?php $i=1; ?>
        @foreach($newsletter as $item)
            <?php
            $name = explode("-",$item->name);
            ?>
            <tr>
                <td>{{$i++}}</td>
                <td>{{$name[0]}}</td>
                <td>{{$name[1]}}</td>
                <td>{{$item->email}}</td>
                <td>{{$item->created_at->toFormattedDateString()}}</td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>