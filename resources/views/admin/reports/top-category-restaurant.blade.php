<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
    <thead>
    <tr>
       {{-- <td colspan="3" style="text-align: center;">Category: {{$category->name}}</td>--}}
    </tr>

    <tr>
        <th>SR#</th>
        <th>Restaurant Name</th>
        <th>Number of Votes</th>
    </tr>
    </thead>
    <tbody>
    @if(isset($votes) && !empty($votes))
        <?php $i=1; ?>
        @foreach($votes as $item)
            <tr class="odd gradeX">
                <td>{{$i++}}</td>
                <td>
                    {{$item->restaurant_name}}</td>
                <td>{{$item->votes}}</td>

            </tr>
        @endforeach
    @endif


    </tbody>
</table>
