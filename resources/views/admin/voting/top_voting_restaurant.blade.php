@extends('admin.master')
@push('site_header')
        <!-- BEGIN PAGE LEVEL STYLES -->
<link href="{{asset('private/assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet"
      type="text/css"/>
<link href="{{asset('private/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}"
      rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
@endpush
@section('content')

    <div class="row">
        <div class="col-md-12">


            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box blue-madison">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> {{'Vote Casted By Category'}}</span>
                    </div>

                </div>
                <div class="portlet-body">
                    @if (session()->has('flash_notification.message'))
                        <div class="alert alert-{{ session('flash_notification.level') }}">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {!! session('flash_notification.message') !!}
                        </div>
                    @endif


                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-6">
                                    <div class="btn-group pull-right">
                                        <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                            <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-right">
                                            <li>
                                                <a href="{{url('/admin/top-restaurants-report',$category->id)}}">
                                                    <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                        <thead>
                        <tr>
                            <td colspan="3" style="text-align: center;">Category: {{$category->name}}</td>
                        </tr>

                        <tr>
                            <th>SR#</th>
                            <th>Restaurant Name</th>
                            <th>Number of Votes</th>

                        </tr>


                        </thead>
                        <tbody>
                        @if(isset($votes) && !empty($votes))
                            <?php $i=1; ?>
                            @foreach($votes as $item)
                                <tr class="odd gradeX">
                                    <td>{{$i++}}</td>
                                    <td>
                                        {{$item->restaurant_name}}</td>
                                    <td>{{$item->votes}}</td>

                                </tr>
                            @endforeach
                        @endif


                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->


        </div>
    </div>
    @endsection
    @push('site_footer')
            <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{asset('private/assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{asset('private/assets/global/plugins/datatables/datatables.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('private/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}"
            type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{asset('private/assets/pages/scripts/table-datatables-managed.min.js')}}"
            type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    @endpush