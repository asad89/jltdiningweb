@extends('admin.master')
@push('site_header')
        <!-- BEGIN PAGE LEVEL STYLES -->
<link href="{{asset('private/assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet"
      type="text/css"/>
<link href="{{asset('private/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}"
      rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
@endpush
@section('content')

    <div class="row">
        <div class="col-md-12">


            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box blue-madison">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> {{'Selected User Casted Vote'}}</span>
                    </div>

                </div>
                <div class="portlet-body">
                    @if (session()->has('flash_notification.message'))
                        <div class="alert alert-{{ session('flash_notification.level') }}">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {!! session('flash_notification.message') !!}
                        </div>
                    @endif

                        <table class="table">
                            <tr>
                                <th>Full Name</th>
                                <th>Email</th>
                                <th>Newsletter</th>
                                <th>Ip Address</th>

                                <th>Date</th>
                            </tr>

                            <tr>
                                <td>{{$vote->name}}</td>
                                <td>{{$vote->email}}</td>
                                <td>@if($vote->newsletter==0) {{"Not Subscribed"}} @else {{"Subscribed"}}@endif</td>
                                <td>{{$vote->ip_address}}</td>
                                <td>{{$vote->created_at->toFormattedDateString()}}</td>
                            </tr>
                        </table>

                    <table class="table table-bordered table-hover">
                        <thead>


                        <tr>
                            <th>SR#</th>
                            <th>Category Name</th>
                            <th>Restaurant Name</th>
                        </tr>


                        </thead>
                        <tbody>
                        @if(isset($vote) && !empty($vote->voteInfo))
                            <?php $i=1; ?>
                            @foreach($vote->voteInfo as $item)
                                <tr class="odd gradeX">
                                    <td>{{$i++}}</td>
                                    <td>{{$item->getCategory->name}}</td>
                                    <td>{{$item->getRestaurant->name}}</td>
                                </tr>
                            @endforeach
                        @endif


                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->


        </div>
    </div>
    @endsection
    @push('site_footer')
            <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{asset('private/assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{asset('private/assets/global/plugins/datatables/datatables.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('private/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}"
            type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{asset('private/assets/pages/scripts/table-datatables-managed.min.js')}}"
            type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    @endpush