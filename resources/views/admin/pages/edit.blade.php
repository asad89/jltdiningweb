@extends('admin.master')
@push('site_header')
        <!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<!-- Include the plugin's CSS and JS: -->
<script type="text/javascript" src="{{asset('js/bootstrap-multiselect.js')}}"></script>
<link rel="stylesheet" href="{{asset('css/bootstrap-multiselect.css')}}" type="text/css"/>

<link href="{{asset('private/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('private/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('private/assets/global/plugins/bootstrap-summernote/summernote.css')}}" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    $(document).ready(function(){
        $('#summernote_1').summernote({
            height: 300,
            lang: "es-ES",
            disableDragAndDrop: true,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'hr']],
                ['view', ['fullscreen', 'codeview']],
                ['help', ['help']]
            ],
            fontNames: ['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New','Oxygen','Oxygen,sans-serif'],
            fontNamesIgnoreCheck: ['Oxygen','Oxygen,sans-serif'],
        });
    });
</script>
@endpush
@section('content')
        <!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <div class="" id="">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>Edit Page
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="javascript:;" class="reload">
                        </a>
                        <a href="javascript:;" class="remove">
                        </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    {!! Form::open(array('url' => '/admin/pages/'.$page->id,'class'=>'horizontal-form','method' => 'PATCH','files' => true)) !!}
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('Page Name',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('name',$page->name,array('class'=>'form-control','placeholder'=>'Add Page name')) !!}
                                    <span class="help-block">Add Page Name </span>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('Page Title',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('title',$page->title,array('class'=>'form-control','placeholder'=>'Page Title','id'=>'title')) !!}
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('Content',null,array('class'=>'control-label')) !!}
                                    {!! Form::textarea('post_content',$page->post_content,array('class'=>'form-control','placeholder'=>'Page Content','id'=>'summernote_1')) !!}
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('Publish Status',null,array('class'=>'control-label')) !!}
                                    {!! Form::select('status',array('1'=>'Published','0'=>'Draft',),$page->status,array('class'=>'form-control')) !!}

                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('Tags',null,array('class'=>'control-label')) !!}
                                    <select name="tags[]" class="" id="tags" multiple="multiple">
                                        @if(isset($tags) && count($tags))
                                            @foreach($tags as $tag)
                                                <option value="{{$tag->id}}">{{$tag->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>

                                </div>
                            </div>


                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('Featured Image',null,array('class'=>'control-label')) !!}
                                    {!! Form::file('file', array('class'=>'btn btn-primary')) !!}
                                </div>
                            </div>
                        </div>

                        <!--/row-->
                        <!--/row-->
                        <h3 class="form-section">SEO Related</h3>

                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group">
                                    {!! Form::label('Meta Title',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('meta_title',$page->meta_title,array('class'=>'form-control','placeholder'=>'Meta Title','id'=>'meta_title')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group">
                                    {!! Form::label('Meta Keywords',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('meta_keywords',$page->meta_keywords,array('class'=>'form-control','placeholder'=>'Meta Keywords','id'=>'meta_keywords')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="form-group">
                                        {!! Form::label('Meta Description',null,array('class'=>'control-label')) !!}
                                        {!! Form::textarea('meta_description',$page->meta_description,array('class'=>'form-control','placeholder'=>'Meta Description','id'=>'meta_description')) !!}
                                    </div>
                                </div>
                            </div>
                            </row>
                            <row class="row">
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="form-group">
                                        {!! Form::label('Meta Robots',null,array('class'=>'control-label')) !!}
                                        {!! Form::select('meta_robots',array('1'=>'Index,Follow','0'=>'No Index, No follow',),null,array('class'=>'form-control')) !!}

                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->


                    </div>
                    <div class="form-actions right">
                        <button type="button" class="btn default">Cancel</button>
                        <button type="submit" class="btn blue"><i class="fa fa-check"></i> Save</button>
                    </div>
                    {!! Form::close() !!}
                            <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@endsection
@push('site_footer')


        <!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->

<script type="text/javascript" src="{{asset('private/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('private/assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js')}}"></script>
<script type="text/javascript" src="{{asset('private/assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js')}}"></script>
<script type="text/javascript" src="{{asset('private/assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js')}}"></script>
<script type="text/javascript" src="{{asset('private/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}"></script>


<script type="text/javascript" src="{{asset('private/assets/global/plugins/select2/select2.min.js')}}"></script>


<script type="text/javascript"
        src="{{asset('private/assets/global/plugins/bootstrap-summernote/summernote.min.js')}}"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{asset('private/assets/global/scripts/metronic.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/admin/layout/scripts/layout.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/admin/layout/scripts/quick-sidebar.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/admin/layout/scripts/demo.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/admin/pages/scripts/components-editors.js')}}"></script>
<script src="{{asset('js/bootstrap-multiselect.js')}}"></script>
<script>
    jQuery(document).ready(function () {
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        QuickSidebar.init(); // init quick sidebar
        Demo.init(); // init demo features
        //
        ComponentsEditors.init();
        TableAdvanced.init();
    });
    $('#tags').multiselect({
        includeSelectAllOption: true,
        selectAllNumber: false,
        enableFiltering: true


    });
</script>
@endpush