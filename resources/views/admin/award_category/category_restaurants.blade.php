@extends('admin.master');
@push('site_header')
        <!-- BEGIN PAGE LEVEL STYLES -->
<link href="{{asset('private/assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet"
      type="text/css"/>
<link href="{{asset('private/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}"
      rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="portlet-title">

            <div class="caption font-dark">
                <i class="icon-settings font-dark"></i>
                <span class="caption-subject bold uppercase"> {{'Restaurants Nominated in ' . $category->name}}</span>
            </div>
        </div>
        <div class="portlet-body">
            @if (session()->has('flash_notification.message'))
                <div class="alert alert-{{ session('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! session('flash_notification.message') !!}
                </div>
            @endif
            <div class="table-toolbar">
                <div class="row">
                    <div class="col-md-6">
                        <div class="btn-group">
                           {{-- <a class="btn sbold green" href="{{route('admin.award-category.create')}}"><i
                                        class="fa fa-plus"></i>{{"Add New Awarad Category"}}</a>--}}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="btn-group pull-right">
                            <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                <i class="fa fa-angle-down"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">

                                <li>
                                    <a href="{{url('/admin/export-category-restaurant',$category->id)}}">
                                        <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                <thead>


                <tr>
                    <th>SR#</th>
                    <th>
                        {{'Restaurant Name'}}
                    </th>
                    <th>
                        {{'Contact Person Name'}}
                    </th>
                    <th>
                        {{'Contact Person Phone'}}
                    </th>
                    <th>
                        {{'Contact Person Email'}}
                    </th>
                    <th>
                        {{'Votes Received'}}
                    </th>

                </tr>
                </thead>
                <tbody>

                @if(isset($restaurants) && !empty($restaurants))
                    <?php $i = 1;?>
                    @foreach($restaurants as $item)
                        <tr class="odd gradeX">
                            <td>{{$i++}}</td>


                            <td>
                                {{$item->getRestaurant->name}}
                            </td>
                            <td>
                                {{$item->getRestaurant->first_name. ' '. $item->getRestaurant->last_name}}
                            </td>

                            <td>
                                {{$item->getRestaurant->phone}}
                            </td>

                            <td>
                                {{$item->getRestaurant->email}}
                            </td>

                            <td>
                                {{$item->getRestaurant->id . '-'. $category->id}}
                            </td>


                         </tr>
                    @endforeach
                @endif
               {{-- <tr>
                    <td colspan="5">{{ $restaurants->links() }}</td>
                </tr>
--}}
                </tbody>

            </table>
        </div>

        <!-- END EXAMPLE TABLE PORTLET-->


    </div>
</div>

@endsection


@push('site_footer')
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{asset('private/assets/pages/scripts/form-samples.min.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
@endpush
