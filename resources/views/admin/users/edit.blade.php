@extends('admin.master')
@push('site_header')
        <!-- BEGIN PAGE LEVEL STYLES -->
<link href="{{asset('private/assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet"
      type="text/css"/>
<link href="{{asset('private/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}"
      rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
@endpush
@section('content')
        <!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <div class="" id="">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>Edit User {{ $users->name }}
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="javascript:;" class="reload">
                        </a>
                        <a href="javascript:;" class="remove">
                        </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->

                    {!! Form::open(array('url' => '/admin/users/'.$users->id,'class'=>'horizontal-form','method' => 'PATCH')) !!}

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('Name',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('name',$users->name,array('class'=>'form-control','placeholder'=>'Add username')) !!}
                                    <span class="help-block">Editing user {{$users->name}} </span>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('Email',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('email',$users->email,array('class'=>'form-control','placeholder'=>'User Email','id'=>'email', 'disabled'=>'disabled')) !!}
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group">
                                    {!! Form::label('Address',null,array('class'=>'control-label')) !!}
                                    {!! Form::textarea('address',$users->address,array('class'=>'form-control','placeholder'=>'Address','id'=>'address')) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('Phone',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('phone',$users->phone,array('class'=>'form-control','placeholder'=>'Phone Number','id'=>'phone')) !!}

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="form-group">
                                        {!! Form::label('Role',null,array('class'=>'control-label')) !!}
                                        {!! Form::select('role',array('1'=>'Admin','0'=>'User',),$users->role,array('class'=>'form-control')) !!}

                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                    <div class="form-actions right">
                        <button type="button" class="btn default">Cancel</button>
                        <button type="submit" class="btn blue"><i class="fa fa-check"></i> Save</button>
                    </div>
                    {!! Form::close() !!}
                            <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@endsection
@push('site_footer')
        <!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{asset('private/assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/datatables/datatables.min.js')}}"
        type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}"
        type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{asset('private/assets/pages/scripts/table-datatables-managed.min.js')}}"
        type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
@endpush