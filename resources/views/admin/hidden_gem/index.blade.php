@extends('admin.master');
@push('site_header')
        <!-- BEGIN PAGE LEVEL STYLES -->
<link href="{{asset('private/assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet"
      type="text/css"/>
<link href="{{asset('private/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}"
      rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet-title">

                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"> {{'JLT Hidden GEM '.date('Y')}}</span>
                </div>
            </div>
            <div class="portlet-body">
                @if (session()->has('flash_notification.message'))
                    <div class="alert alert-{{ session('flash_notification.level') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {!! session('flash_notification.message') !!}
                    </div>
                @endif
              {{--  <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <a class="btn sbold green" href="{{route('admin.award-category.create')}}"><i
                                            class="fa fa-plus"></i>{{"Add New Awarad Category"}}</a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="btn-group pull-right">
                                <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:;">
                                            <i class="fa fa-print"></i> Print </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>--}}
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                    <thead>

                    <tr>
                        <th>SR#</th>
                        <th>
                            {{'First Name'}}
                        </th>

                        <th>
                            {{'Last Name'}}
                        </th>

                        <th>
                            {{'Email Address'}}
                        </th>
                        <th>Restaurant</th>

                      {{--  <th>
                            {{'Actions'}}
                        </th>--}}
                    </tr>
                    </thead>
                    <tbody>

                    @if(isset($nomination) && !empty($nomination))
                        <?php $i = 1;?>
                        @foreach($nomination as $nominate)
                            <tr class="odd gradeX">
                                <td>{{$i++}}</td>
                                <td>
                                    {{$nominate->first_name}}
                                </td>
                                <td>
                                    {{$nominate->last_name}}
                                </td>
                                <td>{{$nominate->email}}</td>

                                <td>{{$nominate->restaurant}}</td>

                               {{-- <td>
                                    <div class="btn-group">
                                        <button class="btn btn-xs green dropdown-toggle" type="button"
                                                data-toggle="dropdown" aria-expanded="false"> Actions
                                            <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-left" role="menu">
                                            <li>
                                                <a href="{{route('admin.award-category.edit',$category->id)}}" class="btn btn-sm">Edit</a>
                                                {!! Form::open([
                                           'method' => 'DELETE',
                                           'route' => ['admin.award-category.destroy', $category->id]
                                   ]) !!}
                                                {!! Form::submit('Delete?', ['class' => 'btn btn-sm red']) !!}
                                                {!! Form::close() !!}


                                            </li>


                                        </ul>
                                    </div>


                                </td>
--}}

                            </tr>
                        @endforeach
                    @endif
                    <tr>
                        <td colspan="5">{{ $nomination->links() }}</td>
                    </tr>

                    </tbody>

                </table>
            </div>

            <!-- END EXAMPLE TABLE PORTLET-->


        </div>
    </div>

    @endsection


    @push('site_footer')
            <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{asset('private/assets/pages/scripts/form-samples.min.js')}}" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    @endpush
