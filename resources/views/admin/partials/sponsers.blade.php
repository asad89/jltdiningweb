<hr>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
        <h3 class="page-heading-custom"
            style="margin-top:15px;text-align: center;font-weight:normal;margin-bottom:0px;font-size:18px;font-family: Oxygen, sans-serif!important;">
            Thank you to our 2017 JLT Restaurant Awards Partners & Sponsors
        </h3>
      {{--  <p style="text-align: center;margin-top:10px;font-size:20px;font-family: Oxygen, sans-serif!important;font-weight: bold;">Congratulations to all winners and runner-ups!</p>--}}
        {{-- <p style="text-align:center;margin-top:10px;">Want to sponsor the awards? <a  href="mailto:munch@jltdining.com?Subject=JLT%20Restaurant%20Awards%20-%20Sponsorship%20Inquiry" target="_top">Contact us today!</a></p>--}}
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12" style="text-align:center;">
        <h5>{{"Official PR Partner"}}</h5>
        <img src="{{asset('images/sps-pr.jpg')}}" alt="SPS PR" width="100"
             height="100">
    </div>
    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12"
         style="text-align:center;">
        <h5>{{"Official Social Media Partner"}}</h5>
        <img src="{{asset('images/sps-digital.jpg')}}" alt="SPS PR" width="100"
             height="100"></div>
</div>

<div class="clearfix"></div>
<div class="row" style="margin-top: 10px;">
    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12"
         style="text-align:center;">
        <h5>{{"Official Media Partner"}}</h5>
       <a href="#" target="_blank">
            <img src="{{asset('images/FoodSheikh.png')}}" alt="Food Sheikh" width="100" height="100">
        </a>
        {{-- <img src="{{asset('images/sps-digital.jpg')}}" alt="SPS PR" width="100" height="100">--}}
    </div>
    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12"
         style="text-align:center;">
        <h5>{{"Official Legal Partner"}}</h5>
        <a href="https://www.twobirds.com/" target="_blank">
            <img src="{{asset('images/bird-and-bird.jpg')}}" alt="bird-and-bird" width="100" height="100">
        </a>
    </div>
</div>

<hr>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
        <h3 class="page-heading-custom"
            style="margin-top:0px;text-align: center;font-weight:normal;margin-bottom:20px;font-size:18px;font-family: Oxygen, sans-serif!important;">
            Platinum Sponsors</h3>
        <div class="table-responsive">
            <table border="0" cellspacing="20" class="table">
                <tr>
                    <td><a href="http://publiccafeme.com/" target="_blank"><img src="{{asset('/images/public-cafe.jpg')}}" alt="Public Cafe"></a></td>
                    <td><a  href="https://www.facebook.com/ankurtrdg/" target="_blank"><img src="{{asset('/images/qromq.png')}}" alt="qromq"></a></td>
                    <td><a href="http://dna1films.com/" target="_blank"><img src="{{asset('/images/dna1-films.png')}}" alt="DNA1 Films"></a></td>
                </tr>
            </table>
        </div>
       {{-- <p style="text-align:center;margin-top:10px;">Want to sponsor the awards? <a  href="mailto:munch@jltdining.com?Subject=JLT%20Restaurant%20Awards%20-%20Sponsorship%20Inquiry" target="_top">Contact us today!</a></p>--}}
    </div>
</div>
<hr>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
        <h3 class="page-heading-custom"
            style="margin-top:0px;text-align: center;font-weight:normal;margin-bottom:20px;font-size:18px;font-family: Oxygen, sans-serif!important;">
            Gold Sponsors</h3>
        <div class="table-responsive">
        <table border="0" cellspacing="20" class="table">
            <tr>
                <td style="width:10%;">&nbsp;</td>

                <td style="width:40%;"><a href="http://pitfiredubai.ae/" target="_blank"><img src="{{asset('/images/pitfire-gold.png')}}" alt="pitfire"></a></td>
                <td style="width: 40%;"><a href="http://www.blossomtreedubai.com/" target="_blank"><img src="{{asset('/images/blossom.PNG')}}" alt="blossom"></a></td>
                <td style="width:10%;">&nbsp;</td>
            </tr>
        </table>
        </div>
           {{-- <p style="text-align:center;margin-top:25px;">Want to sponsor the awards? <a  href="mailto:munch@jltdining.com?Subject=JLT%20Restaurant%20Awards%20-%20Sponsorship%20Inquiry" target="_top">Contact us today!</a></p>--}}

    </div>

</div>
<hr>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
        <h3 class="page-heading-custom"
            style="margin-top:0px;text-align: center;font-weight:normal;margin-bottom:20px;font-size:18px;font-family: Oxygen, sans-serif!important;">
            Silver Sponsors</h3>
        <div class="table-responsive">
            <table border="0" cellspacing="20" class="table">
            <tr>
                <td style="border:0px;"><a href="http://www.localyser.com/" target="_blank"><img src="{{asset('/images/localyser.jpg')}}" alt="localyser"></a></td>
                <td style="border:0px;"><a href="http://www.foodiva.net/" target="_blank"><img src="{{asset('/images/foodiva-sponsor.jpg')}}" alt="foodiva"></a></td>
                <td style="border:0px;"><a href="http://liquidoflife.net/" target="_blank"><img src="{{asset('/images/liquid.PNG')}}" alt="liquid"></a></td>
            </tr>

            <tr>
                <td style="border:0px;"><a href="https://www.facebook.com/betawifanclub/" target="_blank"><img src="{{asset('/images/betawi.jpg')}}" alt="betawi"></a></td>
                <td style="border:0px;"><a href="https://www.facebook.com/IndoIndoSeafood/" target="_blank"><img src="{{asset('/images/indo.jpg')}}" alt="indo"></a></td>
                <td style="border:0px;"><a href="http://wfl.ae/" target="_blank"><img src="{{asset('/images/wolf.jpg')}}" alt="wolf"></a></td>
            </tr>

                <tr>

                    <td style="border:0px;"><a href="http://www.servicexcellence.com" target="_blank"><img src="{{asset('/images/service-xellence.jpg')}}" alt="service-xellence"></a></td>
                </tr>



        </table>
        </div>
       {{-- <p style="text-align:center;margin-top:10px;">Want to sponsor the awards? <a  href="mailto:munch@jltdining.com?Subject=JLT%20Restaurant%20Awards%20-%20Sponsorship%20Inquiry" target="_top">Contact us today!</a></p>--}}
    </div>
</div>

<hr>