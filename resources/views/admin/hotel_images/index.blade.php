@extends('admin.master');
@push('site_head')
        <!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('private/assets/global/plugins/select2/select2.css')}}"/>
<link rel="stylesheet" type="text/css"
      href="{{asset('private/assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css')}}"/>
<link rel="stylesheet" type="text/css"
      href="{{asset('private/assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css')}}"/>
<link rel="stylesheet" type="text/css"
      href="{{asset('private/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}"/>
<!-- END PAGE LEVEL STYLES -->
@endpush
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box blue-madison">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-globe"></i>{{'Cuisines Listing'}}
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="reload">
                        </a>
                        <a href="javascript:;" class="remove">
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    @if (session()->has('flash_notification.message'))
                        <div class="alert alert-{{ session('flash_notification.level') }}">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {!! session('flash_notification.message') !!}
                        </div>
                    @endif
                    <table class="table table-striped table-bordered table-hover" id="sample_5">
                        <thead>

                        <tr>
                            <th>
                                {{'Cuisine Name'}}
                            </th>

                            <th>
                                {{'Status'}}
                            </th>

                            <th>
                                {{'View'}}
                            </th>

                            <th>
                                {{'Delete'}}
                            </th>
                        </tr>
                        </thead>
                        <tbody>

                        @if(isset($dishes) && !empty($dishes))
                            @foreach($dishes as $dish)
                                <tr>
                                    <td>
                                        {{$dish->name}}
                                    </td>
                                    <td>
                                        @if($dish->status==0)
                                            {{'Not Active'}}
                                        @else
                                            {{'Active'}}
                                        @endif

                                    </td>

                                    <td>
                                        <a href="<?php echo url('/admin/dishes/'.$dish->id.'/edit'); ?>">Edit</a>
                                    </td>

                                    <td>
                                        {!! Form::open([
                                                'method' => 'DELETE',
                                                'route' => ['admin.dishes.destroy', $dish->id]
                                        ]) !!}
                                        {!! Form::submit('Delete?', ['class' => 'btn btn-danger']) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                        @endif



                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->


        </div>
    </div>

    @endsection


    @push('site_footer')
            <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="{{asset('private/assets/global/plugins/select2/select2.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('private/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('private/assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('private/assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('private/assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('private/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{asset('private/assets/global/scripts/metronic.js')}}" type="text/javascript"></script>
    <script src="{{asset('private/assets/admin/layout/scripts/layout.js')}}" type="text/javascript"></script>
    <script src="{{asset('private/assets/admin/layout/scripts/quick-sidebar.js')}}" type="text/javascript"></script>
    <script src="{{asset('private/assets/admin/layout/scripts/demo.js')}}" type="text/javascript"></script>
    <script src="{{asset('private/assets/admin/pages/scripts/table-advanced.js')}}"></script>
    <script>
        jQuery(document).ready(function () {
            Metronic.init(); // init metronic core components
            Layout.init(); // init current layout
            QuickSidebar.init(); // init quick sidebar
            Demo.init(); // init demo features
            TableAdvanced.init();
        });
    </script>
    @endpush