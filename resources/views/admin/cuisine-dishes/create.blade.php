@extends('admin.master');
@push('site_head')
        <!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('private/assets/global/plugins/select2/select2.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('private/assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('private/assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('private/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}"/>

<!-- END PAGE LEVEL STYLES -->
@endpush
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>{{'Add New Dishes in Cuisine'}}
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="javascript:;" class="reload">
                        </a>
                        <a href="javascript:;" class="remove">
                        </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    {!! Form::open(array('url' => '/admin/cuisine-dishes','class'=>'horizontal-form')) !!}
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-body">
                        @if (session()->has('flash_notification.message'))
                            <div class="alert alert-{{ session('flash_notification.level') }}">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {!! session('flash_notification.message') !!}
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">

                                    {!! Form::label('cuisines_name',null,array('class'=>'control-label')) !!}
                                    <select name="cuisine_id" class="form-control" id="cuisine_id">
                                        <option value="0">Select Option</option>
                                        @if(isset($cuisines) && count($cuisines))
                                            @foreach($cuisines as $cuisine)
                                                <option value="{{$cuisine->id}}">{{$cuisine->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('Dishes',null,array('class'=>'control-label')) !!}
                                    <select name="dish_id" class="form-control" id="dish_id">
                                        <option value="0">Select Option</option>
                                        @if(isset($dishes) && count($dishes))
                                            @foreach($dishes as $dish)
                                                <option value="{{$dish->id}}">{{$dish->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                    </div>





                    <div class="form-actions right">
                        <button type="button" class="btn default">Cancel</button>
                        <button type="submit" class="btn blue"><i class="fa fa-check"></i> Save</button>
                    </div>
                    {!! Form::close() !!}
                            <!-- END FORM-->
                </div>
            </div>


        </div>





    </div>


    <table class="table table-striped table-bordered table-hover" id="sample_5">
        <h3>Your Selected Cuisine already have below Dishes</h3>
        <thead>

        <tr>
            <th>
                {{'SR#'}}
            </th>
            <th>
                {{'Dish Name'}}
            </th>

            <th>
                {{'Slug'}}
            </th>

            <th>
                {{'Delete'}}
            </th>
        </tr>
        </thead>
      </table>

    @endsection


@push('site_footer')
<script type="text/javascript"
        src="{{asset('private/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript"
        src="{{asset('private/assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js')}}"></script>
<script type="text/javascript"
        src="{{asset('private/assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js')}}"></script>
<script type="text/javascript"
        src="{{asset('private/assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js')}}"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{asset('private/assets/global/scripts/metronic.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/admin/layout/scripts/layout.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/admin/layout/scripts/quick-sidebar.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/admin/layout/scripts/demo.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/admin/pages/scripts/table-advanced.js')}}"></script>
<script src="{{asset('private/assets/admin/pages/scripts/components-editors.js')}}"></script>


<script src="{{asset('private/assets/global/scripts/metronic.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/admin/layout/scripts/layout.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/admin/layout/scripts/quick-sidebar.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/admin/layout/scripts/demo.js')}}" type="text/javascript"></script>

<script>
    jQuery(document).ready(function () {
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        QuickSidebar.init(); // init quick sidebar
        Demo.init(); // init demo features
        TableAdvanced.init();
    });

    $('#cuisine_id').change(function(){
       var option =  $(this).val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: 'POST',
            dataType: 'JSON',
            url: '<?php echo url('admin/show-cuisine-dishes')?>',
            data: {cuisine: option},
            success: function(data){
                if(data){
                    var len = data.length;
                    var txt = "";
                    if(len > 0){
                        sr =1;
                        for(var i=0;i<len;i++){

                            if(data[i].name && data[i].slug){
                                txt += "<tr><td>"+sr+"</td><td>"+data[i].name+"</td><td>"+data[i].slug+"</td><td><a href='<?php echo url('admin/delete-cuisine-dish')?>/"+data[i].id+"'>Delete</a></td></tr>";
                            }
                            sr++;
                        }
                        if(txt != ""){
                            $("#sample_5 tbody").html('');
                            $("#sample_5").append(txt).removeClass("hidden");
                        }
                    }
                }
            },
            //error: functi


        });



    });

</script>
@endpush