@extends('private.master')
@push('site_head')
        <!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('private/assets/global/plugins/select2/select2.css')}}"/>
<link rel="stylesheet" type="text/css"
      href="{{asset('private/assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css')}}"/>
<link rel="stylesheet" type="text/css"
      href="{{asset('private/assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css')}}"/>
<link rel="stylesheet" type="text/css"
      href="{{asset('private/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('private/assets/global/plugins/bootstrap-summernote/summernote.css')}}">

<!-- END PAGE LEVEL STYLES -->
@endpush
@section('content')
        <!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <div class="" id="">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>Add New Page
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="javascript:;" class="reload">
                        </a>
                        <a href="javascript:;" class="remove">
                        </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    {!! Form::open(array('url' => '/admin/settings','class'=>'horizontal-form', 'files' => true)) !!}
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('page_name',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('page_name',null,array('class'=>'form-control','placeholder'=>'Add Page name')) !!}
                                    <span class="help-block">Add Page Name </span>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('Page Title',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('page_title',null,array('class'=>'form-control','placeholder'=>'Page Title','id'=>'page_title')) !!}
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('Content',null,array('class'=>'control-label')) !!}
                                    {!! Form::textarea('page_content',null,array('class'=>'form-control','placeholder'=>'Page Content','id'=>'summernote_1')) !!}
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('Publish Status',null,array('class'=>'control-label')) !!}
                                    {!! Form::select('publish_status',array('1'=>'Published','0'=>'Draft',),null,array('class'=>'form-control')) !!}

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('Upload File',null,array('class'=>'control-label')) !!}
                                    {!! Form::file('file', array('class'=>'btn btn-primary')) !!}
                                </div>
                            </div>
                        </div>

                        <!--/row-->
                        <!--/row-->
                        <h3 class="form-section">SEO Related</h3>

                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group">
                                    {!! Form::label('Meta Title',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('meta_title',null,array('class'=>'form-control','placeholder'=>'Meta Title','id'=>'meta_title')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group">
                                    {!! Form::label('Meta Keywords',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('meta_keywords',null,array('class'=>'form-control','placeholder'=>'Meta Keywords','id'=>'meta_keywords')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="form-group">
                                        {!! Form::label('Meta Description',null,array('class'=>'control-label')) !!}
                                        {!! Form::textarea('meta_description',null,array('class'=>'form-control','placeholder'=>'Meta Description','id'=>'meta_description')) !!}
                                    </div>
                                </div>
                            </div>
                            </row>
                            <row class="row">
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="form-group">
                                        {!! Form::label('Meta Robots',null,array('class'=>'control-label')) !!}
                                        {!! Form::select('meta_robots',array('1'=>'Index,Follow','0'=>'No Index, No follow',),null,array('class'=>'form-control')) !!}

                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->


                    </div>
                    <div class="form-actions right">
                        <button type="button" class="btn default">Cancel</button>
                        <button type="submit" class="btn blue"><i class="fa fa-check"></i> Save</button>
                    </div>
                    {!! Form::close() !!}
                            <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@endsection
@push('site_footer')
        <!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{asset('private/assets/global/plugins/select2/select2.min.js')}}"></script>


<script type="text/javascript"
        src="{{asset('private/assets/global/plugins/bootstrap-summernote/summernote.min.js')}}"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{asset('private/assets/global/scripts/metronic.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/admin/layout/scripts/layout.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/admin/layout/scripts/quick-sidebar.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/admin/layout/scripts/demo.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/admin/pages/scripts/components-editors.js')}}"></script>
<script>
    jQuery(document).ready(function () {
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        QuickSidebar.init(); // init quick sidebar
        Demo.init(); // init demo features
       // TableAdvanced.init();
        ComponentsEditors.init();
    });
</script>
@endpush