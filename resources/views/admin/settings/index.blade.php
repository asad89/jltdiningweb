@extends('admin.master')
@push('site_head')
        <!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('private/assets/global/plugins/select2/select2.css')}}"/>
<link rel="stylesheet" type="text/css"
      href="{{asset('private/assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css')}}"/>
<link rel="stylesheet" type="text/css"
      href="{{asset('private/assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css')}}"/>
<link rel="stylesheet" type="text/css"
      href="{{asset('private/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('private/assets/global/plugins/bootstrap-summernote/summernote.css')}}">

<!-- END PAGE LEVEL STYLES -->
@endpush
@section('content')
        <!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <div class="" id="">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>Website Settings
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="javascript:;" class="reload">
                        </a>
                        <a href="javascript:;" class="remove">
                        </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    {!! Form::open(array('url' => '/admin/settings','class'=>'horizontal-form', 'files' => true)) !!}
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if (session()->has('flash_notification.message'))
                        <div class="alert alert-{{ session('flash_notification.level') }}">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {!! session('flash_notification.message') !!}
                        </div>
                    @endif
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('website',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('website',$setting->website,array('class'=>'form-control','placeholder'=>'Add Website Name',)) !!}
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('Info Email',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('info_email',$setting->info_email,array('class'=>'form-control','placeholder'=>'Info Email','id'=>'info_email')) !!}
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('Admin Email',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('admin_email',$setting->admin_email,array('class'=>'form-control','placeholder'=>'Admin Email','id'=>'admin_email')) !!}
                                </div>
                            </div>



                        </div>


                        <div class="row">

                            <!--/span-->
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('phone',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('contact',$setting->contact,array('class'=>'form-control','placeholder'=>'Add Website Phone')) !!}
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('Address',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('address',$setting->address,array('class'=>'form-control','placeholder'=>'Contact Address','id'=>'address')) !!}
                                </div>
                            </div>
                            <!--/span-->
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('Copyrights',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('copyrights',$setting->copyrights,array('class'=>'form-control','placeholder'=>'Copyrights')) !!}
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('Designed',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('designed',$setting->designed,array('class'=>'form-control','placeholder'=>'Designed & Developed','id'=>'designed')) !!}
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('Google Analytics',null,array('class'=>'control-label')) !!}
                                    {!! Form::textarea('google_analytics',$setting->google_analytics,array('class'=>'form-control','placeholder'=>'Google Analytics',)) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('Facebook Code',null,array('class'=>'control-label')) !!}
                                    {!! Form::textarea('facebook_code',$setting->facebook_code,array('class'=>'form-control','placeholder'=>'Facebook Code',)) !!}
                                </div>
                            </div>
                            <!--/span-->
                        </div>

                        <h3 class="form-section">Social Links</h3>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('Facebook Page',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('facebook',$setting->facebook,array('class'=>'form-control','placeholder'=>'Facebook Page','id'=>'facebook')) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('Linkedin Page',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('linkedin',$setting->linkedin,array('class'=>'form-control','placeholder'=>'LinkedIn','id'=>'linkedin')) !!}
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('Google Plus',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('google',$setting->google,array('class'=>'form-control','placeholder'=>'Google Plus','id'=>'google')) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('Instagram',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('instagram',$setting->instagram,array('class'=>'form-control','placeholder'=>'Instagram','id'=>'instagram')) !!}
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('Twitter',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('twitter',$setting->twitter,array('class'=>'form-control','placeholder'=>'Twitter','id'=>'twitter')) !!}
                                </div>
                            </div>
                          {{--  <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('Instagram',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('instagram',null,array('class'=>'form-control','placeholder'=>'Instagram','id'=>'instagram')) !!}
                                </div>
                            </div>--}}

                        </div>

                        <h3 class="form-section">SEO Related</h3>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('Meta Title',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('meta_title',$setting->meta_title,array('class'=>'form-control','placeholder'=>'Meta Title','id'=>'meta_title')) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('Meta Keywords',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('meta_keywords',$setting->meta_keywords,array('class'=>'form-control','placeholder'=>'Meta Keywords','id'=>'meta_keywords')) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="form-group">
                                        {!! Form::label('Meta Description',null,array('class'=>'control-label')) !!}
                                        {!! Form::textarea('meta_description',$setting->meta_description,array('class'=>'form-control','placeholder'=>'Meta Description','id'=>'meta_description')) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="form-group">
                                        {!! Form::label('Meta Robots',null,array('class'=>'control-label')) !!}
                                        {!! Form::select('meta_robots',array('1'=>'Index,Follow','0'=>'No Index, No follow',),null,array('class'=>'form-control')) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions right">
                        <button type="button" class="btn default">Cancel</button>
                        <button type="submit" class="btn blue"><i class="fa fa-check"></i> Save</button>
                    </div>
                    {!! Form::close() !!}
                            <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@endsection
@push('site_footer')
        <!-- BEGIN PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{asset('private/assets/global/plugins/select2/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('private/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('private/assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js')}}"></script>
<script type="text/javascript" src="{{asset('private/assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js')}}"></script>
<script type="text/javascript" src="{{asset('private/assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js')}}"></script>
<script type="text/javascript" src="{{asset('private/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{asset('private/assets/global/scripts/metronic.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/admin/layout/scripts/layout.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/admin/layout/scripts/quick-sidebar.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/admin/layout/scripts/demo.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/admin/pages/scripts/table-advanced.js')}}"></script>
<script>
    jQuery(document).ready(function() {
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        QuickSidebar.init(); // init quick sidebar
        Demo.init(); // init demo features
        TableAdvanced.init();
    });
</script>
@endpush