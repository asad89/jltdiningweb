@extends('private.master')
@push('site_head')
        <!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('private/assets/global/plugins/select2/select2.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('private/assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('private/assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('private/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}"/>
<!-- END PAGE LEVEL STYLES -->
@endpush
@section('content')    <div class="row">
        <div class="col-md-12">
             <div class="tab-pane active" id="tab_3">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>{{$page->name}}
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            <a href="#portlet-config" data-toggle="modal" class="config">
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                            <a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form class="form-horizontal" role="form">
                            <div class="form-body">
                                <h2 class="margin-bottom-20"> Viewing {{$page->name}}</h2>
                                <h3 class="form-section">Page Info</h3>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Page Name:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static">
                                                    {{$page->name}}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Page Title:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static">
                                                    {{$page->title}}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Page URL:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static">
                                                    {{$page->slug}}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Publish Date:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static">
                                                    {{$page->created_at}}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Page Status:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static">
                                                    @if($page->post_status == 1)
                                                        {{'Published'}}
                                                    @else
                                                        {{'Draft'}}
                                                    @endif
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                            <div class="form-group">
                                            <label class="control-label col-md-3">Last Updated:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static">
                                                    {{$page->updated_at}}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <h3 class="form-section">Page Content</h3>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Page Content:</label>
                                            <div class="col-md-9">
                                                {!! $page->post_content !!}
                                                <p class="form-control-static">

                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--/row-->

                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <i class="fa fa-pencil"></i> <a href="<?php echo url('/admin/post/edit', [$post->id]); ?>">Edit</a>
                                                <button type="button" class="btn default">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>


            </div>
        </div>
    </div>

@endsection





@push('site_footer')


        <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="{{asset('private/assets/global/plugins/select2/select2.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('private/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('private/assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('private/assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('private/assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('private/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{asset('private/assets/global/scripts/metronic.js')}}" type="text/javascript"></script>
    <script src="{{asset('private/assets/admin/layout/scripts/layout.js')}}" type="text/javascript"></script>
    <script src="{{asset('private/assets/admin/layout/scripts/quick-sidebar.js')}}" type="text/javascript"></script>
    <script src="{{asset('private/assets/admin/layout/scripts/demo.js')}}" type="text/javascript"></script>
    <script src="{{asset('private/assets/admin/pages/scripts/table-advanced.js')}}"></script>
    <script>
        jQuery(document).ready(function() {
            Metronic.init(); // init metronic core components
            Layout.init(); // init current layout
            QuickSidebar.init(); // init quick sidebar
            Demo.init(); // init demo features
            TableAdvanced.init();
        });
    </script>
@endpush