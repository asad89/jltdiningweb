@extends('admin.master')
@push('site_header')
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="{{asset('private/assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet"
      type="text/css"/>
<link href="{{asset('private/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}"
      rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
@endpush
@section('content')
        <!-- BEGIN PAGE HEADER-->

<!-- BEGIN PAGE BAR -->
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{url('/admin')}}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{url('admin/posts')}}">Posts</a>

        </li>

    </ul>

</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->

<!-- END PAGE TITLE-->


    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->


                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> {{'Post Listing'}}</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <a class="btn sbold green" href="{{url('/admin/posts/create')}}"><i class="fa fa-plus"></i>{{"Add New Post"}}</a>


                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="btn-group pull-right">
                                    <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a href="javascript:;">
                                                <i class="fa fa-print"></i> Print </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                        <thead>
                        @if (session()->has('flash_notification.message'))
                            <div class="alert alert-{{ session('flash_notification.level') }}">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {!! session('flash_notification.message') !!}
                            </div>
                        @endif
                        <tr>
                            <th>SR#</th>
                            <th> {{"Post Name"}} </th>
                            <th> {{"Post title"}} </th>
                            <th> {{"Post Slug"}} </th>
                            <th> {{"Published status"}} </th>
                            <th> {{"Published by"}} </th>
                            <th> {{"Published Date"}} </th>
                            <th> {{"Action"}} </th>
                        </tr>
                        </thead>
                        <tbody>



                        @if(isset($posts) && !empty($posts))
                            <?php $i = 1;?>
                            @foreach($posts as $post)
                                <tr class="odd gradeX">
                                    <td>{{--{{$i++}}--}}</td>
                                    <td> {{$post->name}} </td>
                                    <td>
                                        {{$post->title}}
                                    </td>

                                    <td>{{$post->slug}}</td>
                                    <td class="text-center">
                                        @if($post->status == 1)
                                            <span class="label label-sm label-success"> {{'Published'}} </span>

                                        @else
                                            <span class="label label-sm label-information"> {{'Draft'}} </span>

                                        @endif
                                    </td>


                                    <td>
                                        {{--{{$page->user->name}}--}}
                                    </td>
                                    <td class="center">{{date('d-M-Y',strtotime($post->created_at))}}</td>
                                    <td>
                                        <div class="btn-group">
                                            <button class="btn btn-xs green dropdown-toggle" type="button"
                                                    data-toggle="dropdown" aria-expanded="false"> Actions
                                                <i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul class="dropdown-menu pull-left" role="menu">
                                                <li>
                                                    <a href="<?php echo url('/admin/posts/' . $post->id . '/edit'); ?>">Edit</a>
                                                </li>
                                                <li>
                                                    {!! Form::open([
                                                       'method' => 'DELETE',
                                                       'route' => ['admin.posts.destroy', $post->id]
                                                    ]) !!}
                                                    {!! Form::submit('Delete?', ['class' => 'btn']) !!}
                                                    {!! Form::close() !!}
                                                </li>

                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection
@push('site_footer')
        <!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{asset('private/assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}"
        type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{asset('private/assets/pages/scripts/table-datatables-managed.min.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
@endpush