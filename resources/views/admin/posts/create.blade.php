@extends('admin.master')
@push('site_header')
        <!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<!-- Include the plugin's CSS and JS: -->
<script type="text/javascript" src="{{asset('js/bootstrap-multiselect.js')}}"></script>
<link rel="stylesheet" href="{{asset('css/bootstrap-multiselect.css')}}" type="text/css"/>

<link href="{{asset('private/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('private/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('private/assets/global/plugins/bootstrap-summernote/summernote.css')}}" rel="stylesheet" type="text/css" />
<script type="text/javascript">
   $(document).ready(function(){
       $('#summernote_1').summernote({
           height: 300,
           lang: "es-ES",
           disableDragAndDrop: true,
           toolbar: [
               // [groupName, [list of button]]
               ['style', ['bold', 'italic', 'underline', 'clear']],
               ['font', ['strikethrough', 'superscript', 'subscript']],
               ['fontname', ['fontname']],
               ['fontsize', ['fontsize']],
               ['color', ['color']],
               ['para', ['ul', 'ol', 'paragraph']],
               ['height', ['height']],
               ['para', ['ul', 'ol', 'paragraph']],
               ['height', ['height']],
               ['table', ['table']],
               ['insert', ['link', 'picture', 'hr']],
               ['view', ['fullscreen', 'codeview']],
               ['help', ['help']]
           ],
           fontNames: ['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New','Oxygen','Oxygen,sans-serif'],

           fontNamesIgnoreCheck: ['Oxygen','Oxygen,sans-serif'],
       });
   });
</script>
@endpush
@section('content')
        <!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <div class="" id="">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-plus"></i>Add New Post
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="javascript:;" class="reload">
                        </a>
                        <a href="javascript:;" class="remove">
                        </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    {!! Form::open(array('url' => '/admin/posts','class'=>'horizontal-form', 'files' => true)) !!}
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session()->has('flash_notification.message'))
                        <div class="alert alert-{{ session('flash_notification.level') }}">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {!! session('flash_notification.message') !!}
                        </div>
                    @endif

                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="hidden" name="post_type" value="1">
                                    <input type="hidden" name="user_id" value="{{Auth::user()->id}}">

                                    {!! Form::label('Post Name',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('name',null,array('class'=>'form-control','placeholder'=>'Add Post name')) !!}
                                    <span class="help-block">Add Post Name </span>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('Post Title',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('title',null,array('class'=>'form-control','placeholder'=>'Post Title','id'=>'title')) !!}
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('Category',null,array('class'=>'control-label')) !!}
                                    <select name="category[]" class="" id="category" multiple="multiple">
                                        @if(isset($categories) && count($categories))
                                            @foreach($categories as $category)
                                                <option value="{{$category->id}}">{{$category->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>


                            <!--/span-->
                        </div>
                        <!--/row-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('Content',null,array('class'=>'control-label')) !!}
                                    {!! Form::textarea('post_content',null,array('class'=>'form-control','placeholder'=>'Post Content','id'=>'summernote_1')) !!}
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('Publish Status',null,array('class'=>'control-label')) !!}
                                    {!! Form::select('publish_status',array('1'=>'Published','0'=>'Draft',),null,array('class'=>'form-control')) !!}

                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('Tags',null,array('class'=>'control-label')) !!}
                                    <select name="tags[]" class="" id="tags" multiple="multiple">
                                        @if(isset($tags) && count($tags))
                                            @foreach($tags as $tag)
                                                <option value="{{$tag->id}}">{{$tag->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>

                                </div>
                            </div>


                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('Featured Image',null,array('class'=>'control-label')) !!}
                                    {!! Form::file('feature_image', array('class'=>'btn btn-primary')) !!}
                                </div>
                            </div>
                        </div>
                        <!--/row-->
                        <!--/row-->
                        <h3 class="form-section">SEO Related</h3>

                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group">
                                    {!! Form::label('Meta Title',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('meta_title',null,array('class'=>'form-control','placeholder'=>'Meta Title','id'=>'meta_title')) !!}

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group">
                                    {!! Form::label('Meta Keywords',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('meta_keywords',null,array('class'=>'form-control','placeholder'=>'Meta Keywords','id'=>'meta_keywords')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="form-group">
                                        {!! Form::label('Meta Description',null,array('class'=>'control-label')) !!}
                                        {!! Form::textarea('meta_description',null,array('class'=>'form-control','placeholder'=>'Meta Description','id'=>'meta_description')) !!}
                                    </div>
                                </div>
                            </div>
                            </row>
                            <row class="row">
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="form-group">
                                            {!! Form::label('Meta Robots',null,array('class'=>'control-label')) !!}
                                            {!! Form::select('meta_robots',array('1'=>'Index,Follow','0'=>'No Index, No follow',),null,array('class'=>'form-control')) !!}

                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                        </div>
                        <!--/row-->


                    </div>
                    <div class="form-actions right">
                        <button type="button" class="btn default">Cancel</button>
                        <button type="submit" class="btn blue"><i class="fa fa-check"></i> Save</button>
                    </div>
                    {!! Form::close() !!}
                            <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@endsection
@push('site_footer')
        <!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{asset('private/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/bootstrap-markdown/lib/markdown.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/bootstrap-summernote/summernote.min.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{asset('private/assets/pages/scripts/components-editors.min.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="{{asset('js/bootstrap-multiselect.js')}}"></script>

<script>
    $(document).ready(function () {



        $('#tags,#category').multiselect({
            includeSelectAllOption: true,
            selectAllNumber: false,
            enableFiltering: true
        })
    });
</script>

@endpush