@extends('admin.master')
@push('site_header')
        <!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"
      href="{{asset('private/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}"/>
<link rel="stylesheet" type="text/css"
      href="{{asset('private/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>

<link href="{{asset('lib/jQuery.filer-master/css/jquery.filer.css')}}" type="text/css" rel="stylesheet"/>
<link href="{{asset('lib/jQuery.filer-master/css/themes/jquery.filer-dragdropbox-theme.css')}}" type="text/css"
      rel="stylesheet"/>

<!-- Include the plugin's CSS and JS: -->
<script type="text/javascript" src="{{asset('js/bootstrap-multiselect.js')}}"></script>
<link rel="stylesheet" href="{{asset('css/bootstrap-multiselect.css')}}" type="text/css"/>

<link href="{{asset('private/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css')}}" rel="stylesheet"
      type="text/css"/>
<link href="{{asset('private/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css')}}"
      rel="stylesheet" type="text/css"/>
<link href="{{asset('private/assets/global/plugins/bootstrap-summernote/summernote.css')}}" rel="stylesheet"
      type="text/css"/>
<script type="text/javascript">
    $(document).ready(function () {
        //$('div#premium').hide();
        $('#summernote_1').summernote({
            height: 300,
            lang: "es-ES",
            disableDragAndDrop: true,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'hr']],
                ['view', ['fullscreen', 'codeview']],
                ['help', ['help']]
            ],
            fontNames: ['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New', 'Oxygen', 'Oxygen,sans-serif'],
            fontNamesIgnoreCheck: ['Oxygen', 'Oxygen,sans-serif'],
        });
    });
</script>
@endpush
@section('content')
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <div class="" id="">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>{{'Edit Restaurant'}}
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                        <a href="javascript:;" class="reload"></a>
                        <a href="javascript:;" class="remove"></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    {!! Form::open(array('url' => '/admin/hotels/'.$hotel->id,'class'=>'horizontal-form','files' => true,'enctype'=>'multipart/form-data','method' => 'PATCH')) !!}
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-body">
                        @if (session()->has('flash_notification.message'))
                            <div class="alert alert-{{ session('flash_notification.level') }}">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">&times;</button>
                                {!! session('flash_notification.message') !!}
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="hidden" name="status" value="1">
                                    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                    {!! Form::label('Restaurant name',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('name',$hotel->name,array('class'=>'form-control','placeholder'=>'Add restaurant name')) !!}
                                    <span class="help-block">Add restaurant </span>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('Advertisement Type',null,array('class'=>'control-label')) !!}
                                    {!! Form::select('ad_type',[0 => 'Free',1 => 'Premium'],$hotel->ad_type,['class'=>'form-control','id' => 'ad_type']) !!}
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('Restaurant Logo',null,array('class'=>'control-label')) !!}
                                    {!! Form::file('logo', array('class'=>'btn btn-primary')) !!}
                                    <span class="help-block">Recommended size 150 X 150 </span>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('Restaurant Image',null,array('class'=>'control-label')) !!}
                                    {!! Form::file('image', array('class'=>'btn btn-primary')) !!}
                                    <span class="help-block">Recommended size 580 X 408</span>
                                </div>
                            </div>
                            <!--/span-->



                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <style>
                                        .black-bg {
                                            background: #000;
                                        }
                                    </style>
                                   <img src="{{asset('uploads/hotels/logo/'.$hotel->logo)}}" alt="No Logo Available" title="{{$hotel->image}}" width="200" height="200">
                                    {{-- @if(file_exists('uploads/hotels/logo/' . $hotel->logo))
                                    @laravelImage('uploads/hotels/logo/', $hotel->logo, 150, 150, [
                                                      'fit' => 'crop-center'
                                                          ], [
                                                          'alt' => $hotel->name,
                                                          'class' => 'black-bg'

                                                       ])
                                     @endif--}}

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="">
                                    <img src="{{asset('uploads/hotels/large/'.$hotel->image)}}" width="200" height="200">
                                </div>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('Restaurant Meals',null,array('class'=>'control-label')) !!}
                                    {{ Form::select('meals[]', $meals, $selected_meals, ['class' => 'control-label','multiple' => 'multiple','id' => 'meals']) }}
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('Cuisines',null,array('class'=>'control-label')) !!}
                                    {{ Form::select('cuisines[]', $cuisines, $selected_cuisines, ['class' => 'control-label','multiple' => 'multiple','id' => 'cuisines']) }}
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('Features',null,array('class'=>'control-label')) !!}
                                    {{ Form::select('features[]', $features, $selected_features, ['class' => 'control-label','multiple' => 'multiple','id' => 'features']) }}
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('Tags',null,array('class'=>'control-label')) !!}
                                    {{ Form::select('tags[]', $tags, $selected_tags, ['class' => 'control-label','multiple' => 'multiple','id' => 'tags']) }}
                                </div>
                            </div>

                            <?php
                            $cluster = [
                                    'a' => 'A', 'b' => 'B', 'c' => 'C', 'd' => 'D', 'e' => 'E','f' => 'F', 'g'  => 'G', 'h' => 'H',
                                    'i' => 'I', 'j' => 'J', 'k' => 'K', 'l' => 'L', 'm' => 'M', 'n' => 'N','o' => 'O','p' => 'P',
                                    'q' => 'Q', 'r' => 'R', 's' => 'S', 't' => 'T', 'u' => 'U', 'v' => 'V', 'w' => 'W','x' => 'X',
                                    'y' => 'Y', 'z' => 'Z','aa' =>'AA', 'bb' => 'BB', 'one-jlt' => 'One JLT','almas-tower' => 'Almas Tower','jlt-park' => 'JLT Park','taj-jlt' => 'Taj JLT'
                            ];
                            ?>

                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('Cluster',null,array('class'=>'control-label')) !!}
                                    {{ Form::select('cluster', $cluster, $hotel->cluster, ['class' => 'form-control','placeholder' => 'Choose Cluster']) }}
                                </div>
                            </div>



                        </div>


                        <!--/row-->
                        <h3 class="form-section">About Restaurant</h3>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('Other Details',null,array('class'=>'control-label')) !!}
                                    {!! Form::textarea('details',$hotel->details,array('class'=>'form-control','placeholder'=>'Other Details','id'=>'summernote_1')) !!}
                                </div>
                            </div>
                            <!--/span-->
                        </div>


                        <!--/row-->
                        <h3 class="form-section">Address</h3>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('Restaurant Phone No.',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('phone',$hotel->phone,array('class'=>'form-control','placeholder'=>'Phone Number','id'=>'phone')) !!}
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('Restaurant Contact Email',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('email',$hotel->email,array('class'=>'form-control','placeholder'=>'Email','id'=>'email')) !!}
                                </div>
                            </div>
                        </div>


                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('Booking Url',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('booking_url',$hotel->booking_url,array('class'=>'form-control','placeholder'=>'Booking Url','id'=>'booking_url')) !!}
                                </div>
                            </div>


                            <div class="col-md-8">
                                <div class="form-group">
                                    {!! Form::label('Street Address',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('address',$hotel->address,array('class'=>'form-control','placeholder'=>'Street Address','id'=>'address')) !!}
                                </div>
                            </div>
                        </div>

                        <!--/row-->
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('Landmark',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('landmark',$hotel->landmark,array('class'=>'form-control','placeholder'=>'Landmark','id'=>'landmark')) !!}
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('Pin Code',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('pincode',$hotel->pincode,array('class'=>'form-control','placeholder'=>'Hotel Pin Code','id'=>'pincode')) !!}
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('Are you the Owner?',null,array('class'=>'control-label')) !!}
                                    {!! Form::select('owner',[0 => 'Yes' , 1 => 'No'],$hotel->owner,['class' =>  'form-control']) !!}
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('Restaurant Website',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('website',$hotel->website,array('class'=>'form-control','placeholder'=>'http://www.your-website.com','id'=>'website')) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('Working Days',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('working_days',$hotel->working_days,array('class'=>'form-control','placeholder'=>'Working Days - Sat-Sun','id'=>'working_days')) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('Opening Time (24 hours clock)',null,array('class'=>'control-label')) !!}
                                    <div class="input-icon">
                                        <i class="fa fa-clock-o"></i>
                                        {!! Form::text('opening_time',$hotel->opening_time,array('class'=>'form-control timepicker timepicker-24','placeholder'=>'Opening Time','id'=>'opening_time')) !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('Closing Time (24 hours clock)',null,array('class'=>'control-label')) !!}
                                    <div class="input-icon">
                                        <i class="fa fa-clock-o"></i>
                                        {!! Form::text('closing_time',$hotel->closing_time,array('class'=>'form-control timepicker timepicker-24','placeholder'=>'closing_time','id'=>'closing_time')) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('Latitude',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('latitude',$hotel->latitude,array('class'=>'form-control','placeholder'=>'Latitude','id'=>'latitude')) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('Longitude',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('longitude',$hotel->longitude,array('class'=>'form-control','placeholder'=>'Longitude','id'=>'longitude')) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('Video Link (YouTube)',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('video',$hotel->video,array('class'=>'form-control','placeholder'=>'http://www.youtube.com','id'=>'video')) !!}
                                    <span class="help-block red">YouTube video links only</span>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('Status',null,array('class'=>'control-label')) !!}
                                    {!! Form::select('status',[0 => 'Draft' , 1 => 'Active'],$hotel->status,['class' =>  'form-control']) !!}


                                    <span class="help-block red">Mark active or draft for listing</span>
                                </div>
                            </div>




                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('Menu',null,array('class'=>'control-label')) !!}
                                    {!! Form::file('menu', array('class'=>'btn btn-primary')) !!}
                                    <a href="{{asset('uploads/hotels/menu/'.$hotel->menu)}}">Download</a>


                                    <span class="help-block red">PDF files only</span>
                                </div>
                            </div>
                        </div>


                        <h3 class="form-section">Social Links</h3>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('Facebook Page',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('facebook',$hotel->facebook,array('class'=>'form-control','placeholder'=>'https://www.facebook.com/your-page','id'=>'facebook')) !!}
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('Twitter',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('twitter',$hotel->twitter,array('class'=>'form-control','placeholder'=>'https://twitter.com/','id'=>'twitter')) !!}
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('Linked In',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('linkedin',$hotel->linkedin,array('class'=>'form-control','placeholder'=>'https://www.linkedin.com/','id'=>'	linkedin')) !!}
                                </div>
                            </div>
                            <!--/span-->


                            <!--/span-->
                        </div>


                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('Google+',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('google',$hotel->google,array('class'=>'form-control','placeholder'=>'https://plus.google.com/','id'=>'google')) !!}
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('Instagram',null,array('class'=>'control-label')) !!}
                                    {!! Form::text('instagram',$hotel->instagram,array('class'=>'form-control','placeholder'=>'https://www.instagram.com/','id'=>'instagram')) !!}
                                </div>
                            </div>
                            <!--/span-->
                        </div>



                        <div class="row">
                            <div class="form-group clearfix" id="premium">
                                <div class="col-sm-12 padding-left-0 padding-right-0">
                                    <input type="file" name="files[]" id="filer_input2" multiple="multiple">
                                </div>
                            </div>
                        </div>

                            <h3 class="form-section">SEO Related</h3>

                            <div class="row">
                                <div class="col-md-12 ">
                                    <div class="form-group">
                                        {!! Form::label('Meta Title',null,array('class'=>'control-label')) !!}
                                        {!! Form::text('meta_title',$hotel->meta_title,array('class'=>'form-control','placeholder'=>'Meta Title','id'=>'meta_title')) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 ">
                                    <div class="form-group">
                                        {!! Form::label('Meta Keywords',null,array('class'=>'control-label')) !!}
                                        {!! Form::text('meta_keywords',$hotel->meta_keywords,array('class'=>'form-control','placeholder'=>'Meta Keywords','id'=>'meta_keywords')) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="form-group">
                                            {!! Form::label('Meta Description',null,array('class'=>'control-label')) !!}
                                            {!! Form::textarea('meta_description',$hotel->meta_description,array('class'=>'form-control','placeholder'=>'Meta Description','id'=>'meta_description')) !!}
                                        </div>
                                    </div>
                                </div>
                                </row>
                                <row class="row">
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-group">
                                                {!! Form::label('Meta Robots',null,array('class'=>'control-label')) !!}
                                                {!! Form::select('meta_robots',array('1'=>'Index,Follow','0'=>'No Index, No follow',),$hotel->meta_robots,array('class'=>'form-control')) !!}

                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                            </div>


                    </div>
                    <div class="form-actions right">
                        <button type="button" class="btn default">Cancel</button>
                        <button type="submit" class="btn blue"><i class="fa fa-check"></i> Save</button>
                    </div>
                    {!! Form::close() !!}
                            <!-- END FORM-->


                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@endsection
@push('site_footer')
        <!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{asset('private/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js')}}"
        type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js')}}"
        type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/bootstrap-markdown/lib/markdown.js')}}"
        type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js')}}"
        type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/bootstrap-summernote/summernote.min.js')}}"
        type="text/javascript"></script>
<script type="text/javascript"
        src="{{asset('private/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript"
        src="{{asset('private/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}"></script>
<script type="text/javascript"
        src="{{asset('private/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{asset('private/assets/pages/scripts/components-date-time-pickers.js')}}"></script>

<script type="text/javascript" src="{{asset('js/jquery-ui.js')}}"></script>
{{--<script type="text/javascript" src="{{asset('js/uploader.js')}}"></script>--}}
<script type="text/javascript" src="{{asset('js/jquery.filer.js')}}"></script>
<script src="{{asset('js/edit-hotel.js')}}"></script>
<script src="{{asset('js/bootstrap-multiselect.js')}}"></script>
<script>

    $(document).ready(function () {
       // ComponentsEditors.init();


        var ad_type = $('#ad_type').val();

        $('div#premium').hide();
        if (!ad_type == 0) {
            $('#premium').show();
        }
        else {
            $('#premium').hide();
        }

        $('#ad_type').change(function () {
            var ad_type = $('#ad_type').val();
            if (ad_type == 1)
                $('#premium').show();
            else
                $('#premium').hide();
        });


        'use-strict';
        $("#filer_input2").filer({
            limit: 10,
            maxSize: 5,
            extensions: null,
            changeInput: '<div class="jFiler-input-dragDrop"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="icon-jfi-cloud-up-o"></i></div><div class="jFiler-input-text"><h3>Drag&Drop files here</h3> <span style="display:inline-block; margin: 15px 0">or</span></div><a class="jFiler-input-choose-btn blue">Browse Files</a></div></div>',
            showThumbs: true,
            theme: "dragdropbox",
            templates: sett,
            addMore: true,
            clipBoardPaste: true,
            excludeName: null,
            beforeRender: null,
            afterRender: null,
            beforeShow: null,
            beforeSelect: null,
            onSelect: null,
            afterShow: null,
            files: [

                    @foreach($hotel->get_hotel_images as $hotel_img)
                    {
                    name: "{{$hotel->name}}",
                    id: "{{$hotel_img->id}}",
                    size: '',
                    type: "image/jpg",
                    file: "{{asset('uploads/hotels/premium/'.$hotel_img->image)}}",
                    url: "{{asset('uploads/hotels/premium/'.$hotel_img->image)}}"
                },

                @endforeach

            ],
            onRemove: function (itemEl, file, id, listEl, boxEl, newInputEl, inputEl) {
                var file = file.id;
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: '{{ url('admin/delete-hotel-image/') }}',
                    method: 'POST',
                    data: {file: file},
                    success: function (data) {
                        alert(data);
                    }
                });
            },
            showThumbs: true,
            onEmpty: null,
            options: null,
            captions: {
                button: "Choose Files",
                feedback: "Choose files To Upload",
                feedback2: "files were chosen",
                drop: "Drop file here to Upload",
                removeConfirmation: "Are you sure you want to remove this file?",
                errors: {}
            }
        })
    });

    $('#meals,#dishes,#cuisines,#tags,#features').multiselect({
        includeSelectAllOption: true,
        selectAllNumber: false,
        enableFiltering: true
    });
</script>

@endpush