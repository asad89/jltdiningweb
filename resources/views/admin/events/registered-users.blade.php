@extends('admin.master')
@push('site_header')
        <!-- BEGIN PAGE LEVEL STYLES -->
<link href="{{asset('private/assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet"
      type="text/css"/>
<link href="{{asset('private/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}"
      rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
@endpush
@section('content')
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
                <i class="fa fa-circle"></i>
            </li>
           {{-- <li>
                <a href="{{url('admin/contact')}}">Registerd Parti</a>
            </li>--}}
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12">


            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box blue-madison">


                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-envelope-o"></i>{{'Registered Participants'}}
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="reload">
                        </a>
                        <a href="javascript:;" class="remove">
                        </a>
                    </div>
                </div>


                <div class="portlet-body">
                    @if (session()->has('flash_notification.message'))
                        <div class="alert alert-{{ session('flash_notification.level') }}">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {!! session('flash_notification.message') !!}
                        </div>
                    @endif
                    <table class="table table-striped table-bordered table-hover table-checkable order-column"
                           id="sample_1">
                        <thead>

                        <tr>
                            <th>SR#</th>
                            <th>
                                First Name
                            </th>

                            <th>
                                Last Name
                            </th>

                            <th>
                                Email
                            </th>
                            <th>
                                Phone
                            </th>
                            <th>
                                Delete
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        // print_r($restaurant);
                        ?>

                        @if(isset($users) && !empty($users))
                            @foreach($users as $person)
                                <tr class="odd gradeX">

                                    <td></td>
                                    <td>
                                        {{$person->first_name}}
                                    </td>
                                    <td>
                                        {{$person->last_name}}
                                    </td>
                                    <td>
                                        {{$person->email}}
                                    </td>
                                    <td>
                                        {{$person->phone}}
                                    </td>

                                    <td>
                                        {!! Form::open([
                                               'method' => 'DELETE',
                                               'route' => ['admin.contact.destroy', $person->id]])
                                       !!}
                                        {!! Form::submit('Delete?', ['class' => 'btn btn-danger']) !!}
                                        {!! Form::close() !!}

                                    </td>
                                </tr>
                            @endforeach
                        @endif


                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->



        </div>
    </div>
    @endsection
    @push('site_footer')
            <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{asset('private/assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{asset('private/assets/global/plugins/datatables/datatables.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('private/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}"
            type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{asset('private/assets/pages/scripts/table-datatables-managed.min.js')}}"
            type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    @endpush