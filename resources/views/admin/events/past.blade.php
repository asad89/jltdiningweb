@extends('admin.master')
@push('site_header')
        <!-- BEGIN PAGE LEVEL STYLES -->
<link href="{{asset('private/assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet"
      type="text/css"/>
<link href="{{asset('private/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}"
      rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
@endpush
@section('content')
        <!-- BEGIN PAGE BAR -->
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{url('/')}}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{url('admin/pages')}}">Events</a>

        </li>

    </ul>

</div>
<div class="row">
    <div class="col-md-12">


        <!-- BEGIN EXAMPLE TABLE PORTLET-->

        <div class="portlet-title">
            <div class="caption font-dark">
                <i class="icon-settings font-dark"></i>
                <span class="caption-subject bold uppercase"> {{'Past Event'}}</span>
            </div>
            <div class="portlet-body">
                @if (session()->has('flash_notification.message'))
                    <div class="alert alert-{{ session('flash_notification.level') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {!! session('flash_notification.message') !!}
                    </div>
                @endif
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <a class="btn sbold green" href="{{route('admin.events.create')}}"><i class="fa fa-plus"></i>{{"Add New Event"}}</a>


                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="btn-group pull-right">
                                <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:;">
                                            <i class="fa fa-print"></i> Print </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                    <thead>

                    <tr>
                        <th>SR#</th>
                        <th>
                            Event Name
                        </th>

                        <th>
                            Event Title
                        </th>

                        <th>
                            Event Slug
                        </th>
                        <th>
                            Event status
                        </th>
                        <th>
                            Ending At
                        </th>
                        <th>
                            Publish Date
                        </th>

                        <th>
                            Edit
                        </th>
                        <th>
                            Delete
                        </th>
                    </tr>
                    </thead>
                    <tbody>

                    @if(isset($events) && !empty($events))
                        <?php $i = 1;?>
                        @foreach($events as $item)
                            <tr class="odd gradeX">
                                <td>{{$i++}}</td>
                                <td>
                                    {{$item->name}}
                                </td>
                                <td>
                                    {{$item->title}}
                                </td>
                                <td>
                                    {{$item->slug}}
                                </td>
                                <td class="text-center">
                                    <span class="label label-sm label-success"> {{'Past'}} </span>

                                </td>
                                <td>
                                    {{date('d-M-Y',strtotime($item->created_at))}}
                                </td>

                                <td>
                                    <a href="<?php echo url('/admin/events/'.$item->id.'/edit'); ?>">Edit</a>
                                </td>
                                <td>
                                    {!! Form::open([
                                         'method' => 'DELETE',
                                         'route' => ['admin.events.destroy', $item->id]
                                 ]) !!}
                                    {!! Form::submit('Delete?', ['class' => 'btn btn-danger']) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    @endif


                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
@endsection
@push('site_footer')
        <!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{asset('private/assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('private/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}"
        type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{asset('private/assets/pages/scripts/table-datatables-managed.min.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
@endpush