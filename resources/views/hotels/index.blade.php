@extends('master')
@section('meta_tags')
    <title>Restaurants, Lounges & Cafes in Jumeirah Lakes Towers | JLT Dining</title>
    <meta name="keywords" content="jumeirah lakes towers restaurants, restaurants in dubai, dubai restaurants, jlt restaurants, best restaurants in jlt, cafes in jlt, shisha in jlt">
    <meta name="description" content="Jumeirah Lakes Towers in Dubai boasts over 200 independent Restaurants, Lounges, and Cafes. Find the best Restaurants in JLT Dubai. ">
    <link rel="canonical" href="https://www.jltdining.com/restaurants/" />
@endsection
@push('site-head')
{{--<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>--}}
<script src="{{asset('js/jquery-1.11.3.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jssor.slider-21.1.6.mini.js')}}" type="text/javascript"></script>
<!-- Include the plugin's CSS and JS: -->
<script type="text/javascript" src="{{asset('js/bootstrap-multiselect.js')}}"></script>
<link rel="stylesheet" href="{{asset('css/bootstrap-multiselect.css')}}" type="text/css"/>
<script type="text/javascript">
    $(document).ready(function() {
        $("#advanced-search").on("click", function () {
            $("#advanced").toggle();                 // .fadeToggle() // .slideToggle()
        });
    });
</script>
<style type="text/css">
    #advanced {
        display: none;
    }
    .form-inline .input-group {
        width:auto !important;
    }
</style>
@endpush
@section('content')
<!--==================================Section Open=================================-->
<!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script> -->
<div class="clearfix"></div>
<!--==================================Header Open=================================-->
<header class="">
    <!-- Popup Open -->
    <div class="md-modal md-effect-3" id="modal-2">
        <div class="container">
            <div class="md-content ">
                <div class="row popup-inner-left-padding ">
                    <div class="col-md-6 lp-insert-data">
                    </div>
                    <div class="col-md-6">
                        <div id="quickmap" class="quickmap"></div>
                        <a class="md-close widget-map-click"><i class="fa fa-close"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Popup Close -->
    <div class="md-overlay"></div> <!-- Overlay for Popup -->
</header>
<!--==================================Header Close=================================-->
<!--==================================Section Open=================================-->
<div id="page">
    <section class="brown-bg">
    <div class="container page-container">
        <div class="row">
            <div class="col-md-12 search-row margin-top-subtract-30  margin-bottom-30">
                {{ Form::open(['url' => '/search', 'method' => 'POST',"class"=> "form-inline clearfix"]) }}
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon lp-border"><i class="fa fa-search"></i></div>
                        {{ Form::text('q', '', ['id' =>  'q', 'placeholder' =>  'What do you fancy? Cuisines, dishes, restaurants, moods...','class' =>'lp-search-input lp-home-search-input'])}}
                        <button type="submit" class="btn lp-search-btn">
                            <!-- Search icon by Icons8 -->
                            <img class="icon icons8-Search"
                                 src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADQAAAA0CAYAAADFeBvrAAAC5klEQVRoge2aXbHjMAyFCyEQAiEQAqEQAqEQwiAQCiEQAqEQFkIgfPtgZ5vKcn5sOb27s5rpk135HEmWZTm3W4YAFdAAd+ABDEDvfx3Q5ui/RDyJDpiAmWMyesLVt/H/EU9kOEEiJgNQ/wtEAmLfIjNGAM24sOuBFreXKsJ9NW2QenFVGHpQvyJETu8H/5+YvqYUj2XxhjDEkogouh8Rb5Uh5UNGWnIG7oZrxAxWW62xLKTtmZf5Qu+1XnIt60X6K+Pbk5Kessl+inLTMNtYt1H2U22hWHqnz1aavvYzV6HmncvKFPREVOcolKm0N0N7HEMnMDxylMnMVttBPYyhEhimHEXrcBuVOW0u4INYntmGxdVhm+GWbK3zWGTYtRZK7mJ8SavFk4Ri3PP7iDAhNJHxsgXkTd1HfYoSeQbUYnxIttZ5LJLQ+fNoBXiRSowvGXA2Q76NJ5uQ9JAMuWdszFqAWmA5X9fhbpZracX42oO21XCIRdZ1XYqSzcyiEC5WsLKTcY8qkRtx3BkvdqXAqmJRFMnEIBOH+e2SsGJJD2/F1Z0yR94uTT1FGNp9jrIgrJQ5MgMtkn3DJLyO50cAYWHYK3OkFdcAkjtChEdHUCCnKJVeAiWkcKlVht8HGETDfosoYQdoxqq6VywVdb0yV/PapBllpUO7qdq2iBXrb7ZrcWHY40L2iQu9dovIiozWMrOt6tFbS9YZLdZmLnPvQm8tZYeDN1avGOyDlJ/XYlmVoLdrF291CUTaiFf25GVGDL1du5blZa7FJw//n5r3k8oYMQw4b0wHiZ0y4h4xWfrkyse59S1SNeHhm0JkJLxzxfZsWVIrYnsvc5LEhEsGdURnrAK5hpQCpuPzLBp4763dcyWBUFlSFkJaBuy+jTsqiV768aRi76//Sf0oySBVvLubLImk8i+GJSWB1CWvJFlyktT13w+lyEFSl74LZ8sOqUs+QTCXCKm/k8wiuMp8wFXtwceEvwElpHadBDbELgAAAABJRU5ErkJggg=="
                                 width="18" height="18" style="margin-right: 5px;">Search
                        </button>
                    </div>
                    <div class="input-group">
                        <a id="advanced-search" class="pull-right" style="text-decoration: underline;margin-top:-10px;">Advanced
                            Search</a>
                        </div>
                    </div>
                {{Form::close()}}
                <div class="row" id="advanced" style="position: absolute;width:100%;margin-top:15px;z-index:100;">
                    <div class="col-md-10 col-md-offset-1 search-row margin-top-subtract-35  margin-bottom-35">
                        {{ Form::open(['url' => '/search', 'method' => 'POST',"class"=> "form-inline clearfix"]) }}
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon lp-border"><i class="fa fa-crosshairs"></i></div>
                                    <select name="meals[]" class="" id="meals" multiple="multiple">
                                        @if(isset($meals) && count($meals))
                                            @foreach($meals as $meal)
                                                <option value="{{$meal->slug}}">{{$meal->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon lp-border"><i class="fa fa-cutlery"></i></div>
                                    <select name="cuisines[]" class="" id="cuisines" multiple="multiple">
                           @if(isset($cuisines) && count($cuisines))
                                            @foreach($cuisines as $cuisine)
                                                <option value="{{ $cuisine->slug }}">{{$cuisine->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group margin-right-0">
                            <div class="input-group margin-right-0">
                                <div class="input-group-addon lp-border"><i class="fa fa-list"></i></div>
                                <select name="features[]" class="" id="tags" multiple="multiple">
                                    @if(isset($features) && count($features))
                                        @foreach($features as $feature)
                                            <option value="{{ $feature->slug }}">{{$feature->name }}</option>
                                        @endforeach
                                    @endif
                                </select>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center padding-bottom-15">
                                {{ Form::submit('Search', array('class' => 'lp-search-btn')) }}
                            </div>
                        </div>
                        {{Form::close()}}
                    </div>
                </div>
            </div>
        </div>

        <div class="row listing-page-result-row margin-bottom-25">
           <div class="container breadcrumb-wrap">
               <div class="col-md-12 col-sm-12 text-left breadcrumb">
                       <p>Viewing <?php echo count($hotels).' / '.$total_hotel ?> Restaurants!</p>
               </div>
           </div>
        </div>
        <div class="mobile-map-space">
            <!-- Popup Open -->
            <div class="md-modal md-effect-3 mobilemap" id="modal-4">
                <div class="md-content mapbilemap-content">
                    <div id='map' class="listingmap"></div>
                    <a class="md-close mapbilemap-close"><i class="fa fa-close"></i></a>
                </div>
            </div>
            <!-- Popup Close -->
            <!-- Popup Open -->
            <div class="md-modal md-effect-3" id="modal-2">
                <div class="container">
                    <div class="md-content ">
                        <div class="row popup-inner-left-padding ">
                            <div class="col-md-6 lp-insert-data">
                            </div>
                            <div class="col-md-6">
                                <div id="quickmap" class="quickmap"></div>
                                <a class="md-close widget-map-click"><i class="fa fa-close"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Popup Close -->
            <div class="md-overlay md-overlayi"></div> <!-- Overlay for Popup -->
        </div>

        <?php
        //Columns must be a factor of 12 (1,2,3,4,6,12)
        $numOfCols = 4;
        $rowCount = 0;
        $bootstrapColWidth = 12 / $numOfCols;
        ?>
        <div class="row">
        @foreach($hotels as $hotel)
            <div class="col-md-3 col-sm-6 lp-grid-box-contianer lp-grid-box-contianer1" data-title="{{$hotel->title}}" data-reviews="4" data-number="{{$hotel->phone}}" data-email="{{$hotel->email}}" data-website="{{$hotel->website}}" data-price="{{$hotel->address}}" data-pricetext="{{$hotel->landmark}}" data-description="{{ strip_tags($hotel->details,'<p>')}}" data-userimage="images/user-thumb-94x94.png" data-username="JLT Dining" data-userlisting="14" data-fb="{{$hotel->facebook}}" data-gplus="{{$hotel->google}}" data-linkedin="{{$hotel->linkedin}}" data-instagram="{{$hotel->instagram}}" data-twitter="{{$hotel->twitter}}" data-lattitue="{{$hotel->latitude}}" data-longitute="{{$hotel->longitude}}"  data-id="{{$hotel->id}}"  data-posturl="" data-authorurl="#">
                    <div class="lp-grid-box lp-border lp-border-radius-8">
                        <div class="lp-grid-box-thumb-container">
                            <div class="lp-grid-box-thumb">
                                @if(!empty($hotel->image))
                                    @laravelImage('uploads/hotels/large/', $hotel->image, 272, 231, [
                                            'fit' => 'crop-top-center'
                                             ], [
                                             'alt' => $hotel->name,
                                              'class' => 'img-responsive'
                                            ])
                                @else
                                    @laravelImage('uploads/restaurant/', 'coming-soon.png', 271, 231, [
                                           'fit' => 'crop-top-center'
                                            ], [
                                            'alt' => $hotel->name,
                                             'class' => 'img-responsive'
                                           ])
                                @endif
                            </div><!-- ../grid-box-thumb -->
                            <div class="lp-grid-box-quick">
                                <ul class="lp-post-quick-links">
                                    <li>
                                        <a class="icon-quick-eye md-trigger" data-modal="modal-2"><i class="fa fa-eye"></i></a>
                                    </li>
                                </ul>
                            </div><!-- ../grid-box-quick-->
                        </div>
                        <div class="lp-grid-box-description ">
                            <h4 class="lp-h4">
                                <a href="{{url('/restaurants/'.$hotel->slug)}}">
                                    {{$hotel->name}}
                                </a>
                            </h4>

                            <p>
                                <i class="fa fa-map-marker"></i>
                                <span>{{$hotel->address}}</span>
                            </p>

                        </div><!-- ../grid-box-description-->

                    </div><!-- ../grid-box -->
                </div>

            <?php
            $rowCount++;
            if($rowCount % $numOfCols == 0) echo '</div><div class="row">';
            ?>
        @endforeach
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav aria-label="Page navigation" class="pull-right">
                        {{ $hotels->links() }}
                    </nav>
                </div>
            </div>
        </div>


        </div>



</section>
</div>

<!--==================================Section Close=================================-->
@endsection

@push('site_footer')
<script>
    $(function () {
        $("#q").autocomplete({
            source: "{{ url('search/autocomplete') }}",
            minLength: 1,
            maxLength: 5,
            select: function (event, ui) {
                $('#q').val(ui.item.value);
            }
        });
    });

    $(".custom.combobox").autocomplete({
        select: function (event, ui) {
        }
    });
</script>
<script src="{{asset('js/bootstrap.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('js/bootstrap-multiselect.js')}}"></script>
<script type="text/javascript">
    // Carousel Auto-Cycle
    $(document).ready(function () {
        $('#meals').multiselect({
            includeSelectAllOption: true,
            selectAllNumber: false,
            nonSelectedText:'Moods',
            enableFiltering: true
        });
        $('#cuisines').multiselect({
            includeSelectAllOption: true,
            selectAllNumber: false,
            nonSelectedText:'Cuisines',
            enableFiltering: true
        });
        $('#tags').multiselect({
            includeSelectAllOption: true,
            selectAllNumber: false,
            nonSelectedText:'Features',
            enableFiltering: true
        });

        $('#dishes').multiselect({
            includeSelectAllOption: true,
            selectAllNumber: false,
            enableFiltering: true
        });
    });
</script>
@endpush