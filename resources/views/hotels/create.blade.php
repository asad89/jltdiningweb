@extends('master')
@section('meta_tags')
    <title>{{config('app.meta_title')}}</title>
    <meta name="keywords" content="{{config('app.meta_keywords')}}">
    <meta name="description" content="{{config('app.meta_description')}}">
    @endsection
@section('content')
        <!--==================================Section Open=================================-->
<section class="aliceblue">
    <div class="container page-container-third">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-0">
                <div class="form-page-heading">
                    <h3>Add your Listing - FREE</h3>

                    <p>Add details about your listing</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-12 col-sm-offset-0">
                <div class="lp-border-radius-8 lp-border lp-form-container">
                    <div class="lp-form-row clearfix lp-border-bottom margin-bottom-40 padding-bottom-40">
                        <div class="lp-form-row-left text-left pull-left">
                            <img src="images/user-thumb-94x94.png" alt="user-thumb-94x94"/>

                            <p>You are currently signed in as <strong>{{--{{ Auth::user()->name}}--}}</strong></p>
                        </div>
                        <div class="lp-form-row-right text-right pull-right  margin-top-35">
                            <a href="{{ url('/logout') }}" class="btn-first-hover lp-signout-btn">Sign out</a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        {!! Form::open(array('url' => '/admin/hotels','class'=>'horizontal-form','files' => true)) !!}
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="hidden" name="status" value="1">
                                        <input type="hidden" name="user_id" value="1">

                                        {!! Form::label('hotel_name',null,array('class'=>'control-label')) !!}
                                        {!! Form::text('name',null,array('class'=>'form-control','placeholder'=>'Add hotel name')) !!}
                                        <span class="help-block">Add restaurant </span>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('Advertisement Type',null,array('class'=>'control-label')) !!}
                                        <select name="ad_type" class="form-control">
                                            <option value="0" selected>Free</option>
                                            <option value="1">Premium</option>
                                        </select>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->


                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('Hotel Logo',null,array('class'=>'control-label')) !!}
                                        {!! Form::file('logo', array('class'=>'btn btn-primary')) !!}
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('Hotel Image',null,array('class'=>'control-label')) !!}
                                        {!! Form::file('image', array('class'=>'btn btn-primary')) !!}
                                    </div>
                                </div>
                                <!--/span-->
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('Hotel Meals',null,array('class'=>'control-label')) !!}

                                        <select name="meals[]" class="" id="meals" multiple="multiple">
                                            @if(isset($meals) && count($meals))
                                                @foreach($meals as $meal)
                                                    <option value="{{$meal->id}}">{{$meal->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('Cuisines',null,array('class'=>'control-label')) !!}
                                        <select name="cuisines[]" class="" id="cuisines" multiple="multiple">
                                            @if(isset($cuisines) && count($cuisines))
                                                @foreach($cuisines as $cuisine)
                                                    <option value="{{$cuisine->id}}">{{$cuisine->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('Features',null,array('class'=>'control-label')) !!}
                                        <select name="features[]" class="" id="tags" multiple="multiple">
                                            @if(isset($features) && count($features))
                                                @foreach($features as $feature)
                                                    <option value="{{$feature->id}}">{{$feature->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('Tags',null,array('class'=>'control-label')) !!}
                                        <select name="tags[]" class="" id="tags" multiple="multiple">
                                            @if(isset($tags) && count($tags))
                                                @foreach($tags as $tag)
                                                    <option value="{{$tag->id}}">{{$tag->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!--/row-->
                            <h3 class="form-section">About Hotel</h3>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {!! Form::label('Other Details',null,array('class'=>'control-label')) !!}
                                        {!! Form::textarea('details',null,array('class'=>'form-control','placeholder'=>'Other Details','id'=>'summernote_1')) !!}
                                    </div>
                                </div>
                                <!--/span-->
                            </div>



                            <!--/row-->
                            <h3 class="form-section">Address</h3>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('Hotel Phone No',null,array('class'=>'control-label')) !!}
                                        {!! Form::text('phone',null,array('class'=>'form-control','placeholder'=>'Phone Number','id'=>'phone')) !!}
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('Hotel Contact Email',null,array('class'=>'control-label')) !!}
                                        {!! Form::text('email',null,array('class'=>'form-control','placeholder'=>'Email','id'=>'email')) !!}
                                    </div>
                                </div>
                            </div>



                            <div class="row">
                                <div class="col-md-12 ">
                                    <div class="form-group">
                                        {!! Form::label('Street Address',null,array('class'=>'control-label')) !!}
                                        {!! Form::text('address',null,array('class'=>'form-control','placeholder'=>'Street Address','id'=>'address')) !!}
                                    </div>
                                </div>
                            </div>

                            <!--/row-->
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('Land Mark',null,array('class'=>'control-label')) !!}
                                        {!! Form::text('landmark',null,array('class'=>'form-control','placeholder'=>'Land Mark','id'=>'landmark')) !!}
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('Pin Code',null,array('class'=>'control-label')) !!}
                                        {!! Form::text('pincode',null,array('class'=>'form-control','placeholder'=>'Hotel Pin Code','id'=>'pincode')) !!}
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('Are you Owner?',null,array('class'=>'control-label')) !!}
                                        <select name="owner" class="form-control" id="owner">
                                            <option value="1">Yes</option>
                                            <option value="0" selected>No</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">

                                </div>


                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('Hotel Website',null,array('class'=>'control-label')) !!}
                                        {!! Form::text('website',null,array('class'=>'form-control','placeholder'=>'Hotel Website','id'=>'website')) !!}
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('Working Days',null,array('class'=>'control-label')) !!}
                                        {!! Form::text('working_days',null,array('class'=>'form-control','placeholder'=>'Working Days - Sat-Sun','id'=>'working_days')) !!}
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('Opening Time',null,array('class'=>'control-label')) !!}
                                        {!! Form::text('opening_time',null,array('class'=>'form-control','placeholder'=>'Opening Time','id'=>'opening_time')) !!}
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('Closing Time',null,array('class'=>'control-label')) !!}
                                        {!! Form::text('closing_time',null,array('class'=>'form-control','placeholder'=>'closing_time','id'=>'closing_time')) !!}
                                    </div>
                                </div>




                                <!--/span-->

                                <!--/span-->
                            </div>


                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('Latitude',null,array('class'=>'control-label')) !!}
                                        {!! Form::text('latitude',null,array('class'=>'form-control','placeholder'=>'Latitude','id'=>'latitude')) !!}
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('Longitude',null,array('class'=>'control-label')) !!}
                                        {!! Form::text('longitude',null,array('class'=>'form-control','placeholder'=>'Longitude','id'=>'longitude')) !!}
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('Video',null,array('class'=>'control-label')) !!}
                                        {!! Form::text('video',null,array('class'=>'form-control','placeholder'=>'Video','id'=>'video')) !!}
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('Menu',null,array('class'=>'control-label')) !!}
                                        {!! Form::text('menu',null,array('class'=>'form-control','placeholder'=>'Menu','id'=>'menu')) !!}
                                    </div>
                                </div>




                                <!--/span-->

                                <!--/span-->
                            </div>


                            <h3 class="form-section">Social Links</h3>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('Facebook Page',null,array('class'=>'control-label')) !!}
                                        {!! Form::text('facebook_page',null,array('class'=>'form-control','placeholder'=>'Facebook Page','id'=>'facebook_page')) !!}
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('Twitter',null,array('class'=>'control-label')) !!}
                                        {!! Form::text('twitter',null,array('class'=>'form-control','placeholder'=>'Twitter','id'=>'twitter')) !!}
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('Linked In',null,array('class'=>'control-label')) !!}
                                        {!! Form::text('linkedin',null,array('class'=>'form-control','placeholder'=>'Linkedin Page','id'=>'	linkedin')) !!}
                                    </div>
                                </div>
                                <!--/span-->


                                <!--/span-->
                            </div>


                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('Google+',null,array('class'=>'control-label')) !!}
                                        {!! Form::text('google',null,array('class'=>'form-control','placeholder'=>'Google','id'=>'google')) !!}
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('Instagram',null,array('class'=>'control-label')) !!}
                                        {!! Form::text('instagram',null,array('class'=>'form-control','placeholder'=>'Instagram','id'=>'instagram')) !!}
                                    </div>
                                </div>



                                <!--/span-->
                            </div>




                        </div>
                        <div class="form-actions right">
                            <button type="button" class="btn default">Cancel</button>
                            <button type="submit" class="btn blue"><i class="fa fa-check"></i> Save</button>
                        </div>
                        {!! Form::close() !!}
                                <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--==================================Section Close=================================-->
<a href="post-submit.html" class="add-listing-mobile lp-search-btn">Add listing</a>
@endsection

