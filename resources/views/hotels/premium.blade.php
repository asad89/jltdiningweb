@extends('master')
@section('meta_tags')
    <title>@if(isset($hotel->meta_title) && !empty($hotel->meta_title)){{$hotel->meta_title}} @else{{config('app.meta_title')}}@endif</title>
    <meta name="keywords" content="@if(isset($hotel->meta_keywords) && !empty($hotel->meta_keywords)){{$hotel->meta_keywords}} @else{{config('app.meta_keywords')}}@endif">
    <meta name="description" content="@if(isset($hotel->meta_description) && !empty($hotel->meta_description)){{$hotel->meta_description}} @else{{config('app.meta_description')}}@endif">
    <link rel="canonical" href="{{url('restaurants/'.$hotel->slug)}}" />
    <meta property="og:url" content="{{url('restaurants/'.$hotel->slug)}}" />
    <meta property="og:title" content="@if(isset($hotel->meta_title) && !empty($hotel->meta_title)){{$hotel->meta_title}} @else{{config('app.meta_title')}}@endif" />
    <meta property="og:description" content="@if(isset($hotel->meta_description) && !empty($hotel->meta_description)){{$hotel->meta_description}} @else{{config('app.meta_description')}}@endif" />

    @if(!empty($hotel->image))
        <meta property="og:image" content="{{asset('uploads/hotels/large/'.$hotel->image)}}" />
    @endif
@endsection
@section('content')
    @push('site-head')
    <link href="{{asset('css/content_slider_style.css')}}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="{{asset('css/prettyPhoto.css')}}" type="text/css" media="screen"
          title="prettyPhoto main stylesheet" charset="utf-8"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
    <script src="{{asset('js/jquery.content_slider.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/jquery.mousewheel.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/jquery.prettyPhoto.js')}}" type="text/javascript" charset="utf-8"></script>
    <script src="{{asset('js/jquery.animate-colors.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/additional_content.js')}}" type="text/javascript"></script>
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-585ba46f0a9379e0"></script>

    @endpush
            <!--==================================Section Open=================================-->

    <!-- Popup Open -->
    <div class="md-modal md-effect-3 single-page-popup" id="modal-6">
        <div class="md-content cotnactowner-box">
            <h3>Contact the Restaurant</h3>
            <div class="">
                <form class="form-horizontal" method="post">
                    <div class="form-group">
                        {!! Form::text('name',null,array('class'=>'form-control','placeholder'=>'Name','id'=>'name')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::text('email',null,array('class'=>'form-control','placeholder'=>'Email','id'=>'email')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::textarea('message',null,array('class'=>'form-control','placeholder'=>'Message:','id'=>'message')) !!}
                    </div>
                    <div class="form-group mr-bottom-0">
                        <input type="submit" value="Submit" class="lp-review-btn btn-second-hover">
                    </div>
                </form>
                <a class="md-close"><i class="fa fa-close"></i></a>
            </div>
        </div>
    </div>
    <!-- Popup Close -->
    <div class="md-modal md-effect-3" id="modal-4">
        <div class="md-content">
            <div id="map" class="singlebigpost"></div>
            <a class="md-close widget-map-click"><i class="fa fa-close"></i></a>
        </div>
    </div>
    <div class="md-overlay"></div> <!-- Overlay for Popup -->

    <!--==================================Section Close=================================-->
    <section class="aliceblue brown-bg">
        <div class="post-meta-info">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="post-meta-left-box">
                            <ul class="breadcrumbs">
                                <li><a href="{{url('/')}}">{{ 'Home' }}</a></li>
                                <li><a href="{{url('/restaurants')}}">{{'Restaurants'}}</a></li>
                                <li><span>{{  $hotel->name }}</span></li>
                            </ul>
                            <h2>{{ $hotel->name }}</h2>
                            <ul class="post-stat">
                                <li><span class="phone-icon"><img class="icon icons8-Phone"
                                                                  src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAEbElEQVRoQ81a4XnUMAy1vEC7QekEwASUCWgnoEzQY4FE9gLQCWgnoJ2AMgF0AtoJgAUivpdPyefzJU6cC5fcv/Z8iZ71JD3JJpP4MPO5iJTGmFfGmCcicsx8k/rNUt9R14uZ+VhEPhljLuPviegtMz8sZXDfe3eAKIhv8IKI/LXWMjN/ZuaNgoNnXjPznzWB2QLCzDAeIOCRR2vtJTP/bAwuiuInEb00xtw55y5WCaQDxFm868z8QkR+AQARXTDz3VrA1B5RA3+oJ26ttZs+6hRFwUSEBPCHiE7XQjGKYuLee38+tMsNxUTk2nu/GVp/iO+pKIrPRHSlMbFDp56shliCB0GxVWQxKssSnH+Ra1BAsVVkMQAR7KxzrrOmpGixJoqBWtjREw3cpxw+a6ZbBcUA5IGI3kxNpyHFnHOnORsx51oAqdPpPhmooecUr84FBum3yUBPU3ZU0/fvqXE2GxA8KIiT7FTaaDAR+e69P5vLsNzn1JkqoNet935H8fY9VL2B9H2cm75zDR1a30gUiMSaHjk8DzZgUW/UdgfK9oaI3ovIKK9EAjKbkkM7nPt9CyQyDP1GK9+7HlqW5VdjDDrIUcBzDctdv1XNg5rw4Jx7m4iNUGtBAWcV0lwjx6yPG6vjqqpQ6Y+I6CM6wx7ROGrdGAPmWtPV6oIuoA36DVCsc7d1MIF1q1DAnUKxKIo7InpnjElSLGqyEPDJuJpr97ue0ztFqaoK/fmJiDjvPfcZURRFne2MMVgPMIsMJXqlOzOf6SAiSR0UxaqqIDwxlFgMTLIHiaiTipfFwQw2U0G8JHcbdUjpeLSEZwaBRNRJzrOgpJVmAAO6YWR0kJgZBIIgj3b7xjn3IVUsAzAHi5lRQBRMu9upYhmvPRTNRgNRAy9F5IsWwQ+pyXxEs9GeAZUx1cmtSVlAJoBBAkBxbVIzwHcWTVUKV8aYpjmDVML6UZP/bCATwISpGbIHCaA1TusVRrB93SUGiDiXSSaNSUAmgoFBUABGRNhae6uHSHVHqkcYmDnXB0lBDcOfg96ZDCQXTIdxbeKDDLLW4gxma9c1ziCBQE18er2zF5ApYEAljZsjEbnXyX+ynxnjnb2BxGBEZOO9v04p3SmZacg7swCJwRhjkkVzHznf553ZgDRgqqoCjyFRkHaRPmeXKLF38J5ZgfRUdaTb/9LTbx1t7OPmhN4KC+FO7Zjzne3cec6Hhs9S1YzUiZa5rh3eezfn+4Jx7ePs1IoNjYIT0h5xszfVwlNoqIX/DkTjpq0dmM6od5Ipeih96xkmjsvrAeFBgCgYaK6Watp4YXaWNXmJTqFxqaE+wD0YkGaHoXI1RZ/o/wAOonAU3cqyRBuBduLZWvuqSe8HBxJ4Z6MXDxqMqDvXfbK9444MPNF6cxEggXeQpnH0V6ti/cAzdyLyYK19hKdU6uO2UnPRZwsEfrcokAAQusLLqqrgpYZyO/GulxrOu2i4CiBR/cFsANdI0GjhIgOmnc/wkl656pQ8/wC3pFkyvejV7AAAAABJRU5ErkJggg=="
                                                                  alt="phone-icon">
									    </span>
									<span class="phone-number">
										{{$hotel->phone}}
									</span>
                                </li>
                                <li>
                                    <a href="mailto:{{$hotel->email}}">
									<span>
										<img class="icon icons8-Message"
                                             src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADQAAAA0CAYAAADFeBvrAAADEElEQVRoQ+2Z7XHUMBCG31QAdAAVJKkA6AAqIKkAqACoAKgAqIBQAVABSQVABYQKYJ4Z7c1mR2fJtnL+GO+fy1xkaR+9+yH5jrQyO1oZjzaguSu6KbQpdOAdWH3IPZP0QtLJgTd26HKXkt5J+mQTmEJ3JX1dEEjcAMAeS7o2oB8LhjE4oE4BIsTeDtV8Zs+dAxTV+S7pTNKvmTkb3bkv6aOkh+4flwD9CyMfLADGXAbqp/c/B7S0Un5DkBwQJZBSSJLN2Wgt5D+tZmc5oHuSLpKUb2YYfoTZK0mkxhNJf0pAFnKv04OoBdj1xHLRKwFBFfzBP6wYcj6H2AEqCQ8B9n4CMECeJxB8owITQWa9gHgIiZngOMGwQ7ujxi2rRn6wkUBdpRCL7aQ3ED4zIRNbAjIpkt8WGOswP5uJsQ4bmQv7QUAmBHJ/cKp8S/HMZwt7lPKET7PzFPb75h8FxKSUSwDuBLCXI0o9c3L88iC/U4iV2sdoIAtB8sofO/ieAtKn1FsJRnlvHL8oSDWVtQmQLU5eUYGiEf9dFdEql5Ve/zzPkS+11hSIRdnJz5nV2d1Y6n0J5u9oT0NJroFqDkSRiCHjHbGKyHe+cuWcJWQpAn2sKVCE+RuKRY1j8Zm+UM2AIowlst1TaMRdxnhyBQVjgekD1QQowlCyyRdvOEuI+fLO/1GE73Pj/c25Fmo0kIfhOEL+7OsV8VZZug3TjwAxdWugRgF5GMorO13TK4BmnD9U7gtHqh/zWjsoQQ0GMhhCJp54a5K/7xg76ROyXVCDgKyBfkkwNar0BciNRy0rGP4O5Mf2BkINgHKJ3MLpmjmswPCJWoOBgGGSrsSvcajFGCsYbK6HqlYICHsR0cKhVnPYCxyDqgKyk26re04rGJuHa4blVxHoNHXvQyX+UFiA6HO8+d0ZLx1oiv6YgiqcepcAxCnfXwqvVvmyHrmiSkPDYMrnOIad+B+8CLXSCXlKh7vWBobQ2/3gZYOt7ywFDJAbfWlpvzQUI2QDKm7RxAM2hSYWoLj8plBxiyYesDqF/gMvYMl65eHpvgAAAABJRU5ErkJggg=="
                                             alt="">
									</span>
									<span>
										{{  $hotel->email }}
									</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ \App\Helpers\Helper::addhttp($hotel->website) }}" target ="_blank">
									<span class="website-link-icon">
										<!-- Globe icon by Icons8 -->
										<img class="icon icons8-Globe"
                                             src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAGhklEQVRoQ91ai3EUORBVywGciQATASYCmwiwIwAiwCSgkZTA4QgwERgiwERwJgJMBOcLwOqrp2rN9Wg134UqjqlyuXZ2Z9RP/brVei0yP+jy3h+llE6I6NgYg78j+dMj3OADM+P/nbX2i/f+7keYQPu8RIx/SUSvGkYvffUtM19Zaz/tA2oTEABg5s4YAwD5YuZ/iOgP9fmDtfYjM1/jHhGdp5TOiOjl2DPGmCsiClsArQLivT9k5j8rAMXgF7gPQNbaV977jzC46zrG/xBCHst7f5ZSgsEAjf+faoBy/633/n6pWxcDgQHM/N4YcygeAACP2fPeXwCggDj13t8WA2ogAuY4pXQDMEQEg98JTb3y2D0RvS4TMgdoEZCu6+CFCwHwRWY8B6nQ7FuhTz1wC0jxjKLdk0InAQRPnYjx70IIb/cCIlQCx0/F0Dx7+qVd133G98x8GWPMYKvvB9TS3znn3hHRG2PMTQjhuf4OXk4pwUOg4EfxzijVRj0iIGDkcYsyelbl+6MWp8c8Is8fppTuhGLntTe99z0FjTG3RPR8LG5GgaiZRvAOeK/4n70hs3XVcv8UEAGDBIHY2/FKHU/wTAjhvDVOE4hyOdKqt9Ze1jPhvQedPsMbMcacALYAwTPOOQQ2Av+ZThTKa2+IyEuMNim8A0SyU8796gI3EYAfykDOOXx+ycwhxpgH2QNIjhVm/hBjzGsTaMXMWHPweTBRWJNqGg6ASFwgAx0iLaKMSCldqAyCMZCtsEbkAYiozzhbgajMlyfMGHOmKwVmRqbEZAEcMig8iHH74B8AUbP8JcaYM5XMzikWLQxARI8rg7Fm3Jf6iZlzWj44OPiOlKpjBAY/PDzk54kIs3wstRnqMtRn/cXM3zFh1torTTfnHNafE+29/D5lLMqOb5KBjsfKBMkkoNXTMTrtc5+Zv8o61S+q+n2yziCDIaZ6NvRAlDd6nk7w/i+ZTeR+uBeVb6l6C5+Pau/JLJdqF8/dWmthMO6h/EEWvA0hPJuajJatpf7J3hCXT3Je4uhv/LbUT1ODzqVf/Wz5LRE9mqqzqmoi25uBOOewgnY171oGqrT7NcY44PXWYC/POedAmaey8OW9y9hVvFJqtQyk6zp4A1TYSWv1i1SBOEtBefdoiVK/2zmHUuRFMW4KiFom7kIIT0i7aQlVlPcm149ixBpqbX03gh5AconAzJ9ijEixk1dJf0u8t9YjG2wpHnxNpRyZW6EVj3MeX8LjDUBK2TNYxybipMT2JYAUw2bjQxLDLwOkxAlWflKBjhJ5MlOsneG1v1+b2ksGxToEIDmrzOXuLcG7Fsja32vgPZC5IP/Vv//9gPzvqeWcw5758c9Ip/twfsnirMql7zr9LspaKl0v+v2alV0ZtmgdGaTf32ZB3FAWrFpAV3oEaub1inLpvxLltykapewoAT9bpvxqZTx2nTHGo7JDLCL07B5DlQWzW9K1Wavrun4LPVcuNTdWra3j2Eq+th5aEyNLy6XRra7Qqwhus17RW1KIDyklSDxF2unFh0YXCyJDLz4wM4QHCBCQfrL4ABVlbgs9Kj4AiBbJRLps9vZEAYRWO7tf31ifYd+OvsioHNQSSpoCXS0oS1xAvoRoBzGtvzCDMstF2inqHxpAOwKdeh4SEt6Fd0KxrHUyTCRSPWTafnuhxPUBc3YkUyXzQzJFoxK9wl51LAogJNNaJGt5YEmMFDZAHCySaaWJAVAokmmrjbFIxC4DaPlS8bSpjq/Zv6jqYiBip5SgL+cJ05M0K2KrfXnpJKGtEKy16PENukUSK0iX9yGER2PxsMQjXddB8AO9xtoK2JujswV7lrUVapFBlG8UiDvBpwpIBOdejR7su7VwXuyQCYOUiqw2qvRMtt6k84oghIy/A0aJZDsy/xJqVW2MZutN9GCAgLiNzlmzjzjZ1cVAKSUUZuiwonXgY4yXmkZK5t/cDG15wzmHLlVuvMITotCvb4ZWxvYxIykRVPpp7WnpKeZMORYTdUwu6rPLgqlPLOBWf9yiFJItCo4dGCiU0QcG9LGQ+gTF3OK6GIiAAdXgnf48CQAxM1px53J/cGKhdYSjnKCA+m+MuZZDOb1ci/vW2oufcoRDz0jjuAW+Bn910zIDJKJ8JoWZ0baDsf1BnPoZAZCPhcx5YDO1Wi8GIPQVpWFa9xYX2YJKAesU+oVbAJRBVlFryjIBhaZpf/BsovVW6rKbfYzX9vwLJ4dMu+kNqhoAAAAASUVORK5CYII="
                                             alt="web-link">
									</span>
									<span class="website-link">
										{{ $hotel->website }}
									</span>
                                    </a>
                                </li>


                                <li>
                                    <ul class="social-icons post-socials">
                                        @if (!empty($hotel->facebook))
                                            <li>
                                                <a href="{{$hotel->facebook}}" target="_blank">
                                                    <!-- Facebook icon by Icons8 -->
                                                    <img class="icon icons8-Facebook"
                                                         src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAADEklEQVRoQ+1a7XHbMAwFNEE6QZIJmk5Qd4N4gmaDOAtQEBaoM0HTCepOEGeCuhPU3aBdgOiBJX10LFmMYsl0T7zLj9i0xAc8fEgPCA3LGPMREScAcOX/mrYO8fkKAFYismDmb3U3xOcfEtG1iHwCgIshTtjhHmtEvCOiRfzbLSBlWSqAmW4QkV9FUcwBYElEapGjLSJSVkystTNEPPcHmVdVdRcOtQFijJkj4q1+4REriOwWEc08Y9TY98zsDO+AeDp99SDeHdsDbdZTD4nId3/eqdLMASnL8qfGRM6eqInl4Jl1VVWXSEQ3IvJZY4KZcw3wWicZYzTwzxFxisaYB0TUVKuZIMu4aKJaiBcR+YJlWSrXrhAx+9iooVeIlZUCEd1QVdVOTWkLuhy+D+c/ChDfNVz7jiGOyyUArEVkVRTFU0r2PAoQnzY1zacmFZeR9nl+cCAexCMAnInIU1EUDwCwIKLf4aBEpL3dhbV2ogkohfKDAwlJRTMMM9+0xVdq7A4KJHQOL6lVWQLpUqtyBbJExPeI+IGINDNtFhFpzJQAoFlsJwm0lYVBqbXPusFbTTFzMkA2Fq3xVltC8A2vK+iDFMR9HkmNhSZQ2VAreyDhgLEl6/hety/+DSK+iYvmc8/07pFDATl6sL+GMtqqiMijtjLMrG1L4xrMI20WrTuhMYYQsUxpZ3IH4p5aRaRiZjpljzR2AicZ7Ih4SUTrLDzSNf2KyB9mPmur7r3HSHyAsbK3uePfy8Wx10qw0/aWkVoJJhuplWCknS0jtRKsNlIrwUgjtV5jpP/n5YMxZoWIb/sUevrKWkEUFZEfg0hvPQJxgqiT3iJpulWL6MLh+CXavrcoXR6JIzV66uS2SB3tRRDtwyOREOrU6EEGBg4NpHFgwHtlM8IhIjNmvu9Kpabn6kNQyxhzi4hORt8Z4Qg3judRvCg5TxUlU56ruwJRD1hrVZbQuRMnPcQg9P/aMSdrrXonTOEcyjEHuY6fWprtHXOK76TZzFrrJGStMwc5RceLaJ3QwbOiKFQ83ZrTCpf8CzG2NErUDo89AAAAAElFTkSuQmCC"
                                                         alt="facebook"></a></li>
                                        @endif
                                        @if(!empty($hotel->google))

                                            <li>
                                                <a href="{{$hotel->google}}" target="_blank">
                                                    <!-- Google Plus icon by Icons8 -->
                                                    <img class="icon icons8-Google-Plus"
                                                         src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAEh0lEQVRoQ+1a4VnUQBDdWQpQKhAqECsQKxAqACsQC2Azuw2oFXhUIFagViBWAFQAFkDG762z+fZCcknOSy5+n/kDHLlk3s7szHszS6blYuYDETkxxhwYYw7b7pvo82/GmCsiumDmq6Z3Uv1DBfB+Bsa3rdE3InpXB7QEhJmPROSTMeapiPwyxiystZfMjBXZ2sXMh2VZHhljTonoiTHmnojeMPNlMqoCwsynCsKIyIW19oyZ77dmfcOLmflpWZYfiAghb4joOIGJQJh5T0R+wBOKdDEnAA3hnxYdnnnBzDcRSFEUX7En4IkQwumcQSTbnHOXRPTaGHPpvT8m9cY19oS1dm9u4bQiqyLMbrBniGifnHOIubci8jGEcPYveCPzSmU7gCCdvSSiV9vOTkMXEdlMRL6KyHcqiuIOm3zoQ2Z2/z2AyMyMWsucCoj3/lGVX+uJE38pOeI/kIkXvvV1/z0yF08kO/7aI86510QEOrOnmqURI3K8tZbHqlFrA9Ei9EkpzQdjzE2NYgMQRNA70O02IbQpz64FJFF9rHIIYUk1QpCVZQmWAL2Aa+G9f7Mpg9ueMxhIIpeqAxrpDDOfiQjUZbymoD2DgWS02awqns45MNJnACIiX0IIUHajXYOB5FSmA0hkpGr5jfd+fzQUf7RUpFi9KntimVnI7LbpFtX9n9O9Y1OfvwVSaeX6auegm5LCpr0zCAhenofWKklcA+JDCNzH+DoL7+vJwUCcc4vUvdCMBNH/qFmWpehB0nkyINqKiRpZVxhFD2m4ahnhHnRjRGTXWns4pBhOBgTGo5aUZYnuxXMFAxALfGatjQ00EbnF72jR9AmpdM+kQNJLkZnKskRlR18YvbAEDEWwMeS6QG0FSG7UOul2XXndlAAGb/a2Fc1brZoE0GAGmWy95goE+wYbPyUBAFhJGGcH5Pz8/CWsttbCA9gv+fUoq7Vt7K79s4olrBVamrVOiAgZCoOgW+gRfRF+HtU80wqmbvxkm905VxBRrNIi4q21i3qKVU2CwlllMdzbp7pPAqQoCgx/Ype+K71q4YTAqmqN9363K3xGB1IbAPVqdCsYhFXUJeiWdxXI0YEURXGtDYZBii9fgC4v1kkp/t44acxXaoh0zTrlv0IIozXJe2ct5xzGW7FG9N24ysuifh97ClYBSYa2ubJG3wEKjLdx1p1xMbBgzC32x5yCKdu+Q2uqc9DTQN/vRYSttRjeP5r6akjBEwAxiMp3Zbb6/5cGPX1Gb1of0FSIlVwvgLgSkTiDJyJU9th1nGq8nduOYShejNE0wgZpsnW2roN71JODWtFDhb+y1gIUDhgM0iJDPaF7EOGLjAoZ8SIOd9IcsYvsrfPCsb6TinRqcFQHBhKDRfyHEPxYBmziuYkuaf/5oDowoK7COZTUjwJfgq6Y3REObckmurR8hCNLm5CwABEPriQ9vrOz83NboJA1Hx4enqeeQDrwY609bTxUk4GBUAKYPENtIiI28gydtwDEUkJpneRqyo0ZSrNUrgA3YlSfh+hxKxRgZEVIh8Zi/BvU1cKW0t95DwAAAABJRU5ErkJggg=="
                                                         alt="google-plus"></a></li>
                                        @endif
                                        @if(!empty($hotel->linkedin))
                                            <li>
                                                <a href="{{$hotel->linkedin}}" target="_blank">
                                                    <!-- LinkedIn icon by Icons8 -->
                                                    <img class="icon icons8-LinkedIn"
                                                         src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAEBUlEQVRoQ+1a7VEbMRDVqgHoAKgAUwFOBTEVBCoAGtCt1EBMBZgKMBUEKohTAaSCkAa0mXeRPPL5PmQP3MGMb4Y/Rnfa1dt9+yVSDY8x5hsRjZVSo/DXtLSP3xdKqYWIPDrn7uo2pOqPzDwRke9KqcM+JNxijxciumbmefruiiJFUUCBKywQkV9a65lS6pGZcSKDPcwMqxh778+J6DgIMrXWXkehlooYY6ZEdCkif7XWzMzTwSRv2ZiZr7z3TER7InLjnCsPvlQkmNN9UGI8NAJdBwiERORnqQDRGcysVKQoimf4RLC9D4lEjS9fBV9+sdYeETOfi8gtfMI5B1v8NI8xZgGfASpkjJkREagWTPAp0IgnDX8BKiJyR0VRwNZGRHTS5BshpsCpImLg9GkTp/cFaeIrCygi2NhauxZTgv/cKqXOG4SbWWsv+hK8bp8of6siCXSgZCASg9DEew+63iOiC2ZGvBnkyVIkMbuS4lJJI0kgdbDWngyixX/GLS2qFZE2s2PmfRH502aWfSiXpYgx5jWYzxEzv1QQORSRZwRR59x+H0Jv7SORmuEb1tqz9ENFUdwrpZBg3jnnmsjg3fXLQoSZD733CDp7SikgsnR2ZAIhpRlV0Xp36ZMNshQJedjIez8nooNUQBH5rbWeDJ2XZSuSRFGk0Si0lNZ6UWWxPlGomHgWa5VRv0PI0n8iVeesZeaxiHxFjZF8/zVUgXOt9UOuuWYhEhd1nTaygty1wc8mHd98DSmQ7dp7I0Va0pdlepOR6pRr8YAklFJTrfU8+hjiUqgCJ0hiw1IQzRdmBlq1z2CKhBIaxVujcEgGvfePgS3XqH8bH+lKKLMRQe0QiKJViYRcltTfls/1jkiXrdf9P8nnyipw68iea/epszf5UxTCGFMQETIBtJteRGSmtb5pMjVjDNo/B7E2ryozCCItFN3o1MYYdEyKtGMyqI8ktc0yIwgpELKGYxGxzjmunniIOT9E5Mk5VwbkQRVJapuVQiy2oprqmqRcqPWT3k2rzd828cWPgEgjle8UeYtSFxCnJ/kWp5qTCu1MCyeQc1I7RCom2paG1B3ojn7rompOrrWLIx2mtzOtnWm1jC/egroxsYrjq7VBT25nZJMuShuFdlWR1XfjoAd9gNbRmzEGDYDTtg1E5ME5N9lkbfV72767Mnr7KHOOLjQaAmUcG56V47akLv40A9G04nTOHa5cGFBKYR6ChtigVza60Gm8MBBQKa9wQBkRYefcTdcHh/i/MeaSiMox+toVjqRVE5XBTxhBo1XzNDRCofN4GtpIZVO92lWpveYUJrYr85AhTr9uzzCXuWq95pS+CDYL8xBcJohXiwbRB3ECFqK1xpWr2lH4PxVzvVlIavAKAAAAAElFTkSuQmCC"
                                                         alt="linkedin"></a></li>
                                        @endif
                                        @if(!empty($hotel->instagram))
                                            <li>
                                                <a href="{{$hotel->instagram}}" target="_blank">
                                                    <!-- Instagram icon by Icons8 -->
                                                    <img class="icon icons8-Instagram"
                                                         src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAFpElEQVRoQ91ai1EcSQxtNQGAIzCOwDgC4wgMERhHYJxA/xI4HIEhAkMEhyMwjsBcBIcDoHX12uopbTOzO+zOfuq6ylXeYXemX0tPepKGzMByzn0gomNjzJH8G/rqJq7fGWPumPk6pXTT90BqL4YQTpj5L2PM4SZ2uMQz7onocwjhWv92Boj3HgDO8QVm/sdae2GMuQ0h4ES2tkII8IrjnPM5Eb2UjVzEGD/XTXVAnHMXRPQJfxDEALFzK4RwLh6Dw/6SUioHX4CIO30TEG+2bYFFpwcLMfMP2e8p3KwA8d7/Aid2yRIhhANm/mqMOWmAXRPRR2PMmVjmPsb4ikIIuPAVnEgp7QzBvfd/gxd91mHmm5TSiXMOxH9JRKfknLskIoRaRIKd4YX3nsV1XoQQHoQCsNK/+H+MEUYofGHmK/Lew9eOiGinuFGBYMPaKvq64sodgBTk7Q8WEW7dfx8DRPj9x3JTA5FT+iBq4ECpAuQiuMgdEV0tioxjXGstQJxznojOnqEI7pn5MqUU+6zrnLslorfzyD4pkBDCsYTJEvEQ/Ywx19ZaSIiHevKSnQ9yzginJypDI/J8DCHc6k0j/OacEYje6+uIWNbasxoAOsut4lqNpPlurQ3thoa4hAPIOYd66sx8nlL68lzurQzEe49kBVd6Immcc++JCCcPK0EnYYEjcKcZBaslhzHmMsaIZDd6DQKp4XjMnZj5t7X2pFpBpD9yEUg+bz2IBa4kP8A6yNj7Y54LIRtjfDeXIxXhmBsS0TuAEDmBTFxOn5nhZpewQsORo5zzmSIxIhjuAS6Ba7jHqFXTxTyLDOYV2TASaKfLNAhm/mmtPV/EE+EH1PZrCccVTFW2CABI0CWj69VyeikgzjmQ0+PEU0pFB1VXFBDHfQ/vO2KJSgixBUyM8Q2+V8MuM8eUUlgLEKWSi0spYLDEaBB1cxpM3bhysaJqJwdSa5aqksWlIP8PKldGOXfzJbXxByJ6BYtqVduWtCu7Vq0ga1Wm5H/nZssA0e4kifGyfdakHKm+W0/fOYdwiXyBrIwItfRSh1LqjGolzcV685Ut4r1HHQA3KnJ/Svmv5ThIrz4/xBhfTGqRoZOYSv6Pvf8UFpnJL6totD4f3BgQ5xyy8OvqWu3npQnyp4tTuiLIRSmlo/bzpK71vyH7UPjVAm5Zq9SOyUbCr4okJeNKVoYm2p8iIYqSPkRCVAriSUNkZbJL4kL27TZeJYoWfs+xSiM4i7ZSOaS3zzYVkCIaqzu1wq9K8jFgWtUMkjcidH2iUaxSu3uloaddTCyD6zP1dwtMTh2df0QrFGfFpVTDbbDrOYlFsKGm4d0VVjnnKsnxtVt0SKy1P3VhlXNG+EZ5XEoALf11YYUWaCsWJ5Mo+lQVN8AZPLRUiZhfYL6yqGSFFYwxF5i/qOoQ0wC0RHtdai1AxMVKz1hOtuuAABDaPdL2QSWJwqmcPpoP0ia6rgWYc+4TEZWeM3q4KaXS0Bhak7nWgGWKOxFRXMSP+nvNEwEx1xJrs4jaEE4f9Xcdid2jQYeuyN7e3m/NkcfHx310XWTm0TX0pM6fmQlu1CKtdcbwo/6m8qSvLt+4a/U9EAoALZ/axG44UprYaBUtamJPZhGpn+EmO7NCCIfMjD5BNwbp2kE98ryWsDs1wZIcVidUtSTu5P+T0Vutn9FJF8mx1Rm7CirYNDqRKLdLn2Bm9KYydddHUrMJ9GiDtfYmhLAVN4M75ZzR5ECzDgmzWAMAlUo+LfM51UfS+gku1jto2RZp9Gyk1WVzXxiAmyFDt8OWTQMRAFADpe00+MKAWKV7hWPZocsmADaSZvYVjroB/T6KDGUg7L4vG/OnAia56S0RQZRWRdCBwHN6X3NqJMdU+5nkPvLWEkYXw6856SchmomCxcsERcFua4lyhiIAT3o12X88y0p38cxWCAAAAABJRU5ErkJggg=="
                                                         alt="instagram"></a></li>
                                        @endif
                                        @if(!empty($hotel->twitter))
                                            <li>
                                                <a href="{{$hotel->twitter}}" target="_blank">
                                                    <!-- Instagram icon by Icons8 -->
                                                    <!-- Twitter icon by Icons8 -->
                                                    <img class="icon icons8-Tumblr"
                                                         src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAADxUlEQVRoQ+1a4VXbMBDWaYHSCYAJoBOQDUomKExAGAD5pAUKExQmIExQmKDJBE0ngC6g6/v8pDzF2LETm8Tpi97Ln8iS7tPdfTrdiVRFM8Z8I6KBUuo0/Ko+3cT/E6XURETGzrmnsgWp+Cczn4vId6XU0SYkXGONGRFdM/M4HbsAJMsyABjhAxH5o7W+VUo9MzN2ZGuNmWEVA+/9iIgOgyC31trrKNQciDHmloiu0BEQA0TvGjOPgsVgs++cc/nG50CCOT0GEF+2rYG63YOGRORXkHcIM8uBZFn2Gz7RZ02U+HLUzMxae0zMfCEiP+ATzrm+OnipkowxcPxDIhqSMeaeiEC1YIJe+kWVqUV/EZEHyrIMtnZKRK19wxjDRJQVF06dss7+V+lPfGUCIILB1tp3Z8oqkwZfe1VKHZSN62L+snmj/J0BYeYDEXkVkb/OuTmYLjdqU0AQETyKyItzDqFN3nYOSBVp7BSQ6HQwK631ETO/7ZxGgm+A+Y5ExDrnOLXlndAIM0N4hDYIGabOOQR3C633QMJ9BQco2GqqtR6kJlU0rSVUfm+tvVyV6ovzr0y/wR8Q7ufMJCJPWuuLMhDoN8Y8E9HZMkHbnDFrnyMxwAz3FWbm+7a7uS0gXUYCredqo5HWi3dJzXsgcQcKfjG21g4rYqGfkRiqfGkrPpLe7VPBqoSpAD4firuEc+6iLWGsTL+rHni9PxCbOuweSENbW5u19qbVwVX6Q2+IdT5Q19/Qgio/25vW3kf2PrLcizbmI8aYCRGdID9bLM5ARGYeiMgVET2U9deRQWdAEkFLU65JPeNNRJBBR+IZmX/c78+TytibtfZzneBVPto61jLGjInoKxFdVt0WY86rTEjcNGMVap0ouDONJBnxJ+ccdri0oZjkvY8amCmlZlrrMYpKbc6aLoHkOV9IT0THzAwhV2q9AAKJE9OpvGAtQ9YbIEjUee/BTp+UUo3zVIGxUE8Bcy1k8ZuqdG5adazTdEII5b2H4wPMTERYaz1NC6s3NzdnRHQQHiLExwjIjSFnjNzYQu28bu0k5zzttPSGiQOYWAtfKgsYCxpEPb8qwbdsgoXSW1KazqujdbvQpB8F1sBQg6ChfBhSq9AWnmNExmoyX9U3STV6mJfbkurozhREE23k1ej/68FA0Mr8CQdCCefcXRu1f9RYYwzisryM/u4JR1y0kLMC89xqrV+2/aQjkAgYD+9O8kcNxZJ36TMn7z2004h5Pmrnq+YNVYDR0mdO6eAkNsJjgpNNC5yuF9guMl3pWfMPzOt9SgN4YEwAAAAASUVORK5CYII="
                                                         alt="tumblr"></a></li>
                                        @endif


                                    </ul><!-- ../social-icons-->
                                </li>

                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="post-meta-right-box text-right">
                            <div class="addthis_inline_share_toolbox"></div>
                            <div class="claimform">
                                <h3>Claim This Listing</h3>

                                <div class="">
                                    <form class="form-horizontal" method="post">
                                        <div class="form-group">
                                            <input class="form-control" id="name5" placeholder="Name:" type="text">
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control" id="email5" placeholder="Email:" type="email">
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control" rows="5" id="message5"
                                                      placeholder="Message:"></textarea>
                                        </div>
                                        <div class="form-group mr-bottom-0">
                                            <input value="Submit" class="lp-review-btn btn-second-hover" type="submit">
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="padding-top-30">
                                <a href="javascript:void(0)"
                                   class="secondary-btn user-position md-trigger quickformtrigger2"
                                   data-modal="modal-6">
                                    Contact the Restaurant
                                </a>
                            </div>
 @if(!empty($hotel->booking_url))

                                       

                                         <div class="form-group mr-bottom-0" style="margin-top:20px;">
                                            <a href="{{$hotel->booking_url}}" target="_blank" class="lp-review-btn btn-second-hover">Reserve Now</a>
                                        </div>
                                      @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container single-inner-container single_listing">

            <div class="row">
                <div class="col-md-8">
                    <div class="post-row blog-single-inner-container lp-border lp-border-radius-8">
                        <?php echo $hotel->details; ?>
                        @if(!empty($hotel->image))
                            @laravelImage('uploads/hotels/large/', $hotel->image, 670, 300, [
                                    'fit' => 'crop-top-center'
                                     ], [
                                     'alt' => $hotel->name,
                                      'class' => 'img-responsive text-center'
                                    ])

                        @endif
                        @if(count($features->get_hotel_features))
                            <div class="post-row padding-top-5">
                                <div class="post-row-header clearfix margin-bottom-15">


                                    <h3><span style="padding-right: 10px;"><img class="icon icons8-Christmas-Star"
                                                                                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAD8UlEQVRoQ92a/ZFNQRDFz0aACBABIkAEiAARIAJEgAjYCNgIEAEiQASIgPpVzdnqHfe+6blvZp/SVfvHvjt3pk93n/6Y9440Xz6VI27MPOpo5uaS7kp6W864J+ndrPNmA0HxO0X5kwJsCpaZQK5I+lppfVXStxlIZgJ5JumpJDyB4Jnnkvh8uMwEgjfwCtxA4ArewCvDZRYQk/x7AYPigLhcgA0n/SwgJnkMpRhqAB0qM4BEkl+S9LNoHD8fTvoZQGz5Y0kPKrO/kXR/BulnADHJb0v6UAG5Jen9DNKPBrJE8poLU0g/GgjWxupPJL1cYfNjSS9KfRlG+pFA1khe47ko6Uf5cBjpRwLBA48kLZG8BmPSv5KEh/aWkUCwMtZeInmtqElPaiZF7y0tIDfDCYQOf5brRXH+BwD/x0reUs6k/xxqDcD438Ka2GR+XNsUIBCOkEBqZVvK1M8fSiJsMkKNeZ1ZuLImgjwGSJwZ6neiBVrWipbN6mdPev0ur7MmRkg84wQgbEbhulZcTOrMWjWr8Oh1eJMUju5fSPmRI84kHEqbQcP3Lwozjmea0wxZk93FCgAAwztu+g4NCuvjBfdvZ4ruUtaC/IC4UDII6fTQYABB10Bm/FXAnJlp1tIvL7CQQYjswJQX0+J5egddAAEY0juG/kuXXXWkTgKk1uGTXcMiKE2KPiX1WnS0CqK5wgyBwCHaivMQapsbz2bbkwHiLEa2MDC8M1PwgkmdunnJAkFpNsZCM5NATWoiIFXTeoAABuJRPAEzOgmwN1dGVHcyE41lOsH0AgEMVuMAMlrK7ckY9KxPZgJUV8rfAgS9uGHnsJEX0x6TMVL3zf0WIHHCi9c9ScOvLttr3y1AbDmaNbwyUvAGzWu3p7cA8Ug7kh82hvfuHoG3ADE/MiNtr7c286QXSIzj3nezoH6XhV3861XGFmNyJM+3hDUeZ+kG6pvHpfdZwyTYxZNeIFl+4Dlamvqqh/fh1q4a4XrSxZNeIBl+xI4Vi3vSdK8GiF2dtK+KuupJD5AWP2gtCCOHHOFHf+brHJ7TN/kCgRAC0NJ3it086QGyix8OI8DSJ+1q9mLziXccbpEv3TzpAbLED6zPHO3CyNwAiFafBGD285xDGDGDOxl086QHSOQHB0cy0+hh6UxWipbHEIQbDShi73i8TfMkCyTyg7To8dNk3ucrZ/bGi3Uy8C8mUvUkCyT+FMMWhcwokJ4ZGkUHL+CR+jYxVU+yQMwPdIHMeGDti5xWkWw9xzjsz/CGpOpJFohvIfkVA1xokbmlbOs54caZ/FqiefHAZlkgrMX1o8KoBcTPOZM60zRcD5Ds4QdZ998A+QPKofBTmPvfsQAAAABJRU5ErkJggg=="></span>Features
                                    </h3>
                                </div>
                                <ul class="features list-style-none">
                                    @foreach($features->get_hotel_features as $feature)
                                        <li>
								<span class="tick-icon">
									<!-- Checked icon by Icons8 -->
									<img class="icon icons8-Checked"
                                         src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAFLklEQVRoQ+1azXHbOBT+nm/2Zb0VREgDUSow1IF9DHWwXYG1FaxTQZIKohykHC1XIGwFtiuAXEHsS7wnv50HghRJgSQoMc5OJpzxjDwEQXzv53sfHkj4RS76RXDgN5CQJ0dze8QEDXZ/h0QYZuOYcQvCg/ufYPCMazNWt31FxM4e0V+txjNOiXDWdVEMrMAw2MMX806Zrs8Xx28NRM/tMYALAnRhwjsGZEEGjFXR4npmh9jDoXgKgCbmYxC9yj0mzxDebwuoMxD91Q7A+JwDYL5n0BR7mJp3atXFqg4ccEzEE4D+kGedIfZxYk5UGoaRVycgzgsCgsSq/MigS5Ooj5Hvqh2mr+whvmOSAfIhd1LNIRlXBzAaiJ7ZCyK4RTPjGgc462q1NsCyUHpyofnGv+fcjNVUfuuZlRz8AGAUIokoIHpmxQsumRn4qw8vNIHSc/uRgAtmvJVFC6EQY1kF1ynZ9dxOyFmCH5lpklmozbq73pdclJzzebSUcGbGezNWl6G5Gz0iOUHAVZMldl1wo2fS3BEQQwa+mETVUnwtEMdOz7hxlniBcAoB0jMr7xdmu+N96KacrAcyt0uhWElsM1ZSM170yvNS6P2AhgJCcqWuzgSBrEOKH3mfBn2zU5tFKnmpiwnPhFEITB2Q1Bs/IaSEZonw2TPkiUnUwtPvJRH+loJpEjWqGmMDSE51zPfL8etBm/X6vF9hqLyGOCCuxvBKFEDIK5tA5nZKwGkT1fW5+GyuCrkEGUrPbOaVjfsbQEZzy86tBNVVO20L0EsUR7MA/lkmqihE82kFLDEsMx5wAFXM3RKQQt24WyYq30tsu8DY5/TcXhEgzNhKs6O5lT3Mm2p4lYGsXffJJGoSu5Bdxq3lDz8y0bAtCgrypVTlS0BGcyuC7YiBnC12WWTbs2uGcvLH0WzsM9X6VvWIq6SZWGubdJf7MUIwWO3XArKUS1WPuERfJipaFUtydhWSsUIwCCSV+t8k4c1Y/ZmN2RpIqXAxSpzfKgSfcEPAoE0I1s2TMWvR4FsD8dV2XYUZIvE/tYVbUQhuy4y9AwmAmZqxOq8DExKCbcBD92OApBztd2axL6mEWRBMVpX9Bi2KoYI5MrNDItxIzSl6tDf6TVmIF04LMUpgSkBr1Gu00dabvXrWKmiZrQqisBERGwfGt3XwHQNvQWlaRJNCbXiu9/P1BTHjdmlvmrF6G2ul4rgSGGmTwgGRXWbjVjX2XRlZNEoUmawP0ZiCgbRxsrZOL7vMTDRKni2T19KxzK8fJuMLPSq07bc7eCNexufhBaxMolTsS+qqMP7FYZsQjH2HntlvLkwDhBGUIgXx+MObcdEg8v5aeL/S2HwIbWBiX9znOL/xsnXekHfVisOCVxYmUSd9LqzrXNnGq6k11digI+ZbXxN+WoiVWkMNG6//d8u0pjUU8mjrviOziMsX6XP5Nn/X8Og6vtLfao2IViBO4foWkfzmSLnedeEVdbA+i4lUBFFAPBh/vODOSBbYx3nfrVTHTk/uWM/1mrt0OqOBeDDHBJ56hSuhdhmzmYrxjpyIyXyFY72zrF0a83wnIA5M2iQTHXXkrbZCqqvkiLnbYagcXQCnYJzJ1teH7jX2MOk6V2cgmXV8M096Xw6QX4R8FGCIYRi43zjMFDEJvMo+Kih+UCBNDCZcvtjxdNXN7oOB1KKnMSFQHuNOhhfi0W0BZPNt7ZHQov1XEJrkMw64DwOcjC9cdwAeOP3awey6+OLEvQLp7pH+nvgNpD9b9jPTfwQDNWCupe5qAAAAAElFTkSuQmCC"
                                         alt="checked">
								</span>
                                            <a href="#" class="parimary-link">{{$feature->name}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif


                        @if(count($hotel->get_hotel_meals))
                            <div class="post-row padding-top-5">
                                <div class="post-row-header clearfix margin-bottom-15">
                                    <h3><span style="padding-right: 10px;"><!-- Waiter Filled icon by Icons8 -->
<img class="icon icons8-Waiter-Filled"
     src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAADHElEQVRoQ+2Z/ZFMQRTFz0SADIgAESACRIAIEAEiQARWBEsEiMBuBIgAEVC/qe6t3rev+97b0zNvampu1db+Mf1xz/04fe7MSgdiqwPBoRaQm5KeS7ov6U4CfCbpq6T3kn7uUxBqQN5KemE4+k7Sy30BMwfke5EBy08ydNdatIvPp0CIMuUUMcrMyl7kvK61JRB64kfXKdKtpXumBNKTjYx78ayUQCK9MU3c4r1SAvnXWVZ526Jv0kECoTxud2blPEDZnisgnleJQOi/P9amfW32T5IeJuffSHodAbJP9FtWx0dJTyNAWNtDwSOp90nSdug7Aouh6dB3ZOlzDdAc00R6ZVRv4PiHwvmav4B6loBdWlOjTE9mRmXCI1CnwK4IVkvGo6GIVmYzMkCaOWiEjPcErJadS4Fc8hF7JOnUamLj88epd5qD1YZ3mNsRqLmhzcWVBVQFgnUNhMhk6c4keD2hBG3LiCZ7MR4sSCIzDOzSesSgU5p7hNH8JwCpaayWNLfeHEAwcNX66EQSVDvC1u9MC8gaaeUmT0QhhQeV/SPKKh+9Vt4tIK0X1RtRZnqYaWqbKu3peasWkItGmnHEG9FpidF/7OX/SDtrAeGiGzNNixO/A16UJcZeAnQtsN+z9NwCcsHTxWk9/F8rMY+TrjUWkDkZ0vsaw2I0JgbNfwmW2K9EPtx/hdotIHOzeO9sn8+KgEASoXr5y0GYzZAFZNon0f6YXgrbUZpWs39LM4hbz3mA8BbQsFhPf7hqvFhEFhCq5nhbHuwBUo6ajJzM0tuyv0l/hUDgjAcIaSZCGJm5tyUUgOCeZi/U7vYAyYD5P/pFLv2ao3p3zLxAsmaCMrdhLV3nus8LhD7BIv2BVvMoXNe3JBYaLxDmCygz0h/IG3qq9aUf5+aZxvK1+bkXSGYRi/+njMj6Gpgumt202Xuilb8PYAiDiUqhuBFDzTnjzcgmQNiLLEEjUZpkgsGsi2aXzEhPEMJ7dpWRsGPRDUcgjojt9Mu/Y0aOGXFEoGcJpRX5PcR7x6jfTbz3LfoltttJz8KdMovHod41/wFjTa3SeLgsyQAAAABJRU5ErkJggg=="
     alt=""></span>Offers</h3>
                                </div>
                                <ul class="features list-style-none">
                                    @foreach($hotel->get_hotel_meals as $meal)
                                        <li>
								<span class="tick-icon">
									<!-- Checked icon by Icons8 -->
									<img class="icon icons8-Checked"
                                         src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAFLklEQVRoQ+1azXHbOBT+nm/2Zb0VREgDUSow1IF9DHWwXYG1FaxTQZIKohykHC1XIGwFtiuAXEHsS7wnv50HghRJgSQoMc5OJpzxjDwEQXzv53sfHkj4RS76RXDgN5CQJ0dze8QEDXZ/h0QYZuOYcQvCg/ufYPCMazNWt31FxM4e0V+txjNOiXDWdVEMrMAw2MMX806Zrs8Xx28NRM/tMYALAnRhwjsGZEEGjFXR4npmh9jDoXgKgCbmYxC9yj0mzxDebwuoMxD91Q7A+JwDYL5n0BR7mJp3atXFqg4ccEzEE4D+kGedIfZxYk5UGoaRVycgzgsCgsSq/MigS5Ooj5Hvqh2mr+whvmOSAfIhd1LNIRlXBzAaiJ7ZCyK4RTPjGgc462q1NsCyUHpyofnGv+fcjNVUfuuZlRz8AGAUIokoIHpmxQsumRn4qw8vNIHSc/uRgAtmvJVFC6EQY1kF1ynZ9dxOyFmCH5lpklmozbq73pdclJzzebSUcGbGezNWl6G5Gz0iOUHAVZMldl1wo2fS3BEQQwa+mETVUnwtEMdOz7hxlniBcAoB0jMr7xdmu+N96KacrAcyt0uhWElsM1ZSM170yvNS6P2AhgJCcqWuzgSBrEOKH3mfBn2zU5tFKnmpiwnPhFEITB2Q1Bs/IaSEZonw2TPkiUnUwtPvJRH+loJpEjWqGmMDSE51zPfL8etBm/X6vF9hqLyGOCCuxvBKFEDIK5tA5nZKwGkT1fW5+GyuCrkEGUrPbOaVjfsbQEZzy86tBNVVO20L0EsUR7MA/lkmqihE82kFLDEsMx5wAFXM3RKQQt24WyYq30tsu8DY5/TcXhEgzNhKs6O5lT3Mm2p4lYGsXffJJGoSu5Bdxq3lDz8y0bAtCgrypVTlS0BGcyuC7YiBnC12WWTbs2uGcvLH0WzsM9X6VvWIq6SZWGubdJf7MUIwWO3XArKUS1WPuERfJipaFUtydhWSsUIwCCSV+t8k4c1Y/ZmN2RpIqXAxSpzfKgSfcEPAoE0I1s2TMWvR4FsD8dV2XYUZIvE/tYVbUQhuy4y9AwmAmZqxOq8DExKCbcBD92OApBztd2axL6mEWRBMVpX9Bi2KoYI5MrNDItxIzSl6tDf6TVmIF04LMUpgSkBr1Gu00dabvXrWKmiZrQqisBERGwfGt3XwHQNvQWlaRJNCbXiu9/P1BTHjdmlvmrF6G2ul4rgSGGmTwgGRXWbjVjX2XRlZNEoUmawP0ZiCgbRxsrZOL7vMTDRKni2T19KxzK8fJuMLPSq07bc7eCNexufhBaxMolTsS+qqMP7FYZsQjH2HntlvLkwDhBGUIgXx+MObcdEg8v5aeL/S2HwIbWBiX9znOL/xsnXekHfVisOCVxYmUSd9LqzrXNnGq6k11digI+ZbXxN+WoiVWkMNG6//d8u0pjUU8mjrviOziMsX6XP5Nn/X8Og6vtLfao2IViBO4foWkfzmSLnedeEVdbA+i4lUBFFAPBh/vODOSBbYx3nfrVTHTk/uWM/1mrt0OqOBeDDHBJ56hSuhdhmzmYrxjpyIyXyFY72zrF0a83wnIA5M2iQTHXXkrbZCqqvkiLnbYagcXQCnYJzJ1teH7jX2MOk6V2cgmXV8M096Xw6QX4R8FGCIYRi43zjMFDEJvMo+Kih+UCBNDCZcvtjxdNXN7oOB1KKnMSFQHuNOhhfi0W0BZPNt7ZHQov1XEJrkMw64DwOcjC9cdwAeOP3awey6+OLEvQLp7pH+nvgNpD9b9jPTfwQDNWCupe5qAAAAAElFTkSuQmCC"
                                         alt="checked">
								</span>
                                            <a href="#" class="parimary-link">{{$meal->name}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="post-row padding-top-5">
                            <div class="post-row-header clearfix margin-bottom-15">
                                <div class="content_slider_wrapper clearfix margin-bottom-15" id="slider1"></div>
                            </div>
                        </div>




                    </div>


                </div>
                <div class="col-md-4 ">
                    <div class="sidebar-post">
                        <div class="widget-box  widget-bg-color post-author-box lp-border-radius-5">
                            <div class="user-info">
                                <div class="premium-logo">
                                    @if(!empty($hotel->logo))
                                        @laravelImage('uploads/hotels/logo/',$hotel->logo, 150, 150, [
                                            'fit' => 'crop-center'
                                                ], [
                                                'alt' => $hotel->name,
                                                'class' => 'img-responsive'
                                             ])
                                    @else
                                        @laravelImage('restaurant/', 'coming-soon.png', 150, 150, [
                                               'fit' => 'crop-top-center'
                                                ], [
                                                'alt' => $hotel->name,
                                                 'class' => 'img-responsive'
                                               ])

                                    @endif
                                </div>
                            </div>

                            <div class="quickform">
                                <h3>Contact Owner</h3>

                                <div class="">
                                    <form class="form-horizontal" method="post">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="name7" placeholder="Name:">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" class="form-control" id="email7" placeholder="Email:">
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control" rows="5" id="message7"
                                                      placeholder="Message:"></textarea>
                                        </div>
                                        <div class="form-group mr-bottom-0">
                                            <input type="submit" value="Submit" class="lp-review-btn btn-second-hover">
                                        </div>
                                      



                                    </form>
                                </div>




                            </div>

                        </div><!-- ../widget-box  -->

                        <!-- Popup Open -->
                        <div class="md-modal md-effect-3 single-page-popup" id="modal-6">
                            <div class="md-content cotnactowner-box">
                                <h3>Contact Owner</h3>

                                <div class="">
                                    <form class="form-horizontal" method="post">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="name" placeholder="Name:">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" class="form-control" id="email6" placeholder="Email:">
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control" rows="5" id="message1"
                                                      placeholder="Message:"></textarea>
                                        </div>
                                        <div class="form-group mr-bottom-0">
                                            <input type="submit" value="Submit" class="lp-review-btn btn-second-hover">
                                        </div>
                                    </form>
                                    <a class="md-close"><i class="fa fa-close"></i></a>
                                </div>
                            </div>
                        </div>
                        <!-- Popup Close -->


                        <div class="widget-box widget-bg-color user-timing lp-border-radius-5">
                            <div class="widget-content">
                                <ul class="user-abailable-table list-style-none">
                                    <li>
										<span class="timing-days">
											<!-- Clock icon by Icons8 -->
                                            <!-- Calendar icon by Icons8 -->
<img class="icon icons8-Calendar"
     src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAABcUlEQVR4Xu1Z0Q4CMQi7+/+P1hhN1MuZ0u0KxNVXWIFS2HLu2+K/ffH6NxNgBSzOgEdgcQF4CV45AreDmq7EfkBL8K9MUpLgB6kSfIaAYwLd10eotpDTq1IT0L3lIzvICiC6ipYQshOhTl0RPrKfgs4o4Hh2KAGCFYSP7HICiFokribAS/CbgdB4h5z8Dngy4IeQZHXpQEPqDjn9GAHmrK7MN7JvgexbwArI0DURo/0IoATV9vKnsLpAhG8CzhhgFtkQw8Q3PYQ/ay9XALHPhlwRQSagegSG2kocsgKqX4KoA9V2+Q6oLhDFNwHqJYg6UG2XK4BY2BJXRLAJUI+ApK0EqBXw7+8AJIZyBaAEZu0mADCACJbfAiiBWXt7BaAE1XZEsFwB6gIRvgmovgZRh9T2dAWoC5rFD33wDTm9MvHf47MtST4fam7IyQpIbl1mOEYBmXmlxTIBaVQ3DWQFNG1MWlpWQBrVTQNZAU0bk5bWHX6SfEEXi4e2AAAAAElFTkSuQmCC"
     width="20" height="20" alt="">

                                            {{"Working Days"}}
										</span>
										<span class="timing-hours">
											{{ $hotel->working_days }}
										</span>
                                    </li>
                                    <li>
										<span class="timing-days">
											<!-- Clock icon by Icons8 -->


														<img class="icon icons8-Clock"
                                                             src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAE8UlEQVRoQ82ai3EbNxCGsVABliuwXEHkCqRUELkCKRVYbgDAoYFQFUSqwFQFpiuIUoGpCqIUIGzmxywYEDqSuDuc5ZvheDy6x37YB/YBUo0u59xJCOGMiE6VUvidyC//wlophd8DMz9orb855/D/yRdNeYMIf0lEVz1C1756zcwLrfX9FKhRIABgZquUAkC8mPlfpdQKq621xr/rUjA8B+AQwrlo7ZyI3mTEt0TUjQEaBOKcO2bmPwqAO6310jm3rFVBfp9z7iKEcEFElwXQZ+fcU+07q0HwQWb+UykFGKw+zGEx5GP7hBIzhYavRUtPRPR77QJVgVhroYVrMaF7rfV1rfqttX/hua7rPtSsrgDBxM7k/kXXdZ8PPbsXREzpi1LqHFoQgNtDL83/bq1lAalatPSsc+4qhLAQ7ayI6OM+7e98uUB8hVMKxLlz7mEIBO4dC4JnnXOnIQRAICA8ENGvu2B2glhrAQFN/K21vqg1pRJ0CkgGA1P7RSm17LruY99i9oIYY6DST6KJ07EQUzWSmdlGM8x8472P/ppfL0AkOn2ZYk4tfKQUNDcz8ZetcL8FIn7xHSFWQt8gx+5T+VTTKvacK9kCEJrf5/6yBWKMgS1eMvO99/5iqGPPDYL3G2Pg/GfMfOe932QWGxBJO7638Is5TCvzF6Q4iGBvRCsx6dyAZNrovPeuhTZaOXspizHGEZHNtRJBkjYiGdHbVmnHXCCFvPCVdQTpI/yZNSIyR38mIiSXiwhirUWkOukLa1OBWkatIoIhiUX6tO667j3lTu69P54qePn8XCCiFYTh6PQAibG5ZcidM2rl7zbGLInoN+x5lKUjTaNV+uDMGknR6wYgcYORzBIlatNrThDnHJLar8z8jTJHj2GsJYUxxhJR3JOY2Xnvu5bvR/7FzCjc1gAZVfgcEiiZbH4fMzc33yT/bCDW2n+QfBbAT13XvT20CEP+PjuIMSaGxkIjj957tISaXbOCSDkA290SelbTMsasiehdnklOWa6i1n8U80J/6rZlMgoZs838sWn4LSBQ66NhUd1kG7qAW+G31Yb4oyGKZPemSYryGhAC8n+KkuX2o0Lja0FI1h5DfEwahSw5PLp5g5rR0hLFDju7T/Sl8cwcQ3qqEK/RZS8L+hrnM8bE7uPcjl3KkkrzrcKqr3SsgXite3aWumJeqRXUPB9qDZxF2k1L6EU7SCmF1OJD60y4FYxoA1lDdPIkZ2+Dbl+zuJVAY9+TNdf7G3Sy5R+HEBDBUAdjWjS5ZTpW4L7nnHMpKGFWc7KzZSowqTsBE8M8YvBMpKXw6V1SRGHUAZN6sU3sHSv8LP6SQ1SPFdIKpFpeJkVYgaZlcK3WBAJDWGy6O5vre0dvMvbCpOhVzKzQxN7M4eAwNISAxAwTVsCgPflDAoAx5pM0LjAOxyT5atQwNFd90UhA+wgRbRZTk30CpoTTEei+9I7aStOsHhnLCQXs/qjDn+T8yF0roOxcS2ofIcRCC1VJbDVIts9gULp13IKZl977+1oHLrSNliemY/m5FhwLwaGE6upyEEgW0zE1QrsyB8JHVzi+xMyro6Ojx75DNc/Pz+gP4DANjkLBfDYtI2TfWms3RsujQHIgpRQOxeD8yLsxGkE9gTMtSIvGAKRvTgIpCh20fs5DCJuDZyUchE4Hz7TWyBhWU4TPv/8fVZPejlNsRCUAAAAASUVORK5CYII="
                                                             alt="clock">
											Opening Hours
										</span>
										<span class="timing-hours">
											{{ date("g:i a", strtotime($hotel->opening_time))}}
                                            - {{date("g:i a", strtotime($hotel->closing_time))}}
										</span>
                                    </li>
                                    @if(isset($hotel->menu) && !empty($hotel->menu))
                                        <li>
										<span class="timing-days">
<!-- Restaurant Menu icon by Icons8 -->
<img class="icon icons8-Restaurant-Menu" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAACHklEQVRoge2ZfZGDMBDFfxKQEAmVgIRKOAlIwEElIAEJlVAJSEBC748kTcgFkrRZ6M3wZjLTYct7PDabDwInTpzIRQuMwAN4ftgmYDCcPv+tIv8IXEITHTBXEIi1bgd+MK5m07qYyzegDJcvKMnfgk7RE+grCITwxST5R9D97Yl2WRsNzogk/4wnlEKLLtQZ/QZ+MsVy+d/Fiz9HKOzvtl1LhISQbUThRpwOnU5rbCbdZb7GyN3Eb8H1wVy/5wpFOFPtHrkn1MsyonBvvgliDS5TqsBIQ/6kOEbuGVkiy8hieIvAZqVbiaf4ayDLiJ1j1or6yrbRFH8NZBmx6WxX4gpvHE8JCSHLSGqyXExIKSEPuxe7Leaw0FMPuhU/pNjtzWsLPWXiU4GR2sgycjOxYSXeJ+Ip/hoomkdiQ+wPbvnf5ggJIcsIuCH2iVso+hulrTlkjX/3Yrfw11bgRrOUiRj/IcW+9kAl3eVrulbsgU4jAjjUyGHFHv7vEyNnsaf4TyOlQkIoNuKvhP91sdcycnixWyN27TW9aaQ2io3Y1Np116NUSAjFRnqWae9LhYRQbAR0/5zR2dna/kaFhPDin8wPJS0kgMXHjwHX9yUgacTW6x30NtWKSZiRMNLgttp2JAX+FvKnbULXk/KuXdDZr3EY6rfw4zoXdIqkDi1r8toBJ+d85iMoZA9Dd4f0YehukD4M3RUPto8dTpz4ZvwCqGiLk/JuKQIAAAAASUVORK5CYII=" width="20" height="20">

                                            {{"Restaurant Menu"}}
										</span>
										<span class="timing-hours">

                                             <a href="{{url('restaurant/menu',$hotel->menu)}}" target="_blank">View</a>
                                         {{--   <a href="https://docs.google.com/gview?url={{ asset('/uploads/hotels/menu/'.$hotel->menu) }}&embedded=true" target="_blank">View</a>--}}
										</span>
                                        </li>
                                    @endif







                                </ul>
                            </div>
                        </div>


                        <div class="widget-box  widget-bg-color post-author-box lp-border-radius-5">
                            <p class="text-center underline">Find our Location:</p>

                            <div class="widget-content">
                                <span>Address: {{$hotel->address}}</span>

                                <div class="clearfix"></div>
                                <br>

                                <span>Landmark: {{$hotel->landmark}}</span>

                                <div class="widget-map">
                                    <?php Mapper::map(
                                            $hotel->latitude,
                                            $hotel->longitude, [
                                            'zoom' => 16,
                                            'draggable' => true,
                                            'marker' => false,
                                            'eventAfterLoad' =>
                                                    'circleListener(maps[0].shapes[0].circle_0);'
                                    ])?>
                                    <div style="height: 200px; width: 100%;"><?php echo Mapper::render(); ?></div>
                                </div>
                            </div>
                        </div><!-- ../widget-box  -->


                        @if(isset($hotel->video) && !empty($hotel->video))
                            <div class="widget-box  widget-bg-color post-author-box lp-border-radius-5">
                                {{--  <p class="text-center underline">Video</p>--}}
                                <div class="widget-content media">
                                    <div class="media-body">
                                        <iframe width="100%" height="280"
                                                src="https://www.youtube.com/embed/{{$hotel_video}}"
                                                frameborder="0" allowfullscreen></iframe>
                                    </div>

                                </div>
                            </div><!-- ../widget-box  -->
                        @endif

                        @if(count($hotel->get_hotel_tags))
                            <div class="widget-box widget-bg-color widget-tags lp-border-radius-5">
                                <div class="widget-content">
                                    <ul class=" list-style-none tags-container">
                                        @foreach($hotel->get_hotel_tags as $tag)
                                            <li><a href="#"><span
                                                            class="tag-icon">#</span><span>{{$tag->name}}</span></a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div><!-- ../widget-box  -->
                        @endif
                    </div><!-- ../sidebar-post  -->
                </div>
            </div>
        </div>
    </section>
@endsection
@push('site_footer')
<link href="{{asset('css/content_slider_style.css')}}" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="{{asset('css/prettyPhoto.css')}}" type="text/css" media="screen"
      title="prettyPhoto main stylesheet" charset="utf-8"/>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
<script src="{{asset('js/jquery.content_slider.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery.mousewheel.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery.prettyPhoto.js')}}" type="text/javascript" charset="utf-8"></script>
<script src="{{asset('js/jquery.animate-colors.js')}}" type="text/javascript"></script>
<script src="{{asset('js/additional_content.js')}}" type="text/javascript"></script>


<script type="text/javascript">
    (function ($) {

        $(document).ready(function () {
            var image_array = new Array();
                    {{$numItems = count($hotel->get_hotel_images)}}
                    {{ $i = 0 }}
                    var image_array = [
                            @foreach ($hotel->get_hotel_images as $hotel_img)

                                      @if(++$i === $numItems)
                                         {
                            image: '{{ asset('uploads/hotels/premium/large/'.$hotel_img->image) }}',
                            link_url: '{{ asset('uploads/hotels/premium/large/'.$hotel_img->image) }}',
                            link_rel: '{{ 'prettyPhoto' }}'
                        }
                            @else
                                {
                            image: '{{ asset('uploads/hotels/premium/large/'.$hotel_img->image) }}',
                            link_url: '{{ asset('uploads/hotels/premium/large/'.$hotel_img->image) }}',
                            link_rel: '{{ 'prettyPhoto' }}'
                        },
                        @endif
                    @endforeach
                ];
            $('#slider1').content_slider({		// bind plugin to div id="slider1"
                map: image_array,				// pointer to the image map
                max_shown_items: 5,				// number of visible circles
                hv_switch: 0,					// 0 = horizontal slider, 1 = vertical
                active_item: 0,					// layer that will be shown at start, 0=first, 1=second...
                wrapper_text_max_height: 220,	// height of widget, displayed in pixels
                middle_click: 1,				// when main circle is clicked: 1 = slider will go to the previous layer/circle, 2 = to the next
                under_600_max_height: 1200,		// if resolution is below 600 px, set max height of content
                border_radius: -1,				// -1 = circle, 0 and other = radius
                border_color: "#e3e3e3",
                arrow_color: "#D3D3D3",
                allow_shadow: 0,
                auto_play: 1
            });
        });
    })(jQuery);
</script>

@endpush