@extends('master')
@section('meta_tags')
    <title>Contact - Get In Touch with JLT Dining, Dubai | JLT Dining</title>
    <meta name="keywords" content="contact jlt dining, contact jlt dubai, get in touch jlt dining, contact jlt restaurants, jlt dining office">
    <meta name="description" content="If you want to add your Jumeirah Lakes Towers Restaurant on JLT Dining, advertise, or get in touch, feel free to contact JLT Dining Dubai right away! ">
    @endsection
@section('content')
        <!--==================================Section Open=================================-->
<section class="clearfix">
    <div class="contact-left col-md-8 col-md-offset-2">
        <h3 class="margin-top-60 margin-bottom-30 lp-border-bottom padding-bottom-20">Contact us</h3>
        {!! Form::open(array('url' => '/contact','class'=>'form-horizontal')) !!}

        <div class="row">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (session()->has('flash_notification.message'))
                <div class="alert alert-{{ session('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! session('flash_notification.message') !!}
                </div>
            @endif
        </div>

        <div class="form-body">
            <div class="row">
                <div class="form-group">
                    <div class="col-sm-6">
                        {!! Form::text('first_name',null,array('class'=>'form-control nameform','placeholder'=>'First Name:','required' => '')) !!}
                    </div>
                    <div class="col-sm-6">
                        {!! Form::text('last_name',null,array('class'=>'form-control nameform','placeholder'=>'Last Name:','required' => '')) !!}
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="form-group">
                    <div class="col-sm-6">
                        {!! Form::text('email',null,array('class'=>'form-control nameform','type'=> 'email','placeholder'=>'Email:','required' => '')) !!}
                    </div>
                   {{-- <div class="col-sm-6 col-md-6">
                        {!! Form::text('phone',null,array('class'=>'form-control nameform','placeholder'=>'Phone:','required' => '')) !!}
                    </div>--}}
                </div>


            </div>


            <div class="row">
                <div class="form-group">
                    <div class="col-sm-12">
                        <label>How can we help you?</label>
                        <select name="help" id="help" class="form-control">
                            <option value="General">Click to select</option>
                            <option value="I found a mistake/bug on your site or an outdated listing">I found a
                                mistake/bug
                                on your site or an outdated listing
                            </option>
                            <option value="I would like to advertise on JLT Dining">I would like to advertise on JLT
                                Dining
                            </option>
                            <option value="I run a restaurant/cafe/bar in JLT and want to get listed">I run a
                                restaurant/café/bar in JLT and want to get listed
                            </option>
                            <option value="I want to write a blog post or restaurant review for JLT Dining">I want to
                                write
                                a blog post or restaurant review for JLT Dining
                            </option>
                            <option value="others">Others</option>
                        </select>
                    </div>
                </div>


            </div>

            <div class="row">
                <div class="form-group">
                    <div class="col-sm-12">
                        {!! Form::textarea('message',null,array('rows' => 5, 'id'   => 'message' , 'name' => 'message', 'placeholder' => 'Message', 'class'=>'form-control')) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <div class="col-md-6">
                        <label class="text-center"> <?php echo captcha_img();?></label>
                        {!! Form::text('captcha',null,array('class' => 'form-control')) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="submit" id="submit" name="submit" value="Submit"
                               class="lp-review-btn btn-second-hover">
                    </div>
                </div>
            </div>


        </div>
        <div id="success">
            <span class="green textcenter"><p>Your message was sent successfully! I will be in touch as soon as I
                    can.</p></span>
        </div>
        <div id="error">
            <span><p>Something went wrong, try refreshing and submitting the form again.</p></span>
        </div>
        {!! Form::close() !!}

    </div>
</section>
<!--==================================Section Close=================================-->
@endsection