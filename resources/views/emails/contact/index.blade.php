<!DOCTYPE html>
<html lang=&quot;en-US&quot;>
<head>
    <meta charset=&quot;utf-8&quot;>
</head>
<body><center>
    <style>
        /* Shrink Wrap Layout Pattern CSS */
        @media only screen and (max-width: 599px) {
            td[class="hero"] img {
                width: 100%;
                height: auto !important;
            }
            td[class="pattern"] td{
                width: 100%;
            }
        }
    </style>

    <table cellpadding="0" cellspacing="0">
        <tr>
            <td class="pattern" width="600">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td><img src="http://i.imgur.com/6YmaVY8.png">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="font-family: arial,sans-serif; color: #333;">
                            <h1>Hi {{--{{$contact->name}}--}}!</h1>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="font-family: arial,sans-serif; font-size: 14px; line-height: 20px !important; color: #666; padding-bottom: 20px;">
                            Thanks for contacting us.
                            <br>One of our staff member will review your submission and reply you back.
                            <br><br><b>Stay tuned!</b>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="font-family: arial,sans-serif; font-size: 11px; line-height: 20px !important; color: #666;">
                            Email: <a href="mailto:info@jltdining.com">info@jltdining.com</a>  |  Web: <a href="http://www.jltdining.com/">www.jltdining.com</a>
                        </td></tr>

                    <tr>
                        <td><img src="http://i.imgur.com/DNnBSgu.png">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</center></body>
</html>