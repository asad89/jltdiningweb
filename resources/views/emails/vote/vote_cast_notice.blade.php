<!DOCTYPE html>
<html lang=&quot;en-US&quot;>
<head>
    <meta charset=&quot;utf-8&quot;>
</head>
<body><center>
    <style>
        /* Shrink Wrap Layout Pattern CSS */
        @media only screen and (max-width: 599px) {
            td[class="hero"] img {
                width: 100%;
                height: auto !important;
            }
            td[class="pattern"] td{
                width: 100%;
            }
        }
    </style>

    <table cellpadding="0" cellspacing="0">
        <tr>
            <td class="pattern" width="600">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td><img src="http://i.imgur.com/6YmaVY8.png">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="font-family: arial,sans-serif; color: #333;">
                            <h1>Hello {{str_replace('-',' ',$user->name)}}!</h1>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="font-family: arial,sans-serif; font-size: 14px; line-height: 20px !important; color: #666; padding-bottom: 20px;">
                            Thanks for voting in the #JLTdiningAwards2019 !
                            <br>Let’s put Jumeirah Lakes Towers firmly on Dubai’s foodie map! Please help us spread the word about the Awards on- and offline. The Awards' hashtag is #JLTdiningAwards2019 – feel free to use it online across Facebook, Twitter, and Instagram! You can keep up with the Awards on our social media pages:
                            <br><br>
                            Facebook: @JLTdining<br><br>
                            Twitter: @JLTdining<br><br>
                            Instagram: @Dining_In_JLT<br><br>

                            Online voting will run until 11.59 p.m. on 31st October 2019. Once voting has closed, we will announce the top 3 shortlisted restaurants in each category on https://www.jltdining.com/blog and on our social media pages. Our panel of expert judges will then scrutinize the shortlist to finalise the winners and runners-up for all categories. The winners will be announced at the awards ceremony, which will take place in JLT on 25th November 2019.  We’ll announce the venue, timings, and ticket prices in early November on https://www.jltdining.com/blog and on our social media pages.
                            <br><br>
                            The JLT Restaurant Awards 2018 were made possible thanks to our corporate sponsors: Repeat, Monviso Water, iiko, Liquid of Life, Fruitful Day, Charles Russell Speechleys, sps:pr & sps:digital – part of Strategic Partnership Solutions.

                            <br><br>
                            We have more great events planned for the coming months – <a href="https://www.jltdining.com/newsletter" target="_blank">subscribe to our newsletter</a> to be the first to find out what’s happening in JLT!
                            <br><br>
                            Foodielicious Greetings,
                            <br><br>
                            The JLT Dining Team
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="font-family: arial,sans-serif; font-size: 11px; line-height: 20px !important; color: #666;">
                            Email: <a href="mailto:munch@jltdining.com">munch@jltdining.com</a>  |  Web: <a href="http://www.jltdining.com/">www.jltdining.com</a>
                        </td></tr>

                    <tr>
                        <td><img src="http://i.imgur.com/DNnBSgu.png">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</center></body>
</html>