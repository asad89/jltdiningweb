@extends('master')
@section('meta_tags')
    <title>Offers - Latest Restaurant News & Offers in Dubai | JLT Dining</title>
    <meta name="keywords" content="jlt dining blog, dubai restaurants blog, best restaurants dubai blog, restaurant news dubai, restaurant offers in dubai">
    <meta name="description" content="Welcome to JLT Dining's Blog page. Read our latest News and Offers from the best Restaurants in Jumeirah Lakes Towers Dubai. ">
    @endsection
    @section('content')
            <!--==================================Section Open=================================-->
    <section>
        <div class="container page-container-five">

            <div class="row">
                <div class="col-lg-12">
                    <h4 style="text-align: center;margin-bottom:20px;font-size:24px;font-family: Oxygen, sans-serif!important;">Welcome to the JLT Dining Restaurateurs Area!</h4>
                </div>
            </div>

            <div class="row">
                @if(count($posts))
                    <?php $i = 0; ?>
                    @foreach($posts as $post)
                        @if($i%3==0)
                            <?php $i = 0;?>
                            <div class="clearfix"></div>
                        @endif
                        <div class="col-md-6 col-sm-12 col-xs-12 col-lg-4 lp-blog-grid-box">
                            <div class="lp-blog-grid-box-container lp-border lp-border-radius-8">
                                <div class="lp-blog-grid-box-thumb">
                                    <a href="@if(!empty($post->offer)) {{asset('/uploads/offers/'.$post->offer)}} @elseif($post->id==85) {{"https://www.jltdining.com/restaurant-awards-2019-nominations"}}@elseif($post->id==90) {{"https://www.jltdining.com/jlt-restaurateurs-its-time-to-party-get-your-2019-jlt-dining-awards-tickets-now"}}@elseif($post->id==99) {{"https://www.jltdining.com/ten4jlt-2020-restaurateurs"}}@elseif($post->id==109) {{"https://www.jltdining.com/jlt-dining-partners"}}@else {{"#"}}@endif" style="color:#292929 !important;">
                                    @if(!empty($post->feature_image))
                                        @laravelImage('uploads/offers/large/', $post->feature_image, 555, 277, [
                                                                'fit' => 'crop-top-center'
                                                                    ], [
                                                                    'data-u' => "image",
                                                                    'class' => 'img-responsive',
                                                                ])
                                    @endif
                                    </a>
                                </div>
                                <div class="lp-blog-grid-box-description text-center">

                                    <div class="lp-blog-grid-category" style="color:#292929 !important;">
                                        <a href="@if(!empty($post->offer)) {{asset('/uploads/offers/'.$post->offer)}} {{"#"}}@endif" style="color:#292929 !important;">
                                            {!! $post->post_content !!}
                                        </a>
                                    </div>
                                  {{--  <div class="lp-blog-grid-title">
                                        <h4 class="lp-h4" style="color:#292929 !important;">
                                            <a href="@if(!empty($post->offer)) {{asset('/uploads/offers/'.$post->offer)}}@else {{"#"}}@endif"
                                               style="color:#292929 !important;">{{"Download"}} </a>
                                        </h4>
                                    </div>--}}

                                </div>
                            </div>
                        </div>
                        <?php $i++; ?>
                    @endforeach
            </div>

            <div class="row">
                <div class="col-lg-12 center-align">{{$posts->links()}}</div>
            </div>
            @else
                <p>No any post was found</p>
            @endif
        </div>
    </section>
    <!--==================================Section Close=================================-->
@endsection