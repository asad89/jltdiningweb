@extends('master')
@section('meta_tags')
    <title>{{ config('app.meta_title')}}</title>
    <meta name="keywords" content="{{config('app.meta_keywords')}}">
    <meta name="description" content="{{config('app.meta_description')}}">
    <link rel="canonical" href="https://www.jltdining.com/" />
@endsection
@push('site-head')
{{--<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>--}}
<script src="{{asset('js/jquery-1.11.3.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jssor.slider-21.1.6.mini.js')}}" type="text/javascript"></script>
<!-- Include the plugin's CSS and JS: -->
<script type="text/javascript" src="{{asset('js/bootstrap-multiselect.js')}}"></script>
<link rel="stylesheet" href="{{asset('css/bootstrap-multiselect.css')}}" type="text/css"/>
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-585ba46f0a9379e0"></script>
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $("#advanced-search").on("click", function () {
            $("#advanced").toggle();                 // .fadeToggle() // .slideToggle()
        });
        var jssor_1_SlideshowTransitions = [
            {
                $Duration: 1200,
                x: 0.3,
                $During: {$Left: [0.3, 0.7]},
                $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                x: -0.3,
                $SlideOut: true,
                $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                x: -0.3,
                $During: {$Left: [0.3, 0.7]},
                $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                x: 0.3,
                $SlideOut: true,
                $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                y: 0.3,
                $During: {$Top: [0.3, 0.7]},
                $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                y: -0.3,
                $SlideOut: true,
                $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                y: -0.3,
                $During: {$Top: [0.3, 0.7]},
                $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                y: 0.3,
                $SlideOut: true,
                $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                x: 0.3,
                $Cols: 2,
                $During: {$Left: [0.3, 0.7]},
                $ChessMode: {$Column: 3},
                $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                x: 0.3,
                $Cols: 2,
                $SlideOut: true,
                $ChessMode: {$Column: 3},
                $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                y: 0.3,
                $Rows: 2,
                $During: {$Top: [0.3, 0.7]},
                $ChessMode: {$Row: 12},
                $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                y: 0.3,
                $Rows: 2,
                $SlideOut: true,
                $ChessMode: {$Row: 12},
                $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                y: 0.3,
                $Cols: 2,
                $During: {$Top: [0.3, 0.7]},
                $ChessMode: {$Column: 12},
                $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                y: -0.3,
                $Cols: 2,
                $SlideOut: true,
                $ChessMode: {$Column: 12},
                $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                x: 0.3,
                $Rows: 2,
                $During: {$Left: [0.3, 0.7]},
                $ChessMode: {$Row: 3},
                $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                x: -0.3,
                $Rows: 2,
                $SlideOut: true,
                $ChessMode: {$Row: 3},
                $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                x: 0.3,
                y: 0.3,
                $Cols: 2,
                $Rows: 2,
                $During: {$Left: [0.3, 0.7], $Top: [0.3, 0.7]},
                $ChessMode: {$Column: 3, $Row: 12},
                $Easing: {$Left: $Jease$.$InCubic, $Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                x: 0.3,
                y: 0.3,
                $Cols: 2,
                $Rows: 2,
                $During: {$Left: [0.3, 0.7], $Top: [0.3, 0.7]},
                $SlideOut: true,
                $ChessMode: {$Column: 3, $Row: 12},
                $Easing: {$Left: $Jease$.$InCubic, $Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                $Delay: 20,
                $Clip: 3,
                $Assembly: 260,
                $Easing: {$Clip: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                $Delay: 20,
                $Clip: 3,
                $SlideOut: true,
                $Assembly: 260,
                $Easing: {$Clip: $Jease$.$OutCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                $Delay: 20,
                $Clip: 12,
                $Assembly: 260,
                $Easing: {$Clip: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                $Delay: 20,
                $Clip: 12,
                $SlideOut: true,
                $Assembly: 260,
                $Easing: {$Clip: $Jease$.$OutCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            }
        ];

        var jssor_1_options = {
            $AutoPlay: true,
            $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1
            },
            $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
            },
            $ThumbnailNavigatorOptions: {
                $Class: $JssorThumbnailNavigator$,
                $Cols: 10,
                $SpacingX: 8,
                $SpacingY: 8,
                $Align: 380
            }
        };

        var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

        //responsive code begin
        //you can remove responsive code if you don't want the slider scales while window resizing
        function ScaleSlider() {
            var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
            if (refSize) {
                refSize = Math.min(refSize, 850);
                jssor_1_slider.$ScaleWidth(refSize);
            }
            else {
                window.setTimeout(ScaleSlider, 30);
            }
        }

        ScaleSlider();
        $(window).bind("load", ScaleSlider);
        $(window).bind("resize", ScaleSlider);
        $(window).bind("orientationchange", ScaleSlider);
        //responsive code end
    });

</script>
<style>

    #advanced {
        display: none;
    }

    /* jssor slider arrow navigator skin 05 css */
    /*
    .jssora05l                  (normal)
    .jssora05r                  (normal)
    .jssora05l:hover            (normal mouseover)
    .jssora05r:hover            (normal mouseover)
    .jssora05l.jssora05ldn      (mousedown)
    .jssora05r.jssora05rdn      (mousedown)
    */
    .jssora05l, .jssora05r {
        display: block;
        position: absolute;
        /* size of arrow element */
        width: 40px;
        height: 40px;
        cursor: pointer;
        background: url('{{asset('img/a17.png')}}') no-repeat;
        overflow: hidden;
    }

    .jssora05l {
        background-position: -10px -40px;
    }

    .jssora05r {
        background-position: -70px -40px;
    }

    .jssora05l:hover {
        background-position: -130px -40px;
    }

    .jssora05r:hover {
        background-position: -190px -40px;
    }

    .jssora05l.jssora05ldn {
        background-position: -250px -40px;
    }

    .jssora05r.jssora05rdn {
        background-position: -310px -40px;
    }

    /* jssor slider thumbnail navigator skin 01 css */
    /*
    .jssort01 .p            (normal)
    .jssort01 .p:hover      (normal mouseover)
    .jssort01 .p.pav        (active)
    .jssort01 .p.pdn        (mousedown)
    */
    .jssort01 .p {
        position: absolute;
        top: 0;
        left: 0;
        width: 72px;
        height: 72px;
    }

    .jssort01 .t {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        border: none;
    }

    .jssort01 .w {
        position: absolute;
        top: 0px;
        left: 0px;
        width: 100%;
        height: 100%;
    }

    .jssort01 .c {
        position: absolute;
        top: 0px;
        left: 0px;
        width: 68px;
        height: 68px;
        border: #000 2px solid;
        box-sizing: content-box;
        background: url('{{asset('img/t01.png')}}') -800px -800px no-repeat;
        _background: none;
    }

    .jssort01 .pav .c {
        top: 2px;
        _top: 0px;
        left: 2px;
        _left: 0px;
        width: 68px;
        height: 68px;
        border: #000 0px solid;
        _border: #fff 2px solid;
        background-position: 50% 50%;
    }

    .jssort01 .p:hover .c {
        top: 0px;
        left: 0px;
        width: 70px;
        height: 70px;
        border: #fff 1px solid;
        background-position: 50% 50%;
    }

    .jssort01 .p.pdn .c {
        background-position: 50% 50%;
        width: 68px;
        height: 68px;
        border: #000 2px solid;
    }

    * html .jssort01 .c, * html .jssort01 .pdn .c, * html .jssort01 .pav .c {
        /* ie quirks mode adjust */
        width /**/: 72px;
        height /**/: 72px;
    }


</style>
@endpush
@section('banner')
    <div class="container">


        <div class="row home-top">
            <div class="col-md-9 col-xs-12 col-sm-offset-0 home-left">
                <div class="text-center">
                    <h1 style="color:#ffffff;margin-top:0px !important;">{{'Find the best restaurants, cafes, bars, and takeaways in JLT!'}}</h1>
                </div>


                <div class="lp-search-bar">
                    {{-- {{ Form::open(['url' => '/search', 'method' => 'POST',"class"=> "form-inline clearfix"]) }}--}}
                    {{ Form::open(['url' => '/search', 'method' => 'GET',"class"=> "form-inline clearfix"]) }}
                    <div class="lp-search-bar-left">
                        {{ Form::text('q', '', ['id' =>  'q', 'placeholder' =>  'What do you fancy? Cuisines, dishes, restaurants, moods...','class' =>'lp-search-input lp-home-search-input'])}}
                        <button type="submit" class="lp-search-btn">
                            <!-- Search icon by Icons8 -->
                            <img class="icon icons8-Search"
                                 src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADQAAAA0CAYAAADFeBvrAAAC5klEQVRoge2aXbHjMAyFCyEQAiEQAqEQAqEQwiAQCiEQAqEQFkIgfPtgZ5vKcn5sOb27s5rpk135HEmWZTm3W4YAFdAAd+ABDEDvfx3Q5ui/RDyJDpiAmWMyesLVt/H/EU9kOEEiJgNQ/wtEAmLfIjNGAM24sOuBFreXKsJ9NW2QenFVGHpQvyJETu8H/5+YvqYUj2XxhjDEkogouh8Rb5Uh5UNGWnIG7oZrxAxWW62xLKTtmZf5Qu+1XnIt60X6K+Pbk5Kessl+inLTMNtYt1H2U22hWHqnz1aavvYzV6HmncvKFPREVOcolKm0N0N7HEMnMDxylMnMVttBPYyhEhimHEXrcBuVOW0u4INYntmGxdVhm+GWbK3zWGTYtRZK7mJ8SavFk4Ri3PP7iDAhNJHxsgXkTd1HfYoSeQbUYnxIttZ5LJLQ+fNoBXiRSowvGXA2Q76NJ5uQ9JAMuWdszFqAWmA5X9fhbpZracX42oO21XCIRdZ1XYqSzcyiEC5WsLKTcY8qkRtx3BkvdqXAqmJRFMnEIBOH+e2SsGJJD2/F1Z0yR94uTT1FGNp9jrIgrJQ5MgMtkn3DJLyO50cAYWHYK3OkFdcAkjtChEdHUCCnKJVeAiWkcKlVht8HGETDfosoYQdoxqq6VywVdb0yV/PapBllpUO7qdq2iBXrb7ZrcWHY40L2iQu9dovIiozWMrOt6tFbS9YZLdZmLnPvQm8tZYeDN1avGOyDlJ/XYlmVoLdrF291CUTaiFf25GVGDL1du5blZa7FJw//n5r3k8oYMQw4b0wHiZ0y4h4xWfrkyse59S1SNeHhm0JkJLxzxfZsWVIrYnsvc5LEhEsGdURnrAK5hpQCpuPzLBp4763dcyWBUFlSFkJaBuy+jTsqiV768aRi76//Sf0oySBVvLubLImk8i+GJSWB1CWvJFlyktT13w+lyEFSl74LZ8sOqUs+QTCXCKm/k8wiuMp8wFXtwceEvwElpHadBDbELgAAAABJRU5ErkJggg=="
                                 width="18" height="18" style="margin-right: 5px;">Search
                        </button>

                        <a href="{{url('feeling-hungry-and-lucky')}}"
                           class="btn lp-search-btn"><!-- Meal icon by Icons8 -->
                            <img class="icon icons8-Meal"
                                 src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAEO0lEQVRogc1aW7HjMAy9EAohEAohEAohEAKhDAohEAqhEAIhEALh7Eel+kSRn/XdXc90prFlSUeWZfnx89OpALgAGAE85PcCsALY5bcBeAJYANwBTL1kf11E+VmU3tFWnsLj8q8APIzyuwC6i2IjgKvQXuT/DcAkNC8H1APA8C8AqPJTq0Wl72IB9dadBY54+7mWF4BrR/6DA+jWi78KmWkUugJwZF0NoHsPphdiugOYTdskrrbKaNkI9YATnaQtaRAx3mf0vwXxJBA308ZulisraA6ZtntCh5F5tALRkdis5UjAZiynEUoLu+SV+tsStTjerrbm6GKdVbndgpD2SdoXVswqKv9f8nmz7XiHYdh2R95ABpljdJ6Se4o5AX0UAHlYBUw7G21I6MVulo5meLtFFjmC200FQNTqd69dvnUuLhn91BOSoFnoM8NQBd8KgNwsT4ee51VyYc2CxtEPxwyzF9MVAnl57VS3SPWUkT0Q6MEjUF9OjobQbswoA+Qqn1sGiLpNifzFpUWYG26U+hKIus2eAaKW3n4yBUdXHLhBI0fWGgbIpSOQE11GBx2VmSsPk7eAic4lBbLiOJntdxSo4evWR3Q4zj0cQ+7QAqSAPgsEzlzK8Dy61wlZGRNNGUrmk/r+SnUekLFBD/WkideO4s0MQoS7F9Ce1qYIkBY9Qh8Uxm/DgGP5mKC7wlmbIkB0lIs3U+DFFpUTnZjoqJysiHAosUfaD0AQ5seOii3zwR0RQmn1rs+A4ZV7ofqHVc4BUu1W0i+EbJjFrQEMu89grBvLnj9AcIya4zdAqkJphOHHPVEQfQwQ9fOmHeCHFypCaYLZhwdCIIiu0AbIIp/3BrkhrYHJZBuBfNwTBamGAVIdrYiPuvHKE7ZsC3lmdkr2kJl3BkiSNiNbM+alB5DTnMiNcmREWqJm2HLD2cE1Mwt1SeMYIM2GhElRireZEWandAVhXXC3owbIN2vIMdklV5gagJwyg9woGyBNHgEvbKNim+kwVCPwAVxyt2eAZMN1hMdiPeGc29cx3Gw/ZEKwAVK1M6Q+/h6KECbPlnoD8b4LZJ62BtzYNCqIpDgp5WyfGiAIx1bxgxLEjln+LyCqYzzKGd+bChmvrJTU5SY7Z8tJWtOv6JxYiXWlBgryLzirODgH8vtwkpnNlolnlZF5kcse2BEtr+zJRQ5h7ZnJytEgg+Nxbt1FKQnbUsNIrrGL1dx9uumj4Bc49ycO/5Xo6/dNxGBPuRn8O/OUhS+G1t2r43iTvHo0NWCeJDB1Z6L37zverpW7HpiEdvVGA+eb5O9fRuB4RbakXK2DrCvMCU0XECSAo5kKGDry915VTL34ewIXOxc81yjkxS+KGMCvjjorYF8naHmKG+ppykAKDwgPa2Ivil74xVcVKUADKdVS+EXR3wcQKwhPOvSJxkYKb5B7E6GZe7rPH4awjP6j83+DAAAAAElFTkSuQmCC"
                                 width="25" height="25"
                                 style="margin-right:5px;margin-top:-2px;">{{"I’m feeling hungry & lucky"}}</a>


                    </div>

                    <div class="clearfix"></div>
                    {{Form::close()}}
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <a id="advanced-search" style="color:#fff;text-decoration: underline;">Advanced
                                Search</a>

                            {{-- <button id="advanced-search" class="pull-right btn-sm">Advanced Search</button>--}}
                        </div>
                    </div>
                    <div class="row" id="advanced" style="position: absolute;width:100%;margin-top:15px;z-index:100;">
                        <div class="col-md-11 col-sm-11 col-xs-11 search-row margin-top-subtract-35  margin-bottom-10">

                            {{ Form::open(['url' => '/search', 'method' => 'GET',"class"=> "form-inline clearfix"]) }}


                            <div class="col-md-3 col-sm-12 col-xs-12 col-lg-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon lp-border"><i class="fa fa-crosshairs"></i></div>
                                        <select name="meals[]" class="" id="meals" multiple="multiple">

                                            @if(isset($meals) && count($meals))
                                                @foreach($meals as $meal)
                                                    <option value="{{$meal->slug}}">{{$meal->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>

                                    </div>
                                </div>
                            </div>


                            <div class="col-md-3 col-sm-12 col-xs-12 col-lg-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon lp-border"><i class="fa fa-cutlery"></i></div>
                                        <select name="cuisines[]" class="" id="cuisines" multiple="multiple">
                                            @if(isset($cuisines) && count($cuisines))
                                                @foreach($cuisines as $cuisine)
                                                    <option value="{{ $cuisine->slug }}">{{$cuisine->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-12 col-xs-12 col-lg-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon lp-border"><i class="fa fa-building"></i></div>
                                        <?php
                                        $cluster = [
                                                'a' => 'A', 'b' => 'B', 'c' => 'C', 'd' => 'D', 'e' => 'E','f' => 'F', 'g'  => 'G', 'h' => 'H',
                                                'i' => 'I', 'j' => 'J', 'k' => 'K', 'l' => 'L', 'm' => 'M', 'n' => 'N','o' => 'O','p' => 'P',
                                                'q' => 'Q', 'r' => 'R', 's' => 'S', 't' => 'T', 'u' => 'U', 'v' => 'V', 'w' => 'W','x' => 'X',
                                                'y' => 'Y', 'z' => 'Z','aa' =>'AA', 'bb' => 'BB', 'one-jlt' => 'One JLT',
                                        ];
                                        ?>

                                        <select name="cluster" class="form-control" id="clusters" placeholder="test" style="height:35px;">
                                            @if(isset($cluster) && count($cluster))
                                                <option value="" disabled selected>{{"Choose Cluster"}}</option>
                                                @foreach($cluster as $key=>$value)
                                                    <option value="{{ strtolower($value) }}">{{"Cluster " .$value }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group margin-right-0">
                                <div class="input-group margin-right-0">
                                    <div class="input-group-addon lp-border"><i class="fa fa-list"></i></div>
                                    <select name="features[]" class="" id="tags" multiple="multiple">
                                        @if(isset($features) && count($features))
                                            @foreach($features as $feature)
                                                <option value="{{ $feature->slug }}">{{$feature->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center padding-bottom-15">
                                    {{ Form::submit('Search', array('class' => 'lp-search-btn')) }}
                                </div>
                            </div>

                            {{Form::close()}}
                        </div>
                    </div>
                   {{-- <div class="row">
                        <div class="col-md-12">
                            <a href="https://www.movenpick.com/en/middle-east/uae/dubai/hotel-dubai-ibn-battuta/offers/offer-detail/turkey-takeaway/" style="height: 80%;">
                                <img src="{{asset('images/turkey.jpg')}}" alt="{{config('app.name')}}" class="img-responsive">
                            </a>
                        </div>
                    </div>--}}
                </div>

                <div class="text-center lp-search-description">

                    <p>Looking for inspiration? Don’t know what you want to eat? Scroll down to find out what’s hot or
                        new, and to check out current special offers!
                    </p>
                    <img src="{{asset('images/banner/banner-arrow.png')}}" alt="banner-arrow"
                         class="banner-arrow"/>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12 col-lg-3">
                {{--<a href="#" target="_blank">
                    <img src="{{asset('images/home-banner-right.jpg')}}" style="height:485px;" alt="" class="pull-right img-responsive"></a>--}}
                <a href="https://photos.app.goo.gl/dDnCuhmPs8fySxHE6" target="_blank">
                    <img src="{{asset('images/jlt-awards-side-banner-compressor.png')}}" alt="Award 2019" class="pull-right img-responsive">
                </a>



              {{--  <a href="https://www.jltdining.com/restaurant-awards-2017" target="_blank">
                    <img src="{{asset('images/voting-banner.png')}}" alt="Award 2017" class="pull-right img-responsive">
                </a>--}}
               {{-- <a href="#" target="_blank">
                    <img src="{{asset('images/bannerhome1.png')}}" alt="" class="pull-right img-responsive">
                </a>--}}

            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="lp-section-row home-slider">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="lp-section-title-container text-center ">
                        <h1 style="color:#292929 !important;">{{'What’s hot in JLT?'}}</h1>

                        <div class="lp-sub-title"
                             style="color:#292929 !important;">{{'Trending restaurants, cafes, and bars in your neighbourhood'}}</div>
                    </div><!-- ../section-title-->
                    <div class="lp-section-content-container">
                        <div class="row">


                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">

                                <div id="jssor_1" class="banner-bg"
                                     style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 850px; height: 484px; overflow: hidden; visibility: hidden; background-color: #24262e;">
                                    <!-- Loading Screen -->
                                    <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
                                        <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
                                        <div style="position:absolute;display:block;background:url('{{asset('img/loading.gif')}}') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
                                    </div>
                                    <div data-u="slides" class="text-center"
                                         style="cursor: default; position: absolute; top: 0px; left: 0px; width: 850px; height: 484px; overflow: hidden;">
                                        @if(!empty($premium))
                                            @foreach($premium as $pre)
                                                @foreach($pre->get_hotel_images as $image)
                                                    <div data-p="144.50" style="display: none;">
                                                            <a href="{{url('/restaurants/'.$pre->slug)}}">
                                                            <h3 style="color:#fff !important;padding:15px;background:#003f24;text-align: center;z-index:100;width:100%;position:absolute;opacity:0.8;">{{$pre->name}}</h3></a>


                                                        <a href="{{url('/restaurants/'.$pre->slug)}}">
                                                            @laravelImage('hotels/premium/large/', $image->image, [
                                                            'fit' => 'crop-center'
                                                                ], [
                                                                'data-u' => "image",
                                                                'class' => 'img-responsive',


                                                            ])
                                                        </a>
                                                        @laravelImage('hotels/premium/large/', $image->image, 72, 72, [
                                                        'fit' => 'crop-center'
                                                            ], [
                                                            'data-u' => "thumb",
                                                            'class' => 'img-responsive'
                                                            ])
                                                    </div>
                                                @endforeach
                                            @endforeach

                                        @endif
                                    </div>
                                    <!-- Thumbnail Navigator -->
                                    <div data-u="thumbnavigator" class="jssort01"
                                         style="position:absolute;left:0px;bottom:0px;width:100%;height:100px;"
                                         data-autocenter="1">
                                        <!-- Thumbnail Item Skin Begin -->
                                        <div data-u="slides" style="cursor: default;">
                                            <div data-u="prototype" class="p">
                                                <div class="w">
                                                    <div data-u="thumbnailtemplate" class="t"></div>
                                                </div>
                                                <div class="c"></div>
                                            </div>
                                        </div>
                                        <!-- Thumbnail Item Skin End -->
                                    </div>
                                    <!-- Arrow Navigator -->
                                      <span data-u="arrowleft" class="jssora05l"
                                            style="top:245px;left:8px;width:40px;height:40px;"></span>
                                      <span data-u="arrowright" class="jssora05r"
                                            style="top:245px;right:8px;width:40px;height:40px;"></span>
                                </div>


                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="padding-left:0px;">
                                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                <!-- Home Page Advertisement -->
                                <ins class="adsbygoogle"
                                     style="display:inline-block;width:278px;height:485px"
                                     data-ad-client="ca-pub-8842445166702832"
                                     data-ad-slot="9068588192"></ins>
                                <script>
                                    (adsbygoogle = window.adsbygoogle || []).push({});
                                </script>
                                {{--<img src="{{asset('images/home-banner-right.jpg')}}" alt="" class="pull-left img-responsive">--}}
                            </div>
                        </div>

                    </div><!-- ../section-content-container-->
                </div>
            </div>
        </div>
    </div><!-- ../section-row-->
    <div class="lp-section-row exculsive-section aliceblue">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="lp-section-title-container text-center ">
                        <h1 style="color: #ffffff !important;">{{"Exclusive"}}</h1>

                        <div class="lp-sub-title"
                             style="color: #ffffff !important;">{{ "The latest listings on JLT Dining" }}</div>
                    </div><!-- ../section-title-->
                </div>
            </div>
            <div class="lp-section-content-container lp-list-grid-section">
                <div class="row">
                    @if(isset($hotels) && count($hotels))
                        @foreach($hotels as $hotel)
                            <div class="col-md-3 col-sm-6 lp-grid-box-contianer lp-grid-box-contianer1">
                                <div class="lp-grid-box lp-border lp-border-radius-8">
                                    <div class="lp-grid-box-thumb-container">
                                        <div class="lp-grid-box-thumb">
                                            @if(!empty($hotel->image))
                                                @laravelImage('uploads/hotels/large/', $hotel->image, 272, 231, [
                                                    'fit' => 'crop-center'
                                                        ], [
                                                        'alt' => $hotel->name,
                                                        'class' => 'img-responsive',
                                                    ])
                                            @else
                                                @laravelImage('uploads/restaurant/', 'coming-soon.png', 272, 231, [
                                                    'fit' => 'crop-center'
                                                        ], [
                                                        'alt' => $hotel->name,
                                                        'class' => 'img-responsive',

                                                     ])
                                            @endif
                                        </div><!-- ../grid-box-thumb -->
                                    </div>
                                    <div class="lp-grid-box-description ">
                                        <h4 class="lp-h4">
                                            <a href="{{url('/restaurants/'.$hotel->slug)}}">
                                                {{$hotel->name}}
                                            </a>
                                        </h4>

                                        <p>
                                            <i class="fa fa-building"></i>
                                            <span>{{$hotel->address}}</span>
                                        </p>
                                        <ul class="lp-grid-box-price">
                                            <li class="category-cion">Phone</li>
                                            <li><span>{{$hotel->phone}}</span></li>
                                        </ul>
                                    </div><!-- ../grid-box-description-->
                                </div><!-- ../grid-box -->
                            </div>
                        @endforeach
                    @endif
                </div>
            </div><!-- ../section-content-container-->
        </div>
    </div><!-- ../section-row -->


    <style type="text/css">
        .quick-links {
            padding: 25px;
        }

        .quick-links a {
            transition: opacity .25s ease-out;
            text-align: center;

            color: #292929 !important;
        }

        .quick-links a:hover {
            text-decoration: none;
        }

        .quick-links img {
            height: 48px;
            width: 48px;
            display: inline-block;
            vertical-align: middle;
            position: relative;
            max-width: 100%;
            background-color: transparent;
            margin-bottom: 5px;
            border: 0px;
            margin-left: 44px;
            margin-right: 44px;
        }

        .quick-links span {
            display: block;
        }


    </style>

    <div class="lp-section-row">
        <div class="container">
            <div class="row">
                <div class="col-md-12 quick-links">
                    <div class="lp-about-section-best-header">
                        <h3 class="margin-top-0 text-center">Quick Links</h3>
                    </div>
                    <div class="col-md-12"
                         style="margin-top:10px;display: flex;-webkit-box-orient: horizontal;-webkit-box-direction: normal;flex-direction: row;align-items: stretch;flex-wrap: wrap;-webkit-box-align: stretch;">
                        <a href="http://jltdining.com/search?meals%5B%5D=breakfast">
                            <img src="{{asset('images/break-fast.png')}}"
                                 style="height: 48px; width: 48px; display: inline-block;" class="img-responsive">
                            <span>Breakfast</span>
                        </a>
                        <a href="http://jltdining.com/search?meals%5B%5D=lunch">
                            <img src="{{asset('images/lunch.png')}}"
                                 style="height: 48px; width: 48px; display: inline-block;" class="img-responsive">
                            <span>Lunch</span>
                        </a>
                        <a href="http://jltdining.com/search?meals%5B%5D=dinner"
                           class="column ta-center start-categories-item">
                            <img src="{{asset('images/dinner.png')}}"
                                 style="height: 48px; width: 48px; display: inline-block;" class="img-responsive">
                            <span>Dinner</span>
                        </a>
                        <a href="http://jltdining.com/search?meals%5B%5D=coffee-tea"
                           class="column ta-center start-categories-item">
                            <img src="{{asset('images/coffee.png')}}"
                                 style="height: 48px; width: 48px; display: inline-block;" class="img-responsive">
                            <span>Coffee & Tea</span>
                        </a>
                        <a href="#">
                            <img src="{{asset('images/juice.png')}}"
                                 style="height: 48px; width: 48px; display: inline-block;" class="img-responsive">
                            <span>Juices</span>
                        </a>
                        <a href="#" class="column ta-center start-categories-item">
                            <img src="{{asset('images/sweet.png')}}"
                                 style="height: 48px; width: 48px; display: inline-block;" class="img-responsive">
                            <span>Sweet Tooth</span>
                        </a>
                        <a href="http://jltdining.com/search?features%5B%5D=delivery-available" class="column ta-center start-categories-item">
                            <img src="{{asset('images/delivery.png')}}"
                                 style="height: 48px; width: 48px; display: inline-block;" class="img-responsive">
                            <span>Delivery</span>
                        </a>
                        <a href="http://jltdining.com/search?features%5B%5D=shisha" class="column ta-center start-categories-item">
                            <img src="{{asset('images/shisha.png')}}"
                                 style="height: 48px; width: 48px; display: inline-block;" class="img-responsive">
                            <span>Shisha</span>
                        </a>
                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- Small text banner -->
                        <ins class="adsbygoogle"
                             style="display:block"
                             data-ad-client="ca-pub-8842445166702832"
                             data-ad-slot="7219573107"
                             data-ad-format="auto"
                             data-full-width-responsive="true"></ins>
                        <script>
                            (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div>
                </div>

            </div>
        </div>
    </div><!-- ../section-row -->

    <div class="lp-section-row  blog-section lp-section-row exculsive-section aliceblue home-blog-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="lp-section-title-container text-center ">
                        <h1>{{'#JLTdining – Latest News'}}</h1>

                        <div class="lp-sub-title">{{'The latest news & articles from our blog'}}</div>
                    </div><!-- ../section-title-->
                    <div class="lp-section-content-container lp-blog-grid-container">
                        <div class="row">
                            @if(isset($latest_posts))
                                @foreach($latest_posts as $post)
                                    <div class="col-md-4 col-sm-4 lp-blog-grid-box">
                                        <div class="lp-blog-grid-box-container lp-border lp-border-radius-8">
                                            <div class="lp-blog-grid-box-thumb">
                                                @if(!empty($post->feature_image))

                                                    @laravelImage('uploads/posts/large/', $post->feature_image, 372, 285, [
                                                     'fit' => 'crop-center'
                                                         ], [
                                                         'alt' => $hotel->name,
                                                         'class' => 'img-responsive',
                                                     ])
                                                 @else
                                                    <img src="{{asset('images/grid/grid-1.png')}}"
                                                         alt="{{$post->name}}"/>
                                                @endif
                                            </div>
                                            <div class="lp-blog-grid-box-description text-center">
                                                <div class="lp-blog-grid-category">
                                                    <a href="{{url('blog/'.$post->slug)}}">
                                                        {{$post->name}}
                                                    </a>
                                                </div>
                                                <div class="lp-blog-grid-title">
                                                    <h4 class="lp-h4">
                                                        <a href="{{url('blog/'.$post->slug)}}">{{$post->title}} </a>
                                                    </h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div><!-- ../section-content-container-->
                </div>
            </div>
        </div>
    </div><!-- ../section-row -->
@endsection

@push('site_footer')
<script>
    $(function () {
        $("#q").autocomplete({
            source: "{{ url('search/autocomplete') }}",
            minLength: 1,
            maxLength: 5,
            select: function (event, ui) {
                $('#q').val(ui.item.value);
            }
        });
    });

    $(".custom.combobox").autocomplete({
        select: function (event, ui) {
        }
    });


</script>
<script src="{{asset('js/bootstrap.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('js/bootstrap-multiselect.js')}}"></script>
<script type="text/javascript">
    // Carousel Auto-Cycle
    $(document).ready(function () {


        $('#meals').multiselect({
            includeSelectAllOption: true,
            selectAllNumber: false,
            nonSelectedText: 'Moods',
            enableFiltering: true
        });

        $('#cuisines').multiselect({
            includeSelectAllOption: true,
            selectAllNumber: false,
            nonSelectedText: 'Cuisines',
            enableFiltering: true
        });


        $('#tags').multiselect({
            includeSelectAllOption: true,
            selectAllNumber: false,
            nonSelectedText: 'Features',
            enableFiltering: true
        });

        $('#dishes').multiselect({
            includeSelectAllOption: true,
            selectAllNumber: false,
            enableFiltering: true
        });

        $('#cluster').multiselect({
            includeSelectAllOption: true,
            selectAllNumber: false,
            enableFiltering: true
        });



    });
</script>
@endpush