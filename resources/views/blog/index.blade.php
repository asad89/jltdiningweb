@extends('master')
@section('meta_tags')
<title>Blog - Latest Restaurant News & Offers in Dubai | JLT Dining</title>
<meta name="keywords" content="jlt dining blog, dubai restaurants blog, best restaurants dubai blog, restaurant news dubai, restaurant offers in dubai">
<meta name="description" content="Welcome to JLT Dining's Blog page. Read our latest News and Offers from the best Restaurants in Jumeirah Lakes Towers Dubai. ">
@push('site-head')
        <!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-585ba46f0a9379e0"></script>

@endpush
@endsection
@section('content')
    <!--==================================Section Open=================================-->
    <section>
        <div class="container page-container-five">
            <div class="row">
                @if(count($posts))
                    <?php $i = 0; ?>
                    @foreach($posts as $post)
                            @if($i%3==0)
                                <?php $i = 0;?>
                                <div class="clearfix"></div>
                            @endif
                                 <div class="col-md-6 col-sm-12 col-xs-12 col-lg-4 lp-blog-grid-box">
                            <div class="lp-blog-grid-box-container lp-border lp-border-radius-8">
                                <div class="lp-blog-grid-box-thumb">
                                       @if(!empty($post->feature_image))
                                        @laravelImage('uploads/posts/large/', $post->feature_image, 410,308, [
                                                                'fit' => 'crop-top-center'
                                                                    ], [
                                                                    'data-u' => "image",
                                                                    'class' => 'img-responsive',
                                                                ])
                                        @endif
                                </div>
                                <div class="lp-blog-grid-box-description text-center">
                                    {{--  <div class="lp-blog-user-thumb margin-top-subtract-25">
                                          <img src="images/blog/blog-user-thumb.png" alt="blog-user-thumb" />
                                      </div>--}}
                                    <div class="lp-blog-grid-category" style="color:#292929 !important;">
                                        <a href="{{url('blog/'.$post->slug)}}" style="color:#292929 !important;">
                                            {{$post->name}}
                                        </a>
                                    </div>
                                    <div class="lp-blog-grid-title">
                                        <h4 class="lp-h4" style="color:#292929 !important;">
                                            <a href="{{url('blog/'.$post->slug)}}"
                                               style="color:#292929 !important;">{{$post->title}} </a>
                                        </h4>
                                    </div>
                                   {{-- <div class="post-meta-right-box text-center">
                                        <div class="addthis_inline_share_toolbox"></div>
                                    </div>
--}}

                                    {{-- <ul class="lp-blog-grid-author">
                                         <li>
                                             <a href="author.html">
                                                 <i class="fa fa-user"></i>
                                                 <span>Admin</span>
                                             </a>
                                         </li>
                                         <li>
                                             <i class="fa fa-calendar"></i>
                                             <span>{{date('D-m-Y',strtotime($post->created_at))}}</span>
                                         </li>
                                     </ul>--}}<!-- ../lp-blog-grid-author -->
                                </div>
                            </div>
                        </div>
                                <?php $i++; ?>
                    @endforeach
            </div>

                        <div class="row">
                            <div class="col-lg-12 center-align">{{$posts->links()}}</div>
                        </div>
                @else
                    <p>No any post was found
                @endif
        </div>
    </section>
    <!--==================================Section Close=================================-->
@endsection