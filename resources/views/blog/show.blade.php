@extends('master')
    @section('meta_tags')
    <title>@if(isset($page->meta_title) && !empty($page->meta_title)){{$page->meta_title}} @else{{config('app.meta_title')}}@endif</title>
    <meta name="keywords" content="@if(isset($page->meta_keywords) && !empty($page->meta_keywords)){{$page->meta_keywords}} @else{{config('app.meta_keywords')}}@endif">
    <meta name="description" content="@if(isset($page->meta_description) && !empty($page->meta_description)){{$page->meta_description}} @else{{config('app.meta_description')}}@endif">
    @endsection
    @push('site-head')
            <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-585ba46f0a9379e0"></script>
    <meta property="og:title" content="{!! $page->title !!}" />
  {{--  <meta property="og:description" content="{{str_limit($page->post_content,60)}}}" />--}}
    <meta property="og:image" content="@if(!empty($page->feature_image)) {{asset('uploads/posts/large/'.$page->feature_image)}} @endif" />
    @endpush

    @section('content')
            <!--==================================Section Open=================================-->
    <section class="aliceblue brown-bg">
        <div class="container breadcrumb-wrap">
                <ol class="breadcrumb">
                    <li><a href="{{url('/')}}">{{"Home"}}</a></li>
                    <li><a href="{{url('/blog')}}">{{"Blog"}}</a></li>
                    <li><a href="{{url($page->slug)}}">{{ ucwords($page->name) }}</a></li>
                </ol>
        </div>

        <div class="container page-container-second">
            <div class="row">
                <div class="col-md-9 padding-40 blog-single-inner-container lp-border lp-border-radius-8">
                    <div class="blog-content popup-gallery">
                      {{--  @if(!empty($page->feature_image))
                            <img src="{{asset('uploads/posts/large/'.$page->feature_image)}}" alt="">
                        @endif--}}
                        <h1 class="page-heading-custom" style="text-align: center;margin-bottom:20px;font-size:24px;font-family: Oxygen, sans-serif!important;">{{$page->name}}</h1>
                        <h3 class="page-heading-custom" style="text-align: center;margin-bottom:20px;font-size:18px;font-family: Oxygen, sans-serif!important;">{{$page->title}}</h3>
                        <?php echo $page->post_content; ?>
                        <div class="col-md-12">
                            <div class="post-meta-right-box text-center">
                                <div class="addthis_inline_share_toolbox"></div>
                            </div>
                        </div>
                        <img src="{{asset('images/banner3.png')}}" alt="" title="" class="img-responsive" style="width:100%;">

                    </div>
                </div>
                <div class="col-md-3">
                    <div class="sidebar-post">
                        <div class="widget-box  widget-bg-color post-author-box lp-border-radius-5">
                            <div class="widget-content">

                                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                <!-- Small text banner -->
                                <ins class="adsbygoogle"
                                     style="display:block"
                                     data-ad-client="ca-pub-8842445166702832"
                                     data-ad-slot="7219573107"
                                     data-ad-format="auto"
                                     data-full-width-responsive="true"></ins>
                                <script>
                                    (adsbygoogle = window.adsbygoogle || []).push({});
                                </script>
                                <br><br>
                                <a href="http://halakiwi.com/" target="_blank"><img src="{{asset('images/kiwi.png')}}" alt="KIWI" style="width:100%;" class="img-responsive"></a>




                            </div>
                        </div>

                       {{-- <div class="widget-box  widget-bg-color post-author-box lp-border-radius-5">
                            <div class="widget-content text-center">
                                <img src="{{asset('images/bannerhome1.png')}}" alt="" class="img-responsive" style="width:100%;">
                            </div>
                        </div>--}}
                    </div>

                </div>

            </div>
        </div>
    </section>
    <!--==================================Section Close=================================-->
@endsection