@extends('master')
@section('meta_tags')
    <title>Restaurants, Lounges & Cafes in Jumeirah Lakes Towers | JLT Dining</title>
    <meta name="keywords" content="jumeirah lakes towers restaurants, restaurants in dubai, dubai restaurants, jlt restaurants, best restaurants in jlt, cafes in jlt, shisha in jlt">
    <meta name="description" content="Jumeirah Lakes Towers in Dubai boasts over 200 independent Restaurants, Lounges, and Cafes. Find the best Restaurants in JLT Dubai. ">
    <link rel="canonical" href="{{url('/')}}" />
@endsection
@push('site-head')
<script src="{{asset('js/jquery-1.11.3.min.js')}}" type="text/javascript"></script>
@endpush
@section('content')
    <div id="page">
        <section class="brown-bg">
            <div class="row listing-page-result-row margin-bottom-25">
                <div class="container breadcrumb-wrap">
                    <div class="col-md-12 col-sm-12 text-left breadcrumb">
                        <p>JLT Dining Partners Directory</p>
                    </div>
                </div>
            </div>
            <div class="container page-container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 lp-grid-box-contianer lp-grid-box-contianer1">
                                <div class="lp-grid-box lp-border lp-border-radius-8">
                                    <div class="lp-grid-box-thumb-container">
                                        <h4 class="lp-h4" style="text-align: center;"><a href="https://www.jltdining.com/social-digital-media-solutions" target="_blank">Social & Digital Media Solutions</a></h4>
                                        <div class="lp-grid-box-thumb">
                                            <img src="https://www.jltdining.com/images/sps-digital-partner.png"  alt="Social & Digital Media Solutions">


                                            {{--@if(!empty($item->feature_image))--}}
                                                {{--@laravelImage('uploads/events/large/', $item->feature_image, 555, 277, [--}}
                                                        {{--'fit' => 'crop-top-center'--}}
                                                         {{--], [--}}
                                                         {{--'alt' => $item->name,--}}
                                                          {{--'class' => 'img-responsive'--}}
                                                        {{--])--}}
                                            {{--@else--}}
                                                {{--@laravelImage('uploads/hotels/', 'coming-soon.png', 555, 555, [--}}
                                                       {{--'fit' => 'crop-top-center'--}}
                                                        {{--], [--}}
                                                        {{--'alt' => $item->name,--}}
                                                         {{--'class' => 'img-responsive'--}}
                                                       {{--])--}}
                                            {{--@endif--}}
                                        </div><!-- ../grid-box-thumb -->
                                    </div>
                                    <div class="lp-grid-box-description "><p><span>Get great deals on social media management, SEO, PPC, website design, and a range of other digital services from sps:digital. If it’s online, make sps:digital your first click!</span></p></div><!-- ../grid-box-description-->
                                </div><!-- ../grid-box -->
                            </div>
                    <div class="col-md-6 col-sm-6 lp-grid-box-contianer lp-grid-box-contianer1">
                                <div class="lp-grid-box lp-border lp-border-radius-8">
                                    <div class="lp-grid-box-thumb-container">
                                        <h4 class="lp-h4" style="text-align: center;"><a href="https://www.jltdining.com/pr-marketing-graphic-design" target="_blank">PR, Marketing, & Graphic Design</a></h4>
                                        <div class="lp-grid-box-thumb">
                                            {{--@if(!empty($item->feature_image))--}}
                                                {{--@laravelImage('uploads/events/large/', $item->feature_image, 555, 277, [--}}
                                                        {{--'fit' => 'crop-top-center'--}}
                                                         {{--], [--}}
                                                         {{--'alt' => $item->name,--}}
                                                          {{--'class' => 'img-responsive'--}}
                                                        {{--])--}}
                                            {{--@else--}}
                                                {{--@laravelImage('uploads/hotels/', 'coming-soon.png', 555, 555, [--}}
                                                       {{--'fit' => 'crop-top-center'--}}
                                                        {{--], [--}}
                                                        {{--'alt' => $item->name,--}}
                                                         {{--'class' => 'img-responsive'--}}
                                                       {{--])--}}
                                            {{--@endif--}}
                                        </div><!-- ../grid-box-thumb -->
                                    </div>
                                    <div class="lp-grid-box-description "><p><span>
                                                For PR, marketing, and graphic design services that don’t cost the earth, make sps:pr your trusted business partner. Online or offline, retained or one-off – sps:pr has deals for all occasions.
                                            </span></p></div><!-- ../grid-box-description-->
                                </div><!-- ../grid-box -->
                            </div>
                    <div class="col-md-6 col-sm-6 lp-grid-box-contianer lp-grid-box-contianer1">
                                <div class="lp-grid-box lp-border lp-border-radius-8">
                                    <div class="lp-grid-box-thumb-container">
                                        <h4 class="lp-h4" style="text-align: center;"><a href="https://www.jltdining.com/pr-marketing-graphic-design" target="_blank">Training, Coaching, & Mystery Shopping</a></h4>
                                        <div class="lp-grid-box-thumb">
                                            {{--@if(!empty($item->feature_image))--}}
                                                {{--@laravelImage('uploads/events/large/', $item->feature_image, 555, 277, [--}}
                                                        {{--'fit' => 'crop-top-center'--}}
                                                         {{--], [--}}
                                                         {{--'alt' => $item->name,--}}
                                                          {{--'class' => 'img-responsive'--}}
                                                        {{--])--}}
                                            {{--@else--}}
                                                {{--@laravelImage('uploads/hotels/', 'coming-soon.png', 555, 555, [--}}
                                                       {{--'fit' => 'crop-top-center'--}}
                                                        {{--], [--}}
                                                        {{--'alt' => $item->name,--}}
                                                         {{--'class' => 'img-responsive'--}}
                                                       {{--])--}}
                                            {{--@endif--}}
                                        </div><!-- ../grid-box-thumb -->
                                    </div>
                                    <div class="lp-grid-box-description "><p><span>
                                               Upselling, cross-selling, customer service, communication skills – if you want to upskill your team or if you’re looking to create better performing, highly motivate teams, talk to sps:training.
                                            </span></p></div><!-- ../grid-box-description-->
                                </div><!-- ../grid-box -->
                            </div>
                </div>
            </div>
        </section>
    </div>
@endsection