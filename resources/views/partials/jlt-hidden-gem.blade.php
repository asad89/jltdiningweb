@extends('master')

@section('content')
        <!--==================================Section Open=================================-->
<section class="aliceblue brown-bg">
    <div class="container breadcrumb-wrap">
        {{--    <ol class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a></li>
                <li><a href="{{url($page->slug)}}">{{$page->name}}</a></li>
            </ol>--}}
    </div>

    <div class="container page-container-second">
        <div class="row">
            <div class="col-md-9 padding-40 blog-single-inner-container lp-border lp-border-radius-8">
                <div class="popup-gallery">
                    <h1 class="page-heading-custom"
                        style="text-align: center;margin-bottom:20px;font-size:24px;font-family: Oxygen, sans-serif!important;">{{"It’s time to nominate your favourite JLT Hidden Gem 2019!"}}</h1>

                    <h3 class="page-heading-custom" style="text-align: center;margin-bottom:20px;font-size:18px;font-family: Oxygen, sans-serif!important;">Tell us your JLT favourite and you could win a dinner for two in a mystery JLT restaurant. Mysterious!</h3>


                    <p>We're gearing up for the 2019 JLT Dining Restaurant Awards and, just like last year, the 2019 Awards feature a "JLT Hidden Gem" category. Right now, the teams in the over 200 independent restaurants in our neighbourhood are busy nominating themselves in the various 2019 Awards categories, but for the #HiddenGemInJLT category, it’s YOU we want to hear from!  We’d love to know what JLT residents, office dwellers, visitors, and all Dubai foodies think is the best hidden gem in our neighbourhood.</p>
                    <p>The rules are simple. You’ve got until 30th September to nominate any JLT-based restaurant and you can nominate as many as you like. We'll draw up a shortlist of the top five nominations, which you can then vote for online once voting for the 2019 JLT Dining Restaurant Awards opens on 1st October 2019.</p>
                    <p>You can nominate any restaurant so long as: </p>
                    <ul>
                        <li>The restaurant is in JLT</li>
                        <li>It's not an international fast-food chain (KFC, Pizza Hut, etc.) or coffee shop chain (Cafe Nero, Starbucks, etc.)</li>
                        <li>It's open for dining-in (no "delivery only" concepts, please)</li>
                    </ul>

                    <p>
                        Scroll down to the nomination form and start nominating your 2019 favourite #HiddenGemInJLT . Want to nominate more than one hidden gem? Simply come back to this page and nominate again. You can nominate as many JLT restaurants as you like. Even better? For every nomination, you'll get entered into a prize draw to win a dinner for 2 in a mystery JLT restaurant. We’ll announce the winner on our social media pages at the beginning of October once online voting for the 2019 awards has started!
                    </p>




                    {!! Form::open(array('url' => '/nominate-your-restaurant','class'=>'form-horizontal','method' => 'POST')) !!}

                    <div class="row">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (session()->has('flash_notification.message'))
                            <div class="alert alert-{{ session('flash_notification.level') }}">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">&times;</button>
                                {!! session('flash_notification.message') !!}
                            </div>
                        @endif
                    </div>

                    <div class="form-body"
                         style="font-family: Oxygen, sans-serif;font-size:14px;line-height:21px;color:#333333;">


                        <div class="row">
                            <p style="text-align: center;margin-bottom:20px;font-size:18px;font-family: Oxygen, sans-serif!important;">Your Details</p>

                            <div class="form-group">
                                <div class="col-sm-6">
                                    {{--  <label>First Name</label>--}}
                                    {!! Form::text('first_name',null,array('class'=>'form-control nameform','placeholder' => 'Your first name','required' => '')) !!}
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    {{--  <label>Last Name</label>--}}
                                    {!! Form::text('last_name',null,array('class'=>'form-control nameform','placeholder' => 'Your last name','required' => '')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">

                                <div class="col-sm-6">
                                    {{--  <label>First Name</label>--}}
                                    {!! Form::text('email',null,array('class'=>'form-control nameform','placeholder'=>'Your email address','required' => '')) !!}
                                </div>

                                <div class="col-sm-6">
                                    {{--  <label>First Name</label>--}}
                                    {!! Form::text('restaurant',null,array('class'=>'form-control nameform','placeholder' => 'The name of your JLT hidden gem','required' => '')) !!}
                                </div>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-center">
                               <p style="font-style: italic;font-size:12px;">Disclaimer: By nominating restaurants for the 2019 #HiddenGemInJLT , you agree to your name and email address being captured and added to our weekly JLT Dining email newsletter. Your data is stored securely and you can view the data we hold for you by clicking on the appropriate link in any of our email newsletters. You can unsubscribe at any time and we never share your data with 3rd parties.</p>


                                <input type="submit" id="submit" name="submit" value="Submit"
                                       class="lp-review-btn btn-second-hover">
                            </div>

                            </div>
                    </div>
                    {!! Form::close() !!}
                </div>

                @include('partials.sponsers')
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12 col-lg-3">
                <div class="sidebar-post">
                    <div class="widget-box  widget-bg-color post-author-box lp-border-radius-5">
                        <div class="widget-content">
                            <a href="https://business.facebook.com/events/1733357806972872" target="_blank">
                                <img src="{{asset('images/voting-banner.png')}}" alt="Award 2017">
                            </a>
                        </div>
                    </div>
                    <div class="widget-box  widget-bg-color post-author-box lp-border-radius-5">
                        <div class="widget-content text-center">
                            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                            <!-- Small text banner -->
                            <ins class="adsbygoogle"
                                 style="display:block"
                                 data-ad-client="ca-pub-8842445166702832"
                                 data-ad-slot="7219573107"
                                 data-ad-format="auto"
                                 data-full-width-responsive="true"></ins>
                            <script>
                                (adsbygoogle = window.adsbygoogle || []).push({});
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--==================================Section Close=================================-->
<style>
    .modal-backdrop.in {
        opacity: 0;
    }
</style>
<div class="modal" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">JLT Dining Restaurant Awards Terms & Conditions</h4>
            </div>

            <div class="modal-body">
                <ol>
                    <li>By making a nomination to the 2019 JLT Dining Restaurant Awards (“the awards”), entrants
                        hereby agree to abide by the following terms and conditions.
                    </li>
                    <li>Entry requirements
                        <ul>
                            <li>Only outlets located in Jumeirah Lakes Towers are eligible to enter the awards</li>
                            <li>You can enter an outlet into multiple categories. The organisers will make the final
                                decision which categories an outlet is included in.
                            </li>
                            <li>Nominations must be made using the online nomination form; other formats will not be
                                considered.
                            </li>
                        </ul>
                    </li>
                    <li>We will accept no responsibility for submissions that are submitted incorrectly, lost,
                        mislaid or delayed, regardless of cause. Any submissions made after the submission deadline
                        will not be accepted.
                    </li>
                    <li>By nominating your outlet for the awards, you agree to be added to the JLT Dining mailing
                        list to receive occasional updates.
                    </li>
                    <li>The organisers may, at any time and without notice, amend or alter categories and/or
                        deadlines, or cancel the awards.
                    </li>
                    <li>The organisers reserve the right, at their sole discretion, to disqualify any entrant who
                        breaches any of these terms and conditions.
                    </li>
                    <li>In order to feature on the online voting form, you need to submit the nomination form before
                        30th September 2019.
                    </li>
                </ol>
            </div>
        </div>
    </div>
</div>
@endsection

@push('site_footer')


<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
{{--<script src="/dist/scripts.min.js"></script>--}}
@endpush