<div class="row">
    <div class="col-md-12 padding-40 blog-single-inner-container lp-border lp-border-radius-8">
        <div class="popup-gallery">
            <div class="form-body" style="font-family: Oxygen, sans-serif;font-size:14px;line-height:21px;color:#333333;">
                <div class="row">
                    <div class="col-sm-12">
                        <p style="text-align: center;margin-bottom:20px;font-size:18px;font-family: Oxygen, sans-serif!important;">Please choose the categories you want to vote in. Select one, more, or all:</p>
                        <div class="form-group">
                        @foreach($categories as $category)
                        <div class="col-sm-4">
                        <label class="checkbox-inline no_indent">
                            {!! Form::checkbox('category_name[]', $category->id,null,['class' => 'checkbox']) !!}{{$category->name}}
                        </label>
                        </div>
                        @endforeach
                        </div>

                        </div>
</div>
                </div>
            </div>

        @foreach($categories as $category)

           <div class="row" id="category_<?php echo $category->id?>">

           </div>
        @endforeach


        <div style="clear:both;margin-top:10px;margin-bottom: 10px;"></div>

        <div class="row">
            <div class="col-md-12">  <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- Home Page top banner -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-8842445166702832"
                     data-ad-slot="9417563246"
                     data-ad-format="auto"
                     data-full-width-responsive="true"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script></div>
        </div>
        <div style="clear:both;margin-top:10px;margin-bottom: 10px;"></div>
        <div class="row">
                <p style="text-align: center;margin-bottom:20px;font-size:18px;font-family: Oxygen, sans-serif!important;">Your Personal Details</p>

                <div class="form-group">
                        <div class="col-sm-6">
                              <label>First Name</label>
                            {!! Form::text('first_name',null,array('class'=>'form-control nameform','placeholder' => 'First Name','required' => '')) !!}
                        </div>
                        <div class="col-sm-6 col-md-6">
                              <label>Last Name</label>
                            {!! Form::text('last_name',null,array('class'=>'form-control nameform','placeholder'=>'Last Name','required' => '')) !!}
                        </div>
                    </div>
                </div>
            <div class="row">
                    <div class="form-group">

                        <div class="col-sm-6">
                              <label>Email</label>
                            {!! Form::text('email',null,array('class'=>'form-control nameform','placeholder' => 'Email Address','required' => '')) !!}
                        </div>

                    </div>
                </div>
            <div class="row">
                    <div class="form-group">
                        <div class="col-md-4">{!! Form::checkbox('newsletter',true,true,['class']) !!} Subscribe to our newsletter!</div>
                    </div>
                </div>
            <div class="row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="row">

                                <div class="col-md-12 text-center">
                                    <input type="submit" id="submit" name="submit" value="Submit"
                                           class="lp-review-btn btn-second-hover">
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
        </div>
    </div>


@push('site_footer')
<script language="javascript">

    $(document).ready(function() {
        $('.checkbox:checkbox').bind('change', function(e) {
            if ($(this).is(':checked')) {
                cat_id = $(this).val();
                if($("div#category_"+cat_id).css('display') == 'none'){
                    $("div#category_"+cat_id).css('display', 'block');
                }
                $.ajax({
                    data: {'category_id': cat_id,},
                    type: "POST",
                    url: "{{ url('/vote/restaurant/category') }}",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function (res) {
                       // alert('test');
                        $("div#category_"+cat_id).html(res);
                    }
                });

            }
            else {
                if($("div#category_"+cat_id).css('display') == 'block'){
                    $("div#category_"+cat_id).css('display', 'none');
                }
                cat_id = $(this).val();
               $("div#category_"+cat_id).d
            }
        })
    });

</script>
@endpush