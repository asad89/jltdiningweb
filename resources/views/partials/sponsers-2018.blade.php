<hr>


<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
        <h3 class="page-heading-custom"
            style="margin-top:15px;text-align: center;font-weight:normal;margin-bottom:0px;font-size:18px;font-family: Oxygen, sans-serif!important;">
            Thank you to our 2018 JLT Restaurant Awards Partners & Sponsors
        </h3>
        {{--  <p style="text-align: center;margin-top:10px;font-size:20px;font-family: Oxygen, sans-serif!important;font-weight: bold;">Congratulations to all winners and runner-ups!</p>--}}
        {{-- <p style="text-align:center;margin-top:10px;">Want to sponsor the awards? <a  href="mailto:munch@jltdining.com?Subject=JLT%20Restaurant%20Awards%20-%20Sponsorship%20Inquiry" target="_top">Contact us today!</a></p>--}}
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12" style="text-align:center;">
        <h5>{{"Official PR Partner"}}</h5>
        <img src="{{asset('images/sponsor-sps-pr.png')}}" alt="SPS PR" width="200"
             height="200">
    </div>
    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12"
         style="text-align:center;">
        <h5>{{"Official Social Media Partner"}}</h5>
        <img src="{{asset('images/sponsor-sps-digital.png')}}" alt="SPS PR" width="200"
             height="200"></div>
</div>

<div class="clearfix"></div>
<div class="row" style="margin-top: 10px;">
    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12"
         style="text-align:center;">
        <h5>{{"Official Water Supplier"}}</h5>
        {{-- <a href="#" target="_blank">--}}
        <img src="{{asset('images/sponsor-water.jpg')}}" alt="Food Sheikh" width="200" height="200">
        {{--  </a>--}}
        {{-- <img src="{{asset('images/sps-digital.jpg')}}" alt="SPS PR" width="100" height="100">--}}
    </div>
    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12"
         style="text-align:center;">
        <h5>{{"Official Flower Supplier"}}</h5>
        {{--  <a href="https://www.twobirds.com/" target="_blank">--}}
        <img src="{{asset('images/sponsor-blossom-tree.png')}}" alt="bird-and-bird" width="200" height="200">
        {{-- </a>--}}
    </div>

    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12" style="text-align:center;">
        <h5>{{"Official Media Partner"}}</h5>
        <a href="http://foodsheikh.com/" target="_blank">
            <img src="{{asset('images/food-sheikh-partner.jpg')}}" alt="Food Sheikh" width="200" height="200">
        </a>
    </div>
    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12" style="text-align:center;">
        <h5>{{"Official Videograpgy & Photography"}}</h5>
        <a href="https://thefilmfactory.co/" target="_blank">
            <img src="{{asset('images/film-factory.jpg')}}" alt="Film Factory" width="200" height="200">
        </a>
    </div>
    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12" style="text-align:center;">
        <h5>{{"Legal Partner"}}</h5>
        <a href="https://www.charlesrussellspeechlys.com/en/locations/offices/dubai/" target="_blank">
            <img src="{{asset('images/crs.jpg')}}" alt="CRS" width="200" height="200">
        </a>
    </div>


</div>

<hr>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
        <h3 class="page-heading-custom"
            style="margin-top:0px;text-align: center;font-weight:normal;margin-bottom:20px;font-size:18px;font-family: Oxygen, sans-serif!important;">
            Platinum Sponsors</h3>
        <div class="table-responsive text-center">
            {{--
                        <p class="text-center">Interested in sponsoring the 2018 JLT Dining Restaurant Awards? <a href="https://www.jltdining.com/contact" target="_blank">Contact us</a> today or download our sponsorship flyer <a href="https://www.jltdining.com/flyer.pdf" target="_blank">here!</a></p>--}}


            <table border="0" cellspacing="20" class="table">
                <tr>
                    <td>
                        <a href="http://www.coldstone.ae/" target="_blank"><img src="{{asset('/images/cold-stone.png')}}" alt="Cold Stone"></a></td>
                    {{--          <td><a  href="https://www.facebook.com/ankurtrdg/" target="_blank"><img src="{{asset('/images/qromq.png')}}" alt="qromq"></a></td>
                              <td><a href="http://dna1films.com/" target="_blank"><img src="{{asset('/images/dna1-films.png')}}" alt="DNA1 Films"></a></td>--}}
                </tr>
            </table>
        </div>
        {{-- <p style="text-align:center;margin-top:10px;">Want to sponsor the awards? <a  href="mailto:munch@jltdining.com?Subject=JLT%20Restaurant%20Awards%20-%20Sponsorship%20Inquiry" target="_top">Contact us today!</a></p>--}}
    </div>
</div>
{{--<hr>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
        <h3 class="page-heading-custom"
            style="margin-top:0px;text-align: center;font-weight:normal;margin-bottom:20px;font-size:18px;font-family: Oxygen, sans-serif!important;">
            Gold Sponsors</h3>
        <div class="table-responsive">

       --}}{{--     <p class="text-center">Interested in sponsoring the 2018 JLT Dining Restaurant Awards? <a href="https://www.jltdining.com/contact" target="_blank">Contact us</a> today or download our sponsorship flyer <a href="https://www.jltdining.com/flyer.pdf" target="_blank">here!</a></p>--}}{{--



            --}}{{-- <table border="0" cellspacing="20" class="table">
            <tr>
                <td><a href="http://pitfiredubai.ae/" target="_blank"><img src="{{asset('/images/pitfire-gold.png')}}" alt="pitfire"></a></td>
                <td><a href="http://www.blossomtreedubai.com/" target="_blank"><img src="{{asset('/images/blossom.PNG')}}" alt="blossom"></a></td>

            </tr>
        </table>--}}{{--
        </div>
           --}}{{-- <p style="text-align:center;margin-top:25px;">Want to sponsor the awards? <a  href="mailto:munch@jltdining.com?Subject=JLT%20Restaurant%20Awards%20-%20Sponsorship%20Inquiry" target="_top">Contact us today!</a></p>--}}{{--

    </div>

</div>--}}
<hr>

<div class="row" style="margin-top: 10px;">
    <h3 class="page-heading-custom"
        style="margin-top:0px;text-align: center;font-weight:normal;margin-bottom:20px;font-size:18px;font-family: Oxygen, sans-serif!important;">
        Silver Sponsors
    </h3>
    <hr>
    {{--
        <div class="table-responsive">
           --}}
    {{-- <p class="text-center">Interested in sponsoring the 2018 JLT Dining Restaurant Awards? <a href="https://www.jltdining.com/contact" target="_blank">Contact us</a> today or download our sponsorship flyer <a href="https://www.jltdining.com/flyer.pdf" target="_blank">here!</a></p>--}}{{--

        </div>
    --}}
    <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12" style="text-align:center;">

        <a href="http://www.localyser.com/" target="_blank"><img src="{{asset('/images/localyser-ss.jpg')}}" alt="localyser"></a>
    </div>
    <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12" style="text-align:center;">
        <a href="http://bodytime.ae/" target="_blank"><img src="{{asset('/images/body-time-ss.jpg')}}" alt="bodytime"></a>
    </div>
    <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12" style="text-align:center;">
        <a href="http://halakiwi.com/" target="_blank"><img src="{{asset('/images/hala-kiwi-ss.jpg')}}" alt="halakiwi"></a>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-4 text-center">
        <a href="http://www.foodiva.net/" target="_blank"><img src="{{asset('/images/foodiva-ss.png')}}" alt="foodiva"></a>
    </div>

    <div class="col-md-4 text-center">
        <a href="https://www.instagram.com/stylextend/" target="_blank"><img src="{{asset('/images/stylexten-silver-sponsor-compressor.jpg')}}" alt="stylextend"></a>
    </div>

    <div class="col-md-4 text-center">
        <a href="http://liquidoflife.net/" target="_blank"><img src="{{asset('/images/liquid-of-life.png')}}" alt="Liquid of life"></a>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-4 text-center">
        <a href="https://www.makemymeal.ae/" target="_blank"><img src="{{asset('/images/make-my-meal.png')}}" alt="Make my meal"></a>
    </div>
</div>


<hr>

<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
        <h3 class="page-heading-custom"
            style="margin-top:0px;text-align: center;font-weight:normal;margin-bottom:20px;font-size:18px;font-family: Oxygen, sans-serif!important;">
            Category Sponsors</h3>
        <div class="table-responsive">
            <table border="0" cellspacing="20" class="table">
                <tr>
                    <td><a href="https://fruitfulday.com/" target="_blank"><img src="{{asset('/images/fruitful-day.jpg')}}" alt="pitfire"></a></td>
                    <td><a href="https://www.charlesrussellspeechlys.com" target="_blank"><img src="{{asset('/images/crs.png')}}" alt="crs"></a></td>
                    <td><a href="http://liquidoflife.net/" target="_blank"><img src="{{asset('/images/liquid-of-life-category.jpg')}}" alt="blossom"></a></td>

                </tr>

                <tr>
                    <td><a href="http://www.vogue-interior.com/" target="_blank"><img src="{{asset('/images/Vogue.jpg')}}" alt="Vogue"></a></td>


                </tr>

            </table>
        </div>
    </div>
</div>