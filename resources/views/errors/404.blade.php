@extends('master')
@push('site-head')
{{--<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>--}}
<script src="{{asset('js/jquery-1.11.3.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jssor.slider-21.1.6.mini.js')}}" type="text/javascript"></script>
<!-- Include the plugin's CSS and JS: -->
<script type="text/javascript" src="{{asset('js/bootstrap-multiselect.js')}}"></script>
<link rel="stylesheet" href="{{asset('css/bootstrap-multiselect.css')}}" type="text/css"/>
<script type="text/javascript">
    $(document).ready(function() {
        $("#advanced-search").on("click", function () {
            $("#advanced").toggle();                 // .fadeToggle() // .slideToggle()
        });
    });
</script>
<style type="text/css">
    #advanced {
        display: none;
    }

    .form-inline .input-group {
        width:auto !important;
    }
</style>
@endpush
@section('content')
        <!--==================================Section Open=================================-->



<!--==================================Section Open=================================-->
<div id="page">
    <section>
        <div class="lp-section-row white text-center lp-section-content-container">
            <div class="container white mr-top-60 mr-bottom-60">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <h2 class="page-404-title pg-404-tit mr-top-0 ">4<img alt="" src="{{asset('images/404.png')}}" />4</h2>
                        <p class="pg-404-p text-center padding-top-20"> Ooops, no food found ! </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center pad-top-15 padding-bottom-30">
                        <div class="row">
                            <div class="col-md-4 col-md-offset-4">
                                <p>
                                    The page you are looking for might have been removed, had its name changed, or is temporarily unavailable. Why don’t you return to our homepage and see if you can find what you are looking for?
                                </p>
                                <a href="{{url('/')}}" class="btn btn-primary">Go Home</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!--==================================Section Close=================================-->
@endsection

@push('site_footer')


@endpush