@extends('master')
@section('meta_tags')
    <title>@if(isset($page->meta_title) && !empty($page->meta_title)){{$page->meta_title}} @else{{config('app.meta_title')}}@endif</title>
    <meta name="keywords"
          content="@if(isset($page->meta_keywords) && !empty($page->meta_keywords)){{$page->meta_keywords}} @else{{config('app.meta_keywords')}}@endif">
    <meta name="description"
          content="@if(isset($page->meta_description) && !empty($page->meta_description)){{$page->meta_description}} @else{{config('app.meta_description')}}@endif">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css">

    @endsection

    @section('content')

            <!--==================================Section Open=================================-->
    <section class="aliceblue brown-bg">
        <div class="container breadcrumb-wrap">
            {{--    <ol class="breadcrumb">
                    <li><a href="{{url('/')}}">Home</a></li>
                    <li><a href="{{url($page->slug)}}">{{$page->name}}</a></li>
                </ol>--}}
        </div>

        <div class="container page-container-second">
            <div class="row">
                <div class="col-md-9 padding-40 blog-single-inner-container lp-border lp-border-radius-8">
                    <div class="popup-gallery">
                        <h1 class="page-heading-custom"
                            style="text-align: center;margin-bottom:20px;font-size:24px;font-family: Oxygen, sans-serif!important;">
                           {{-- {{"JLT Restaurant Awards 2019 – Online Nominations"}}--}}
                            {{"Nominations for the JLT Dining Restaurant Awards are now closed!"}}
                        </h1>

<p style="text-align: center;"> Missed the deadline? You're still welcome to attend the 2019 awards ceremony on Monday, 25th November in JLT and meet & mingle with fellow JLT restaurateurs and F&B pros. Keep an eye on our social pages for details!</p>
                        <p style="text-align: center;">Are you receiving "The JLT Restaurateur" - our regular trade-only newsletter exclusively for JLT restaurateurs? If not,
                            <a href="https://www.jltdining.com/contact" target="_blank">contact us today!</a></p>





                      {{--  <p>Thank you for registering your restaurant for the 2019 JLT Dining Restaurant Awards! Please
                            complete the online nomination form below before 30th September 2019. Once you have
                            submitted the form, your restaurant will be added to the online voting form. Public online
                            voting will start on 1st October 2019. Questions? Just click <a href="{{url('/contact')}}"><strong>here</strong></a>
                            and drop us a line!</p>

                        <h3 class="text-center page-heading-custom" style="margin-bottom:20px;font-size:18px;font-family: Oxygen, sans-serif!important;">For a list of all 2019 categories and an explanation of which categories you can nominate your restaurant in, please click <a href="https://www.jltdining.com/blog/jlt-restaurant-awards-2019-all-21-categories-revealed" target="_blank">here!</a></h3>

                        {!! Form::open(array('url' => '/register-for-award','class'=>'form-horizontal','method' => 'POST')) !!}

                        <div class="row">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if (session()->has('flash_notification.message'))
                                <div class="alert alert-{{ session('flash_notification.level') }}">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-hidden="true">&times;</button>
                                    {!! session('flash_notification.message') !!}
                                </div>
                            @endif
                        </div>

                        <div class="form-body"
                             style="font-family: Oxygen, sans-serif;font-size:14px;line-height:21px;color:#333333;">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        --}}{{--  <label>Outlet Name</label>--}}{{--
                                        {!! Form::text('name',null,array('class'=>'form-control nameform','placeholder'=>'Outlet Name:','required' => '')) !!}
                                        <p style="color:#333;font-size:12px;text-align: center;">How you would like your
                                            outlet to appear on the online voting form.</p>
                                    </div>
                                    <div class="col-sm-6">

                                        <select name="category[]" id="category" class="form-control" multiple>
                                            <option value="0" disabled>Categories</option>
                                            @if(count($categories))
                                                @foreach($categories as $cat)
                                                    <option value="{{$cat->id}}">{{$cat->name}}</option>
                                                @endforeach
                                            @endif




                                           --}}{{-- <option value="Best Service">Best Service
                                            </option>
                                            <option value="Best Bar">Best Bar
                                            </option>
                                            <option value="Best European (Licensed)">Best European (Licensed)
                                            </option>
                                            <option value="Best Café">Best Café</option>
                                            <option value="Best Chinese">Best Chinese</option>
                                            <option value="Best European (Unlicensed)">Best European (Unlicensed)
                                            </option>
                                            <option value="Best Healthy">Best Healthy</option>
                                            <option value="Best Middle Eastern">Best Middle Eastern</option>
                                            <option value="Best Indian">Best Indian</option>
                                            <option value="Best Pizza">Best Pizza</option>
                                            <option value="Best South Asian">Best South Asian</option>
                                            <option value="Best South-East Asian">Best South-East Asian</option>
                                            <option value="Best Street Food">Best Street Food</option>
                                            <option value="Best Sweet Tooth">Best Sweet Tooth</option>
--}}{{--

                                        </select>

                                        <p style="color:#333;font-size:12px;text-align: center;">You can choose multiple categories. For Windows, hold down the control (CTRL) button and for Mac, hold down the command button, while selecting your options.</p>
                                        --}}{{--{!! Form::text('last_name',null,array('class'=>'form-control nameform','placeholder'=>'Last Name:','required' => '')) !!}--}}{{--
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        --}}{{-- <label>Outlet Website</label>--}}{{--
                                        {!! Form::text('website',null,array('class'=>'form-control nameform','placeholder' => 'Outlet Website')) !!}
                                        <p style="color:#333;font-size:12px;text-align: center;">If your outlet has a
                                            website, please enter the URL here.</p>
                                    </div>
                                    <div class="col-sm-6 col-md-6">
                                        --}}{{-- <label>Outlet Facebook Page</label>--}}{{--
                                        {!! Form::text('facebook',null,array('class'=>'form-control nameform','placeholder'=>'Outlet Facebook Page',)) !!}
                                        <p style="color:#333;font-size:12px;text-align: center;">If your outlet has a
                                            Facebook page, please enter the URL here.</p>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        --}}{{--  <label>Outlet Twitter Account</label>--}}{{--
                                        {!! Form::text('twitter',null,array('class'=>'form-control nameform','placeholder' => 'Outlet Twitter Account')) !!}
                                        <p style="color:#333;font-size:12px;text-align: center;">If your outlet has a
                                            Twitter account, please enter the username here.</p>
                                    </div>
                                    <div class="col-sm-6 col-md-6">
                                        --}}{{--  <label>Outlet Instagram Accounte</label>--}}{{--
                                        {!! Form::text('instagram',null,array('class'=>'form-control nameform','placeholder'=>'Outlet Instagram Account',)) !!}
                                        <p style="color:#333;font-size:12px;text-align: center;">If your outlet has an
                                            Instagram account, please enter the username here.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <h5 style="text-align: center;">Authorized Representative Details</h5>

                                    <div class="col-sm-6">
                                        --}}{{--  <label>First Name</label>--}}{{--
                                        {!! Form::text('first_name',null,array('class'=>'form-control nameform','placeholder' => 'First Name','required' => '')) !!}
                                    </div>
                                    <div class="col-sm-6 col-md-6">
                                        --}}{{--  <label>Last Name</label>--}}{{--
                                        {!! Form::text('last_name',null,array('class'=>'form-control nameform','placeholder'=>'Last Name','required' => '')) !!}
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="form-group">

                                    <div class="col-sm-6">
                                        --}}{{--  <label>First Name</label>--}}{{--
                                        {!! Form::text('email',null,array('class'=>'form-control nameform','placeholder' => 'Email Address','required' => '')) !!}
                                    </div>
                                    <div class="col-md-2">{!! Form::text('country-code',null,array('class'=>'form-control nameform','placeholder'=>'+971','disabled')) !!}</div>
                                    <div class="col-sm-4 col-md-4">
                                        --}}{{--  <label>Last Name</label>--}}{{--
                                        {!! Form::text('phone',null,array('class'=>'form-control nameform','placeholder'=>'Mobile Number','required' => '',)) !!}
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">

                                        <h3 class="page-heading-custom"
                                            style="text-align: center;margin-bottom:20px;font-size:18px;font-family: Oxygen, sans-serif!important;">
                                            Terms & Conditions</h3>

                                        <p>By submitting this online nomination form, you agree to our terms &
                                            conditions, which are available <a href="#" data-toggle="modal"
                                                                               data-target="#basicModal">here</a>. </p>


                                        <div class="row">

                                            <div class="col-md-12 text-center">
                                                <input type="submit" id="submit" name="submit" value="Submit"
                                                       class="lp-review-btn btn-second-hover"
                                                >
                                            </div>
                                        </div>
--}}
                                        <hr>

                                        <div class="row">
                                            <div class="col-lg-12 text-center">
                                                <a href="{{url('/restaurant-awards-2019')}}" target="_blank">
                                                    <img src="{{asset('images/award-2019.png')}}" class="img-responsive" style="width:100%;">
                                                </a>
                                            </div>
                                        </div>



                                        @include('partials.sponsers')


                                    </div>
                                </div>
                            </div>


                        </div>
                      {{--  {!! Form::close() !!}
                    </div>--}}
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12 col-lg-3">
                    <div class="sidebar-post">
                        <div class="widget-box  widget-bg-color post-author-box lp-border-radius-5">
                            <div class="widget-content">
                                <a href="https://www.jltdining.com/restaurant-awards-2019" target="_blank">
                                    <img src="{{asset('images/jlt-awards-2018.png')}}" alt="Award 2019">
                                </a>
                            </div>
                        </div>
                        <div class="widget-box  widget-bg-color post-author-box lp-border-radius-5">
                            <div class="widget-content text-center">
                                <a href="https://www.jltdining.com/restaurant-awards-2017" target="_blank">
                                    <img src="{{asset('images/voting-banner.png')}}" alt="Award 2017" class="img-responsive text-center">
                                </a>
                                {{-- <img src="{{asset('images/bannerhome1.png')}}" alt="" class="img-responsive text-center">--}}
                            </div>
                        </div>
                        <div class="widget-box  widget-bg-color post-author-box lp-border-radius-5">
                            <div class="widget-content text-center">
                                <a href="http://www.dna1films.com/" target="_blank">
                                    <img src="{{asset('images/jltdining-award.png')}}" alt="" class="img-responsive text-center">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--==================================Section Close=================================-->
    <style>
        .modal-backdrop.in {
            opacity: 0;
        }
    </style>
    <div class="modal" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">JLT Dining Restaurant Awards Terms & Conditions</h4>
                </div>

                <div class="modal-body">
                    <ol>
                        <li>By making a nomination to the 2019 JLT Dining Restaurant Awards (“the awards”), entrants
                            hereby agree to abide by the following terms and conditions.
                        </li>
                        <li>Entry requirements
                            <ul>
                                <li>Only outlets located in Jumeirah Lakes Towers are eligible to enter the awards</li>
                                <li>You can enter an outlet into multiple categories. The organisers will make the final
                                    decision which categories an outlet is included in.
                                </li>
                                <li>Nominations must be made using the online nomination form; other formats will not be
                                    considered.
                                </li>
                            </ul>
                        </li>
                        <li>We will accept no responsibility for submissions that are submitted incorrectly, lost,
                            mislaid or delayed, regardless of cause. Any submissions made after the submission deadline
                            will not be accepted.
                        </li>
                        <li>By nominating your outlet for the awards, you agree to be added to the JLT Dining mailing
                            list to receive occasional updates.
                        </li>
                        <li>The organisers may, at any time and without notice, amend or alter categories and/or
                            deadlines, or cancel the awards.
                        </li>
                        <li>The organisers reserve the right, at their sole discretion, to disqualify any entrant who
                            breaches any of these terms and conditions.
                        </li>
                        <li>In order to feature on the online voting form, you need to submit the nomination form before
                            30th September 2019.
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('site_footer')
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script src="/dist/scripts.min.js"></script>
@endpush