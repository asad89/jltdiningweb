@extends('master')
@section('meta_tags')
    <title>@if(isset($page->meta_title) && !empty($page->meta_title)){{$page->meta_title}} @else{{config('app.meta_title')}}@endif</title>
    <meta name="keywords"
          content="@if(isset($page->meta_keywords) && !empty($page->meta_keywords)){{$page->meta_keywords}} @else{{config('app.meta_keywords')}}@endif">
    <meta name="description"
          content="@if(isset($page->meta_description) && !empty($page->meta_description)){{$page->meta_description}} @else{{config('app.meta_description')}}@endif">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css">

    @endsection

    @section('content')

            <!--==================================Section Open=================================-->
    <section class="aliceblue brown-bg">
        <div class="container breadcrumb-wrap">
            {{--    <ol class="breadcrumb">
                    <li><a href="{{url('/')}}">Home</a></li>
                    <li><a href="{{url($page->slug)}}">{{$page->name}}</a></li>
                </ol>--}}
        </div>

        <div class="container page-container-second">
            <div class="row">
                <div class="col-md-9 padding-40 blog-single-inner-container lp-border lp-border-radius-8">
                  <div class="popup-gallery">
                     {{-- @include('partials.jlt-hidden-gem')--}}

                  </div>
                    @include('partials.sponsers')
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12 col-lg-3">
                    <div class="sidebar-post">
                        <div class="widget-box  widget-bg-color post-author-box lp-border-radius-5">
                            <div class="widget-content">
                                <a href="https://www.jltdining.com/restaurant-awards-2019" target="_blank">
                                    <img src="{{asset('images/jlt-awards-2018.png')}}" alt="Award 2019">
                                </a>
                            </div>
                        </div>
                        <div class="widget-box  widget-bg-color post-author-box lp-border-radius-5">
                            <div class="widget-content text-center">
                                <a href="https://www.jltdining.com/restaurant-awards-2017" target="_blank">
                                    <img src="{{asset('images/voting-banner.png')}}" alt="Award 2017" class="img-responsive text-center">
                                </a>
                                {{-- <img src="{{asset('images/bannerhome1.png')}}" alt="" class="img-responsive text-center">--}}
                            </div>
                        </div>
                        <div class="widget-box  widget-bg-color post-author-box lp-border-radius-5">
                            <div class="widget-content text-center">
                                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                <!-- Small text banner -->
                                <ins class="adsbygoogle"
                                     style="display:block"
                                     data-ad-client="ca-pub-8842445166702832"
                                     data-ad-slot="7219573107"
                                     data-ad-format="auto"
                                     data-full-width-responsive="true"></ins>
                                <script>
                                    (adsbygoogle = window.adsbygoogle || []).push({});
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--==================================Section Close=================================-->
    <style>
        .modal-backdrop.in {
            opacity: 0;
        }
    </style>
    <div class="modal" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">JLT Dining Restaurant Awards Terms & Conditions</h4>
                </div>

                <div class="modal-body">
                    <ol>
                        <li>By making a nomination to the 2019 JLT Dining Restaurant Awards (“the awards”), entrants
                            hereby agree to abide by the following terms and conditions.
                        </li>
                        <li>Entry requirements
                            <ul>
                                <li>Only outlets located in Jumeirah Lakes Towers are eligible to enter the awards</li>
                                <li>You can enter an outlet into multiple categories. The organisers will make the final
                                    decision which categories an outlet is included in.
                                </li>
                                <li>Nominations must be made using the online nomination form; other formats will not be
                                    considered.
                                </li>
                            </ul>
                        </li>
                        <li>We will accept no responsibility for submissions that are submitted incorrectly, lost,
                            mislaid or delayed, regardless of cause. Any submissions made after the submission deadline
                            will not be accepted.
                        </li>
                        <li>By nominating your outlet for the awards, you agree to be added to the JLT Dining mailing
                            list to receive occasional updates.
                        </li>
                        <li>The organisers may, at any time and without notice, amend or alter categories and/or
                            deadlines, or cancel the awards.
                        </li>
                        <li>The organisers reserve the right, at their sole discretion, to disqualify any entrant who
                            breaches any of these terms and conditions.
                        </li>
                        <li>In order to feature on the online voting form, you need to submit the nomination form before
                            30th September 2019.
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('site_footer')
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script src="/dist/scripts.min.js"></script>
@endpush