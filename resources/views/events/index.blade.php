@extends('master')
@section('meta_tags')
    <title>Restaurants, Lounges & Cafes in Jumeirah Lakes Towers | JLT Dining</title>
    <meta name="keywords" content="jumeirah lakes towers restaurants, restaurants in dubai, dubai restaurants, jlt restaurants, best restaurants in jlt, cafes in jlt, shisha in jlt">
    <meta name="description" content="Jumeirah Lakes Towers in Dubai boasts over 200 independent Restaurants, Lounges, and Cafes. Find the best Restaurants in JLT Dubai. ">
    <link rel="canonical" href="{{url('/')}}" />
@endsection
@push('site-head')
<script src="{{asset('js/jquery-1.11.3.min.js')}}" type="text/javascript"></script>
@endpush
@section('content')
    <div id="page">
        <section class="brown-bg">
            <div class="row listing-page-result-row margin-bottom-25">
                <div class="container breadcrumb-wrap">
                    <div class="col-md-12 col-sm-12 text-left breadcrumb">
                        <p>@if(isset($event_title)) {{$event_title}} @endif</p>
                    </div>
                </div>
            </div>
            <div class="container page-container">
                <?php
                //Columns must be a factor of 12 (1,2,3,4,6,12)
                $numOfCols = 2;
                $rowCount = 0;
                $bootstrapColWidth = 12 / $numOfCols;
                ?>
                <div class="row">
                    @if(isset($events) && count($events))
                        @foreach($events  as $item)
                            <div class="col-md-6 col-sm-6 lp-grid-box-contianer lp-grid-box-contianer1">
                                <div class="lp-grid-box lp-border lp-border-radius-8">
                                    <div class="lp-grid-box-thumb-container">
                                        <h4 class="lp-h4" style="text-align: center;">
                                            @if($item->id==16)
                                                <a href="https://www.jltdining.com/january-2020-10-days-10-dirham" target="_blank">
                                            @elseif($item->id==2)
                                                <a href="https://www.jltdining.com/january-2018-10-days-10-dirham" target="_blank">
                                                    @elseif($item->id==18)
                                                <a href="https://www.jltdining.com/pop-up-2" target="_blank">
                                                    @elseif($item->id==17)
                                                        <a href="https://www.jltdining.com/pop-up-1" target="_blank">
                                                            @elseif($item->id==99)
                                                        <a href="https://www.jltdining.com/ten4jlt-2020-restaurateurs" target="_blank">

                                                            @elseif($item->id==16)
                                                                <a href="http://jltdining.com/january-2020-10-days-10-dirham" target="_blank">


                                                                @elseif($item->id==6)
                                                        <a href="https://www.jltdining.com/27th-march-2018-the-jlt-battle-of-the-chefs-pizza-edition" target="_blank">
                                                    @else<a href="#">@endif{{$item->title}}
                                                    </a>

                                        </h4>
                                        <div class="lp-grid-box-thumb">
                                            @if(!empty($item->feature_image))
                                                @laravelImage('uploads/events/large/', $item->feature_image, 555, 277, [
                                                        'fit' => 'crop-top-center'
                                                         ], [
                                                         'alt' => $item->name,
                                                          'class' => 'img-responsive'
                                                        ])
                                            @else
                                                @laravelImage('uploads/hotels/', 'coming-soon.png', 555, 555, [
                                                       'fit' => 'crop-top-center'
                                                        ], [
                                                        'alt' => $item->name,
                                                         'class' => 'img-responsive'
                                                       ])
                                            @endif
                                        </div><!-- ../grid-box-thumb -->
                                    </div>
                                    <div class="lp-grid-box-description "><p><span>{!! $item->description !!}</span></p></div><!-- ../grid-box-description-->
                                </div><!-- ../grid-box -->
                            </div>
                            <?php
                            $rowCount++;
                            if ($rowCount % $numOfCols == 0) echo '</div><div class="row">';
                            ?>
                        @endforeach
                    @endif
                </div>
                <?php
                $rowCount++;
                if ($rowCount % $numOfCols == 0) echo '</div><div class="row">';
                ?>
            </div>
        </section>
    </div>
@endsection