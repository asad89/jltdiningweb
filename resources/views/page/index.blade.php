@extends('public.master')

@section('meta_tags')
    <title>@if(isset($page->meta_title) && !empty($page->meta_title)){{$page->meta_title}} @else{{config('app.meta_title')}}@endif</title>
    <meta name="keywords" content="@if(isset($page->meta_keywords) && !empty($page->meta_keywords)){{$page->meta_keywords}} @else{{config('app.meta_keywords')}}@endif">
    <meta name="description" content="@if(isset($page->meta_description) && !empty($page->meta_description)){{$page->meta_description}} @else{{config('app.meta_description')}}@endif">
    <link rel="canonical" href="{{url($page->slug)}}" />
    @endsection

@section('content')
        <!--==================================Section Open=================================-->
<section class="aliceblue">
    <div class="container page-container-second">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 padding-40 blog-single-inner-container lp-border lp-border-radius-8">

                <div class="blog-content popup-gallery">
                    <?php echo $page->post_content; ?>
                </div>

            </div>
        </div>
    </div>
</section>
<!--==================================Section Close=================================-->
@endsection