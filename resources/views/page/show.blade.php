@extends('master')
@section('meta_tags')
    <title>@if(isset($page->meta_title) && !empty($page->meta_title)){{$page->meta_title}} @else{{config('app.meta_title')}}@endif</title>
    <meta name="keywords" content="@if(isset($page->meta_keywords) && !empty($page->meta_keywords)){{$page->meta_keywords}} @else{{config('app.meta_keywords')}}@endif">
    <meta name="description" content="@if(isset($page->meta_description) && !empty($page->meta_description)){{$page->meta_description}} @else{{config('app.meta_description')}}@endif">
    <link rel="canonical" href="{{url($page->slug)}}" />
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-585ba46f0a9379e0"></script>
    <meta property="og:title" content="{!! $page->title !!}" />
    {{--  <meta property="og:description" content="{{str_limit($page->post_content,60)}}}" />--}}
    <meta property="og:image" content="@if(!empty($page->feature_image)) {{asset('uploads/posts/large/'.$page->feature_image)}} @endif" />
    @endsection
    @section('content')
            <!--==================================Section Open=================================-->
    <section class="aliceblue brown-bg">
        <div class="container breadcrumb-wrap">
            <ol class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a></li>
                <li><a href="{{url($page->slug)}}">{{$page->name}}</a></li>
            </ol>
        </div>

        <div class="container page-container-second">
            <div class="row">
                <div class="col-md-9 padding-40 blog-single-inner-container lp-border lp-border-radius-8">
                    <div class="popup-gallery">
                        {{--@if(!empty($page->feature_image))
                            <img src="{{asset('uploads/posts/large/'.$page->feature_image)}}" alt="">
                        @endif--}}
                        <h1 class="page-heading-custom" style="text-align: center;margin-bottom:20px;font-size:24px;font-family: Oxygen, sans-serif!important;">{{$page->title}}</h1>
                           {{-- @if($page->id==72)
                                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                <!-- Full size banner advertisement -->
                                <ins class="adsbygoogle"
                                     style="display:block"
                                     data-ad-client="ca-pub-8842445166702832"
                                     data-ad-slot="9281436373"
                                     data-ad-format="auto"
                                     data-full-width-responsive="true"></ins>
                                <script>
                                    (adsbygoogle = window.adsbygoogle || []).push({});
                                </script>
                            @endif--}}
                            <?php echo $page->post_content; ?>
                          {{--  @if($page->id==76)
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank">
                                            <input type="hidden" name="cmd" value="_s-xclick">
                                            <input type="hidden" name="hosted_button_id" value="8TEVRSLAGU3MU">
                                            <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                                            <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                                        </form>
                                    </div>
                                </div>
                                <div align="center"><span style="color: rgb(34, 34, 34); font-family: Oxygen, sans-serif; font-size: 12px;">(Pay securely via PayPal. Your payment will be processed in USD by PayPal on behalf of Strategic Partnership Solutions, the operator of&nbsp;</span><a href="http://www.jltdining.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://www.jltdining.com&amp;source=gmail&amp;ust=1554709188946000&amp;usg=AFQjCNHkSsIBz1LvpoDr8uuU-TCGnkEXyA" style="background-color: rgb(255, 255, 255); color: rgb(17, 85, 204); font-family: Oxygen, sans-serif; font-size: 14px;"><span style="font-size: 12px;">www.jltdining.com</span></a><span style="color: rgb(34, 34, 34); font-family: Oxygen, sans-serif; font-size: 12px;">. Your payment receipt will act as your reservation confirmation. </span><span style="color: rgb(34, 34, 34); font-family: Oxygen, sans-serif; font-size: 12px;">You'll be charged the USD equivalent of AED 72 = USD$ 20 per person incl. VAT.)</span><span style="color: rgb(34, 34, 34); font-family: Oxygen, sans-serif; font-size: 12px;"></span></div>
                            <br>
                            @endif--}}


                            <div class="col-md-12">
                                <div class="post-meta-right-box text-center">
                                    <div class="addthis_inline_share_toolbox"></div>
                                </div>
                            </div>
                         @if($page->slug=='jlt-hidden-gem-2019')
                            @include('partials.jlt-hidden-gem')
                            @include('partials.sponsers')
                         @endif
                         @if($page->id==22)
                             @include('admin.partials.sponsers')
                         @endif

                        @if($page->id==39)

                                @include('partials.sponsers-2018')
                            @endif

                        @if($page->id==87)

                            @include('partials.sponsers')
                        @endif
                        @if($page->id==89)
                            @include('partials.sponsers')
                        @endif

                        @if($page->id==104)
                            @include('partials.sponsers-2020')
                        @endif

                        @if($page->id==71)

                            @include('partials.sponsers')
                        @endif


                        @if($page->id==82)
                            @include('forms.event-register')
                            @endif

                            @if(!$page->id==22)
                        <span style="display: block;margin-top:10px;" class="text-center"><img src="{{asset('images/banner3.png')}}" alt="" title="" class="img-responsive"></span>
                    @endif
                    </div>
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12 col-lg-3">
                    <div class="sidebar-post">
                        <div class="widget-box  widget-bg-color post-author-box lp-border-radius-5">
                            <div class="widget-content">
                                <a href="https://photos.app.goo.gl/dDnCuhmPs8fySxHE6" target="_blank">
                                    <img src="{{asset('images/jlt-awards-side-banner-compressor.png')}}" alt="Award 2018">
                                </a>
                            </div>
                        </div>
                        <div class="widget-box  widget-bg-color post-author-box lp-border-radius-5">
                            <div class="widget-content">
                                <a href="https://www.jltdining.com/restaurant-awards-2018" target="_blank">
                                    <img src="{{asset('images/jlt-awards-2018.png')}}" alt="Award 2018">
                                </a>
                            </div>
                        </div>
                        <div class="widget-box  widget-bg-color post-author-box lp-border-radius-5">
                            <div class="widget-content text-center">
                                <a href="https://www.jltdining.com/restaurant-awards-2017" target="_blank">
                                    <img src="{{asset('images/voting-banner.png')}}" alt="Award 2017" class="img-responsive text-center"">
                                </a>
                                {{-- <img src="{{asset('images/bannerhome1.png')}}" alt="" class="img-responsive text-center">--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--==================================Section Close=================================-->
@endsection