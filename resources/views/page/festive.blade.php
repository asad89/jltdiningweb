@extends('master')
@section('meta_tags')
    <title>@if(isset($page->meta_title) && !empty($page->meta_title)){{$page->meta_title}} @else{{config('app.meta_title')}}@endif</title>
    <meta name="keywords" content="@if(isset($page->meta_keywords) && !empty($page->meta_keywords)){{$page->meta_keywords}} @else{{config('app.meta_keywords')}}@endif">
    <meta name="description" content="@if(isset($page->meta_description) && !empty($page->meta_description)){{$page->meta_description}} @else{{config('app.meta_description')}}@endif">
   {{-- <link rel="canonical" href="{{url($page->slug)}}" />--}}
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-585ba46f0a9379e0"></script>
    <meta property="og:title" content="The 2019 Festive Season in Jumeirah Lakes Towers" />
      <meta property="og:description" content="Jingle the bells! The festive season is here and so is our overview of great Christmas & New Year's food & drinks offers in Jumeirah Lakes Towers." />
    <meta property="og:image" content="@if(!empty($page->feature_image)) {{asset('uploads/posts/large/'.$page->feature_image)}} @endif" />
    @endsection
    @section('content')
            <!--==================================Section Open=================================-->
    <section class="aliceblue brown-bg">
        <div class="container breadcrumb-wrap">
            <ol class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a></li>
               {{-- <li><a href="{{url($page->slug)}}">{{$page->name}}</a></li>--}}
            </ol>
        </div>

        <div class="container page-container-second">
            <div class="row">
                <div class="col-md-9 padding-40 blog-single-inner-container lp-border lp-border-radius-8">
                    <div class="popup-gallery">
                        {{--@if(!empty($page->feature_image))
                            <img src="{{asset('uploads/posts/large/'.$page->feature_image)}}" alt="">
                        @endif--}}
                        {{--<h1 class="page-heading-custom" style="text-align: center;margin-bottom:20px;font-size:24px;font-family: Oxygen, sans-serif!important;">{{$page->title}}</h1>--}}

                        <div class="row" style="line-height:1.7;">
                            <div class="col-md-12">
                                <h1 class="page-heading-custom" style="text-align: center;margin-bottom:20px;font-size:24px;font-family: Oxygen, sans-serif!important;">The 2019 Festive Season in Jumeirah Lakes Towers</h1>
                                <h3 class="page-heading-custom" style="text-align: center;margin-bottom:20px;font-size:18px;font-family: Oxygen, sans-serif!important;">JLT Christmas & New Year's Eve Deals</h3>
                                <p>Jingle the bells! The festive season is here and so is our overview of great Christmas & New Year's food & drinks offers in Jumeirah Lakes Towers. Don't forget to check out <a href="http://www.jltdining.com" target="_blank">www.jltdining.com</a> when you’re out & about in JLT! You can also subscribe to our JLT foodie newsletter <a href="https://www.jltdining.com/newsletter" target="_blank">here</a> and get the latest deals & events delivered straight to your inbox. Ho, ho, ho!</p>
                                <hr>
                                <h3 class="page-heading-custom" style="text-align: center;margin-bottom:20px;font-size:18px;font-family: Oxygen, sans-serif!important;">JLT goes Santa & NYE: Festive events in JLT</h3>
                                <table class="table-responsive" style="font-size:14px;font-family: 'Oxygen, sans-serif';">
                                  {{--  <tr>
                                        <td class="text-center" style="width:50%;"><img src="{{asset('images/offer-8-jlt.jpg')}}" alt="" class="img-responsive" style="max-width:98%;" ></td>
                                        <td class="text-center" style="width:50%;"><img src="{{asset('images/offer-9-jlt.jpg')}}" alt="" class="img-responsive" style="max-width:98%;"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td><p style="width:90%;">Planning on dining at home this Christmas? Have the best of Christmas on your table with
                                                <a href="https://www.jltdining.com/restaurants/mcgettigans-jlt" target="_blank">McGettigan’s JLT</a>! Click <a href="mailto:contact@mcgettigans.com?subject=I need Christmas treats [via JLT Dining]&">here</a>
                                                to place your order now.</p></td>
                                        <td><p style="width:90%;"><a href="https://www.jltdining.com/restaurants/mcgettigans-jlt" target="_blank">McGettigan's</a> is bringing back its traditional December special: Roast Turkey & Ham. It's available every day throughout December for just AED 99/portion. Dine-in only.</p></td>
                                    </tr>--}}

                                    <tr>
                                        <td class="text-center" style="width:50%;" colspan="2">
                                            <a href="https://www.facebook.com/pg/JLTdining/offers/" target="_blank">
                                            <img src="{{asset('images/offer-25-jlt.png')}}" alt="" style="max-width:98%;">
                                            </a>
                                        </td>

                                    </tr>

                                    <tr>
                                        <td colspan="2">&nbsp;</td>
                                    </tr>


                                    <tr>
                                        <td class="text-center" style="width:50%;"><img src="{{asset('images/offer-26-jlt.jpg')}}" alt="" class="img-responsive" style="max-width:98%;"></td>
                                        <td class="text-center" style="width:50%;"><img src="{{asset('images/offer-27-jlt.png')}}" alt="" class="img-responsive" style="max-width:98%;"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <td><p style="width:90%;">Usher in the New Year in Taj style at the Taj Jumeirah Lakes Towers. Celebrate the year ahead with an exclusive Gala Dinner at Shamiana, live entertainment, and views of the twinkling Dubai skyline.
                                            </p></td>
                                        <td><p style="width:90%;">

                                                Get ready to start 2020 with some foodilicious deals when #Ten4JLT2020 kicks off on 5th January. Every dish only AED 10! Click
                                                <a href="http://jltdining.com/january-2020-10-days-10-dirham">here</a> to see the current line-up!

                                            </p></td>
                                    </tr>

                                    <tr>
                                        <td colspan="2">&nbsp;</td>
                                    </tr>


                                    <tr>
                                        <td class="text-center" style="width:50%;"><img src="{{asset('images/offer-22-jlt.jpg')}}" alt="" class="img-responsive" style="max-width:98%;"></td>
                                        <td class="text-center" style="width:50%;"><img src="{{asset('images/offer-23-jlt.jpg')}}" alt="" class="img-responsive" style="max-width:98%;"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td><p style="width:90%;">It's the season of giving and <a href="https://www.jltdining.com/restaurants/beirut-bites" target="_blank">Beirut Bites</a> in cluster W wants to give YOU the gift of great Lebanese food! Every time you dine in the restaurant this festive season, you get to pick a coupon from the Christmas Tree and will win a foodilicious gift!
                                            </p></td>
                                        <td><p style="width:90%;">
                                                Stuck for a lastminute Christmas or New Year's gift? Give the gift of tasty food with
                                                <a href="https://www.jltdining.com/restaurants/beirut-bites" target="_blank"></a>Beirut Bites’ secret santa coupons. Pick them up in cluster W!
                                            </p></td>
                                    </tr>






                                        <tr>
                                            <td class="text-center" style="width:50%;"><img src="{{asset('images/offer-8-jlt.jpg')}}" alt="" class="img-responsive" style="max-width:98%;"></td>
                                            <td class="text-center" style="width:50%;"><img src="{{asset('images/offer-9-jlt.jpg')}}" alt="" class="img-responsive" style="max-width:98%;"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td><p style="width:90%;">Enjoy a festive Christmas bazaar and the chance of some lastminute shopping for fashion, home décor, and gifts at the Khau Galli Christmas Carnival on 20th and 21st December 2019, 4 p.m. – 10 p.m. in the area between clusters S & T.
                                                    There’s food and kids entertainment, too.
                                                </p></td>
                                            <td><p style="width:90%;">Fancy a Christmas with a difference? How about an African Christmas? The Gbemi’s Kitchen is offering a turkey package for delivery anywhere in Dubai for AED 399, which comes with jollof Rrice, suya roasted potatoes, pineapple chutney, salad, plantain, and Sauces. Call or WhatsApp +971 52 547 9479 to book.
                                                </p></td>
                                        </tr>

                                    <tr>
                                        <td class="text-center" style="width:50%;"><img src="{{asset('images/offer-20-jlt.jpg')}}" alt="" class="img-responsive" style="max-width:98%;"></td>
                                        <td class="text-center" style="width:50%;"><img src="{{asset('images/offer-21-jlt.jpg')}}" alt="" class="img-responsive" style="max-width:98%;"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td><p style="width:90%;">Experience five magical days of festive fun, food and activities at the first-ever DMCC festive market in the JLT Park. Capture your favourite holiday moment as you take a picture with Santa at his grotto, shop for presents at the market or enjoy the sight of happy penguins and elves in our holiday-themed venue. 20th – 24th December. For details, click
                                                <a href="https://www.eventbrite.com/e/jlt-festive-market-tickets-85614201381?fbclid=IwAR2UCOhfdX8m66UviWULTKlq4gblGtoZoDdrNoqADsJ21JO3MlZTWlyeWP0" target="_blank">here</a>.
                                            </p></td>
                                        <td><p style="width:90%;">Need festive treats? <a href="https://jltdining.com/restaurants/gs-bakery-cafe" target="_blank">G’s Bakery & Cafe</a> in cluster F gives you more of this season’s must-have treats for a whole lot less! Get a box of six Christmas-themed cupcakes for just AED 90 or grab a yule-log cake or a plum cake, or even a lush red velvet cake. For inquiries and orders, click
                                                <a href="http://ginnys.ae/" target="_blank">here</a>.
                                            </p></td>
                                    </tr>




                                    <tr>
                                        <td colspan="2"><p class="text-center" style="font-style: italic;">More events, deals, and offers coming soon!</p></td>
                                    </tr>


                                    </table>
                                <hr>
                                    <h3 class="page-heading-custom" style="text-align: center;margin-bottom:20px;font-size:18px;font-family: Oxygen, sans-serif!important;">The festive season at the Bonnington Hotel JLT & McGettigan’s</h3>
                                <table class="table-responsive" style="font-size:14px;font-family: 'Oxygen, sans-serif';">
                                        <tr>
                                            <td class="text-center" style="width:50%;"><img src="{{asset('images/offer-1-jlt.jpg')}}" alt="" class="img-responsive" style="max-width:98%;"></td>
                                            <td class="text-center" style="width:50%;"><img src="{{asset('images/offer-2-jlt.jpg')}}" alt="" class="img-responsive" style="max-width:98%;"></td>
                                        </tr>

                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>

                                        <tr>
                                            <td><p style="width:90%;">Planning on dining at home this Christmas? Have the best of Christmas on your table with
                                                    <a href="https://www.jltdining.com/restaurants/mcgettigans-jlt" target="_blank">McGettigan’s JLT</a>! Click <a href="mailto:contact@mcgettigans.com?subject=I need Christmas treats [via JLT Dining]&amp;">here</a>
                                                    to place your order now.</p></td>
                                            <td><p style="width:90%;"><a href="https://www.jltdining.com/restaurants/mcgettigans-jlt" target="_blank">McGettigan's</a> is bringing back its traditional December special: Roast Turkey &amp; Ham. It's available every day throughout December for just AED 99/portion. Dine-in only.</p></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center" style="width:50%;"><img src="{{asset('images/offer-3-jlt.jpg')}}" alt="" class="img-responsive" style="max-width:98%;"></td>
                                            <td class="text-center" style="width:50%;"><img src="{{asset('images/offer-10-jlt.jpg')}}" alt="" class="img-responsive" style="max-width:98%;"></td>
                                        </tr>
                                        <tr>
                                            <td><p style="width:90%;">There’s only one place in JLT, where you can celebrate New Year’s twice in one evening: The Garden at
                                                    <a href="https://www.jltdining.com/restaurants/mcgettigans-jlt" target="_blank">McGettigan’s JLT</a>! See in the new year al fresco with great food and free-flowing drinks. Click <a href="mailto:contact@mcgettigans.com?subject=I need Christmas treats [via JLT Dining]&amp;">here</a> secure your table now!</p></td>
                                            <td><p style="width: 90%;padding-top:5px;">We all know that getting out of your PJs is pretty much impossible between Christmas & New Year. With the
                                                    <a href="https://www.jltdining.com/restaurants/mcgettigans-jlt" target="_blank">McGettigan’s JLT</a> Pyjama Party you don’t have to!
                                                    On Friday 27th December, McGettigan’s JLT is giving you the perfect excuse to party in your PJs with a brunch designed to help you through the post-Christmas, pre-New Year limbo. 1 p.m. - 5 p.m., AED 230 p.p. Book your brunch <a href="mailto:contact@mcgettigans.com?subject=PJ Brunch Inquiry [via JLT Dining]&">here</a>

                                                </p></td>
                                        </tr>



                                    <tr>
                                        <td class="text-center" style="width:50%;"><img src="{{asset('images/offer-16-jlt.jpg')}}" alt="" class="img-responsive" style="max-width:98%;"></td>
                                        <td class="text-center" style="width:50%;"><img src="{{asset('images/offer-17-jlt.jpg')}}" alt="" class="img-responsive" style="max-width:98%;"></td>
                                    </tr>

                                    <tr>
                                        <td colspan="2">&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <td><p style="width:90%;">Host a hassle-free festive dinner with the Bonnington Jumeirah Lakes Towers' Turkey-to-Go with all the trimmings from only AED 700. The Bonnington also offers other festive treats including honey glazed ham, gingerbread houses, festive hampers, and many more. For details or to book, call +971 4 356 0536 or click <a href="mailto:restaurantreservations@bonningtontower.com?subject=Festive Take-Away Inquiry [via JLT Dining]&">here</a>
                                                to order by email.</p></td>
                                        <td><p style="width:90%;">Add a sparkle of joy to your festive celebrations with the Bonnington Jumeirah Lakes Towers' Christmas Day Family Brunch in the vibrant setting of the
                                                <a href="https://www.jltdining.com/restaurants/the-cavendish-restaurant">Cavendish Restaurant</a>. Enjoy traditional festive dishes while Santa keeps the little ones entertained with games and fun activities. AED 240 p.p. (soft), AED 399 p.p. (house beverages), AED 99 p.p. (kids between 6 - 12 years), kids under 6 eat free.
                                                25th December, 12.30 p.m. - 4 p.m. ENTERTAINER vouchers will be accepted during the day for soft drinks packages.
                                                For details or to book, call +971 4 356 0536 or click <a href="mailto:restaurantreservations@bonningtontower.com?subject=Christmas Brunch Inquiry [via JLT Dining]&">here</a>

                                            </p></td>
                                    </tr>

                                    <tr>
                                        <td class="text-center" style="width:50%;"><img src="{{asset('images/offer-18-jlt.jpg')}}" alt="" class="img-responsive" style="max-width:98%;"></td>
                                        <td class="text-center" style="width:50%;"><img src="{{asset('images/offer-19-jlt.jpg')}}" alt="" class="img-responsive" style="max-width:98%;"></td>
                                    </tr>

                                    <tr>
                                        <td colspan="2">&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <td><p style="width:90%;">Ring in the New Year in a spectacular way and enjoy a mouthwatering spread of delicious festive specialties complemented by live entertainment at the
                                                <a href="https://www.jltdining.com/restaurants/the-cavendish-restaurant" target="_blank">Cavendish Restaurant</a>. AED 299 p.p. (soft), AED 499 p.p. (house beverages). 31st December, 9 p.m. - 12.30 a.m. ENTERTAINER vouchers will be accepted during the day for soft drinks packages. For details or to book, call +971 4 356 0536 or click <a href="mailto:restaurantreservations@bonningtontower.com?subject=NYE Gala Dinner Inquiry [via JLT Dining]&">here</a>
                                                .</p></td>
                                        <td><p style="width:90%;">Celebrate New Year in style as you party into the night with live music, free flowing beverages, and sharing tapas at
                                                <a href="https://www.jltdining.com/restaurants/healeys-bar-terrace" target="_blank">Healey’s Bar & Terrace</a>. AED 399 p.p. 31st December, 8 p.m. - 12.30 a.m. For details or to book, call +971 4 356 0536 or click <a href="mailto:restaurantreservations@bonningtontower.com?subject=NYE Healey's Bar Inquiry [via JLT Dining]&">here</a>.

                                            </p></td>
                                    </tr>




                                        </table>


                                <table class="table-responsive" style="font-size:14px;font-family: 'Oxygen, sans-serif';">
                                    <tbody><tr>
                                        <td class="text-center" style="width:50%;"><img src="https://www.jltdining.com/images/offer-4-jlt.jpg" alt="" class="img-responsive" style="max-width:98%;"></td>
                                        <td class="text-center" style="width:50%;"><img src="https://www.jltdining.com/images/offer-5-jlt.jpg" alt="" class="img-responsive" style="max-width:98%;"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center" style="width:50%;"><img src="https://www.jltdining.com/images/offer-6-jlt.jpg" alt="" class="img-responsive" style="max-width:98%;"></td>
                                        <td class="text-center" style="width:50%;"><img src="https://www.jltdining.com/images/offer-7-jlt.jpg" alt="" class="img-responsive" style="max-width:98%;"></td>
                                    </tr>
                                    <hr>
                                        <h3 class="page-heading-custom" style="text-align: center;margin-bottom:20px;font-size:18px;font-family: Oxygen, sans-serif!important;">The festive season at the Pullman Hotel JLT</h3>
                                    <table class="table-responsive" style="font-size:14px;font-family: 'Oxygen, sans-serif';">
                                        <tr>
                                            <td class="text-center" style="width:50%;"><img src="https://www.jltdining.com/images/offer-4-jlt.jpg" alt="" class="img-responsive" style="max-width:98%;"></td>
                                            <td class="text-center" style="width:50%;"><img src="https://www.jltdining.com/images/offer-5-jlt.jpg" alt="" class="img-responsive" style="max-width:98%;"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td><p style="width:90%;">The Pullman JLT has all the festive treats you could possibly need if you’re looking to celebrate at home or need a gift for friends or work colleagues. In the run-up to Christmas, ou can get slow-roasted, map glazed turkeys with all the trimmings for AED 475 (5-6 kgs.) and also Christmas cookies, gingerbread, Panettone, and traditional stollen bread. Click <a href="mailto:pullmanjlt.dining@accor.com?subject=Festive treats booking inquiry [via JLT Dining]&amp;">here</a>
                                                    to place your order now!</p></td>
                                            <td><p style="width:90%;">Planning a Christmas lunch with love-ones or colleagues? Manzoni Bistro &amp; Bar offers special Christmas business lunches. Enjoy the festive touches in the restaurant’s menu and share the spirit of the festive season. Daily, except Friday from 12 noon – 3.30 p.m. for AED 55 p.p.
                                                    Group rates are available upon request.
                                                </p></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center" style="width:50%;"><img src="https://www.jltdining.com/images/offer-6-jlt.jpg" alt="" class="img-responsive" style="max-width:98%;"></td>
                                            <td class="text-center" style="width:50%;"><img src="https://www.jltdining.com/images/offer-7-jlt.jpg" alt="" class="img-responsive" style="max-width:98%;"></td>
                                        </tr>

                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td><p style="width:90%;">The Pullman’s Christmas Brunches let you indulge in the spirit and the food of the festive season. On Friday, 13th and 20th December from 1 p.m. – 4 p.m., enjoy festive delicacies and traditional dishes incl. roast turkey with all the trimmings and fruit puddings. There’s also a live band, kids’ activities, and a visit from Santa. AED 149 p.p. (soft drinks) or AED 249 p.p. (house beverages). Kids between 9 – 12 years old get a 50% discount and kids 8 years or younger eat for free.
                                                    Click <a href="mailto:pullmanjlt.dining@accor.com?subject=Festive brunch booking inquiry [via JLT Dining]&amp;">here</a>
                                                    to book your brunch now!
                                                </p></td>
                                            <td><p style="width:90%;"> <b>Christmas Eve and Christmas Day at Seasons:</b> Enter a wonderland of festivities and celebrate Christmas Eve or Christmas Day with love-ones and friends at Seasons. You’ll find a sumptuous Christmas buffet complete with roast turkey, tender meat cuts, festive logs, mince pies, party puddings, and much more.
                                                    The program includes special kids’ activities, live entertainment, and surprise visits from Santa. 24th December, 7.30 p.m. – 10.30 p.m. 25the December, 1 p.m. – 4 p.m. and 7.30 p.m. – 10.30 p.m.
                                                    AED 169 p.p. (soft drinks) or AED 269 p.p. (house beverages).
                                                    Click <a href="mailto:pullmanjlt.dining@accor.com?subject=Christmas booking inquiry for Seasons inquiry [via JLT Dining]&amp;">here</a>
                                                    to book your table now!</p>
                                            </td>
                                        </tr>

                                        </table>

                                    <hr>
                                    <h3 class="page-heading-custom" style="text-align: center;margin-bottom:20px;font-size:18px;font-family: Oxygen, sans-serif!important;">The festive season at the Mövenpick Hotel JLT & UBK</h3>
                                    <table class="table-responsive" style="font-size:14px;font-family: 'Oxygen, sans-serif';">
                                       <tr>
                                            <td class="text-center" style="width:50%;"><img src="{{asset('images/offer-11-jlt.jpg')}}" alt="" class="img-responsive" style="max-width:98%;"></td>
                                            <td class="text-center" style="width:50%;"><img src="{{asset('images/offer-12-jlt.jpg')}}" alt="" class="img-responsive" style="max-width:98%;"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td><p style="width:90%;">Need a bird? <a href="https://www.jltdining.com/restaurants/crema" target="_blank">Crema</a> at the Mövenpick JLT is accepting orders for take-away turkeys from 14th – 30th Decembe. You can get a full bird for AED 499 or half a bird for AED 279.</p></td>
                                            <td><p style="width:90%;">This Christmas Eve, why not spoil yourself and your family with an extravagant Christmas
                                                    Eve buffet with turkey carving, roasted beef, Christmas pudding, and much more? Kids will have the chance to celebrate with Santa, too. 24th December, 6.30 p.m. - 10.30 p.m., AED 165 (soft), AED 275 (house beverages), under 6s dine free, 6 – 12 year olds get 50% off.

                                                </p></td>
                                        </tr>


                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="text-center" style="width:50%;"><img src="{{asset('images/offer-13-jlt.jpg')}}" alt="" class="img-responsive" style="max-width:98%;"></td>
                                            <td class="text-center" style="width:50%;"><img src="{{asset('images/offer-14-jlt.jpg')}}" alt="" class="img-responsive" style="max-width:98%;"></td>
                                        </tr>
                                        <tr>
                                            <td><p style="width:90%;padding-top:5px;">The Mövenpick’s festive brunch includes traditional Christmas delicacies such as roast turkey with all the accompaniments, roasted beef, live cooking stations & Yorkshire pudding, panettone, stollens, Christmas pudding, mulled wine, and everything else that makes your Christmas special! Santa will visit the little ones in the kids’ corner, all set-up with games and activities. 25th December, 12.30 p.m. - 3.30 p.m., AED 199 (soft), AED 299 (house beverages), under 6s dine free, 6 – 12 year olds get 50% off.</p></td>
                                            <td><p style="width:90%;padding-top:5px;">Enjoy your Christmas Day brunch at <a
                                                            href="https://www.jltdining.com/restaurants/ubk-urban-bar-kitchen" target="_blank">[u]bk – Urban Bar & Kitchen</a> with tasty turkey carvings, roast beef, Christmas desserts, and seasonal beverages from 1 p.m. – 4 p.m. for AED 199 p.p. There’s also the newly opened Winter terrace to discover and you can listen to live music from UBK’s own Danny.

                                                </p></td>
                                        </tr>

                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="text-center" style="width:50%;"><img src="{{asset('images/offer-15-jlt.jpg')}}" alt="" class="img-responsive" style="max-width:98%;"></td>
                                            <td class="text-center" style="width:50%;">&nbsp;</td>
                                        </tr>


                                        <tr>
                                            <td><p style="width:90%;padding-top:5px;"><a href="https://www.jltdining.com/restaurants/ubk-urban-bar-kitchen" target="_blank">[u]bk – Urban Bar & Kitchen</a> welcomes all the kings and queens of the disco floor to electric slide into the year 2020 with retro style. The Disco Fever NYE party boasts offers all night and into the New Year. Guests can sip on Woo Woo cocktails and flaunt a Diana Ross Fro while enjoying [u]bk’s Disco Fever Party package for AED 349, which includes free-flowing house beverages and a buffet available from 9 p.m. – 2 a.m.</p></td>
                                            <td><p style="width:90%;">&nbsp;</p></td>
                                        </tr>
                                       </table>


                                    <br>
                                    <hr>

                                    <p style="font-size:14px !important;">Coming soon: More great Jumeirah Lakes Towers festive offers and foodie deals! In the meantime, why don’t you
                                        <a href="https://www.jltdining.com/newsletter">subscribe</a> to our weekly events and deals newsletter or follow us on Facebook, Instagram, or Twitter?
                                </p>

                                <hr>
                                <p style="font-size:12px !important;">Offers and deals are controlled by the restaurants advertising them and are subject to change. JLT Dining accepts no responsibility for incomplete or out of date offers/deals. You should contact the restaurants directly for more information or to book.</p>

                                <p style="font-size:12px !important;">If you're running a restaurant, bar, or cafe in JLT and want to advertise on this page, please click
                                    <a href="mailto:contact@mcgettigans.com?subject= McGettigan’s JLT NYE inquiry [via JLT Dining]&">here</a>

                                </p>
                                <img src="{{asset('images/festive.jpg')}}" alt="" class="text-center img-responsive">
                                <br>

                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="post-meta-right-box text-center">
                                <div class="addthis_inline_share_toolbox"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12 col-lg-3">
                    <div class="sidebar-post">
                        <div class="widget-box  widget-bg-color post-author-box lp-border-radius-5">
                            <div class="widget-content">
                                <a href="#" target="_blank">
                                    <img src="{{asset('images/jlt-awards-side-banner-compressor.png')}}" alt="Award 2018">
                                </a>
                            </div>
                        </div>
                        <div class="widget-box  widget-bg-color post-author-box lp-border-radius-5">
                            <div class="widget-content">
                                <a href="https://www.jltdining.com/restaurant-awards-2018" target="_blank">
                                    <img src="{{asset('images/jlt-awards-2018.png')}}" alt="Award 2018">
                                </a>
                            </div>
                        </div>
                        <div class="widget-box  widget-bg-color post-author-box lp-border-radius-5">
                            <div class="widget-content text-center">
                                <a href="https://www.jltdining.com/restaurant-awards-2017" target="_blank">
                                    <img src="{{asset('images/voting-banner.png')}}" alt="Award 2017" class="img-responsive text-center">
                                </a>
                                {{-- <img src="{{asset('images/bannerhome1.png')}}" alt="" class="img-responsive text-center">--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--==================================Section Close=================================-->
@endsection